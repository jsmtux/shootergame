using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace shooter
{
	public class GrassEntity : PreloadedSBaseEntity<GrassEntity>
	{
		static GrassEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("rock/grass_tiles"),
				new ModelDefinition("rock/grass_l"),
				new ModelDefinition("rock/grass_m"),
				new ModelDefinition("rock/grass_r"),
			});
		}

		public GrassEntity() {}

		protected GrassEntity(
			Vector2 _position,
			string _baseName,
			float _viewVolume = ModelComponent.DefaultViewVolume,
			float _zRotation = 0)
		{
			position = _position;
			baseName = _baseName;
			viewVolume = _viewVolume;
			zRotation = _zRotation;
		}

		public virtual void initialize()
		{
		}

		protected override void DoCreation(Stage _stage)
		{
			SimplePhysicsComponent location = new SimplePhysicsComponent(position);
			location.setModelRotation(new Vector3(zRotation, 0, 0));

			AddComponent(location);
			var model = new ModelComponent(
				location,
				TiledMapReader.entitySize.X / 2.0f, getPreloadedResource<Model>(baseName),
			    getPreloadedResource<Texture2D>("rock/grass_tiles"),
				Depth.Type.Action1,
				RenderConfiguration.Default,
				ModelComponent.RenderMode.Perspective);
			model.SetViewVolume(viewVolume);
			AddComponent(model);
			AddComponent(new RoundPhysicsBodyComponent(GrassBaseCollisionSize, location));
		}

		protected Vector2 position;
		string baseName;
		float viewVolume;
		float zRotation;
		protected static readonly float GrassBaseCollisionSize = 0.8f * TiledMapReader.entitySize.X / 2.0f;
	}

	public class GrassEntityL : GrassEntity
	{
		public GrassEntityL(Vector2 _position)
			: base (_position, "rock/grass_l", 150)
		{}
	}

	public class GrassEntityM : GrassEntity
	{
		public GrassEntityM(Vector2 _position)
			: base (_position, "rock/grass_m", 150)
		{}

		protected override void DoCreation(Stage _stage)
		{
			base.DoCreation(_stage);
			SimplePhysicsComponent location = new SimplePhysicsComponent(position + new Vector2(TiledMapReader.entitySize.X, 0));
			AddComponent(new RoundPhysicsBodyComponent(GrassBaseCollisionSize, location));
		}
	}

	public class GrassEntityR : GrassEntity
	{
		public GrassEntityR(Vector2 _position)
			: base (_position, "rock/grass_r", 150)
		{}
	}
}
