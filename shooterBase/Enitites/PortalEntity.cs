﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace shooter
{
	public class PortalEntity : PreloadedSBaseEntity<PortalEntity>
	{
		public class PortalEntityReceiver : MessageReceiverComponent
		{
			public PortalEntityReceiver(PortalEntity _parent)
			{
				parent = _parent;
			}

			public override void ReceiveMessage(Message message)
			{
				if (message is BananaKeyRetrievedMessage bananaKeyMessage)
				{
					parent.AddKey(bananaKeyMessage.position);
				}
			}

			PortalEntity parent;
		}

		static PortalEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("food/BananaTexture"),
				new TextureDefinition("particles/star"),
				new ModelDefinition("food/Banana"),
				new TextureDefinition("portal/portal_bg"),
				new SoundEffectDefinition("audio/bananaportal_powerup"),
				new SoundEffectDefinition("audio/bananaportal_powerdown"),
				new SoundEffectDefinition("audio/bananaportal_closed")
			});
		}

		public void Initialize(Vector2 _position, bool _isEndPortal = false, List<Vector2> _bananaPositions = null)
		{
			position = _position;

			isEndPortal = _isEndPortal;
			currentlyMissingBananas = _bananaPositions;

			scale = new FloatTween();
			scale.SetDefault(_isEndPortal ? 1.0f : 0.0f);
			alpha = new FloatTween();
			alpha.SetDefault(_isEndPortal ? 1.0f : 0.0f);

			powerUpSoundComponent = new SoundEffectComponent(getPreloadedResource<SoundEffect>("audio/bananaportal_powerup"));
			powerDownSoundComponent = new SoundEffectComponent(getPreloadedResource<SoundEffect>("audio/bananaportal_powerdown"));
			closedSoundComponent = new SoundEffectComponent(getPreloadedResource<SoundEffect>("audio/bananaportal_closed"));
		}

		protected override void DoCreation(Stage _stage)
		{
			AddComponent(powerUpSoundComponent);
			AddComponent(powerDownSoundComponent);
			AddComponent(closedSoundComponent);

			int numComponents = 10;
			location = new SimplePhysicsComponent(position);

			bgLocation = new ChildPhysicsComponent(location, Vector2.Zero);
			var bg = new SpriteComponent(
				bgLocation,
				getPreloadedResource<Texture2D>("portal/portal_bg"),
				Depth.Type.Bg3,
				new RenderConfiguration(
					RenderConfiguration.TranslationType.Camera,
					RenderConfiguration.BlendType.Alpha,
					_stage.getGameService<EffectsManager>().PortalEffect
				));
			bg.Alpha = new TweenWrapper<float>(alpha);
			AddComponent(bg);

			var coverLocation = new ChildPhysicsComponent(location, Vector2.Zero);
			coverSpriteComponent = new SpriteComponent(
				coverLocation,
				getPreloadedResource<Texture2D>("portal/portal_bg"),
				Depth.Type.Top,
				new RenderConfiguration(
					RenderConfiguration.TranslationType.Camera,
					RenderConfiguration.BlendType.Alpha,
					_stage.getGameService<EffectsManager>().PortalEffect
				));
			AddComponent(coverSpriteComponent);
			coverSizeTween = new Vector2Tween();
			coverSizeTween.SetDefault(Vector2.Zero);
			AddComponent(coverSizeTween);
			coverLocation.SizeTween = new TweenWrapper<Vector2>(coverSizeTween);

			for (int i = 0; i < numComponents; i++)
			{
				var componentLocation = new ChildPhysicsComponent(location, Vector2.Zero);
				AddComponent(componentLocation);
				ModelComponent portalModel = new ModelComponent(componentLocation, 80f, getPreloadedResource<Model>("food/Banana")
																	, getPreloadedResource<Texture2D>("food/BananaTexture"), Depth.Type.Action1);
				portalComponentsModel.Add(portalModel);
				AddComponent(portalModel);
				portalComponentsLocation.Add(componentLocation);
			}

			AddComponent(scale);

			onCloseEmitter = new ParticleEmitterComponent(getPreloadedResource<Texture2D>("particles/star"),
				location, Depth.Type.UI2, Vector2.Zero,
				new RenderConfiguration(RenderConfiguration.TranslationType.Camera, RenderConfiguration.BlendType.Additive))
			{
				angleVariance = 2 * (float)Math.PI,
				cadence = TimeSpan.FromSeconds(100000),
				particlesOnEmission = 35,
				lifeTime = TimeSpan.FromMilliseconds(1250),
				lifeTimeVariance = TimeSpan.Zero,
				rotationSpeed = 0.1f,
				rotationSpeedVariance = 0,
				speed = 4,
				beginTint = Color.Yellow,
				endTint = Color.White * 0.0f,
				beginSize = 30,
				endSize = 70
			};
			AddComponent(onCloseEmitter);

			logicComponent = new PortalLogicComponent();
			int missingBananasNumber = 0;
			if (currentlyMissingBananas != null)
			{
				missingBananasNumber = currentlyMissingBananas.Count;
			}
			logicComponent.Initialize(portalComponentsLocation, portalComponentsModel, bgLocation, scale, alpha, missingBananasNumber);
			AddComponent(logicComponent);

			AddComponent(new RoundPhysicsBodyComponent(90f, location, Collided));

			messagingSystem = _stage.getGameService<IMessagingSystem>();

			AddComponent(new PortalEntityReceiver(this));

			particleTrailSpawner = new SpawnerComponent<BananaParticleTrailEntity>(_stage);
		}

		private void AddKey(Vector2 _position)
		{
			if (currentlyMissingBananas != null && currentlyMissingBananas.Count > 0)
			{
				currentlyMissingBananas.Remove(_position);
				logicComponent.missingBananas = currentlyMissingBananas.Count;
			}
		}

		private bool Collided(SBaseEntity _entity, Vector2 _collisionPos)
		{
			bool collision = false;
			if (_entity is PCEntity && isEndPortal)
			{
				if(currentlyMissingBananas.Count == 0)
				{
					messagingSystem.SendMessage(new PortalTouchedMessage(this));
					collision = true;
				}
				else
				{
					logicComponent.shouldEmmitBananas = true;
				}
			}

			return collision;
		}

		public void EmitParticlesToBananas()
		{
			foreach(var bananaPosition in currentlyMissingBananas)
				particleTrailSpawner.spawn((BananaParticleTrailEntity _trail) => {
					_trail.initialize(location, new SimplePhysicsComponent(bananaPosition));
				});
		}

		public void Open()
		{
			scale.onEnd = onOpen;
			scale.Start(0.0f, 1.0f, TimeSpan.FromSeconds(2), ScaleFuncs.CubicEaseOut);
			if (!isEndPortal)
			{
				powerUpSoundComponent.Play();
			}
		}

		public void Close()
		{
			scale.onEnd = () =>
			{
				closedSoundComponent.Play();
				onCloseEmitter.setEmitting(true);
				if (isEndPortal)
				{
					coverSizeTween.Start(Vector2.Zero, new Vector2(3000, 3000), TimeSpan.FromSeconds(1), ScaleFuncs.CubicEaseOut);
				}
				RemoveComponent<ModelComponent>();
				onClose?.Invoke();
			};
			scale.Start(1.0f, 0.0f, TimeSpan.FromSeconds(2), ScaleFuncs.CubicEaseOut);
			powerDownSoundComponent.Play();
		}

		public void setPosition(Vector2 _position)
		{
			location.Position = _position;
		}

		Vector2 position;
		protected IMessagingSystem messagingSystem;
		
		SpawnerComponent<BananaParticleTrailEntity> particleTrailSpawner;

		LocationComponent location;
		List<ChildPhysicsComponent> portalComponentsLocation = new List<ChildPhysicsComponent>();
		List<ModelComponent> portalComponentsModel = new List<ModelComponent>();
		ChildPhysicsComponent bgLocation;
		Vector2Tween coverSizeTween;
		PortalLogicComponent logicComponent;
		ParticleEmitterComponent onCloseEmitter;
		SpriteComponent coverSpriteComponent;
		SoundEffectComponent powerUpSoundComponent, powerDownSoundComponent, closedSoundComponent;
		FloatTween scale;
		FloatTween alpha;

		bool isEndPortal;
		List<Vector2> currentlyMissingBananas;

		public Action onOpen;
		public Action onClose;
	}
}
