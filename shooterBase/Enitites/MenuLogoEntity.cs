﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace shooter
{
	public class MenuLogoEntity : PreloadedSBaseEntity<MenuLogoEntity>
	{
		static MenuLogoEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("ui/space_pizza_logo")
			});
		}

		protected override void DoCreation(Stage _stage)
		{
			location = new UILocationComponent(Vector2.Zero, UILocationComponent.AnchorPoint.Center);
			AddComponent(location);

			AddComponent(locationTween);
			location.position = new TweenWrapper<Vector2>(locationTween);

			logo = new SpriteComponent(location, getPreloadedResource<Texture2D>("ui/space_pizza_logo"), Depth.Type.UI,
									  new RenderConfiguration(RenderConfiguration.TranslationType.UI));
			AddComponent(logo);

			AddComponent(alphaTween);
			logo.Alpha = new TweenWrapper<float>(alphaTween);
		}

		public void Show()
		{
			alphaTween.onEnd = OnShown;
			locationTween.Start(initPos, centerPos, TimeSpan.FromSeconds(0.5f), ScaleFuncs.CubicEaseIn);
			alphaTween.Start(0.0f, 1.0f, TimeSpan.FromSeconds(0.5f), ScaleFuncs.Linear);
		}

		public void Hide()
		{
			alphaTween.onEnd = OnHide;
			locationTween.Start(centerPos, endPos, TimeSpan.FromSeconds(0.5f), ScaleFuncs.CubicEaseOut);
			alphaTween.Start(1.0f, 0.0f, TimeSpan.FromSeconds(0.4f), ScaleFuncs.CubicEaseOut);
		}

		public Action OnShown;
		public Action OnHide;

		UILocationComponent location;
		SpriteComponent logo;
		FloatTween alphaTween = new FloatTween();
		Vector2Tween locationTween = new Vector2Tween();
		readonly Vector2 initPos = new Vector2(0, -400);
		readonly Vector2 centerPos = new Vector2(0, -100);
		readonly Vector2 endPos = new Vector2(0, 300);
	}
}
