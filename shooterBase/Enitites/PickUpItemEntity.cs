﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace shooter
{
	public class PickUpItemEntity : PreloadedSBaseEntity<PickUpItemEntity>
	{
		static PickUpItemEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new SoundEffectDefinition("audio/sci-fi_power_up_09"),
				new TextureDefinition("#FF9412")
			});
		}

		protected void Initialize(Vector2 _position, string _modelPath, string _texturePath, Color _particleAColor, Color _particleBColor, Vector2? _startPosition = null)
		{
			position = _position;
			modelPath = _modelPath;
			texturePath = _texturePath;
			aColor = _particleAColor;
			bColor = _particleBColor;
			startPosition = _startPosition;

			location = new SimplePhysicsComponent(position);
			sizeTween = new FloatTween();
		}

		protected override void DoCreation(Stage _stage)
		{
			AddComponent(location);

			AddComponent(sizeTween);

			float itemSize = 30f;

			AddComponent(new RoundPhysicsBodyComponent(itemSize, location, Collided));

			model = new ModelComponent(location,
										   itemSize,
										   getPreloadedResource<Model>(modelPath),
										   getPreloadedResource<Texture2D>(texturePath),
										   Depth.Type.Action2,
										   RenderConfiguration.Default,
										   ModelComponent.RenderMode.Perspective);

			if (startPosition != null)
			{
				var positionTween = new Vector2Tween();
				AddComponent(positionTween);
				location.PositionTween = new TweenWrapper<Vector2>(positionTween);
				positionTween.Start(startPosition.GetValueOrDefault(), position, TimeSpan.FromSeconds(0.5f), ScaleFuncs.ElasticEaseOut);
			}

			model.scale = new TweenWrapper<float>(sizeTween);
			model.tint = new TweenWrapper<Color>(Color.White);
			AddComponent(model);

			pickUpSoundComponent = new SoundEffectComponent(getPreloadedResource<SoundEffect>("audio/sci-fi_power_up_09"));
			AddComponent(pickUpSoundComponent);


			messagingSystem = _stage.getGameService<IMessagingSystem>();

			SetUpEmitters(_stage);
		}

		protected virtual void SetUpEmitters(Stage _stage)
		{

			pickUpEmitter = new ParticleEmitterComponent(getPreloadedResource<Texture2D>("particles/star"), location, Depth.Type.Top)
			{
				angleVariance = 2 * (float)Math.PI,
				cadence = TimeSpan.FromMilliseconds(100000),
				particlesOnEmission = 15,
				lifeTime = TimeSpan.FromMilliseconds(500),
				lifeTimeVariance = TimeSpan.Zero,
				rotationSpeed = 0.0f,
				rotationSpeedVariance = 0,
				speed = 2,
				beginTint = aColor,
				endTint = aColor * 0.0f,
				beginSize = 15,
				endSize = 70
			};
			pickUpEmitterB = new ParticleEmitterComponent(getPreloadedResource<Texture2D>("particles/star"), location, Depth.Type.Top)
			{
				angleVariance = 2 * (float)Math.PI,
				cadence = TimeSpan.FromMilliseconds(100000),
				particlesOnEmission = 15,
				lifeTime = TimeSpan.FromMilliseconds(500),
				lifeTimeVariance = TimeSpan.Zero,
				rotationSpeed = 0.0f,
				rotationSpeedVariance = 0,
				speed = 2.5f,
				beginTint = bColor,
				endTint = bColor * 0.0f,
				beginSize = 20,
				endSize = 70
			};
			AddComponent(pickUpEmitter);
			AddComponent(pickUpEmitterB);
		}
		
		protected virtual void ActivateEmitters()
		{
			pickUpEmitter.emitAmount(1, delete);
			pickUpEmitterB.emitAmount(1);
		}

		public bool Collided(SBaseEntity _e, Vector2 _collisionPos)
		{
			return false;
		}

		protected void Destroy()
		{
			pickUpSoundComponent.Play();
			ActivateEmitters();
			RemoveComponent<PhysicsBodyComponent>();
		}

		public virtual void Collected()
		{
			throw new NotImplementedException();
		}

		public Vector2 GetPosition()
		{
			return position;
		}

		protected Vector2 position;

		Vector2? startPosition;

		protected ModelComponent model;

		protected FloatTween sizeTween;
		protected SimplePhysicsComponent location;

		ParticleEmitterComponent pickUpEmitter;
		ParticleEmitterComponent pickUpEmitterB;

		SoundEffectComponent pickUpSoundComponent;

		string modelPath;
		string texturePath;
		public Color aColor, bColor;

		protected IMessagingSystem messagingSystem;
	}

	public class LootItemEntity : PickUpItemEntity
	{
		protected void Initialize(Vector2 _position, string _modelName, string _textureName, Color _effectsColor)
		{
			base.Initialize(_position, _modelName, _textureName, _effectsColor, _effectsColor);
			effectsColor = _effectsColor;
			lightIntensityTween = new FloatTween();
			logicComponent = new LootItemLogicComponent(location, sizeTween, lightIntensityTween);
		}

		public virtual void Initialize(Vector2 _position)
		{
			throw new NotSupportedException();
		}

		protected override void DoCreation(Stage _stage)
		{
			base.DoCreation(_stage);

			AddComponent(lightIntensityTween);
			lightIntensityTween.SetDefault(0);

			var light = new LightEmitterComponent(location, effectsColor, 0.015f, 10.0f);
			AddComponent(light);
			light.intensity = new TweenWrapper<float>(lightIntensityTween);

			model.Alpha = new TweenWrapper<float>(0.7f);
			AddComponent(logicComponent);
		}

		public override void Collected()
		{
			if (logicComponent.Collected())
			{
				Destroy();
			}
		}

		public bool CanBePickedUp()
		{
			return logicComponent.CanBePickedUp();
		}

		protected LootItemLogicComponent logicComponent;
		protected FloatTween lightIntensityTween;
		Color effectsColor;
	}

	public class HeartItemEntity : LootItemEntity
	{
		public override void Initialize(Vector2 _position)
		{
			base.Initialize(_position, "energy/heart", Utils.GetStringRepresentation(Color.White), Color.Red);
		}

		static HeartItemEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition(Utils.GetStringRepresentation(Color.White)),
				new ModelDefinition("energy/heart")
			});
		}
	}

	public class StarSeedItemEntity : LootItemEntity
	{
		public void Initialize(Vector2 _position)
		{
			base.Initialize(_position, "food/Seed", "#ffffff", Color.Blue);
		}

		protected override void DoCreation(Stage _stage)
		{
			base.DoCreation(_stage);
			location.setModelRotation(new Vector3(0, (float)Math.PI / 4, 0));
		}


		static StarSeedItemEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("#ffffff"),
				new ModelDefinition("food/Seed")
			});
		}
	}

	public class AmmoItemEntity : PickUpItemEntity
	{
		public virtual void Initialize(Vector2 _position)
		{
			throw new NotSupportedException();
		}

		public virtual AmmoType RelatedAmmoType()
		{
			throw new NotSupportedException();
		}

		protected override void DoCreation(Stage _stage)
		{
			base.DoCreation(_stage);

			logicComponent = new FoodLogicComponent(location, sizeTween);
			AddComponent(logicComponent);
		}

		public override void Collected()
		{
			messagingSystem.SendMessage(new AmmoItemCollectedMessage(RelatedAmmoType()));
			Destroy();
			logicComponent.die();
		}

		FoodLogicComponent logicComponent;

		static AmmoItemEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("particles/star")
			});
		}
	}

	public class CarrotFoodItemEntity : AmmoItemEntity
	{
		public void Initialize(Vector2 _position, Vector2? _startPosition)
		{
			base.Initialize(_position, "food/Carrot", "food/CarrotTexture", Color.Green, Color.Orange, _startPosition);
		}

		public override void Initialize(Vector2 _position)
		{
			Initialize(_position, null);
		}

		public override AmmoType RelatedAmmoType()
		{
			return AmmoType.Carrot;
		}

		protected override void DoCreation(Stage _stage)
		{
			base.DoCreation(_stage);
			location.setModelRotation(new Vector3(0, (float)Math.PI / 4, 0));
		}


		static CarrotFoodItemEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("food/CarrotTexture"),
				new ModelDefinition("food/Carrot")
			});
		}
	}

	public class MelonFoodItemEntity : AmmoItemEntity
	{
		public override void Initialize(Vector2 _position)
		{
			base.Initialize(_position, "food/Melon", "food/MelonTexture", Color.Red, Color.Green);
		}

		public override AmmoType RelatedAmmoType()
		{
			return AmmoType.Melon;
		}

		static MelonFoodItemEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("food/MelonTexture"),
				new ModelDefinition("food/Melon")
			});
		}
	}
}
