using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace shooter
{
	public class CristalEntity : PreloadedSBaseEntity<CristalEntity>
	{
		static CristalEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("decorations/cristal_tex"),
				new ModelDefinition("decorations/cristal")
			});
		}

		public CristalEntity() {}

		public CristalEntity(Vector2 _position)
		{
			position = _position;
		}

		public virtual void initialize()
		{
		}

		protected override void DoCreation(Stage _stage)
		{
			SimplePhysicsComponent location = new SimplePhysicsComponent(position);

			AddComponent(location);
			var model = new ModelComponent(
				location,
				TiledMapReader.entitySize.X / 2.0f, getPreloadedResource<Model>("decorations/cristal"),
			    getPreloadedResource<Texture2D>("decorations/cristal_tex"),
				Depth.Type.Action1,
				RenderConfiguration.Default,
				ModelComponent.RenderMode.Perspective);
			AddComponent(model);

			SimplePhysicsComponent lightLocation = new SimplePhysicsComponent(position + new Vector2(0 , 20));
			var light = new LightEmitterComponent(lightLocation, Color.Violet, 0.025f, 2.0f);
			AddComponent(light);
			var light2 = new LightEmitterComponent(location, Color.Violet, 0.025f, 8.0f);
			AddComponent(light2);
		}

		protected Vector2 position;
	}

}
