﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace shooter
{
	public enum AmmoType { None, Carrot, Melon };

	public interface ControllerStick
	{
		void Hide();
		void Show();
		Vector2 getVelocity();
	}

	public class ControllerEntity : SBaseEntity
	{
		public void initialize(
			FireBtnEntity _fireBtn,
			ControllerStick _movementStick)
		{
			movementStick = _movementStick;
			slot = _fireBtn;
		}

        protected override void DoCreation(Stage _stage)
		{
        }

		public void Show(bool _showFireBtn)
		{
			movementStick.Show();
			if (_showFireBtn && slot != null)
			{
				slot.Show();
			}
		}

		public void Hide()
		{
			movementStick.Hide();
			if (slot != null)
			{
				slot.Hide();
			}
		}

        public Vector2 getVelocity()
        {
            return movementStick.getVelocity();
        }

		public void AddAmmo(int _amt, AmmoType _type)
		{
			if (slot != null)
			{
				slot.ModifyAmount(_amt, _type);
			}
		}

		public AmmoType GetCurrentAmmoType()
		{
			if (slot != null)
			{
				return slot.CurrentAmmoType;
			}
			return AmmoType.None;
		}

		public FireBtnEntity slot;
		ControllerStick movementStick;
	}
}
