﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace shooter
{
	public class MovementSurface : PreloadedSBaseEntity<MovementSurface>, ControllerStick
	{
		static MovementSurface()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("ui/movement_surface"),
				new TextureDefinition("ui/movement_surface_segmentation"),
				new TextureDefinition("ui/movement_surface_arrows")
			});
		}

		public MovementSurface()
		{
		}

		public void Initialize(PCEntity _playerEntity)
		{
			playerEntity = _playerEntity;
			positionTween = new Vector2Tween();
			alphaTween = new FloatTween();
			location = new UILocationComponent(position, UILocationComponent.AnchorPoint.BottomLeft);
		}

		public void InitializeJoystick(PlayerIndex _index)
		{
			//logicComponent.gamepadIndex = _index;
		}

		protected override void DoCreation(Stage _stage)
		{
			AddComponent(positionTween);
			AddComponent(alphaTween);

			AddComponent(location);

			location.position = new TweenWrapper<Vector2>(positionTween);

			effect = _stage.getGameService<EffectsManager>().MovementSurfaceEffect;


			crashTimeout = new CountDownTimerComponent();
			AddComponent(crashTimeout);

			logicComponent = new MovementSurfaceBtnLogic(location, playerEntity, crashTimeout);
			effectLogicComponent = new MovementSurfaceEffectLogic(this, logicComponent, effect, playerEntity, crashTimeout);

			touchHandler = new TouchHandlerComponent(logicComponent.handleNewActiveTouch, logicComponent.stopHandlingActiveTouch, location);
			touchHandler.SetActive(false);

			SpriteComponent surface = new SpriteComponent(
				location,
				getPreloadedResource<Texture2D>("ui/movement_surface"),
				Depth.Type.UI,
				new RenderConfiguration(RenderConfiguration.TranslationType.UI,
					RenderConfiguration.BlendType.Alpha,
					effect
				));
			AddComponent(surface);

			SpriteComponent arrows = new  SpriteComponent(
				location,
				getPreloadedResource<Texture2D>("ui/movement_surface_arrows"),
				Depth.Type.UI,
				new RenderConfiguration(RenderConfiguration.TranslationType.UI));
			AddComponent(arrows);

			surface.Alpha = arrows.Alpha = new TweenWrapper<float>(alphaTween);


			AddComponent(touchHandler);
			AddComponent(logicComponent);
			AddComponent(effectLogicComponent);

			AddComponent(new CrashReceiver(this));
		}

		void OnCrash(SBaseEntity _collidedEntity, Vector2 _collisionPos)
		{
			logicComponent.OnCrash(_collidedEntity, _collisionPos);
			crashTimeout.Start(TimeSpan.FromMilliseconds(500f));
		}

		public void Show()
		{
			positionTween.Start(startPosition, position, TimeSpan.FromSeconds(0.5f), ScaleFuncs.CubicEaseIn);
			alphaTween.Start(0.0f, 1.0f, TimeSpan.FromSeconds(0.5f), ScaleFuncs.CubicEaseIn);
			touchHandler.SetActive(true);
		}

		public void Hide()
		{
			positionTween.Start(position, startPosition, TimeSpan.FromSeconds(0.5f), ScaleFuncs.CubicEaseIn);
			alphaTween.Start(1.0f, 0.0f, TimeSpan.FromSeconds(0.5f), ScaleFuncs.CubicEaseIn);
			touchHandler.SetActive(false);
		}

		public Vector2 getVelocity()
		{
			return logicComponent.velocity;
		}

        class CrashReceiver : MessageReceiverComponent
        {
			public CrashReceiver(MovementSurface _surface)
			{
				surface = _surface;
			}
            public override void ReceiveMessage(Message message)
            {
				if (message is PlayerCrashedMessage crashMessage)
				{
					surface.OnCrash(crashMessage.collidedEntity, crashMessage.collisionPos);
				}
            }
			
			MovementSurface surface;
        }

        MovementSurfaceBtnLogic logicComponent;
		MovementSurfaceEffectLogic effectLogicComponent;
		TouchHandlerComponent touchHandler;
		UILocationComponent location;

		Vector2Tween positionTween;
		FloatTween alphaTween;
		Vector2 position = new Vector2(200, 223);
		Vector2 startPosition = new Vector2(100, 133);

		MovementSurfaceEffect effect;
		PCEntity playerEntity;
		CountDownTimerComponent crashTimeout;
	}
}
