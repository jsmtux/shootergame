﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace shooter
{
	public interface FruitInSlotEntity
	{
		void Show();
		void Hide();
	}

	internal class CarrotInSlotEntity : PreloadedSBaseEntity<CarrotInSlotEntity>, FruitInSlotEntity
	{
		internal class CarrotInSlotLogicEntity : LogicComponent
		{
			public override void UpdateLogic(GameTime _elapsedTime)
			{
				((CarrotInSlotEntity)entity).modelPosition.ModelRotationOffset += new Vector3(0, 0, 0.1f);
			}
		}

		static CarrotInSlotEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("food/CarrotTexture"),
				new ModelDefinition("food/Carrot")
			});
		}

		internal void Initialize(ChildPhysicsComponent _location, TweenWrapper<float> _alpha)
		{
			modelPosition = _location;
			alpha = _alpha;
		}

		protected override void DoCreation(Stage _stage)
		{
			scaleTween = new FloatTween();
			AddComponent(scaleTween);

			carrotModel = new ModelComponent(
				modelPosition,
				0,
				getPreloadedResource<Model>("food/Carrot"),
				getPreloadedResource<Texture2D>("food/CarrotTexture"),
				Depth.Type.UI3,
				new RenderConfiguration(RenderConfiguration.TranslationType.UI));
			carrotModel.tint = new TweenWrapper<Color>(Color.White);
			carrotModel.scale = new TweenWrapper<float>(scaleTween);
			carrotModel.Alpha = alpha;
			AddComponent(carrotModel);

			AddComponent(new CarrotInSlotLogicEntity());
		}

		public void Hide()
		{
			scaleTween.Start(scaleTween.CurrentValue, 0, TimeSpan.FromSeconds(0.5f), ScaleFuncs.CubicEaseOut);
		}

		public void Show()
		{
			scaleTween.Start(scaleTween.CurrentValue, 200, TimeSpan.FromSeconds(0.5f), ScaleFuncs.CubicEaseOut);
		}

		ModelComponent carrotModel;
		ChildPhysicsComponent modelPosition;
		FloatTween scaleTween;
		TweenWrapper<float> alpha;
	}

	internal class MelonInSlotEntity : PreloadedSBaseEntity<MelonInSlotEntity>, FruitInSlotEntity
	{
		internal class MelonInSlotLogicEntity : LogicComponent
		{
			public override void UpdateLogic(GameTime _elapsedTime)
			{
				alpha += 0.05f;
				((MelonInSlotEntity)entity).melonModel.Alpha.SetStatic(Math.Min(1.0f, 0.2f + (float)Math.Abs(Math.Sin(alpha))));
			}

			float alpha = 0;
		}

		static MelonInSlotEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("food/MelonTexture"),
				new ModelDefinition("food/Melon")
			});
		}

		internal void Initialize(ChildPhysicsComponent _location)
		{
			modelPosition = _location;
		}

		protected override void DoCreation(Stage _stage)
		{
			scaleTween = new FloatTween();
			AddComponent(scaleTween);

			melonModel = new ModelComponent(
				modelPosition,
				0,
				getPreloadedResource<Model>("food/Melon"),
				getPreloadedResource<Texture2D>("food/MelonTexture"),
				Depth.Type.UI3,
				new RenderConfiguration(RenderConfiguration.TranslationType.UI));
			melonModel.tint = new TweenWrapper<Color>(Color.White);
			melonModel.scale = new TweenWrapper<float>(scaleTween);
			AddComponent(melonModel);
			AddComponent(new MelonInSlotLogicEntity());
		}

		public void Hide()
		{
			scaleTween.Start(scaleTween.CurrentValue, 0, TimeSpan.FromSeconds(0.5f), ScaleFuncs.CubicEaseOut);
		}

		public void Show()
		{
			scaleTween.Start(scaleTween.CurrentValue, 200, TimeSpan.FromSeconds(0.5f), ScaleFuncs.CubicEaseOut);
		}

		ModelComponent melonModel;
		ChildPhysicsComponent modelPosition;
		FloatTween scaleTween;
	}


	public class FireBtnEntity : PreloadedSBaseEntity<FireBtnEntity>
	{
		static FireBtnEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new SpriteFontDefinition("fonts/btntext"),
				new TextureDefinition("ui/slotbtn")
			});

			AddSubEntitiesToPreload(new List<Type>()
			{
				typeof(CarrotInSlotEntity),
				typeof(MelonInSlotEntity)
			});
		}

		public FireBtnEntity()
		{
		}

		public virtual void InitializeBase(Vector2 _initPos, Vector2 _endPos)
		{
			initPos = _initPos;
			endPos = _endPos;

			location = new UILocationComponent(endPos, UILocationComponent.AnchorPoint.BottomRight);
			AddComponent(location);

			var slotAlpha = new TweenWrapper<float>(alphaTween);
			var carrotSlotPosition = new ChildPhysicsComponent(location, new Vector2(20, -20));
			carrotSlotPosition.ModelRotationOffset = new Vector3(0, 0, (float)Math.PI);
			carrotSlot = new CarrotInSlotEntity();
			carrotSlot.Initialize(carrotSlotPosition, slotAlpha);

			var melonSlotPosition = new ChildPhysicsComponent(location, new Vector2(20, -20));
			melonSlotPosition.ModelRotationOffset = new Vector3(0, 0, (float)Math.PI);
			melonSlot = new MelonInSlotEntity();
			melonSlot.Initialize(melonSlotPosition);
		}

		protected override void DoCreation(Stage _stage)
		{
			messagingSystem = _stage.getGameService<IMessagingSystem>();

			AddComponent(positionTween);
			AddComponent(alphaTween);

			location.position = new TweenWrapper<Vector2>(positionTween);

			btn = new SpriteComponent(location, getPreloadedResource<Texture2D>("ui/slotbtn"), Depth.Type.UI,
												   new RenderConfiguration(RenderConfiguration.TranslationType.UI));
			AddComponent(btn);

			var textLocation = new ChildPhysicsComponent(location, new Vector2(50, 55));
			text = new TextComponent(textLocation, getPreloadedResource<SpriteFont>("fonts/btntext"), "",
										Depth.Type.UI, new RenderConfiguration(RenderConfiguration.TranslationType.UI));
			AddComponent(text);
			text.tint.SetStatic(Colors.TextHighlightColor);

			text.Alpha = btn.Alpha = new TweenWrapper<float>(alphaTween);

			AddSubEntity(carrotSlot);
			AddSubEntity(melonSlot);
		}

		public void Show()
		{
			positionTween.Start(initPos, endPos, TimeSpan.FromSeconds(0.5f), ScaleFuncs.CubicEaseIn);
			alphaTween.Start(0.0f, 1.0f, TimeSpan.FromSeconds(0.5f), ScaleFuncs.CubicEaseIn);
		}

		public void Hide()
		{
			positionTween.Start(endPos, initPos, TimeSpan.FromSeconds(0.5f), ScaleFuncs.CubicEaseOut);
			alphaTween.Start(alphaTween.CurrentValue, 0.0f, TimeSpan.FromSeconds(0.5f), ScaleFuncs.CubicEaseOut);
		}

		public bool ModifyAmount(int _amount, AmmoType _type)
		{
			bool ret = false;
			if (CurrentAmmoType != _type)
			{
				if (CurrentAmmoType != AmmoType.None)
				{
					messagingSystem.SendMessage(new AmmoDroppedMessage(CurrentAmmoType, amount));
				}
				amount = 0;
				SetAmmoModel(_type);
			}
			amount += _amount;
			if (amount > 0)
			{
				CurrentAmmoType = _type;
				text.Text = amount.ToString();
			}
			else
			{
				CurrentAmmoType = AmmoType.None;
				SetAmmoModel(CurrentAmmoType);
				text.Text = "";
				ret = true;
			}
			return ret;
		}

		void SetAmmoModel(AmmoType _type)
		{
			messagingSystem.SendMessage(new AmmoTypeChangedMessage(_type));
			carrotSlot.Hide();
			melonSlot.Hide();
			switch (_type)
			{
				case AmmoType.Carrot:
					carrotSlot.Show();
					break;
				case AmmoType.Melon:
					melonSlot.Show();
					break;
				case AmmoType.None:
					break;
			}
		}

		public virtual bool GetState() { throw new NotSupportedException(); }
		public virtual bool GetAndResetState() { throw new NotSupportedException(); }

		public UILocationComponent GetLocation()
		{
			return location;
		}

		protected UILocationComponent location;

		SpriteComponent btn;
		TextComponent text;
		int amount = 0;
		public AmmoType CurrentAmmoType { get; private set; } = AmmoType.None;

		Vector2Tween positionTween = new Vector2Tween();
		FloatTween alphaTween = new FloatTween();

		Vector2 initPos;
		Vector2 endPos;

		CarrotInSlotEntity carrotSlot;
		MelonInSlotEntity melonSlot;
		IMessagingSystem messagingSystem;

		protected bool Empty { get => CurrentAmmoType == AmmoType.None;}
	}
	public class PlatformFireBtnEntity : FireBtnEntity
	{
		protected override void DoCreation(Stage _stage)
		{
			AddComponent(new TouchHandlerComponent(handleNewActiveTouch, stopHandlingActiveTouch, location));
			base.DoCreation(_stage);
		}

		public void Initialize()
		{
			InitializeBase(new Vector2(100, 50), new Vector2(200, 150));
			logicComponent = new DesktopBtnEntityLogicComponent(Keys.Space);
			AddComponent(logicComponent);
		}

		public void InitializeGamePad(PlayerIndex _index, Buttons _button)
		{
			joystickLogicCompnent = new JoystickBtnLogicComponent(_index, _button);
			AddComponent(joystickLogicCompnent);
		}

		public virtual bool OnNewActiveTouch(ActiveTouch _activeTouch)
		{
			bool ret = false;
			if (active)
			{
				ret = handleNewActiveTouch(_activeTouch);
			}
			return ret;
		}

		public virtual void OnStoppedActiveTouch()
		{
			if (!active)
			{
				stopHandlingActiveTouch();
			}
		}

		public override bool GetState()
		{
			bool ret = false;

			if (!Empty)
			{
				if(active)
				{
					ret = true;
				}
				else if (logicComponent != null)
				{
					ret = logicComponent.GetState();
				}
				if (joystickLogicCompnent != null)
				{
					ret |= joystickLogicCompnent.GetState();
				}
			}

			return ret;
		}

		public override bool GetAndResetState()
		{			
			bool ret = false;
			bool prevActive = active;
			active = false;

			if (!Empty)
			{
				if (prevActive)
				{
					ret = true;
				}
				else if (logicComponent != null)
				{
					ret = logicComponent.GetAndResetState();
				}
				if (joystickLogicCompnent != null)
				{
					ret |= joystickLogicCompnent.GetAndResetState();
				}
			}

			return ret;
		}

		public bool handleNewActiveTouch(ActiveTouch _activeTouch)
		{
			active = true;
			return true;
		}

		public void stopHandlingActiveTouch()
		{
			active = false;
		}

		protected bool active = false;

		DesktopBtnEntityLogicComponent logicComponent;
		JoystickBtnLogicComponent joystickLogicCompnent;
	}
}
