﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Audio;

namespace shooter
{
	public class SodaEnemyEntity : ShipEntity
	{
		static SodaEnemyEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new ModelDefinition("SodaEnemy/Soda")
			});
		}

		public void initialize(Vector2 _position, LocationComponent _targetLocation)
		{
			base.initialize(radius);
			location = new SimplePhysicsComponent(_position);
			targetLocation = _targetLocation;
		}

		public override void create(Stage _stage)
		{
		}

		public void dropHeart()
		{
			var rnd = new Random();
			//TODO: use spawner instead of cached stage
			LootItemEntity loot = new HeartItemEntity();
			loot.Initialize(location.Position);
			stage.addEntity(loot);
		}

		protected override void die()
		{
			base.die();

			RemoveComponent<BurgerEnemyLogicComponent>();
		}

		LocationComponent targetLocation;
		BurgerEnemyLogicComponent logicComponent;
		IMessagingSystem messagingSystem;
		public static readonly float radius = 45f;
		SoundEffectComponent shootSoundComponent;
		Stage stage;
	}
}
