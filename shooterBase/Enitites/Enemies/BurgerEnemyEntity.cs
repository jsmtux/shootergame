﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Audio;

namespace shooter
{
	public class BurgerEnemyEntity : EnemyEntity
	{
		static BurgerEnemyEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new ModelDefinition("BurgerEnemy/Burger"),
				new TextureDefinition("louse/fire"),
				new SoundEffectDefinition("audio/sci-fi_weapon_blaster_laser_boom_small_01"),
				new SoundEffectDefinition("audio/sci-fi_alarm_warning_loop_04")
			});
		}

		public void initialize(Vector2 _position, EnemyTarget _target, List<Point> _enemyPath)
		{
			base.initialize(radius);
			location = new SimplePhysicsComponent(_position);
			target = _target;
			enemyPath = _enemyPath;
		}

		protected override void DoCreation(Stage _stage)
		{
			stage = _stage;
			steering = new SteeringComponent(location, new Velocity(250), 90, new Velocity(9f));
			ChildPhysicsComponent modelLocaction = new ChildPhysicsComponent(location, Vector2.Zero, null, 1);
			modelLocaction.ModelRotationOffset = new Vector3(0, (float)(Math.PI) * 1.2f, 0);
			var drawable = new ModelAnimationComponent(modelLocaction, 40.0f,
											  getPreloadedResource<Model>("BurgerEnemy/Burger"),
											  getPreloadedResource<Texture2D>("particles/pixel"),
											  Depth.Type.Action1,
											  RenderConfiguration.Default,
											  ModelComponent.RenderMode.Perspective);
			AddComponent(drawable);
			base.create(_stage, drawable);

			AnimationSequences burgerAnimation = new AnimationSequences();
            burgerAnimation["idle"] = new AnimationSequence(TimeSpan.Zero, TimeSpan.Zero);
            burgerAnimation["eat"] = new AnimationSequence(TimeSpan.Zero, TimeSpan.FromSeconds(1.0));
			var animator = new AnimatorComponent(drawable, burgerAnimation);
			AddComponent(animator);

			messagingSystem = _stage.getGameService<IMessagingSystem>();

			if (enemyPath == null)
			{
				wanderSteering = new RandomWanderingSteeringComponent(location, new Velocity(50), 100, new Velocity(1.5f), _stage.getGameService<PhysicsResolver>(), radius);
			}
			else
			{
				wanderSteering = new PathWanderingSteeringComponent(location, new Velocity(50), 100, new Velocity(4.5f), enemyPath);
			}

			var colorTween = new ColorTween();
			AddComponent(colorTween);
			colorTween.SetDefault(Color.Black);
			drawable.tint = new TweenWrapper<Color>(colorTween);

			var dyingSound = new SoundEffectComponent(getPreloadedResource<SoundEffect>("audio/sci-fi_alarm_warning_loop_04"));
			AddComponent(dyingSound);

			logicComponent = new BurgerEnemyLogicComponent(
													 location,
													 target,
													 steering,
			                                         wanderSteering,
			                                         colorTween,
													 dyingSound,
													 animator);
			AddComponent(logicComponent);
		}

		protected override bool collided(SBaseEntity _e, Vector2 _collisionPos)
		{
			bool ret = base.collided(_e, _collisionPos);
			Vector2? collisionDirection;
			if ((collisionDirection = logicComponent.collided(_e, messagingSystem, this)) != null)
			{
				deadShipLogic.setDirection(collisionDirection.GetValueOrDefault());
			}
			return ret;
		}

		public void dropHeart()
		{
			var rnd = new Random();
			//TODO: use spawner instead of cached stage
			LootItemEntity loot = new HeartItemEntity();
			loot.Initialize(location.Position);
			stage.addEntity(loot);
		}

		public override void die()
		{
			base.die();

			RemoveComponent<BurgerEnemyLogicComponent>();
		}

		BurgerEnemyLogicComponent logicComponent;
		IMessagingSystem messagingSystem;
		public static readonly float radius = 45f;
		SoundEffectComponent shootSoundComponent;
		ModelAnimationComponent animation;
		Stage stage;
		List<Point> enemyPath;
	}
}
