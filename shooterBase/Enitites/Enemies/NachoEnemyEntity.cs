﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Audio;

namespace shooter
{
	public class NachoEnemyEntity : EnemyEntity
	{
		static NachoEnemyEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new ModelDefinition("NachoEnemy/Nacho")
			});
		}

		public void initialize(Vector2 _position, EnemyTarget _target, List<Point> _enemyPath)
		{
			base.initialize(radius);
			location = new SimplePhysicsComponent(_position);
			target = _target;
			enemyPath = _enemyPath;
		}

		protected override void DoCreation(Stage _stage)
		{
			stage = _stage;
			steering = new SteeringComponent(location, new Velocity(500), 10, new Velocity(1f));
			ChildPhysicsComponent modelLocaction = new ChildPhysicsComponent(location, Vector2.Zero, null, 1);
			modelLocaction.ModelRotationOffset = new Vector3(0, (float)(Math.PI) * 0.2f, 0);
			var drawable = new ModelComponent(modelLocaction, 20.0f,
											  getPreloadedResource<Model>("NachoEnemy/Nacho"),
											  getPreloadedResource<Texture2D>("particles/pixel"),
											  Depth.Type.Action1);
			AddComponent(drawable);
			base.create(_stage, drawable);

			messagingSystem = _stage.getGameService<IMessagingSystem>();

			var colorTween = new ColorTween();
			AddComponent(colorTween);
			colorTween.SetDefault(Color.Black);
			drawable.tint = new TweenWrapper<Color>(colorTween);

			var dyingSound = new SoundEffectComponent(getPreloadedResource<SoundEffect>("audio/sci-fi_alarm_warning_loop_04"));
			AddComponent(dyingSound);

			logicComponent = new NachoEnemyLogicComponent(
													 location,
													 target,
													 steering,
													 colorTween,
													 dyingSound);
			AddComponent(logicComponent);
		}

		protected override bool collided(SBaseEntity _e, Vector2 _collisionPos)
		{
			die();
			deadShipLogic.setDirection(new Vector2(-1, 0));
			return true;
		}

		public override void die()
		{
			base.die();

			RemoveComponent<BurgerEnemyLogicComponent>();
		}

		NachoEnemyLogicComponent logicComponent;
		IMessagingSystem messagingSystem;
		public static readonly float radius = 45f;
		SoundEffectComponent shootSoundComponent;
		EnemyTarget target;
		Stage stage;
		List<Point> enemyPath;
	}
}
