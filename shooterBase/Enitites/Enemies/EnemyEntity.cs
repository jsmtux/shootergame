﻿using System;
using Microsoft.Xna.Framework;

namespace shooter
{
	public abstract class EnemyEntity : ShipEntity
	{
		public EnemyEntity()
		{
		}

		protected override bool collided(SBaseEntity _e, Vector2 _collisionPos)
		{
			bool ret = false;
            if (_e is RockEntity || _e is FenceEntity || _e is ShipEntity)
			{
				ret = true;
                ReactToCollision(_e);
            }
			return ret;
		}

		public void ReactToCollision(SBaseEntity _e)
		{
				Vector2 colliderPosition = _e.GetComponent<PhysicsBodyComponent>().locationComponent.Position;
                Vector2 currentPosition = location.Position;
                Vector2 normal = Vector2.Normalize(currentPosition - colliderPosition);
				Vector2 reflectVelocity = location.velocity;
				reflectVelocity *= 0.7f;
				if (reflectVelocity.Length() < minCollisionVelocity)
				{
					reflectVelocity.Normalize();
					reflectVelocity *= minCollisionVelocity;
				}
				location.velocity = Vector2.Reflect(reflectVelocity, normal);
		}

		protected WanderingSteeringComponent wanderSteering;
		protected EnemyTarget target;

		private float minCollisionVelocity = 10.0f;
	}
}
