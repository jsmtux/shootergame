﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace shooter
{
	public abstract class TreeEntity : PreloadedSBaseEntity<TreeEntity>
	{
		static TreeEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("#FFFFFF"),
				new TextureDefinition("particles/smoke"),
				new SoundEffectDefinition("audio/tree_appear")
			});
		}

		public enum Status { Alive, Dead};

		public TreeEntity() { }

		public TreeEntity(Vector2 _position, Status _status)
		{
			position = _position;
			curStatus = _status;
		}

		protected override void DoCreation(Stage _stage)
		{
			SimplePhysicsComponent location = new SimplePhysicsComponent(position);
			AddComponent(location);

			highlightTween = new ColorTween();
			AddComponent(highlightTween);

			Color treeHighlightColor;
			CreateTreeSpecific(location, out trunkModel, out leavesModel, out treeHighlightColor);
			trunkModel.tint = new TweenWrapper<Color>(highlightTween);

			lightIntensityOnSeedEquippedTween = new FloatTween();
			AddComponent(lightIntensityOnSeedEquippedTween);
			SimplePhysicsComponent lightLocation = new SimplePhysicsComponent(position - new Vector2(0, TiledMapReader.entitySize.Y));
			var lightEmitterOnSeedEquipped = new LightEmitterComponent(lightLocation, treeHighlightColor, 0.008f, 0);
			lightEmitterOnSeedEquipped.intensity = new TweenWrapper<float>(lightIntensityOnSeedEquippedTween);
			AddComponent(lightEmitterOnSeedEquipped);

			highlightLogic = new TreeHighlightLogicComponent(highlightTween, lightIntensityOnSeedEquippedTween, treeHighlightColor);
			AddComponent(highlightLogic);
			AddComponent(new CurrentAmmoReceiver(highlightLogic));

			scaleTween = new FloatTween();
			AddComponent(scaleTween);
			leavesModel.scale = new TweenWrapper<float>(scaleTween);
			float initialLeavesScale = curStatus == Status.Alive ? 50f : 0f;
			scaleTween.SetDefault(initialLeavesScale);

			var leavesPosition = new ChildPhysicsComponent(location, leavesCollisionOffset);

			AddComponent(new RoundPhysicsBodyComponent(135, leavesPosition, Collided));
			
			treeAppearSoundComponent = new SoundEffectComponent(getPreloadedResource<SoundEffect>("audio/tree_appear"));
			AddComponent(treeAppearSoundComponent);

			var lightEmitter = new LightEmitterComponent(lightLocation, Color.Blue, 0.0025f, 0);
			AddComponent(lightEmitter);
			lightIntensityTween = new FloatTween();
			AddComponent(lightIntensityTween);
			lightEmitter.intensity = new TweenWrapper<float>(lightIntensityTween);

			messagingSystem = _stage.getGameService<IMessagingSystem>();
		}

		protected abstract void CreateTreeSpecific(
			LocationComponent location,
			out ModelComponent trunkModel,
			out ModelComponent leavesModel,
			out Color treeHighlightColor);

		private void Grow()
		{
			treeAppearSoundComponent.Play();
			messagingSystem.SendMessage(new TreeActivatedMessage());
			scaleTween.onEnd = () =>
			{
				lightIntensityTween.Start(10, 0, TimeSpan.FromSeconds(0.5f), ScaleFuncs.ElasticEaseOut);
				Spawn();
			};
			scaleTween.Start(0.0f, 50.0f, TimeSpan.FromSeconds(0.5f), ScaleFuncs.ElasticEaseOut);
			lightIntensityTween.Start(0.0f, 10, TimeSpan.FromSeconds(0.5f), ScaleFuncs.ElasticEaseOut);
			curStatus = Status.Alive;
			highlightLogic.Disable();
		}

		protected abstract void Spawn();

		private bool Collided(SBaseEntity _entity, Vector2 _collisionPos)
		{
			if (curStatus == Status.Alive && _entity is ShipEntity ship)
			{
				float distance = (ship.getLocation().Position - leavesCollisionOffset - position).Length();

				leavesModel.Alpha.SetStatic(Math.Min(Math.Max((distance - 120) / 30, 0.5f), 1.0f));
			}
			if (curStatus == Status.Dead && _entity is PlayerSeedEntity projectile)
			{
				Grow();
			}

			return false;
		}

        class TreeHighlightLogicComponent : LogicComponent
        {
			public TreeHighlightLogicComponent(ColorTween _highlight, FloatTween _lightIntensityOnSeedEquippedTween, Color _treeHighlightColor)
			{
				highlight = _highlight;
				lightIntensityOnSeedEquippedTween = _lightIntensityOnSeedEquippedTween;
				treeHighlightColor = _treeHighlightColor;
			}
            public override void UpdateLogic(GameTime _elapsedTime)
            {
            }

			public void Start()
			{
				if (disabled)
				{
					return;
				}
				highlight.onEnd = () => {
					highlight.onEnd = Start;
					highlight.Start(treeHighlightColor, Color.Black, TimeSpan.FromMilliseconds(500), ScaleFuncs.CubicEaseIn);
					lightIntensityOnSeedEquippedTween.Start(4, 0, TimeSpan.FromMilliseconds(500), ScaleFuncs.CubicEaseOut);
				};
				highlight.Start(Color.Black, treeHighlightColor, TimeSpan.FromMilliseconds(500), ScaleFuncs.CubicEaseOut);
				lightIntensityOnSeedEquippedTween.Start(0, 4, TimeSpan.FromMilliseconds(500), ScaleFuncs.CubicEaseOut);
			}

			public void Stop()
			{
				highlight.onEnd = null;
				highlight.Stop(StopBehavior.ForceComplete);
				highlight.SetDefault(Color.Transparent);
				lightIntensityOnSeedEquippedTween.Stop(StopBehavior.ForceComplete);
				lightIntensityOnSeedEquippedTween.SetDefault(0);
			}

			public void Disable()
			{
				Stop();
				disabled = true;
			}

			ColorTween highlight;
			FloatTween lightIntensityOnSeedEquippedTween;
			Color treeHighlightColor;
			bool disabled = false;
        }

        class CurrentAmmoReceiver : MessageReceiverComponent
        {
			public CurrentAmmoReceiver(TreeHighlightLogicComponent _treeHighlight)
			{
				treeHighlight = _treeHighlight;
			}
            public override void ReceiveMessage(Message message)
            {
				if (message is PlayerEnergyChangedMessage energyMessage)
				{
					if (energyMessage.totalPercentage > 0)
					{
						treeHighlight.Start();
					}
					else
					{
						treeHighlight.Stop();
					}
				}
            }

			TreeHighlightLogicComponent treeHighlight;
        }

        Vector2 leavesCollisionOffset = new Vector2(0, -110);
		protected Vector2 position;
		ModelComponent trunkModel, leavesModel;
		FloatTween scaleTween;
		Status curStatus;
		SoundEffectComponent treeAppearSoundComponent;
		IMessagingSystem messagingSystem;
		FloatTween lightIntensityTween, lightIntensityOnSeedEquippedTween;
		ColorTween highlightTween;
		TreeHighlightLogicComponent highlightLogic;
	}

	public class CarrotTreeEntity : TreeEntity
	{
		static CarrotTreeEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("tree/Trees_1_tex"),
				new ModelDefinition("tree/tree_trunk"),
				new ModelDefinition("tree/tree_leaves")
			});
		}

		public CarrotTreeEntity() { }

		public CarrotTreeEntity(Vector2 _position)
			: base(_position, Status.Dead)
		{
			fruitsToSpawn = 6;
		}

		protected override void DoCreation(Stage _stage)
		{
			base.DoCreation(_stage);		

			fruitSpawner = new SpawnerComponent<CarrotFoodItemEntity>(_stage);
		}

		protected override void CreateTreeSpecific(
			LocationComponent location,
			out ModelComponent trunkModel,
			out ModelComponent leavesModel,
			out Color treeHighlightColor)
		{
			trunkModel = new ModelComponent(location, 50f, getPreloadedResource<Model>("tree/tree_trunk"),
											getPreloadedResource<Texture2D>("tree/Trees_1_tex"), Depth.Type.Action1,
											RenderConfiguration.Default,
											ModelComponent.RenderMode.Perspective);
			trunkModel.SetViewVolume(150);
			AddComponent(trunkModel);

			leavesModel = new ModelComponent(location, 0f, getPreloadedResource<Model>("tree/tree_leaves"),
											getPreloadedResource<Texture2D>("tree/Trees_1_tex"), Depth.Type.Action1,
											RenderConfiguration.Default,
											ModelComponent.RenderMode.Perspective);
			leavesModel.SetViewVolume(250);
			leavesModel.tint = new TweenWrapper<Color>(Color.White);
			AddComponent(leavesModel);

			treeHighlightColor = Color.Orange;
		}

		protected override void Spawn()
		{
			float minDistance = 100;
			float maxDistance = 250;
			float maxAlpha = (float)Math.PI / 5;
			for (int i = 0; i < fruitsToSpawn; i++)
			{
				float distance = (float)rnd.NextDouble() * (maxDistance - minDistance) + minDistance;
				float alpha = ((2 * (float)rnd.NextDouble()) - 1) * maxAlpha + (float)Math.PI / 2;
				Vector2 offset = new Vector2(distance * (float)Math.Cos(alpha), distance * (float)Math.Sin(alpha));
				fruitSpawner.spawn((CarrotFoodItemEntity _carrot) =>
				{
					_carrot.Initialize(position - offset, position + new Vector2(0, -50));
				});
			}
		}

		SpawnerComponent<CarrotFoodItemEntity> fruitSpawner;
		int fruitsToSpawn;
		Random rnd = new Random();
	}

	public class BananaTreeEntity : TreeEntity
	{
		static BananaTreeEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("tree/tree_bananas_tex"),
				new ModelDefinition("tree/tree_bananas_trunk"),
				new ModelDefinition("tree/tree_bananas_leaves"),
			});
		}

		public BananaTreeEntity() { }

		public BananaTreeEntity(Vector2 _position, Vector2 _portalPosition)
			: base(_position, Status.Dead)
		{
			portalPosition = _portalPosition;
		}

		protected override void DoCreation(Stage _stage)
		{
			base.DoCreation(_stage);
			messagingSystem = _stage.getGameService<IMessagingSystem>();

			bananaAndParticleTrail = new SpawnerComponent<BananaAndParticleTrailEntity>(_stage);
		}

		protected override void CreateTreeSpecific(
			LocationComponent location,
			out ModelComponent trunkModel,
			out ModelComponent leavesModel,
			out Color treeHighlightColor)
		{
			trunkModel = new ModelComponent(location, 50f, getPreloadedResource<Model>("tree/tree_bananas_trunk"),
											getPreloadedResource<Texture2D>("tree/tree_bananas_tex"), Depth.Type.Action1,
											RenderConfiguration.Default,
											ModelComponent.RenderMode.Perspective);
			trunkModel.SetViewVolume(150);
			AddComponent(trunkModel);

			leavesModel = new ModelComponent(location, 0f, getPreloadedResource<Model>("tree/tree_bananas_leaves"),
											getPreloadedResource<Texture2D>("tree/tree_bananas_tex"), Depth.Type.Action1,
											RenderConfiguration.Default,
											ModelComponent.RenderMode.Perspective);
			leavesModel.SetViewVolume(250);
			leavesModel.tint = new TweenWrapper<Color>(Color.White);
			AddComponent(leavesModel);

			treeHighlightColor = Color.Yellow;
		}

        protected override void Spawn()
        {
			bananaAndParticleTrail.spawn((BananaAndParticleTrailEntity _trail) => {
				_trail.onEnd = () => messagingSystem.SendMessage(new BananaKeyRetrievedMessage(position));
				_trail.initialize(new SimplePhysicsComponent(position -  new Vector2(0, 150)), new SimplePhysicsComponent(portalPosition));
			});
        }

		Vector2 portalPosition;
		IMessagingSystem messagingSystem;
		SpawnerComponent<BananaAndParticleTrailEntity> bananaAndParticleTrail;
	}
}
