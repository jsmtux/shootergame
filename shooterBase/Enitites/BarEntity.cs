﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
namespace shooter
{
	public class BarEntity : PreloadedSBaseEntity<BarEntity>
	{
		static BarEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("ui/pixel")
			});
		}

		public void initialize(ChildPhysicsComponent _location, Color _tint, Color _focustTint, Color _bgTint, float _initialPercentage, Vector2 _maxSize)
		{
			maxSize = _maxSize;
			bgLocation = new ChildPhysicsComponent(_location, Vector2.Zero);
			location = new ChildPhysicsComponent(_location, Vector2.Zero);
			tint = _tint;
			focusTint = _focustTint;
			bgTint = _bgTint;
			initialPercentage = _initialPercentage;
			sizeTween = new FloatTween();
			tintTween = new ColorTween();
			tintTween.SetDefault(tint);
			sprite = new SpriteComponent(location, getPreloadedResource<Texture2D>("ui/pixel"), Depth.Type.UI2,
												   new RenderConfiguration(RenderConfiguration.TranslationType.UI));
			spriteBg = new SpriteComponent(bgLocation, getPreloadedResource<Texture2D>("ui/pixel"), Depth.Type.UI,
												   new RenderConfiguration(RenderConfiguration.TranslationType.UI));
		}

		protected override void DoCreation(Stage _stage)
		{
			AddComponent(tintTween);
			sprite.tint = new TweenWrapper<Color>(tintTween);
			AddComponent(sprite);
			AddComponent(spriteBg);
			spriteBg.tint = new TweenWrapper<Color>(bgTint);

			Vector2 pos = location.Position;
			location.Size = maxSize;
			location.Anchors.TopLeft = pos;
			bgLocation.Size = maxSize;
			bgLocation.Anchors.TopLeft = pos;

			updatePercentage(initialPercentage);

			AddComponent(sizeTween);
			sizeTween.onUpdate = updatePercentage;
		}
		public void setPercentage(float _percentage)
		{
			sizeTween.Start(currentPercentage, _percentage, TimeSpan.FromSeconds(0.2f), ScaleFuncs.CubicEaseOut);
			tintTween.Start(focusTint, tint, TimeSpan.FromSeconds(1.0f), ScaleFuncs.CubicEaseOut);
		}

		void updatePercentage(float _percentage)
		{
			float newSize = maxSize.X * _percentage;
			topLeft = location.Anchors.TopLeft;
			location.Size = new Vector2(newSize, maxSize.Y);
			location.Anchors.TopLeft = topLeft;
			currentPercentage = _percentage;
		}

		public TweenWrapper<float> Alpha { get { return sprite.Alpha; } set { spriteBg.Alpha = sprite.Alpha = value; } }

		Vector2 maxSize;
		Vector2 topLeft;
		ChildPhysicsComponent location;
		ChildPhysicsComponent bgLocation;
		ColorTween tintTween;
		float initialPercentage;
		float currentPercentage;
		FloatTween sizeTween;
		SpriteComponent sprite;
		SpriteComponent spriteBg;

		Color tint;
		Color focusTint;
		Color bgTint;
	}
}
