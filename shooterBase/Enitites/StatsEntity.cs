﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace shooter
{
	public class StatsEntity : PreloadedSBaseEntity<StatsEntity>
	{
		static StatsEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("ui/statssurface")
			});
		}

		public class StatsEntityReceiver : MessageReceiverComponent
        {
            public override void ReceiveMessage(Message message)
			{
				if (message is PlayerLifeChangedMessage playerHurtMessage)
				{
					StatsEntity entity = getEntity() as StatsEntity;
					entity.healthBar.setPercentage(playerHurtMessage.totalPercentage);
				}
				else if (message is PlayerEnergyChangedMessage playerEnergyMessage)
				{
					StatsEntity entity = getEntity() as StatsEntity;
					entity.energyBar.setPercentage(playerEnergyMessage.totalPercentage);
				}
            }
        }
		public void initialize()
		{
			positionTween = new Vector2Tween();
			alphaTween = new FloatTween();
		}

		protected override void DoCreation(Stage _stage)
		{
			AddComponent(positionTween);
			AddComponent(alphaTween);

			UILocationComponent location = new UILocationComponent(Vector2.Zero, UILocationComponent.AnchorPoint.TopLeft);
			AddComponent(location);
			location.position = new TweenWrapper<Vector2>(positionTween);
			SpriteComponent btn = new SpriteComponent(location, getPreloadedResource<Texture2D>("ui/statssurface"), Depth.Type.UI,
												   new RenderConfiguration(RenderConfiguration.TranslationType.UI));
			AddComponent(btn);

			AddComponent(new StatsEntityReceiver());

			var healthBarLocation = new ChildPhysicsComponent(location, healthBarCorner);
			healthBar.initialize(healthBarLocation, new Color(255, 76, 103), new Color(255, 0, 38), Colors.TextHighlightColor, 1.0f, healthBarSize);
			_stage.addEntity(healthBar);

			var energyBarLocation = new ChildPhysicsComponent(location, energyBarCorner);
			energyBar.initialize(energyBarLocation, new Color(157, 83, 248), new Color(118, 46, 210), Colors.TextHighlightColor, 0.0f, energyBarSize);
			_stage.addEntity(energyBar);

			energyBar.Alpha = healthBar.Alpha = btn.Alpha = new TweenWrapper<float>(alphaTween);
		}

		public void Show()
		{
			positionTween.Start(hiddenPos, shownPos, TimeSpan.FromMilliseconds(500), ScaleFuncs.CubicEaseIn);
			alphaTween.Start(0.0f, 1.0f, TimeSpan.FromMilliseconds(500), ScaleFuncs.CubicEaseIn);
		}

		public void Hide()
		{
			positionTween.Start(shownPos, hiddenPos, TimeSpan.FromMilliseconds(500), ScaleFuncs.CubicEaseIn);
			alphaTween.Start(1.0f, 0.0f, TimeSpan.FromMilliseconds(500), ScaleFuncs.CubicEaseIn);
		}

		public override void delete()
		{
			base.delete();
			healthBar.delete();
			energyBar.delete();
		}

		readonly Vector2 hiddenPos = new Vector2(219, -50);
		readonly Vector2 shownPos = new Vector2(219, 65);

		Vector2 healthBarCorner = new Vector2(-60, -31);
		Vector2 healthBarSize = new Vector2(240, 23);
		BarEntity healthBar = new BarEntity();

		Vector2 energyBarCorner = new Vector2(-60, 11);
		Vector2 energyBarSize = new Vector2(200, 23);
		BarEntity energyBar = new BarEntity();

		Vector2Tween positionTween;
		FloatTween alphaTween;

	}
}
