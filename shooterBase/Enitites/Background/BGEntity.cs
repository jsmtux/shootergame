﻿using System;
using ECS;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace shooter
{
	public class BGEntity : PreloadedSBaseEntity<BGEntity>
	{
        public class BGTexturesLogicComponent : LogicComponent
        {
			public BGTexturesLogicComponent(CameraEntity _camera, LocationComponent _bgLocation, CloudsEffect _cloudsEffect)
			{
				camera = _camera;
				bgLocation = _bgLocation;
				cloudsEffect = _cloudsEffect;
			}

            public override void UpdateLogic(GameTime _elapsedTime)
            {
				bgLocation.Size = (camera.CameraComponent.FrustumRectangle.Size + new Point(2,2)).ToVector2();
				cloudsEffect.SetCameraPosition(camera.Location.Position);
            }

			CameraEntity camera;
			LocationComponent bgLocation;
			CloudsEffect cloudsEffect;
        }
        static BGEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("particles/star"),
				new TextureDefinition("particles/planet_glow"),
				new TextureDefinition("planets/Earth_2k"),
				new ModelDefinition("planets/Earth"),
				new TextureDefinition("planets/Jupiter_2k"),
				new ModelDefinition("planets/Jupiter"),
				new TextureDefinition("planets/Mars_2k"),
				new ModelDefinition("planets/Mars"),
				new TextureDefinition("planets/Mercury_2k"),
				new ModelDefinition("planets/Mercury"),
				new TextureDefinition("planets/Moon_2k"),
				new ModelDefinition("planets/Moon"),
				new TextureDefinition("planets/Neptune_2k"),
				new ModelDefinition("planets/Neptune"),
				new TextureDefinition("planets/Pluto_2k"),
				new ModelDefinition("planets/Pluto"),
				new TextureDefinition("planets/Saturn_2k"),
				new ModelDefinition("planets/Saturn"),
				new TextureDefinition("planets/Sun_2k"),
				new ModelDefinition("planets/Sun"),
				new TextureDefinition("planets/Uranus_2k"),
				new ModelDefinition("planets/Uranus"),
				new TextureDefinition("planets/Venus_2k"),
				new ModelDefinition("planets/Venus")
			});
		}

		public void initialize(CameraEntity _camera, Point _resolution, TextureCreator _textureCreator, List<Color> _colorNebulas = null)
		{
			camera = _camera;
			cameraLocation = _camera.Location;
			resolution = _resolution;
			textureCreator = _textureCreator;
			colorNebulas = _colorNebulas;
		}

		private void createStars(Stage _stage)
		{
			int levels = 6;
			float minMovement = 0.95f;
			float maxMovement = 0.4f;
			float minSize = 8.0f;
			float maxSize = 28.0f;
			for (int z = 1; z <= levels; z++)
			{
				float levelPercentage = (float)z / (float)levels;
				float starSize = Utils.Lerp(minSize, maxSize, levelPercentage);
				float starMovement = Utils.Lerp(minMovement, maxMovement, levelPercentage);
				for (int i = 0; i < 15 * (levels - z); i++)
				{
					ChildPhysicsComponent location =
						new ChildPhysicsComponent(cameraLocation,
											  new Vector2(rnd.Next(-1200, 800),
														  rnd.Next(-900, 900)),
						                      new Vector2(starSize, starSize),
						                      starMovement);
					AddComponent(location);
					SpriteComponent starSprite = new SpriteComponent(location, getPreloadedResource<Texture2D>("particles/star"), Depth.Type.Bg3,
					                                                 new RenderConfiguration(RenderConfiguration.TranslationType.Camera, RenderConfiguration.BlendType.Additive));
					location.Size = new Vector2(starSize, starSize);
					starSprite.tint.SetStatic(Utils.HsvToRgb(rnd.Next(-16, 66), 0.3f, 1.0f));
					AddComponent(starSprite);
				}
			}
		}

		public enum PlanetTypes {Earth, Jupiter, Mars, Mercury, Moon, Neptune, Pluto, Saturn, Sun, Uranus, Venus};
		List<String> PlanetNames = new List<string>()
			{ "Earth", "Jupiter", "Mars", "Mercury", "Moon", "Neptune", "Pluto", "Saturn", "Sun", "Uranus", "Venus"};
		List<Color> planetGlows = new List<Color>()
			{ new Color(86,163,163), new Color(160,171,175), new Color(227,106,63), new Color(165,135,108), new Color(112,112,112),
			new Color(72,100,223), new Color(133,108,96), new Color(157,170,174), new Color(246,223,67), new Color(132,189,208), new Color(218,170,106) };

		public void CreatePlanets(Stage _stage, int _number)
		{
			List<Vector2> assignedPositions = new List<Vector2>();

			bool checkCollides(Vector2 _pos)
			{
				foreach(Vector2 pos in assignedPositions)
				{
					if ((pos - _pos).LengthSquared() < 100000)
						return true;
				}
				return false;
			}

			for (int i = 0; i < _number; i++)
			{
				Vector2 position;
				do
				{
					position = new Vector2((float)rnd.NextDouble() * resolution.X, (float)rnd.NextDouble() * resolution.Y);
				} while (checkCollides(position));
				assignedPositions.Add(position);

				PlanetTypes planet = (PlanetTypes)rnd.Next(0, 11);

				float sizePercentage = (float)rnd.NextDouble();

				Vector3 rotation = new Vector3(
					(float)(rnd.NextDouble() * Math.PI * 2),
					(float)(rnd.NextDouble() * Math.PI * 2),
					(float)(rnd.NextDouble() * Math.PI * 2));

				float rotationSpeed = (float)rnd.NextDouble() * 0.01f;

				// TODO: reimplement planets in Camera location type
				//CreatePlanet(planet, position, sizePercentage, rotation, rotationSpeed, planetGlows[(int)planet]);
			}
		}

		public void CreatePlanet(PlanetTypes _planet, Vector2 _position, float _sizePercentage, Vector3 _angle, float _rotationSpeed, Color _tint)
		{
			string name = PlanetNames[(int)(_planet)];
			SimplePhysicsComponent location = new SimplePhysicsComponent(_position);
			location.setModelRotation(_angle);
			float scale = _sizePercentage * _sizePercentage + 0.5f;
			var model = new ModelComponent(location, 30f * scale, getPreloadedResource<Model>("planets/" + name),
											getPreloadedResource<Texture2D>("planets/" + name + "_2k"), Depth.Type.Bg2,
											new RenderConfiguration(RenderConfiguration.TranslationType.UI));
			model.lightOverride = new LightingDefinition();
			model.lightOverride.light0.diffuse = Color.Lerp(Colors.nebulaColors[0] * 0.5f, Color.White * 0.5f, _sizePercentage * _sizePercentage);
			model.lightOverride.ignoreProcessedLights = true;
			AddComponent(model);
			AddComponent(new PlanetLogicComponent(location, _rotationSpeed));

			var glowLocation = new ChildPhysicsComponent(location, Vector2.Zero);

			var glow = new SpriteComponent(glowLocation, getPreloadedResource<Texture2D>("particles/planet_glow"), Depth.Type.Bg2);
			glowLocation.Size *= scale;

			glow.tint.SetStatic(_tint);
			AddComponent(glow);
		}

		protected override void DoCreation(Stage _stage)
		{
			createStars(_stage);

			CreatePlanets(_stage, rnd.Next(3, 6));

			LocationComponent location = new ChildPhysicsComponent(cameraLocation, Vector2.Zero);
			

			AddComponent(location);

			if (colorNebulas == null)
			{
				InitializeNebulas(2);
			}

			BGCreator creator = new BGCreator(textureCreator, resolution, colorNebulas, 0.1f, 0.25f);
			SpriteComponent starSprite = new SpriteComponent(location, creator.getStarTexture(), Depth.Type.Bg2,
			                                                 new RenderConfiguration(RenderConfiguration.TranslationType.Camera, RenderConfiguration.BlendType.Additive));
			AddComponent(starSprite);
			SpriteComponent nebulaSprite = new SpriteComponent(location, creator.getNebulaTexture(), Depth.Type.Bg1,
				new RenderConfiguration(
					RenderConfiguration.TranslationType.Camera,
					RenderConfiguration.BlendType.Alpha
					//,_stage.getGameService<EffectsManager>().CloudsEffect
				));
			AddComponent(nebulaSprite);

			AddComponent(new BGTexturesLogicComponent(camera, location, _stage.getGameService<EffectsManager>().CloudsEffect));
		}

		void InitializeNebulas(int _numColors)
		{
			colorNebulas = new List<Color>();
			for (int i = 0; i < _numColors; i++)
			{
				int hue = rnd.Next(360);
				Color color = Utils.HsvToRgb(hue, 1.0f, 0.5f);
				colorNebulas.Add(color);
			}
		}

		CameraEntity camera;
		LocationComponent cameraLocation;
		Point resolution;
		TextureCreator textureCreator;
		List<Color> colorNebulas = null;
		Random rnd = new Random();
	}
}
