using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace shooter
{
	public class CloudsEntity : PreloadedSBaseEntity<CloudsEntity>
	{
		static CloudsEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
                new TextureDefinition("particles/carrot")
			});
		}

		public void Initialize(Vector2 _position, Vector2 _size)
		{
            position = _position;
            size = _size;
		}

		protected override void DoCreation(Stage _stage)
		{
			location = new SimplePhysicsComponent(position + new Vector2(0.5f, 0.5f), 0, size);
			spriteComponent = new SpriteComponent(
				location,
				getPreloadedResource<Texture2D>("particles/carrot"),
				Depth.Type.Top,
				new RenderConfiguration(
					RenderConfiguration.TranslationType.Camera,
					RenderConfiguration.BlendType.Alpha,
					_stage.getGameService<EffectsManager>().CloudsEffect
				));
			AddComponent(spriteComponent);
		}

		Vector2 position, size;
		LocationComponent location;
        SpriteComponent spriteComponent;
	}
}
