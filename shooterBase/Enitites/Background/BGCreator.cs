﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace shooter
{
	public class BGCreator
	{
		class NebulaDescription
		{
			public NebulaDescription(float _density, float _falloff, Color _color, Vector2 _offset)
			{
				density = _density;
				falloff = _falloff;
				color = _color;
				offset = _offset;
			}
			public float density;
			public float falloff;
			public Color color;
			public Vector2 offset;
		}

		public BGCreator(TextureCreator _textureCreator, Point _resolution, List<Color> _colorNebulas, float _starDensity, float _starBrightness)
		{
			foreach (Color color in _colorNebulas)
			{
				float density = 0.25f;
				float fallOff = 4.0f;

				nebulas.Add(new NebulaDescription(density, fallOff, color, new Vector2(rnd.Next(1000), rnd.Next(1000))));
			}

			textureCreator = _textureCreator;
			resolution = _resolution;
			starDensity = _starDensity;
			starBrightness = _starBrightness;
		}

		public Texture2D getStarTexture()
		{
			return textureCreator.CreateTexture((int)resolution.X / starResolutionFactor,
												(int)resolution.Y / starResolutionFactor,
												paintStarFunction);
		}

		public Texture2D getNebulaTexture()
		{
			return textureCreator.CreateTexture((int)resolution.X / nebulaResolutionFactor,
												(int)resolution.Y / nebulaResolutionFactor,
												paintNebulaFunction);
		}

		private Color colorFunction(Vector2 _position)
		{
			Color ret = Color.Black;
			foreach (NebulaDescription nebula in nebulas)
			{
				Color curNebulaColor = GetNebulaValue(_position, nebula.density, nebula.falloff, nebula.color, nebula.offset);
				ret = Color.Lerp(ret, curNebulaColor, 1.0f / nebulas.Count);
			}
			return ret;
		}

		public Color[] paintStarFunction(Color[] _data)
		{
			Point bgResolution = new Point(resolution.X / starResolutionFactor, resolution.Y / starResolutionFactor);
			Vector2 position;
			for (position.Y = 0; position.Y < bgResolution.Y; position.Y++)
			{
				for (position.X = 0; position.X < bgResolution.X; position.X++)
				{
					if (rnd.NextDouble() < starDensity)
					{
						int index = (int)(position.X + position.Y * bgResolution.X);
						float brightness = GetNebulaValue(position, 0.25f, 4.0f, Color.White, Vector2.Zero).R / 255;
						float c = (float)Math.Log(1.0f - rnd.NextDouble()) * -starBrightness * brightness * brightness;
						_data[index] = new Color(c, c, c, c);
					}
				}
			}

			return _data;
		}

		public Color[] paintNebulaFunction(Color[] _data)
		{
			Vector2 position;
			Point nebulaResolution = new Point(resolution.X / nebulaResolutionFactor, resolution.Y / nebulaResolutionFactor);
			for (position.Y = 0; position.Y < nebulaResolution.Y; position.Y++)
			{
				for (position.X = 0; position.X < nebulaResolution.X; position.X++)
				{
					int index = (int)(position.X + position.Y * nebulaResolution.X);
					_data[index] = colorFunction(position);
				}
			}

			return _data;
		}

		Color GetNebulaValue(Vector2 _position, float _density, float _falloff, Color _color, Vector2 _offset)
		{
			float value = noise(new Vector2(_position.X / 70, _position.Y / 70), _offset);
			value = (float)Math.Pow(value + _density, _falloff);
			Vector3 baseColor = _color.ToVector3();
			return new Color(value * baseColor.X, value * baseColor.Y, value * baseColor.Z);
		}

		float normalnoise(Vector2 _position)
		{
			return Perlin.perlin(_position.X, _position.Y, 0.0f) * 0.5f + 0.5f;
		}

		float noise(Vector2 _position, Vector2 offset)
		{
			_position += offset;
			const int steps = 2;
			float scale = (float)Math.Pow(steps, 2);

			float displace = 0.0f;

			for (int i = 0; i < steps; i++)
			{
				displace = normalnoise(_position * scale + new Vector2(displace, displace));
				scale *= 0.5f;
			}
			return normalnoise(_position + new Vector2(displace, displace));
		}

		List<NebulaDescription> nebulas = new List<NebulaDescription>();
		float starDensity;
		float starBrightness;
		Point resolution;
		int nebulaResolutionFactor = 8;
		int starResolutionFactor = 2;
		TextureCreator textureCreator;
		Random rnd = new Random();
	}
}
