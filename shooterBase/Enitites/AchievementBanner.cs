﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace shooter
{
	public class AchievementBanner : PreloadedSBaseEntity<AchievementBanner>
	{
		public static AchievementBanner CreateSubEntity(AchievementEntity achievement)
		{
			if (achievement is LessThanCrashAchievementEntity)
			{
				return new LessThanCrashAchievementBanner(achievement);
			}
			else if (achievement is LessThanTimeAchievementEntity)
			{
				return new LessThanTimeAchievementBanner(achievement);
			}
			else if (achievement is DestroyRocksAchievementEntity)
			{
				return new DestroyRocksAchievementBanner(achievement);
			}
			else if (achievement is ActivateTreesAchievementEntity)
			{
				return new ActivateTreesAchievementBanner(achievement);
			}
			else if (achievement is DestroyBurgersAchievementEntity)
			{
				return new DestroyBurgersAchievementBanner(achievement);
			}
			else if (achievement is UseLessThanCarrotsAchievementEntity)
			{
				return new UseLessThanCarrotsAchievementBanner(achievement);
			}
			else
			{
				throw new NotImplementedException("Achievement has no associated banner");
			}
		}

		class AchievementBannerLogicComponent : LogicComponent
		{
			public AchievementBannerLogicComponent(AchievementBanner _parent)
			{
				parent = _parent;
			}
		
			public override void UpdateLogic(GameTime _gameTime)
			{
				if(!notified)
				{
					if (!parent.achievement.IsAchievable())
					{
						parent.ShowFailure();
						notified = true;
					}
					else if (parent.achievement.checkUnlockedAchievement(null))
					{
						parent.ShowSuccess();
						notified = true;
					}
				}
			}

			AchievementBanner parent;
			bool notified = false;
		}

		static AchievementBanner()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new SpriteFontDefinition("fonts/missionText"),
				new TextureDefinition("ui/challenge_no_crash"),
				new TextureDefinition("ui/challenge_less_time"),
				new TextureDefinition("ui/challenge_banana"),
				new TextureDefinition("ui/challenge_failed"),
				new TextureDefinition("ui/challenge_achieved"),
				new TextureDefinition("ui/challenge_destroy_rock"),
				new TextureDefinition("ui/challenge_activate_tree"),
				new TextureDefinition("ui/challenge_burgers"),
				new TextureDefinition("ui/challenge_starseeds"),
				new TextureDefinition("ui/challenge_carrot"),
				new SoundEffectDefinition("audio/achievement_success"),
				new SoundEffectDefinition("audio/achievement_fail")
			});
		}

		public AchievementBanner() { }

		protected AchievementBanner(string _image, AchievementEntity _achievement)
		{
			image = _image;
			achievement = _achievement;
		}

		public void Initialize(Vector2 _position)
		{
			endPosition = _position;
			startPosition = new Vector2(endPosition.X - positionMovement, endPosition.Y);
			location = new UILocationComponent(startPosition, UILocationComponent.AnchorPoint.TopRight);
		}

		protected override void DoCreation(Stage _stage)
		{
			AddComponent(location);
			baseImage = new SpriteComponent(location, getPreloadedResource<Texture2D>(image), Depth.Type.UI,
									  new RenderConfiguration(RenderConfiguration.TranslationType.UI));
			AddComponent(baseImage);

			var textLocation = new ChildPhysicsComponent(location, new Vector2(22, 0));
			label = new TextComponent(textLocation, getPreloadedResource<SpriteFont>("fonts/missionText"), "",
										  Depth.Type.UI,
										  new RenderConfiguration(RenderConfiguration.TranslationType.UI));
			label.tint = new TweenWrapper<Color>(Colors.TextHighlightColor);
			label.Alpha = baseImage.Alpha;
			AddComponent(label);

			failedImageAlpha = new FloatTween();
			AddComponent(failedImageAlpha);
			failedImageScale = new Vector2Tween();
			AddComponent(failedImageScale);

			var failedImageLocation = new ChildPhysicsComponent(location, Vector2.Zero);
			failedImage = new SpriteComponent(failedImageLocation, getPreloadedResource<Texture2D>("ui/challenge_failed"), Depth.Type.UI2,
									  new RenderConfiguration(RenderConfiguration.TranslationType.UI));
			AddComponent(failedImage);
			failedImage.Alpha = new TweenWrapper<float>(failedImageAlpha);
			failedImageAlpha.SetDefault(1);
			failedImageLocation.SizeTween = new TweenWrapper<Vector2>(failedImageScale);
			
			failedSoundEffect = new SoundEffectComponent(getPreloadedResource<SoundEffect>("audio/achievement_fail"));
			AddComponent(failedSoundEffect);
			achievedSoundEffect = new SoundEffectComponent(getPreloadedResource<SoundEffect>("audio/achievement_success"));
			AddComponent(achievedSoundEffect);

			achievedImageAlpha = new FloatTween();
			AddComponent(achievedImageAlpha);
			achievedImageScale = new Vector2Tween();
			AddComponent(achievedImageScale);

			var achievedImageLocation = new ChildPhysicsComponent(location, Vector2.Zero);
			achievedImage = new SpriteComponent(achievedImageLocation, getPreloadedResource<Texture2D>("ui/challenge_achieved"), Depth.Type.UI2,
									  new RenderConfiguration(RenderConfiguration.TranslationType.UI));
			AddComponent(achievedImage);
			achievedImage.Alpha = new TweenWrapper<float>(achievedImageAlpha);
			achievedImageAlpha.SetDefault(0);
			achievedImageLocation.SizeTween = new TweenWrapper<Vector2>(achievedImageScale);

			AddComponent(positionTween);
			positionTween.SetDefault(startPosition);
			location.position = new TweenWrapper<Vector2>(positionTween);

			AddComponent(new AchievementBannerLogicComponent(this));
		}

		protected void SetText(String _text)
		{
			label.Text = _text;
		}

		public virtual void Show()
		{
			positionTween.Start(startPosition, endPosition, TimeSpan.FromSeconds(1), ScaleFuncs.CubicEaseOut);
		}

		public virtual void Hide()
		{
			positionTween.Start(positionTween.CurrentValue, startPosition, TimeSpan.FromSeconds(1), ScaleFuncs.CubicEaseOut);
		}

		public void ShowFailure()
		{
			failedImageScale.onEnd = () =>
			{
				positionTween.Start(endPosition, startPosition, TimeSpan.FromSeconds(1), ScaleFuncs.CubicEaseOut);
			};
			failedImageAlpha.Start(0, 1, TimeSpan.FromSeconds(0.5f), ScaleFuncs.BounceEaseIn);
			failedImageScale.Start(location.Size * 5, location.Size, TimeSpan.FromSeconds(1), ScaleFuncs.CubicEaseIn);
			failedSoundEffect.Play();
		}

		public void ShowSuccess()
		{
			achievedImageScale.onEnd = () =>
			{
				positionTween.Start(endPosition, startPosition, TimeSpan.FromSeconds(1), ScaleFuncs.CubicEaseOut);
			};
			achievedImageAlpha.Start(0, 1, TimeSpan.FromSeconds(0.5f), ScaleFuncs.BounceEaseIn);
			achievedImageScale.Start(location.Size * 5, location.Size, TimeSpan.FromSeconds(1), ScaleFuncs.CubicEaseIn);
			achievedSoundEffect.Play();
		}

		UILocationComponent location;
		SpriteComponent baseImage;
		SpriteComponent failedImage, achievedImage;
		SoundEffectComponent failedSoundEffect, achievedSoundEffect;
		String image;
		TextComponent label;
		Vector2Tween positionTween = new Vector2Tween();
		Vector2 endPosition, startPosition;
		FloatTween failedImageAlpha, achievedImageAlpha;
		Vector2Tween failedImageScale, achievedImageScale;
		float positionMovement = 110;
		protected AchievementEntity achievement;
	}

	public class LessThanTimeAchievementBanner : AchievementBanner
	{
		class LessThanTimeLogicComponent : LogicComponent
		{
			public LessThanTimeLogicComponent(
				LessThanTimeAchievementBanner _parent,
				LessThanTimeAchievementEntity _achievement)
			{
				parent = _parent;
				achievement = _achievement;
			}

			public override void UpdateLogic(GameTime _elapsedTime)
			{
				parent.SetText(achievement.GetElapsedSeconds().ToString());
			}

			LessThanTimeAchievementBanner parent;
			LessThanTimeAchievementEntity achievement;
		}

		public LessThanTimeAchievementBanner() { }

		public LessThanTimeAchievementBanner(AchievementEntity _achievement)
			: base("ui/challenge_less_time", _achievement)
		{
		}

		protected override void DoCreation(Stage _stage)
		{
			base.DoCreation(_stage);
			AddComponent(new LessThanTimeLogicComponent(this, (LessThanTimeAchievementEntity)achievement));
		}

		public override void Show()
		{
			base.Show();
		}
	}

	public class LessThanCrashAchievementBanner : AchievementBanner
	{
		class LessThanCrashReceiver : MessageReceiverComponent
		{
			public LessThanCrashReceiver(
				LessThanCrashAchievementBanner _parent,
				LessThanCrashAchievementEntity _achievement)
			{
				parent = _parent;
				achievement = _achievement;
			}

			public override void ReceiveMessage(Message message)
			{

				if (message is PlayerCrashedMessage)
				{
					parent.UpdateText(achievement.GetNumCrashes());
				}
			}

			LessThanCrashAchievementBanner parent;
			LessThanCrashAchievementEntity achievement;
		}

		public LessThanCrashAchievementBanner(AchievementEntity _achievement)
			: base("ui/challenge_no_crash", _achievement)
		{
		}

		protected override void DoCreation(Stage _stage)
		{
			base.DoCreation(_stage);
			AddComponent(new LessThanCrashReceiver(this, (LessThanCrashAchievementEntity)achievement));
			UpdateText(0);
		}

		internal void UpdateText(int _numCrashes)
		{
			SetText(_numCrashes.ToString());
		}
	}

	public class DestroyRocksAchievementBanner : AchievementBanner
	{
		class DestroyRocksReceiver : MessageReceiverComponent
		{
			public DestroyRocksReceiver(
				DestroyRocksAchievementBanner _parent,
				DestroyRocksAchievementEntity _achievement)
			{
				parent = _parent;
				achievement = _achievement;
			}

			public override void ReceiveMessage(Message message)
			{
				if (message is BrokenFenceMessage)
				{
					parent.UpdateText(achievement.GetDestroyedRocks());
				}
			}

			DestroyRocksAchievementBanner parent;
			DestroyRocksAchievementEntity achievement;
		}

		public DestroyRocksAchievementBanner(AchievementEntity _achievement)
			: base("ui/challenge_destroy_rock", _achievement)
		{
		}

		protected override void DoCreation(Stage _stage)
		{
			base.DoCreation(_stage);
			AddComponent(new DestroyRocksReceiver(this, (DestroyRocksAchievementEntity)achievement));
			UpdateText();
		}

		internal void UpdateText(int _numCrashes = 0)
		{
			SetText(_numCrashes.ToString() + "/" + ((DestroyRocksAchievementEntity)achievement).GetTotalRocks());
		}

	}

	public class ActivateTreesAchievementBanner : AchievementBanner
	{
		class ActivateTreesReceiver : MessageReceiverComponent
		{
			public ActivateTreesReceiver(
				ActivateTreesAchievementBanner _parent,
				ActivateTreesAchievementEntity _achievement)
			{
				parent = _parent;
				achievement = _achievement;
			}

			public override void ReceiveMessage(Message message)
			{
				if (message is TreeActivatedMessage)
				{
					parent.UpdateText(achievement.GetActivatedTrees());
				}
			}

			ActivateTreesAchievementBanner parent;
			ActivateTreesAchievementEntity achievement;
		}

		public ActivateTreesAchievementBanner(AchievementEntity _achievement)
			: base("ui/challenge_activate_tree", _achievement)
		{
		}

		protected override void DoCreation(Stage _stage)
		{
			base.DoCreation(_stage);
			AddComponent(new ActivateTreesReceiver(this, (ActivateTreesAchievementEntity)achievement));
			UpdateText();
		}

		internal void UpdateText(int _numTrees = 0)
		{
			SetText(_numTrees.ToString() + "/" + ((ActivateTreesAchievementEntity)achievement).GetTargetTrees());
		}

	}

	public class DestroyBurgersAchievementBanner : AchievementBanner
	{
		class DestroyBurgersReceiver : MessageReceiverComponent
		{
			public DestroyBurgersReceiver(
				DestroyBurgersAchievementBanner _parent,
				DestroyBurgersAchievementEntity _achievement)
			{
				parent = _parent;
				achievement = _achievement;
			}

			public override void ReceiveMessage(Message message)
			{
				if (message is EnemyKilledMessage)
				{
					parent.UpdateText();
				}
			}

			DestroyBurgersAchievementBanner parent;
			DestroyBurgersAchievementEntity achievement;
		}

		public DestroyBurgersAchievementBanner(AchievementEntity _achievement)
			: base("ui/challenge_burgers", _achievement)
		{
		}

		protected override void DoCreation(Stage _stage)
		{
			base.DoCreation(_stage);
			AddComponent(new DestroyBurgersReceiver(this, (DestroyBurgersAchievementEntity)achievement));
			UpdateText();
		}

		internal void UpdateText()
		{
			SetText(((DestroyBurgersAchievementEntity)achievement).GetDestroyedBurgers().ToString() + "/" +
					((DestroyBurgersAchievementEntity)achievement).GetTotalBurgers());
		}

	}

	public class UseLessThanAmmoAchievementBanner : AchievementBanner
	{
		class UseLessThanAmmoReceiver : MessageReceiverComponent
		{
			public UseLessThanAmmoReceiver(
				UseLessThanAmmoAchievementBanner _parent,
				UseLessThanAmmoAchievementEntity _achievement)
			{
				parent = _parent;
				achievement = _achievement;
			}

			public override void ReceiveMessage(Message message)
			{
				if (message is PlayerShotMessage shotMessage)
				{
					parent.UpdateText();
				}
			}

			UseLessThanAmmoAchievementBanner parent;
			UseLessThanAmmoAchievementEntity achievement;
		}

		public UseLessThanAmmoAchievementBanner(string _image, AchievementEntity _achievement)
			: base(_image, _achievement)
		{
		}

		protected override void DoCreation(Stage _stage)
		{
			base.DoCreation(_stage);
			AddComponent(new UseLessThanAmmoReceiver(this, (UseLessThanAmmoAchievementEntity)achievement));
			UpdateText();
		}

		internal void UpdateText()
		{
			SetText(((UseLessThanAmmoAchievementEntity)achievement).GetUsedAmmo().ToString());
		}
	}

	public class UseLessThanCarrotsAchievementBanner : UseLessThanAmmoAchievementBanner
	{
		public UseLessThanCarrotsAchievementBanner(AchievementEntity _achievement)
			: base("ui/challenge_carrot", _achievement)
		{
		}
	}
}
