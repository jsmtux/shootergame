using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace shooter
{
	public class StageDescriptionMessageEntity : PreloadedSBaseEntity<StageDescriptionMessageEntity>
	{
		static StageDescriptionMessageEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("ui/text_box"),
				new SpriteFontDefinition("fonts/btntext")
			});
		}

		protected override void DoCreation(Stage _stage)
		{
			LocationComponent iconLocation = new UILocationComponent(new Vector2(0, 140), UILocationComponent.AnchorPoint.BottomCenter);

			SpriteComponent bgSprite = new SpriteComponent(iconLocation, getPreloadedResource<Texture2D>("ui/text_box"), Depth.Type.UI,
																		new RenderConfiguration(RenderConfiguration.TranslationType.UI));
			AddComponent(bgSprite);

			label = new TextComponent(new ChildPhysicsComponent(iconLocation, Vector2.Zero), getPreloadedResource<SpriteFont>("fonts/btntext"), "",
										  Depth.Type.UI,
										  new RenderConfiguration(RenderConfiguration.TranslationType.UI));
			label.tint = new TweenWrapper<Color>(Colors.TextHighlightColor);
			AddComponent(label);

			AddComponent(opacityTween);
			bgSprite.Alpha = label.Alpha = new TweenWrapper<float>(opacityTween);
            opacityTween.SetDefault(0);

			showTimer = new CountDownTimerComponent();
			AddComponent(showTimer);
		}

        public void ShowText(String _text, Action _onEnd)
        {
			opacityTween.onEnd = null;
            opacityTween.Start(0.0f, 1.0f, TimeSpan.FromMilliseconds(500), ScaleFuncs.CubicEaseIn);
			label.Text = _text;
			showTimer.onEnd = () => {
				opacityTween.onEnd = _onEnd;
				opacityTween.Start(1.0f, 0.0f, TimeSpan.FromMilliseconds(500), ScaleFuncs.CubicEaseIn);
			};
			showTimer.Start(TimeSpan.FromSeconds(3));
        }

		FloatTween opacityTween = new FloatTween();
		TextComponent label;
		CountDownTimerComponent showTimer;
	}
}
