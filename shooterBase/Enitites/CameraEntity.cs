﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace shooter
{
	public class CameraEntity : SBaseEntity
    {
        public class CameraEntityReceiver : MessageReceiverComponent
        {
            public CameraEntityReceiver(FollowShipCameraLogicComponent _logicComponent)
            {
                logicComponent = _logicComponent;
            }
            public override void ReceiveMessage(Message message)
            {
                if (message is EnemyKilledMessage)
                {
                    logicComponent.Shake(TimeSpan.FromSeconds(0.5));
                }
				if (message is ScreenShakeMessage screenShakeMessage)
				{
					logicComponent.Shake(screenShakeMessage.time);
				}
            }

            FollowShipCameraLogicComponent logicComponent;
        }

        public void initialize(Platform _platform, Vector2 _initialPos, Dictionary<string, Rectangle> _cameraZones = null)
		{
			platform = _platform;
			initialPos = _initialPos;
			cameraZones = _cameraZones;
			location = new SimplePhysicsComponent(new Vector2(initialPos.X, initialPos.Y), 0);
			camera = new CameraComponent(location, platform);
			steeringPhysicsComponent = new SimplePhysicsComponent(new Vector2(initialPos.X, initialPos.Y), 0);
			
			followShipLogicComponent = new FollowShipCameraLogicComponent(location, steeringPhysicsComponent, camera, platform);
			setAreaLogicComponent = new SetAreaCameraLogicComponent(location, camera, platform);
		}

		protected override void DoCreation(Stage _stage)
		{
			AddComponent(location);
			AddComponent(camera);
			AddComponent(steeringPhysicsComponent);
            AddComponent(followShipLogicComponent);
            AddComponent(setAreaLogicComponent);
            AddComponent(new CameraEntityReceiver(followShipLogicComponent));
		}

		public void SetFollowLocation(LocationComponent _pos)
		{
			followShipLogicComponent.SetShipLocation(_pos);
			followShipLogicComponent.Active = true;
		}

		public void ResetToFocusArea(Rectangle _area)
		{
			setAreaLogicComponent.GoToZone(_area);
		}

		public void ResetToFocusArea(string _areaName)
		{
			ResetToFocusArea(cameraZones[_areaName]);
		}

		public void SetCurrentFocusArea(Rectangle _area, TimeSpan _time, ScaleFunc _scaleFunc, Action _onAreaSet = null)
		{
			followShipLogicComponent.Active = false;
			setAreaLogicComponent.GoTowardsZone(_area, _time, _scaleFunc, _onAreaSet);
		}

		public void SetCurrentFocusArea(string _areaName, TimeSpan _time, ScaleFunc _scaleFunc, Action _onAreaSet = null)
		{
			SetCurrentFocusArea(cameraZones[_areaName], _time, _scaleFunc, _onAreaSet);			
		}

		public void SetFollowShip(float _zoom = 0.0f)
		{
			followShipLogicComponent.Active = true;
			followShipLogicComponent.Zoom = _zoom > 0 ? _zoom : FollowShipCameraLogicComponent.DefaultZoom;
			setAreaLogicComponent.Reset();
		}

		public UILocationComponent TransformToUILocation(Vector2 _position)
		{
			return new UILocationComponent(camera.WorldToScreen(_position));
		}

		public LocationComponent TransformLocationToUI(Vector2 _position)
		{
			return new SimplePhysicsComponent(camera.ScreenToWorld(_position));
		}

		public LocationComponent Location { get { return location; } }

		public CameraComponent CameraComponent { get => camera; }

		Platform platform;
        Vector2 initialPos;
		SimplePhysicsComponent location;
		CameraComponent camera;
		SimplePhysicsComponent steeringPhysicsComponent;
		FollowShipCameraLogicComponent followShipLogicComponent;
		SetAreaCameraLogicComponent setAreaLogicComponent;
		Dictionary<string, Rectangle> cameraZones;
	}
}
