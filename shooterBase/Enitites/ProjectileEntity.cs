﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using ECS;
using Microsoft.Xna.Framework.Audio;

namespace shooter
{
	public abstract class ProjectileEntity : PreloadedSBaseEntity<ProjectileEntity>
	{
		static ProjectileEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("particles/star")
			});
		}

		protected void initialize(Vector2 _position, float _rotation, Entity _parent, float _projectileSpeed, Color _lightColor, float _range = 0)
		{
			position = _position;
			rotation = _rotation;
			parent = _parent;
			projectileSpeed = _projectileSpeed;
			range = _range;
			lightColor = _lightColor;
		}

		public abstract void initialize(Vector2 _position, float _rotation, Entity _parent);

		protected void createCommon(Stage _stage)
		{
			location = new SimplePhysicsComponent(position, rotation);
			AddComponent(location);
			AddComponent(new ProjectileLogicComponent(location, projectileSpeed, range));
			body = new RoundPhysicsBodyComponent(25f, location, onCollision);
			AddComponent(body);
			
			var pcLight = new LightEmitterComponent(location, lightColor, 0.025f, 2.0f);
			AddComponent(pcLight);
		}

		public virtual bool onCollision(SBaseEntity _e, Vector2 _collisionPos)
		{
			if (_e != getShooter() && !(_e is PickUpItemEntity) && !(_e is TreeEntity) && !(_e is PortalEntity))
			{
				Destroy(DestructionReason.Crash);
			}

			return true;
		}

		public virtual void Destroy(DestructionReason _reason)
		{
			RemoveComponent<SpriteComponent>();
			RemoveComponent<RoundPhysicsBodyComponent>();
			RemoveComponent<ProjectileLogicComponent>();
			location.velocity = Vector2.Zero;
			emitterComponent.emitAmount(2, delete);
		}

		public Entity getShooter()
		{
			return parent;
		}

		public virtual bool ProducesPlayerDestruction()
		{
			return false;
		}

		public LocationComponent Location {get { return location; } }

		Vector2 position;
		protected SimplePhysicsComponent location;
		float rotation;
		Entity parent;
		float projectileSpeed;
		float range = 0;

		protected ParticleEmitterComponent emitterComponent;
		protected RoundPhysicsBodyComponent body;
		Color lightColor;
		public enum DestructionReason {OutOfRange, Crash};
	}

	public class PlayerProjectileEntity : ProjectileEntity
	{
		static PlayerProjectileEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("ship/carrot_projectile"),
				new SoundEffectDefinition("audio/carrot_impact_1"),
				new SoundEffectDefinition("audio/carrot_impact_2")
			});
		}

		public override void initialize(Vector2 _position, float _rotation, Entity _parent)
		{
			base.initialize(_position, _rotation, _parent, 600.0f, Color.Yellow, 500);
		}

		protected override void DoCreation(Stage _stage)
		{
			base.createCommon(_stage);

			emitterComponent = new ParticleEmitterComponent(getPreloadedResource<Texture2D>("particles/star"), location, Depth.Type.Top)
			{
				angleVariance = 2 * (float)Math.PI,
				cadence = TimeSpan.FromMilliseconds(100),
				particlesOnEmission = 10,
				lifeTime = TimeSpan.FromMilliseconds(170),
				lifeTimeVariance = TimeSpan.Zero,
				rotationSpeed = 0.1f,
				speed = 3,
				beginTint = Color.Orange,
				endTint = Color.Yellow * 0.1f,
				beginSize = 5,
				endSize = 60
			};
			AddComponent(emitterComponent);

			onCrashSound = new SoundEffectComponent(new List<SoundEffect>{
				getPreloadedResource<SoundEffect>("audio/carrot_impact_1"),
				getPreloadedResource<SoundEffect>("audio/carrot_impact_2")
			});
			AddComponent(onCrashSound);

			AddComponent(new SpriteComponent(location, getPreloadedResource<Texture2D>("ship/carrot_projectile"), Depth.Type.Action2,
											 new RenderConfiguration(RenderConfiguration.TranslationType.Camera, RenderConfiguration.BlendType.Additive)));
		}

		public override void Destroy(DestructionReason _reason)
		{
			base.Destroy(_reason);
			if (_reason == DestructionReason.Crash)
				onCrashSound.Play();
		}

		public override bool ProducesPlayerDestruction()
		{
			return true;
		}

		SoundEffectComponent onCrashSound;
	}

	public class PlayerSeedEntity : ProjectileEntity
	{
		internal class Logic : LogicComponent
		{
			public override void UpdateLogic(GameTime _elapsedTime)
			{
				var location = ((PlayerSeedEntity)entity).location;
				var curRotation = location.ModelRotation;
				location.setModelRotation(curRotation + new Vector3(0, 0, 0.1f));
			}

			float alpha = 0;
		}

		static PlayerSeedEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("ship/seed_projectile")
			});
		}

		public override void initialize(Vector2 _position, float _rotation, Entity _parent)
		{
			base.initialize(_position, _rotation, _parent, 600.0f, Color.Blue);
		}

		protected override void DoCreation(Stage _stage)
		{
			base.createCommon(_stage);

			emitterComponent = new ParticleEmitterComponent(getPreloadedResource<Texture2D>("particles/star"), location, Depth.Type.Top)
			{
				angleVariance = 2 * (float)Math.PI,
				cadence = TimeSpan.FromMilliseconds(50),
				particlesOnEmission = 10,
				lifeTime = TimeSpan.FromMilliseconds(300),
				lifeTimeVariance = TimeSpan.FromMilliseconds(70),
				rotationSpeed = 0.1f,
				speed = 3,
				beginTint = new Color(50, 215, 215),
				endTint = new Color(50, 215, 215) * 0.1f,
				beginSize = 15,
				endSize = 60
			};
			AddComponent(emitterComponent);

			AddComponent(new SpriteComponent(location, getPreloadedResource<Texture2D>("ship/seed_projectile"), Depth.Type.Action2,
											 new RenderConfiguration(RenderConfiguration.TranslationType.Camera, RenderConfiguration.BlendType.Additive)));
		}
	}

	public class LouseProjectileEntity : ProjectileEntity
	{
		static LouseProjectileEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("louse/projectile")
			});
		}

		public override void initialize(Vector2 _position, float _rotation, Entity _parent)
		{
			base.initialize(_position, _rotation, _parent, 400.0f, Color.Yellow);
		}

		protected override void DoCreation(Stage _stage)
		{
			base.createCommon(_stage);


			emitterComponent = new ParticleEmitterComponent(getPreloadedResource<Texture2D>("particles/star"), location, Depth.Type.Top)
			{
				angleVariance = 2 * (float)Math.PI,
				cadence = TimeSpan.FromMilliseconds(400),
				particlesOnEmission = 10,
				lifeTime = TimeSpan.FromMilliseconds(170),
				lifeTimeVariance = TimeSpan.Zero,
				rotationSpeed = 0.1f,
				speed = 3,
				beginTint = Color.Orange,
				endTint = Color.Yellow * 0.1f,
				beginSize = 5,
				endSize = 30
			};
			AddComponent(emitterComponent);

			AddComponent(new SpriteComponent(location, getPreloadedResource<Texture2D>("louse/projectile"), Depth.Type.Action2,
											 new RenderConfiguration(RenderConfiguration.TranslationType.Camera, RenderConfiguration.BlendType.Additive)));
			AddComponent(new LouseProjectileLogicComponent(location));
		}
	}
}
