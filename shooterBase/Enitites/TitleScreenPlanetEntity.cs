using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace shooter
{
	public class TitleScreenPlanetEntity : PreloadedSBaseEntity<TitleScreenPlanetEntity>
	{
        class LocalLogicComponent : LogicComponent
        {
            public LocalLogicComponent(UILocationComponent _location)
            {
                location = _location;
            }
            public override void UpdateLogic(GameTime _elapsedTime)
            {
                location.modelRotation = new TweenWrapper<Vector3>(location.ModelRotation + new Vector3(0.001f, 0.0f, 0.0f));
            }

            UILocationComponent location;
        }
		static TitleScreenPlanetEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("planets/Earth_2k"),
				new TextureDefinition("planets/Earth_2k_dead"),
				new ModelDefinition("planets/Earth")
			});
		}

		public TitleScreenPlanetEntity() {}

		public virtual void initialize()
		{
		}

		protected override void DoCreation(Stage _stage)
		{
			var baseLocation = new UILocationComponent(new Vector2(0, 800), UILocationComponent.AnchorPoint.Center);
			location = new ChildPhysicsComponent(baseLocation, Vector2.Zero);
			location.OffsetTween = new TweenWrapper<Vector2>(earthOffsetTween);
			AddComponent(earthOffsetTween);

			AddComponent(location);
			var earthModel = new ModelComponent(location, 400f, getPreloadedResource<Model>("planets/Earth"),
							getPreloadedResource<Texture2D>("planets/Earth_2k"), Depth.Type.Action2,
							new RenderConfiguration(RenderConfiguration.TranslationType.UI));
			AddComponent(earthModel);
			earthModel.Alpha = new TweenWrapper<float>(earthAlphaTween);
			earthAlphaTween.SetDefault(1.0f);
			AddComponent(earthAlphaTween);

			var earthDeadModel = new ModelComponent(location, 400f, getPreloadedResource<Model>("planets/Earth"),
							getPreloadedResource<Texture2D>("planets/Earth_2k_dead"), Depth.Type.Action1,
							new RenderConfiguration(RenderConfiguration.TranslationType.UI));
			AddComponent(earthDeadModel);
            AddComponent(new LocalLogicComponent(baseLocation));
		}

		public void KillPlanet(Action _onEnd)
		{
			onEnd = _onEnd;
			ShakePlanet();
			earthAlphaTween.onEnd = StopShakingPlanet;
			earthAlphaTween.Start(1.0f, 0.0f, TimeSpan.FromSeconds(3.0f), ScaleFuncs.CubicEaseOut);
		}

		void ShakePlanet()
		{
			earthOffsetTween.onEnd = () => {
				earthOffsetTween.onEnd = ShakePlanet;
				earthOffsetTween.Start(location.Offset, new Vector2(0, 10), TimeSpan.FromMilliseconds(30), ScaleFuncs.Linear);
			};
			earthOffsetTween.Start(location.Offset, new Vector2(0, -10), TimeSpan.FromMilliseconds(30), ScaleFuncs.Linear);
		}

		void StopShakingPlanet()
		{
			earthOffsetTween.onEnd = null;
			earthOffsetTween.Start(location.Offset, new Vector2(0, 0), TimeSpan.FromMilliseconds(70), ScaleFuncs.Linear);
			onEnd?.Invoke();
		}
		
		ModelComponent earthModel;
		ModelComponent venusModel;
		FloatTween earthAlphaTween = new FloatTween();
		Vector2Tween earthOffsetTween = new Vector2Tween();
		ChildPhysicsComponent location;
		Action onEnd;
	}
}
