using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace shooter
{
	public class CollisionEffectEntity : PreloadedSBaseEntity<CollisionEffectEntity>
	{
		static CollisionEffectEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("particles/star")
			});
		}

		public CollisionEffectEntity() {}

		public virtual void Initialize(Vector2 _position)
		{
			position = _position;
		}

		protected override void DoCreation(Stage _stage)
		{
			SimplePhysicsComponent location = new SimplePhysicsComponent(position);
			AddComponent(location);

            var emitterComponent = new ParticleEmitterComponent(
                getPreloadedResource<Texture2D>("particles/star"),
                location,
                Depth.Type.Top,
                Vector2.Zero,
                new RenderConfiguration(RenderConfiguration.TranslationType.Camera, RenderConfiguration.BlendType.Additive))
			{
				angleVariance = 2 * (float)Math.PI,
				cadence = TimeSpan.FromMilliseconds(100),
				particlesOnEmission = 10,
				lifeTime = TimeSpan.FromMilliseconds(700),
				lifeTimeVariance = TimeSpan.FromMilliseconds(200),
				speed = 3.5f,
				beginTint = Color.Red,
				endTint = Color.Red * 0.1f,
				beginSize = 5,
				endSize = 80
			};
			AddComponent(emitterComponent);

			SimplePhysicsComponent lightLocation = new SimplePhysicsComponent(position);
			var light = new LightEmitterComponent(lightLocation, Color.Red, 0.025f, 0.0f);
			AddComponent(light);

            var intensityTween = new FloatTween();
            AddComponent(intensityTween);

            light.intensity = new TweenWrapper<float>(intensityTween);

            emitterComponent.emitAmount(4);
            intensityTween.Start(18.0f, 0.0f, TimeSpan.FromMilliseconds(800), ScaleFuncs.Linear);
		}

		Vector2 position;
	}

}
