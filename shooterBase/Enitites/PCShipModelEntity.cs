﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace shooter
{
	public class PCShipModelEntity : PreloadedSBaseEntity<PCShipModelEntity>
	{
		static PCShipModelEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new ModelDefinition("ship/model"),
				new TextureDefinition("particles/pixel")
			});
		}

		public PCShipModelEntity()
		{
		}

		public void Initialize(Vector2 _initialPosition)
		{
			initialPosition = _initialPosition;
		}

		protected override void DoCreation(Stage _stage)
		{
			alphaTween = new FloatTween();
			alphaTween.SetDefault(0);
			AddComponent(alphaTween);

			location = new SimplePhysicsComponent(initialPosition);
			var showAngle = new Vector3((float)Math.PI * 0.8f, (float)Math.PI * 0.2f, (float)Math.PI * 0.2f);
			location.setModelRotation(showAngle);

			var shipModel = new ModelComponent(location, 50.0f, getPreloadedResource<Model>("ship/model"),
											   getPreloadedResource<Texture2D>("particles/pixel"), Depth.Type.UI3);
			shipModel.Alpha = new TweenWrapper<float>(alphaTween);

			AddComponent(shipModel);

			var logic = new FloatingModelLogicComponent(location);
			AddComponent(logic);
		}

		internal void Show()
		{
			alphaTween.Start(0.0f, 1.0f, TimeSpan.FromSeconds(0.5f), ScaleFuncs.CubicEaseIn);
		}

		internal void Hide()
		{
			alphaTween.Start(1.0f, 0.0f, TimeSpan.FromSeconds(0.5f), ScaleFuncs.CubicEaseIn);
		}

		FloatTween alphaTween;
		SimplePhysicsComponent location;
		Vector2 initialPosition;
	}
}
