﻿using System;
using System.Diagnostics;
using ECS;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace shooter
{
	public abstract class SBaseEntity : Entity
	{
		public SBaseEntity()
		{ }

		public void Create(Stage _stage)
		{
			DoCreation(_stage);
			SystemObserver.NotifySystems(this);
			foreach (var entity in subEntities)
			{
				entity.Create(_stage);
			}
		}

		protected abstract void DoCreation(Stage _stage);

		protected void AddSubEntity(SBaseEntity _entity)
		{
			subEntities.Add(_entity);
		}

		// TODO Add subEntities so that this doesn't need to be virtual
		public virtual void delete()
		{
			foreach (var entity in subEntities)
			{
				entity.delete();
			}
			ShouldBeDeleted = true;
		}

		public bool ShouldBeDeleted { get; set; } = false;

		List<SBaseEntity> subEntities = new List<SBaseEntity>();
	}


	public class ResourceDefinition
	{
		public ResourceDefinition(string _path, Type _type)
		{
			path = _path;
			type = _type;
		}
		public string path;
		public Type type;
	}

	public class TextureDefinition : ResourceDefinition
	{
		public TextureDefinition(string _path)
			: base(_path, typeof(Texture2D))
		{ }
	}

	public class ModelDefinition : ResourceDefinition
	{
		public ModelDefinition(string _path)
			: base(_path, typeof(Model))
		{ }
	}

	public class SpriteFontDefinition : ResourceDefinition
	{
		public SpriteFontDefinition(string _path)
			: base(_path, typeof(SpriteFont))
		{ }
	}

	public class SoundEffectDefinition : ResourceDefinition
	{
		public SoundEffectDefinition(string _path)
					: base(_path, typeof(SoundEffect))
		{ }
	}

	public class EffectDefinition : ResourceDefinition
	{
		public EffectDefinition(string _path)
					: base(_path, typeof(Effect))
		{ }
	}

	public interface IPreloadedSBaseEntity
	{
		Dictionary<string, Tuple<Type, Object>> getPreloadedResources();
		List<Type> getPreloadedSubEntities();
	}

	public abstract class PreloadedSBaseEntity<T> : SBaseEntity, IPreloadedSBaseEntity
	{
		public void AddToPreload(ResourceDefinition _resource)
		{
			if (!preloadedResources.ContainsKey(_resource.path))
			{
				Console.WriteLine("Warning: resource " + _resource.path + " already set for preload");
			}

			preloadedResources[_resource.path] = new Tuple<Type, Object>(_resource.type, null);
		}

		protected P getPreloadedResource<P>(string _name) where P : class
		{
			P ret = null;
			if (preloadedResources.ContainsKey(_name))
			{
				ret = preloadedResources[_name].Item2 as P;
				if (ret == null)
				{
					Console.WriteLine("Resource has been loaded with incorrect type");
				}
			}
			else
			{
				Console.WriteLine("Resource has not been loaded");
			}
			return ret;
		}

		protected static void AddElementsToPreload(List<ResourceDefinition> _resources)
		{
			foreach (var resource in _resources)
			{
				preloadedResources.Add(resource.path, new Tuple<Type, object>(resource.type, null));
			}
		}

		protected static void AddSubEntitiesToPreload(List<Type> _preloadedSubEntities)
		{
			preloadedSubEntities = _preloadedSubEntities;
		}

		public Dictionary<string, Tuple<Type, Object>> getPreloadedResources()
		{
			return preloadedResources;
		}

		public List<Type> getPreloadedSubEntities()
		{
			return preloadedSubEntities;
		}

		static private Dictionary<string, Tuple<Type, Object>> preloadedResources = new Dictionary<string, Tuple<Type, object>>();
		static private List<Type> preloadedSubEntities = new List<Type>();
	}
}
