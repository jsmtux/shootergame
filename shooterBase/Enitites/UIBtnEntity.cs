﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace shooter
{
	public class UIBtnEntityBase : PreloadedSBaseEntity<UIBtnEntityBase>
	{
		static UIBtnEntityBase()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new SoundEffectDefinition("audio/sci-fi_beep_computer_ui_03"),
				new SpriteFontDefinition("fonts/btntext")
			});
		}

		public void Initialize(
			Action _onTouchAction,
			LocationComponent _location,
			Keys _key = Keys.None,
			Buttons _button = Buttons.BigButton,
			string _btnTexture = "",
			string _btnGlowTexture = "")
		{
			onTouch = _onTouchAction;
			location = _location;
			btnKey = _key;
			gamepadBtn = _button;
			btnTexture = _btnTexture;
			btnGlowTexture = _btnGlowTexture;
		}

		protected override void DoCreation(Stage _stage)
		{
			messagingSystem = _stage.getGameService<IMessagingSystem>();

			masterAlpha = new TweenWrapper<float>(opacityTween);
			AddComponent(opacityTween);

			if (btnTexture != "")
			{
				btn = new SpriteComponent(location, getPreloadedResource<Texture2D>(btnTexture), Depth.Type.UI,
										new RenderConfiguration(
											RenderConfiguration.TranslationType.UI,
											RenderConfiguration.BlendType.Alpha));
				AddComponent(btn);
				btn.Alpha = masterAlpha;
			}
			tintTween = new ColorTween();
			AddComponent(tintTween);
			labelTintTween = new ColorTween();
			labelTintTween.SetDefault(Colors.TextHighlightColor);
			AddComponent(labelTintTween);

			if (btnGlowTexture != "")
			{
				var btnGlow = new SpriteComponent(location, getPreloadedResource<Texture2D>(btnGlowTexture), Depth.Type.UI2,
									  new RenderConfiguration(RenderConfiguration.TranslationType.UI, RenderConfiguration.BlendType.Additive));
				AddComponent(btnGlow);
				btnGlow.Alpha = new TweenWrapper<float>(glowAlpha);
				glowAlpha.SetDefault(0.0f);
				AddComponent(glowAlpha);
			}

			clickSoundComponent = new SoundEffectComponent(getPreloadedResource<SoundEffect>("audio/sci-fi_beep_computer_ui_03"));
			AddComponent(clickSoundComponent);

			touchHandlerComponent = new TouchHandlerComponent(handleNewActiveTouch, stopHandlingActiveTouch, location);
			AddComponent(touchHandlerComponent);
			touchHandlerComponent.SetActive(false);

			btnLogicComponent = new DesktopBtnEntityLogicComponent(btnKey);
			AddComponent(btnLogicComponent);

			joystickLogicCompnent = new JoystickBtnLogicComponent(PlayerIndex.One, gamepadBtn);
			AddComponent(joystickLogicCompnent);
			btnLogicComponent.onChange = joystickLogicCompnent.onChange =
				(bool status) => {
					if (status)
					{
						DoClick();
					}
				};
		}

		public bool handleNewActiveTouch(ActiveTouch _activeTouch)
		{
			DoClick();
			return true;
		}

		public void stopHandlingActiveTouch()
		{
		}

		internal void DoClick()
		{
			if (masterAlpha >= 0.99f)
			{
				clickSoundComponent.Play();
				if (btn != null)
				{
					btn.tint = new TweenWrapper<Color>(tintTween);
				}
				tintTween.onEnd = () =>
				{
					onTouch?.Invoke();
					tintTween.onEnd = null;
					tintTween.Start(new Color(170, 170, 255), new Color(255, 255, 255), TimeSpan.FromSeconds(0.2f), ScaleFuncs.CubicEaseIn);
				};
				tintTween.Start(new Color(255, 255, 255), new Color(170, 170, 255), TimeSpan.FromSeconds(0.2f), ScaleFuncs.CubicEaseOut);
				labelTintTween.onEnd = () =>
				{
					labelTintTween.onEnd = null;
					labelTintTween.Start(new Color(170, 170, 255), Colors.TextHighlightColor, TimeSpan.FromSeconds(0.2f), ScaleFuncs.CubicEaseIn);
				};
				labelTintTween.Start(Colors.TextHighlightColor, new Color(170, 170, 255), TimeSpan.FromSeconds(0.2f), ScaleFuncs.CubicEaseOut);
			}
		}

		public void Glow()
		{
			if (opacityTween.CurrentValue < 0.1f)
			{
				return;
			}
			glowAlpha.onEnd = () =>
			{
				glowAlpha.onEnd = null;
				glowAlpha.Start(1.0f, 0.0f, TimeSpan.FromSeconds(1), ScaleFuncs.CubicEaseIn);
			};
			glowAlpha.Start(0.0f, 1.0f, TimeSpan.FromSeconds(1), ScaleFuncs.CubicEaseOut);
		}

		public void Show()
		{
			opacityTween.onEnd = () =>
			{
				touchHandlerComponent.SetActive(true);
				onShown?.Invoke();
			};
			opacityTween.Start(opacityTween.CurrentValue, active? 1.0f : 0.5f, TimeSpan.FromSeconds(0.4f), ScaleFuncs.CubicEaseIn);
		}

		public void Hide()
		{
			opacityTween.onEnd = () =>
			{
				touchHandlerComponent.SetActive(false);
				onHidden?.Invoke();
				opacityTween.onEnd = null;
			};
			opacityTween.Start(active ? 1.0f : 0.5f, 0.0f, TimeSpan.FromSeconds(0.4f), ScaleFuncs.CubicEaseIn);
		}

		Action onTouch;
		Keys btnKey;
		Buttons gamepadBtn;

		FloatTween opacityTween = new FloatTween();
		FloatTween glowAlpha = new FloatTween();
		ColorTween tintTween;
		protected ColorTween labelTintTween;
		TouchHandlerComponent touchHandlerComponent;
		SoundEffectComponent clickSoundComponent;

		IMessagingSystem messagingSystem;

		DesktopBtnEntityLogicComponent btnLogicComponent;
		JoystickBtnLogicComponent joystickLogicCompnent;

		protected SpriteComponent btn;
		protected LocationComponent location;

		protected TweenWrapper<float> masterAlpha;

		string btnTexture;
		string btnGlowTexture;

		public Action onShown;
		public Action onHidden;
		protected bool active = true;
	}

	public class UITextBtnEntity : UIBtnEntityBase
	{
		static UITextBtnEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("ui/startbtn")
			});
		}

		public void Initialize(string _text, Action _onTouchAction, UILocationComponent _location, Keys _key = Keys.None, Buttons _button = Buttons.BigButton)
		{
			base.Initialize(_onTouchAction, _location, _key, _button, "ui/startbtn");
			text = _text;
		}

		protected override void DoCreation(Stage _stage)
		{
			base.DoCreation(_stage);
			var textLocation = new ChildPhysicsComponent(location, Vector2.Zero);
			label = new TextComponent(textLocation, getPreloadedResource<SpriteFont>("fonts/btntext"), text,
										  Depth.Type.UI,
										  new RenderConfiguration(
											  RenderConfiguration.TranslationType.UI));
			label.tint = new TweenWrapper<Color>(labelTintTween);
			label.Alpha = masterAlpha;
			AddComponent(label);
		}

		public void SetText(string _newText)
		{
			label.Text = _newText;
		}

		string text;
		TextComponent label;
	}

	

	public class UITextEmptyBtnEntity : UIBtnEntityBase
	{
		static UITextEmptyBtnEntity()
		{
		}

		public void Initialize(string _text, Action _onTouchAction, LocationComponent _location, Keys _key = Keys.None, Buttons _button = Buttons.BigButton)
		{
			base.Initialize(_onTouchAction, _location, _key, _button, "");
			text = _text;
		}

		protected override void DoCreation(Stage _stage)
		{
			location.Size = new Vector2(235, 60);
			base.DoCreation(_stage);
			var textLocation = new ChildPhysicsComponent(location, Vector2.Zero);
			label = new TextComponent(textLocation, getPreloadedResource<SpriteFont>("fonts/btntext"), text,
										  Depth.Type.UI,
										  new RenderConfiguration(
											  RenderConfiguration.TranslationType.UI));
			label.tint = new TweenWrapper<Color>(labelTintTween);
			label.Alpha = masterAlpha;
			AddComponent(label);
		}

		public void SetText(string _newText)
		{
			label.Text = _newText;
		}

		string text;
		TextComponent label;
	}

	public class UILevelBtnEntity : UIBtnEntityBase
	{
		static UILevelBtnEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("ui/stagebtn_open"),
				new TextureDefinition("ui/stagebtn_locked"),
				new TextureDefinition("ui/stagebtn_star"),
				new TextureDefinition("ui/stagebtn_2star"),
				new TextureDefinition("ui/stagebtn_3star"),
				new TextureDefinition("ui/stagebtn_star_glow"),
				new TextureDefinition("ui/stagebtn_2star_glow"),
				new TextureDefinition("ui/stagebtn_3star_glow")
			});
		}

		public void Initialize(Action _onTouchAction, UILocationComponent _location, State _state, int _number,
			Keys _key = Keys.None, Buttons _button = Buttons.BigButton)
		{
			state = _state;
			number = _number;
			string btnBackground = "";
			string btnGlow = "";
			switch (_state)
			{
				case State.Open:
					btnBackground = "ui/stagebtn_open";
					btnGlow = "";
					break;
				case State.Locked:
					btnBackground = "ui/stagebtn_locked";
					btnGlow = "";
					break;
				case State.Star:
					btnBackground = "ui/stagebtn_star";
					btnGlow = "ui/stagebtn_star_glow";
					break;
				case State.Star2:
					btnBackground = "ui/stagebtn_2star";
					btnGlow = "ui/stagebtn_2star_glow";
					break;
				case State.Star3:
					btnBackground = "ui/stagebtn_3star";
					btnGlow = "ui/stagebtn_3star_glow";
					break;

			}
			active = state != State.Locked;
			base.Initialize(_onTouchAction, _location, _key, _button, btnBackground, btnGlow);
		}

		protected override void DoCreation(Stage _stage)
		{
			base.DoCreation(_stage);
			var textLocation = new ChildPhysicsComponent(location, new Vector2(-68, -7));
			var label = new TextComponent(textLocation, getPreloadedResource<SpriteFont>("fonts/btntext"), number.ToString(),
										  Depth.Type.UI,
										  new RenderConfiguration(RenderConfiguration.TranslationType.UI));
			label.tint = new TweenWrapper<Color>(labelTintTween);
			label.Alpha = masterAlpha;
			AddComponent(label);
		}

		public enum State
		{
			Open,
			Locked,
			Star,
			Star2,
			Star3
		};

		int number;
		State state;
	}

	public class PauseBtnEntity : UIBtnEntityBase
	{
		static PauseBtnEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("ui/pausebtn")
			});
		}

		public void Initialize(Action _onTouchAction, UILocationComponent _location, Keys _key = Keys.None, Buttons _button = Buttons.BigButton)
		{
			base.Initialize(_onTouchAction, _location, _key, _button, "ui/pausebtn");
		}
	}
}
