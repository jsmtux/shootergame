﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace shooter
{
	public class ItemDescriptionPanelEntity : PreloadedSBaseEntity<ItemDescriptionPanelEntity>
	{
		static ItemDescriptionPanelEntity()
		{
			AddSubEntitiesToPreload(new List<Type>()
			{
				typeof(FoodItemDisplayEntity)
			});

			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new SpriteFontDefinition("fonts/itemDescriptionTitle"),
				new SpriteFontDefinition("fonts/missionText"),
				new TextureDefinition("ui/projectile_description_bg")
			});
		}

		protected override void DoCreation(Stage _stage)
		{
			location = new UILocationComponent(startPosition, UILocationComponent.AnchorPoint.TopLeft);

			AddComponent(offsetTween);
			location.position = new TweenWrapper<Vector2>(offsetTween);

			var panel = new SpriteComponent(location, getPreloadedResource<Texture2D>("ui/projectile_description_bg"), Depth.Type.UI,
									  new RenderConfiguration(RenderConfiguration.TranslationType.UI));
			AddComponent(panel);

			var itemNameLocation = new ChildPhysicsComponent(new AnchoredLocationComponent(location, AnchorProxy.Anchor.TopLeft), new Vector2(370, 10));
			itemName = new TextComponent(new AnchoredLocationComponent(itemNameLocation, AnchorProxy.Anchor.BottomRight),
										  getPreloadedResource<SpriteFont>("fonts/itemDescriptionTitle"), "",
										  Depth.Type.UI,
										  new RenderConfiguration(RenderConfiguration.TranslationType.UI));
			itemName.tint = new TweenWrapper<Color>(Colors.TextHighlightColor);
			AddComponent(itemName);

			var labelLocation = new ChildPhysicsComponent(new AnchoredLocationComponent(location, AnchorProxy.Anchor.TopLeft), new Vector2(420, 95));
			itemDescription = new TextComponent(new AnchoredLocationComponent(labelLocation, AnchorProxy.Anchor.BottomRight),
										  getPreloadedResource<SpriteFont>("fonts/missionText"), "",
										  Depth.Type.UI,
										  new RenderConfiguration(RenderConfiguration.TranslationType.UI));
			itemDescription.tint = new TweenWrapper<Color>(Colors.TextHighlightColor);
			AddComponent(itemDescription);

			var foodLocation = new ChildPhysicsComponent(new AnchoredLocationComponent(location, AnchorProxy.Anchor.TopLeft), new Vector2(920, 135));
			foodLocation.ModelRotationOffset.X += (float)Math.PI * 0.3f;

			carrotFoodDisplay = new FoodItemDisplayEntity();
			carrotFoodDisplay.Initialize(AmmoType.Carrot, foodLocation);
			AddSubEntity(carrotFoodDisplay);
			carrotFoodDisplay.SetAlpha(new TweenWrapper<float>(0));
			melonFoodDisplay = new FoodItemDisplayEntity();
			melonFoodDisplay.Initialize(AmmoType.Melon, foodLocation);
			AddSubEntity(melonFoodDisplay);
			melonFoodDisplay.SetAlpha(new TweenWrapper<float>(0));

			AddComponent(hidingTimer);

			AddComponent(alphaTween);
			panel.Alpha = itemName.Alpha = itemDescription.Alpha = new TweenWrapper<float>(alphaTween);
			alphaTween.SetDefault(new TweenWrapper<float>(0));
		}

		public void Show(TimeSpan _showTime, AmmoType _type)
		{
			string title, description;
			switch (_type)
			{
				case AmmoType.Carrot:
					title = "CARROTJECTILE";
					description = "Shoot it to break small objects";
					carrotFoodDisplay.SetAlpha(itemName.Alpha);
					break;
				case AmmoType.Melon:
					title = "INVISIMELON";
					description = "Once activated, enemies won't see you";
					melonFoodDisplay.SetAlpha(itemName.Alpha);
					break;
				default:
					throw new NotSupportedException("Type " + _type.ToString() + " not supported");
			}
			itemName.Text = title;
			itemDescription.Text = description;

			alphaTween.onEnd = null;
			alphaTween.Start(0.0f, 1.0f, TimeSpan.FromSeconds(1.0f), ScaleFuncs.CubicEaseIn);
			offsetTween.Start(startPosition, showPosition, TimeSpan.FromSeconds(1.0f), ScaleFuncs.CubicEaseOut);
			hidingTimer.onEnd = Hide;
			hidingTimer.Start(_showTime);
		}

		public void Hide()
		{
			alphaTween.Start(1.0f, 0.0f, TimeSpan.FromSeconds(1.0f), ScaleFuncs.CubicEaseOut);
			offsetTween.Start(showPosition, endPosition, TimeSpan.FromSeconds(1.0f), ScaleFuncs.CubicEaseIn);
			alphaTween.onEnd = () =>
			{
				onHidden?.Invoke();
				carrotFoodDisplay.SetAlpha(new TweenWrapper<float>(0));
				melonFoodDisplay.SetAlpha(new TweenWrapper<float>(0));
			};
		}

		protected FloatTween alphaTween = new FloatTween();
		Vector2 startPosition = new Vector2(640, 0);
		Vector2 showPosition = new Vector2(640, 250);
		Vector2 endPosition = new Vector2(640, 500);
		Vector2Tween offsetTween = new Vector2Tween();
		UILocationComponent location;
		CountDownTimerComponent hidingTimer = new CountDownTimerComponent();
		TextComponent itemDescription;
		TextComponent itemName;

		FoodItemDisplayEntity carrotFoodDisplay, melonFoodDisplay;

		public Action onHidden;
	}
}
