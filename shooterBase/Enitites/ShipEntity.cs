using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace shooter
{
	public abstract class ShipEntity : PreloadedSBaseEntity<ShipEntity>
	{
		static ShipEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("particles/smoke")
			});
		}

		public void initialize(float _radius)
		{
			radius = _radius;
		}


		public void create(Stage _stage, DrawableComponent _drawable)
		{
			AddComponent(location);
			bodyComponent = new RoundPhysicsBodyComponent(radius, location, collided, true);
			AddComponent(bodyComponent);
			deadShipLogic = new DeadShipLogicComponent(_drawable, location);
            AddComponent(deadShipLogic);

			deathTimer = new CountDownTimerComponent();
            
			deathEmitter = new ParticleEmitterComponent(getPreloadedResource<Texture2D>("particles/smoke"), location, Depth.Type.Top);
			deathEmitter.angleVariance = 2* (float) Math.PI;
			deathEmitter.cadence = TimeSpan.FromMilliseconds(50);
			deathEmitter.particlesOnEmission = 3;
			deathEmitter.lifeTime = TimeSpan.FromMilliseconds(850);
			deathEmitter.lifeTimeVariance = TimeSpan.FromMilliseconds(170);
			deathEmitter.rotationSpeed = 0.1f;
			deathEmitter.rotationSpeedVariance = 0;
			deathEmitter.speed = 4;
			deathEmitter.beginTint = Color.Black * 0.6f;
			deathEmitter.endTint = Color.White * 0.0f;
			deathEmitter.beginSize = 10;
			deathEmitter.endSize = 150;
			AddComponent(deathEmitter);
			deathEmitter2 = new ParticleEmitterComponent(getPreloadedResource<Texture2D>("particles/smoke"), location, Depth.Type.Top);
			deathEmitter2.angleVariance = 2* (float) Math.PI;
			deathEmitter2.cadence = TimeSpan.FromMilliseconds(10);
			deathEmitter2.particlesOnEmission = 1;
			deathEmitter2.lifeTime = TimeSpan.FromMilliseconds(170);
			deathEmitter2.lifeTimeVariance = TimeSpan.FromMilliseconds(250);
			deathEmitter2.rotationSpeed = 0.1f;
			deathEmitter2.rotationSpeedVariance = 0;
			deathEmitter2.speed = 6;
			deathEmitter2.beginTint = Color.Red;
			deathEmitter2.endTint = Color.Red * 0.0f;
			deathEmitter2.beginSize = 10;
			deathEmitter2.endSize = 35;
			AddComponent(deathEmitter2);
		}

        public override void delete()
        {
			base.delete();
        }

        public virtual void die()
        {
			deathEmitter.setEmitting(true);
            deathEmitter2.setEmitting(true);
            location.velocity = Vector2.Zero;
			deadShipLogic.setActive(stopEmitting);

            RemoveComponent<PhysicsBodyComponent>();

			deathTimer.onEnd = delete;
			deathTimer.Start(TimeSpan.FromSeconds(30));
        }

		protected abstract bool collided(SBaseEntity _e, Vector2 _collisionPos);

		private void stopEmitting()
		{
			deathEmitter.setEmitting(false);
			deathEmitter2.setEmitting(false);
		}

		public LocationComponent getLocation()
		{
			return location;
		}

		float radius;

		protected SimplePhysicsComponent location;
		protected SteeringComponent steering;
        protected PhysicsBodyComponent bodyComponent;

        protected DeadShipLogicComponent deadShipLogic;
        private ParticleEmitterComponent deathEmitter;
        private ParticleEmitterComponent deathEmitter2;
		CountDownTimerComponent deathTimer;
	}
}
