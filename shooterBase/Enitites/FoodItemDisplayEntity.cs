﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace shooter
{
	public class FoodItemDisplayEntity : PreloadedSBaseEntity<AmmoItemEntity>
	{
		static FoodItemDisplayEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("food/CarrotTexture"),
				new ModelDefinition("food/Carrot"),
				new TextureDefinition("food/MelonTexture"),
				new ModelDefinition("food/Melon"),
				new TextureDefinition("#FFFFFF"),
				new ModelDefinition("food/Seed")
			});
		}

		public void Initialize(AmmoType _type, LocationComponent _location)
		{
			type = _type;
			location = _location;
		}

		protected override void DoCreation(Stage _stage)
		{
			string modelPath;
			string texturePath;
			switch(type)
			{
				case AmmoType.Carrot:
					modelPath = "food/Carrot";
					texturePath = "food/CarrotTexture";
					break;
				case AmmoType.Melon:
					modelPath = "food/Melon";
					texturePath = "food/MelonTexture";
					break;
				default:
					throw new NotSupportedException("Type " + type.ToString() + " not supported");
			}
			model = new ModelComponent(
				location,
				150,
				getPreloadedResource<Model>(modelPath),
				getPreloadedResource<Texture2D>(texturePath),
				Depth.Type.UI3,
				new RenderConfiguration(RenderConfiguration.TranslationType.UI));
			model.Alpha = modelAlpha;
			AddComponent(model);
			model.tint = new TweenWrapper<Color>(Color.White);
		}

		public void SetAlpha(TweenWrapper<float> _alpha)
		{
			if (model != null)
			{
				model.Alpha = _alpha;
			}
			else
			{
				modelAlpha = _alpha;
			}
		}

		AmmoType type;
		LocationComponent location;
		ModelComponent model;
		TweenWrapper<float> modelAlpha = new TweenWrapper<float>(1.0f);
	}
}
