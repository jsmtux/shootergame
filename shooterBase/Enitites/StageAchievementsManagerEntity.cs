﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace shooter
{
    public class StageAchievementsManagerEntity : SBaseEntity
    {
		class AchievementMessageReceiver : MessageReceiverComponent
		{
			public AchievementMessageReceiver(StageAchievementsManagerEntity _manager)
			{
				achievementsManager = _manager;
			}

			public override void ReceiveMessage(Message message)
            {
				foreach(var achievement in achievementsManager.activeAchievements)
				{
					if (!achievementsManager.finishedAchievements.Contains(achievement) && achievement.checkUnlockedAchievement(message))
					{
						achievementsManager.finishedAchievements.Add(achievement);
						achievementsManager.messagingSystem.SendMessage(new AchievementUnlockedMessage(achievement.Parent));
					}
				}
			}

			StageAchievementsManagerEntity achievementsManager;
		}

		internal StageAchievementsManagerEntity(IMessagingSystem _messagingSystem, List<AchievementDescription> _stageAchievements)
        {
			messagingSystem = _messagingSystem;
			achievements = _stageAchievements;
			foreach(var achievement in _stageAchievements)
			{
				activeAchievements.Add(achievement.CreateNewAchievementLogic());
			}
		}

		internal void Reset()
		{
			foreach (var achievement in activeAchievements)
			{
				achievement.Restart();
			}
		}

		protected override void DoCreation(Stage _stage)
        {
            AddComponent(new AchievementMessageReceiver(this));
			foreach (var achievement in activeAchievements)
			{
				AddSubEntity(achievement);
			}
		}

		public Dictionary<string, bool> CheckUnlockedOnStageEnd()
		{
			Dictionary<string, bool> unlockStatus = new Dictionary<string, bool>();
			foreach(var achievement in activeAchievements)
			{
				if (achievement.checkUnlockedAchievementAtStageEnd() || finishedAchievements.Contains(achievement))
				{
					messagingSystem.SendMessage(new AchievementUnlockedMessage(achievement.Parent));
					unlockStatus[achievement.Parent.title] = true;
				}
				else
				{
					unlockStatus[achievement.Parent.title] = false;
				}
			}
			return unlockStatus;
		}

		public List<AchievementEntity> ActiveAchievements {get { return activeAchievements;}}

		List<AchievementDescription> achievements = new List<AchievementDescription>();
		List<AchievementEntity> activeAchievements = new List<AchievementEntity>();
		List<AchievementEntity> finishedAchievements = new List<AchievementEntity>();
		IMessagingSystem messagingSystem;
	}
}
