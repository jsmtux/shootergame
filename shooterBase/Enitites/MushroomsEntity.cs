using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace shooter
{
	public class MushroomsEntity : PreloadedSBaseEntity<MushroomsEntity>
	{
		static MushroomsEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("decorations/mushrooms_tex"),
				new ModelDefinition("decorations/mushrooms")
			});
		}

		public MushroomsEntity() {}

		public MushroomsEntity(Vector2 _position)
		{
			position = _position;
		}

		public virtual void initialize()
		{
		}

		protected override void DoCreation(Stage _stage)
		{
			SimplePhysicsComponent location = new SimplePhysicsComponent(position);

			AddComponent(location);
			var model = new ModelComponent(
				location,
				TiledMapReader.entitySize.X / 2.0f, getPreloadedResource<Model>("decorations/mushrooms"),
			    getPreloadedResource<Texture2D>("decorations/mushrooms_tex"),
				Depth.Type.Action1,
				RenderConfiguration.Default,
				ModelComponent.RenderMode.Perspective);
			AddComponent(model);

			SimplePhysicsComponent lightLocation = new SimplePhysicsComponent(position - new Vector2(0 , 20));
			var light = new LightEmitterComponent(lightLocation, Color.Blue, 0.025f, 8.0f);
			AddComponent(light);
		}

		protected Vector2 position;
	}

}
