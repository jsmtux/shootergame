﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace shooter
{
	public class GuanacoEntity : PreloadedSBaseEntity<GuanacoEntity>
	{
		static GuanacoEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("ui/guanaco_logo"),
				new SoundEffectDefinition("audio/intro")
			});
		}

		public void initialize(Action _onEnd)
		{
			onEnd = _onEnd;
		}

		protected override void DoCreation(Stage _stage)
		{
			LocationComponent logoLocation = new UILocationComponent(Vector2.Zero, UILocationComponent.AnchorPoint.Center);

			FloatTween alphaTween = new FloatTween();
			AddComponent(alphaTween);

			CountDownTimerComponent waitTimer = new CountDownTimerComponent();
			AddComponent(waitTimer);

			SpriteComponent starSprite = new SpriteComponent(logoLocation, getPreloadedResource<Texture2D>("ui/guanaco_logo"), Depth.Type.Bg1,
																		new RenderConfiguration(RenderConfiguration.TranslationType.Camera));
			starSprite.Alpha = new TweenWrapper<float>(alphaTween);
			AddComponent(starSprite);

			SoundEffectComponent introAudio = new SoundEffectComponent(getPreloadedResource<SoundEffect>("audio/intro"));
			AddComponent(introAudio);

			alphaTween.onEnd = () =>
			{
				waitTimer.onEnd = () =>
				{
					alphaTween.onEnd = () =>
					{
						waitTimer.onEnd = onEnd;
						waitTimer.Start(TimeSpan.FromMilliseconds(200));
					};
					alphaTween.Start(1.0f, 0.0f, TimeSpan.FromSeconds(1.0f), ScaleFuncs.CubicEaseIn);
				};
				waitTimer.Start(TimeSpan.FromSeconds(1.0f));
			};
			waitTimer.onEnd = () =>
			{
				introAudio.Play();
				alphaTween.Start(0.0f, 1.0f, TimeSpan.FromSeconds(1.0f), ScaleFuncs.CubicEaseIn);
			};
			waitTimer.Start(TimeSpan.FromSeconds(0.5f));
		}

		Action onEnd;
	}
}
