﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace shooter
{
	public class StraightRootEntity : PreloadedSBaseEntity<StraightRootEntity>
	{
		static StraightRootEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("rock/Trees_1_trunk_tex"),
				new ModelDefinition("rock/root_1")
			});
		}

		public StraightRootEntity() { }

		public StraightRootEntity(Vector2 _position, float _rotation)
		{
			position = _position;
			rotation = _rotation;
		}

		public virtual void initialize()
		{
		}

		protected override void DoCreation(Stage _stage)
		{
			SimplePhysicsComponent location = new SimplePhysicsComponent(position, rotation);
			AddComponent(location);
			var model = new ModelComponent(location, 50f, getPreloadedResource<Model>("rock/root_1"),
											getPreloadedResource<Texture2D>("rock/Trees_1_trunk_tex"), Depth.Type.Action2,
											RenderConfiguration.Default,
											ModelComponent.RenderMode.Perspective);
			model.SetViewVolume(150);
			AddComponent(model);
		}

		protected Vector2 position;
		protected float rotation;
	}

	public class CornerRootEntity : PreloadedSBaseEntity<CornerRootEntity>
	{
		static CornerRootEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("rock/Trees_1_trunk_tex"),
				new ModelDefinition("rock/root_2")
			});
		}

		public CornerRootEntity() { }

		public CornerRootEntity(Vector2 _position, float _rotation)
		{
			position = _position;
			rotation = _rotation;
		}

		public virtual void initialize()
		{
		}

		protected override void DoCreation(Stage _stage)
		{
			SimplePhysicsComponent location = new SimplePhysicsComponent(position, rotation);
			AddComponent(location);
			var model = new ModelComponent(location, 50f, getPreloadedResource<Model>("rock/root_2"),
											getPreloadedResource<Texture2D>("rock/Trees_1_trunk_tex"), Depth.Type.Action2,
											RenderConfiguration.Default,
											ModelComponent.RenderMode.Perspective);
			model.SetViewVolume(150);
			AddComponent(model);
		}

		protected Vector2 position;
		protected float rotation;
	}
}
