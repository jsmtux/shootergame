﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Audio;

namespace shooter
{
	public class EnemyTarget
	{
		public void SetEntity(PCEntity _entity)
		{
			entity = _entity;
		}

		public LocationComponent GetLocation()
		{
			if (entity != null)
			{
				return entity.getLocation();
			}
			return new SimplePhysicsComponent(Vector2.Zero);
		}

		public bool IsCloacked()
		{
			if (entity != null)
			{
				return entity.IsCloacked();
			}
			return false;
		}

		PCEntity entity;
	}

	public class PCEntity : ShipEntity
	{
		static PCEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				//new TextureDefinition("scarab/ship"),
				new ModelDefinition("ship/model"),
				new ModelDefinition("ship/model_shield"),
				new TextureDefinition("ship/shield_texture"),
				new TextureDefinition("particles/pixel"),
				new TextureDefinition("particles/carrot"),
				new TextureDefinition("particles/seed"),
				new TextureDefinition("scarab/fire"),
				new SoundEffectDefinition("audio/ship_shoot_1"),
				new SoundEffectDefinition("audio/ship_shoot_2"),
				new SoundEffectDefinition("audio/ship_impact_1"),
				new SoundEffectDefinition("audio/ship_impact_2"),
				new SoundEffectDefinition("audio/ship_impact_3"),
				new SoundEffectDefinition("audio/ship_impact_4"),
				new SoundEffectDefinition("audio/ship_impact_5"),
				new SoundEffectDefinition("audio/ship_impact_6"),
				new SoundEffectDefinition("audio/ship_impact_7"),
				new SoundEffectDefinition("audio/sci-fi_power_up_09"),
				new SoundEffectDefinition("audio/ship_appear")
			});

			AddSubEntitiesToPreload(new List<Type>(){ typeof(CollisionEffectEntity) });
		}

		public void initialize(SimplePhysicsComponent _location, CameraEntity _cameraEntity, ControllerEntity _controller, Dictionary<string, Rectangle> _cameraZones)
		{
			cameraEntity = _cameraEntity;
			controller = _controller;
			base.initialize(35.0f);
			location = _location;
			cameraZones = _cameraZones;
			shipAppearSoundComponent = new SoundEffectComponent(getPreloadedResource<SoundEffect>("audio/ship_appear"));
		}

		protected override void DoCreation(Stage _stage)
		{
			AddComponent(shipAppearSoundComponent);

			cloackIntensity = new FloatTween();
			cloackIntensity.SetDefault(0.0f);
			AddComponent(cloackIntensity);

			var shipModel = new ModelComponent(
				location,
				10.0f,
				getPreloadedResource<Model>("ship/model"),
				getPreloadedResource<Texture2D>("particles/pixel"),
				Depth.Type.Action1,
				RenderConfiguration.Default,
				ModelComponent.RenderMode.Perspective);
			shipModel.Alpha = new TweenWrapper<float>(cloackIntensity);
			shipModel.Alpha.valueModifier = (float _value) => { return 1.0f - _value; };
			AddComponent(shipModel);

			shipSize = new FloatTween();
			AddComponent(shipSize);
			shipSize.SetDefault(10);
			shipModel.scale = new TweenWrapper<float>(shipSize);

			var cloackedShipModel = new ModelComponent(
				location, 10.0f, getPreloadedResource<Model>("ship/model"),
				getPreloadedResource<Texture2D>("#000000"),
				Depth.Type.Action1,
				new RenderConfiguration(RenderConfiguration.TranslationType.Camera, RenderConfiguration.BlendType.Additive),
				ModelComponent.RenderMode.Perspective);
			cloackedShipModel.Alpha = new TweenWrapper<float>(cloackIntensity);
			AddComponent(cloackedShipModel);

			base.create(_stage, shipModel);

			shieldIntensity = new FloatTween();
			AddComponent(shieldIntensity);
			var ShieldIntensityWrapper = new TweenWrapper<float>(shieldIntensity);

			var shieldLight = new LightEmitterComponent(location, Color.DarkGreen, 0.002f, 0);
			shieldLight.intensity = ShieldIntensityWrapper;
			AddComponent(shieldLight);

			messagingSystem = _stage.getGameService<IMessagingSystem>();
			shieldComponent = new ModelComponent(
				location,
				13.0f,
				getPreloadedResource<Model>("ship/model_shield"),
				getPreloadedResource<Texture2D>("ship/shield_texture"),
				Depth.Type.Action2,
				new RenderConfiguration(RenderConfiguration.TranslationType.Camera, RenderConfiguration.BlendType.Additive),
				ModelComponent.RenderMode.Perspective);
			shieldComponent.Alpha = ShieldIntensityWrapper;
			AddComponent(shieldComponent);

			smokeEmitter = createSmokeEmitter(_stage);

			var fireSpriteLocation = new SimplePhysicsComponent(Vector2.Zero);
			var fireSprite = new SpriteComponent(fireSpriteLocation, getPreloadedResource<Texture2D>("scarab/fire"), Depth.Type.Action3,
												 new RenderConfiguration(RenderConfiguration.TranslationType.Camera, RenderConfiguration.BlendType.Additive));
			AddComponent(fireSprite);

			collisionSoundComponent = new SoundEffectComponent(new List<SoundEffect>{
				getPreloadedResource<SoundEffect>("audio/ship_impact_1"),
				getPreloadedResource<SoundEffect>("audio/ship_impact_2"),
				getPreloadedResource<SoundEffect>("audio/ship_impact_3"),
				getPreloadedResource<SoundEffect>("audio/ship_impact_4"),
				getPreloadedResource<SoundEffect>("audio/ship_impact_5"),
				getPreloadedResource<SoundEffect>("audio/ship_impact_6"),
				getPreloadedResource<SoundEffect>("audio/ship_impact_7"),
			});
			AddComponent(collisionSoundComponent);

			shootSoundComponent = new SoundEffectComponent(
				new List<SoundEffect> {
					getPreloadedResource<SoundEffect>("audio/ship_shoot_1"),
					getPreloadedResource<SoundEffect>("audio/ship_shoot_2")
				});
			AddComponent(shootSoundComponent);

			var shooterComponent = new PlayerShooterLogicComponent(location,
															 _stage,
															 new List<Vector2>() { new Vector2(-26, -55), new Vector2(26, -55) },
															 fireSprite,
															 fireSpriteLocation,
															 shootSoundComponent);
			AddComponent(shooterComponent);

			collisionEffectSpawner = new SpawnerComponent<CollisionEffectEntity>(_stage);

			logicComponent = new PCLogicComponent(location,
												  shooterComponent,
												  smokeEmitter,
												  shieldIntensity,
												  controller,
												  messagingSystem,
												  collisionSoundComponent,
												  cloackIntensity,
												  collisionEffectSpawner);
			AddComponent(logicComponent);

			PCFocusAreaLogicComponent focusLogicComponent = new PCFocusAreaLogicComponent(location, messagingSystem, cameraZones);
			AddComponent(focusLogicComponent);

			trailEmitter = new SpawnerComponent<PickUpItemParticleTrailEntity>(_stage);
			shipAppearSoundComponent.Play();
		}

        private ParticleEmitterComponent createSmokeEmitter(Stage _stage)
		{
			ChildPhysicsComponent smokeLocation = new ChildPhysicsComponent(location, new Vector2(0, 30));
			var emitter = new ParticleEmitterComponent(getPreloadedResource<Texture2D>("particles/smoke"), smokeLocation, Depth.Type.Action1);
			emitter.angleVariance = 2.0f;
			emitter.cadence = TimeSpan.FromMilliseconds(40);
			emitter.lifeTime = TimeSpan.FromMilliseconds(1500);
			emitter.lifeTimeVariance = TimeSpan.FromMilliseconds(340);
			emitter.rotationSpeed = 0.1f;
			emitter.rotationSpeedVariance = 0;
			emitter.speed = 2;
			emitter.beginTint = Color.White;
			emitter.endTint = Color.White * 0.0f;
			emitter.beginSize = 10;
			emitter.endSize = 100;
			emitter.setEmitting(true);
			AddComponent(emitter);

			return emitter;
		}

		public Vector2 GetDropPosition()
		{
			float distance = 75;
			float angle = location.Rotation;
			return location.Position - new Vector2((float)Math.Sin(angle) * distance, -(float)Math.Cos(angle) * distance);
		}

		public float Health
		{
			get { return health; }
		}

		protected override bool collided(SBaseEntity _e, Vector2 _collisionPos)
		{
			bool ret = false;
			bool collided = false;
			bool shouldDie = false;

			float healthSubtraction = 0;


            if (_e is RockEntity || _e is FenceEntity || _e is ShipEntity)
			{
				ret = true;
            }
			
			if (_e is AmmoItemEntity pickUpItem)
			{
				onPickedUpFood(pickUpItem);
			}
			else if (_e is ProjectileEntity && ((ProjectileEntity)_e).getShooter() != this)
			{
				healthSubtraction = 0.05f;
				collided = true;
			}
			else if (_e is RockEntity || _e is FenceEntity)
			{
				collisionSoundComponent.Play();
				collided = true;
				float maxHealthDecrease = 0.1f;
				float velocityPercentage = 1 - (location.velocity.Length() / MaxVelocity.NominalValue());
				float healthModifier = 1 - (velocityPercentage * velocityPercentage);
				healthSubtraction = maxHealthDecrease * healthModifier;
				messagingSystem.SendMessage(new PlayerCrashedMessage(_e, _collisionPos));
				collisionEffectSpawner.spawn((CollisionEffectEntity _entity) => {_entity.Initialize(_collisionPos);});
			}
			else if (_e is BurgerEnemyEntity)
			{
				collided = true;
				healthSubtraction = 0.25f;
			}
			else if (_e is HeartItemEntity heartItemEntity)
			{
				health += 0.2f;
				health = health > 1 ? 1f : health;
				messagingSystem.SendMessage(new PlayerLifeChangedMessage(health));
				heartItemEntity.Collected();
			}
			else if (_e is StarSeedItemEntity starSeedEntity)
			{
				energy += 0.1f;
				energy = energy > 1 ? 1f : energy;
				messagingSystem.SendMessage(new PlayerEnergyChangedMessage(energy));
				if (energy < 1.0)
				{
					starSeedEntity.Collected();
				}
			}

			if (collided && shieldIntensity.CurrentValue < 0.1f)
			{
				logicComponent.SetCloacked(false);
				if (healthSubtraction > 0)
				{
					messagingSystem.SendMessage(new ScreenShakeMessage(TimeSpan.FromMilliseconds(400)));
				}
				health -= healthSubtraction;
				if (health < 0)
				{
					health = 0;
					messagingSystem.SendMessage(new PlayerKilledMessage());
					messagingSystem.SendMessage(new PlayerLifeChangedMessage(0));
					shouldDie = true;
				}
				shieldIntensity.Start(2.0f, 0.0f, TimeSpan.FromMilliseconds(500), ScaleFuncs.CubicEaseIn);
				messagingSystem.SendMessage(new PlayerLifeChangedMessage(health));
			}

			if (health < 0.2)
			{
				smokeEmitter.beginTint = Color.Red * 0.7f;
			}
			else if (health < 0.6)
			{
				smokeEmitter.beginTint = Color.Orange * 0.7f;
			}
			else if (health < 0.8)
			{
				smokeEmitter.beginTint = Color.Gray * 0.7f;
			}
			
			if(shouldDie)
			{
				die();
			}

			return ret;
		}

		public void onPickedUpFood(AmmoItemEntity _pickUpItem)
		{
			var foodType = _pickUpItem.RelatedAmmoType();

			_pickUpItem.Collected();

			trailEmitter.spawn((_particleTrail) => {
				_particleTrail.initialize(
					cameraEntity.TransformToUILocation(_pickUpItem.GetPosition()),
					controller.slot.GetLocation(),
					RenderConfiguration.TranslationType.UI,
					_pickUpItem.aColor,
					_pickUpItem.bColor);
			});

			controller.AddAmmo(1, foodType);
		}

		public void StartJump(Action _onJumped = null)
		{
			RemoveComponent<PCLogicComponent>();
			RemoveComponent<PhysicsBodyComponent>();
			smokeEmitter.setEmitting(false);
			shipSize.onEnd = _onJumped;
			shipSize.Start(10, 0, TimeSpan.FromMilliseconds(200), ScaleFuncs.CubicEaseIn);
			logicComponent.die();
		}

		public override void die()
		{
			base.die();
			shieldComponent.Alpha.SetStatic(0.0f);
			logicComponent.die();
		}

		public bool IsCloacked()
		{
			return cloackIntensity.CurrentValue > 0.1;
		}

		IMessagingSystem messagingSystem;
		PCLogicComponent logicComponent;
		ModelComponent shieldComponent;
		SoundEffectComponent shootSoundComponent;
		SoundEffectComponent collisionSoundComponent;
		SoundEffectComponent shipAppearSoundComponent;
		CameraEntity cameraEntity;
		ControllerEntity controller;
		FloatTween shieldIntensity;
		FloatTween cloackIntensity;
		FloatTween shipSize;
		ParticleEmitterComponent smokeEmitter;
		private Dictionary<string, Rectangle> cameraZones;


		readonly Velocity maxVelocity = new Velocity(500);
		readonly Velocity accelerationVelocity = new Velocity(20);
		public float health = 1.0f;
		public float energy = 0.0f;

		public float VelocityPercentage {get {return (MaxVelocity.NominalValue() - location.velocity.Length()) / MaxVelocity.NominalValue();}}
		public Velocity MaxVelocity {get {return maxVelocity;}}
		public Velocity AccelerationVelocity {get {return accelerationVelocity;}}

		SpawnerComponent<PickUpItemParticleTrailEntity> trailEmitter;
		SpawnerComponent<CollisionEffectEntity> collisionEffectSpawner;
	}
}
