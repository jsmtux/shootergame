using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace shooter
{
	public class FenceEntity : PreloadedSBaseEntity<FenceEntity>
	{
		static FenceEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("particles/smoke"),
				new TextureDefinition("particles/wood"),
				new TextureDefinition("rock/fence_tex"),
				new ModelDefinition("rock/fence"),
				new SoundEffectDefinition("audio/fence_destruction")
			});
		}

		public FenceEntity() {}

		public FenceEntity(
			Vector2 _position,
            bool _isVertical = true)
		{
			position = _position;
            isVertical = _isVertical;
		}

		public virtual void initialize()
		{
		}

		protected override void DoCreation(Stage _stage)
		{
			messagingSystem = _stage.getGameService<IMessagingSystem>();
			SimplePhysicsComponent location = new SimplePhysicsComponent(position);
            if (!isVertical)
            {
                location.setModelRotation(new Vector3(-(float)Math.PI/2.0f, 0, 0));
            }
			AddComponent(location);
			var model = new ModelComponent(location, TiledMapReader.entitySize.X / 2.0f, getPreloadedResource<Model>("rock/fence"),
			                                getPreloadedResource<Texture2D>("rock/fence_tex"), Depth.Type.Action1,
											RenderConfiguration.Default,
											ModelComponent.RenderMode.Perspective);
            model.SetViewVolume(TiledMapReader.entitySize.X * 2.0f);
            AddComponent(model);

			breakSoundComponent = new SoundEffectComponent(getPreloadedResource<SoundEffect>("audio/fence_destruction"));
			AddComponent(breakSoundComponent);

			AddComponent(new RoundPhysicsBodyComponent(FenceBaseCollisionSize, location, Collided));
			SimplePhysicsComponent colliderLocation;
            if (isVertical)
            {
			    colliderLocation = new SimplePhysicsComponent(position + new Vector2(0, TiledMapReader.entitySize.Y));
            }
            else
            {
			    colliderLocation = new SimplePhysicsComponent(position + new Vector2(TiledMapReader.entitySize.X, 0));
            }
            AddComponent(new RoundPhysicsBodyComponent(FenceBaseCollisionSize, colliderLocation, Collided));

            SimplePhysicsComponent particleEmitterLocation;
            if (isVertical)
            {
                particleEmitterLocation = new SimplePhysicsComponent(position + new Vector2(0, TiledMapReader.entitySize.Y / 2.0f));
            }
            else
            {
                particleEmitterLocation = new SimplePhysicsComponent(position + new Vector2(FenceBaseCollisionSize, 0));
            }
			breaksmokeEmitter = new ParticleEmitterComponent(getPreloadedResource<Texture2D>("particles/smoke"), particleEmitterLocation, Depth.Type.Top);
			breaksmokeEmitter.angleVariance = 2 * (float)Math.PI;
			breaksmokeEmitter.cadence = TimeSpan.FromMilliseconds(100000);
			breaksmokeEmitter.particlesOnEmission = 15;
			breaksmokeEmitter.lifeTime = TimeSpan.FromMilliseconds(800);
			breaksmokeEmitter.lifeTimeVariance = TimeSpan.Zero;
			breaksmokeEmitter.rotationSpeed = 0.1f;
			breaksmokeEmitter.rotationSpeedVariance = 0;
			breaksmokeEmitter.speed = 3;
			breaksmokeEmitter.speedVariance = 2;
			breaksmokeEmitter.beginTint = Color.SandyBrown * 0.7f;
			breaksmokeEmitter.endTint = Color.SandyBrown * 0.0f;
			breaksmokeEmitter.beginSize = 10;
			breaksmokeEmitter.endSize = 150;
			AddComponent(breaksmokeEmitter);

			breakWoodEmitter = new ParticleEmitterComponent(getPreloadedResource<Texture2D>("particles/wood"), particleEmitterLocation, Depth.Type.Top);
			breakWoodEmitter.angleVariance = 2 * (float)Math.PI;
			breakWoodEmitter.cadence = TimeSpan.FromMilliseconds(100000);
			breakWoodEmitter.particlesOnEmission = 7;
			breakWoodEmitter.lifeTime = TimeSpan.FromMilliseconds(600);
			breakWoodEmitter.lifeTimeVariance = TimeSpan.FromMilliseconds(200);
			breakWoodEmitter.rotationSpeed = 0.2f;
			breakWoodEmitter.rotationSpeedVariance = 0.4f;
			breakWoodEmitter.speed = 6;
			breakWoodEmitter.speedVariance = 2;
			breakWoodEmitter.beginTint = Color.White * 1.0f;
			breakWoodEmitter.endTint = Color.White * 0.1f;
			breakWoodEmitter.beginSize = 60;
			breakWoodEmitter.endSize = 30;
			AddComponent(breakWoodEmitter);
		}

		public bool Collided(SBaseEntity _e, Vector2 _collisionPos)
		{
			if (!broken && _e is ProjectileEntity _projectile && _projectile.ProducesPlayerDestruction())
			{
				breakSoundComponent.Play();
				Break();
			}

			return false;
		}

		public void Break()
		{
			breaksmokeEmitter.emitAmount(1);
			breakWoodEmitter.emitAmount(1);
			RemoveComponent<RoundPhysicsBodyComponent>();
			RemoveComponent<ModelComponent>();
			messagingSystem.SendMessage(new BrokenFenceMessage());
			broken = true;
		}

		ParticleEmitterComponent breaksmokeEmitter;
		ParticleEmitterComponent breakWoodEmitter;
		SoundEffectComponent breakSoundComponent;
		IMessagingSystem messagingSystem;
        bool isVertical;
		Vector2 position;
		bool broken = false;

		protected static readonly float FenceBaseCollisionSize = 0.7f * TiledMapReader.entitySize.X / 2.0f;
	}
}
