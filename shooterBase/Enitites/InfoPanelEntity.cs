﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace shooter
{
	public class InfoPanelEntity : PreloadedSBaseEntity<InfoPanelEntity>
	{
		static InfoPanelEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new SpriteFontDefinition("fonts/panelHeadingText"),
				new SpriteFontDefinition("fonts/missionHeading"),
				new SpriteFontDefinition("fonts/missionText"),
				new SpriteFontDefinition("fonts/panelDataText"),
				new TextureDefinition("ui/checkmark"),
				new TextureDefinition("ui/cross"),
				new TextureDefinition("ui/forward")
			});
		}

		public void Initialize(UILocationComponent _location)
		{
			location = _location;
		}

		public void create(Stage _stage, string _panel_name)
		{
			panel = new SpriteComponent(location, getPreloadedResource<Texture2D>(_panel_name), Depth.Type.UI,
									  new RenderConfiguration(RenderConfiguration.TranslationType.UI));
			AddComponent(panel);

			AddComponent(alphaTween);
			panel.Alpha = new TweenWrapper<float>(alphaTween);

			alphaTween.SetDefault(0);
		}

		protected override void DoCreation(Stage _stage)
		{
			throw new NotImplementedException();
		}

		protected void Show()
		{
			alphaTween.Start(0.0f, 1.0f, TimeSpan.FromSeconds(0.5f), ScaleFuncs.CubicEaseIn);
		}

		public void Hide()
		{
			alphaTween.onEnd = () => {
				onHidden();
				alphaTween.onEnd = null;
			};
			alphaTween.Start(1.0f, 0.0f, TimeSpan.FromSeconds(0.5f), ScaleFuncs.CubicEaseIn);
		}

		Vector2 position;
		protected FloatTween alphaTween = new FloatTween();
		protected SpriteComponent panel;
		protected LocationComponent location;
		public Action onHidden;
	}

	public class InfoMedPanelEntity : InfoPanelEntity
	{
		static InfoMedPanelEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("ui/med_panel")
			});
		}

		protected override void DoCreation(Stage _stage)
		{
			base.create(_stage, "ui/med_panel");
		}
	}

	public class InfoBigPanelEntity : InfoPanelEntity
	{
		static InfoBigPanelEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("ui/big_panel")
			});
		}

		protected override void DoCreation(Stage _stage)
		{
			base.create(_stage, "ui/big_panel");
		}
	}

	public class PopupPanelEntity : InfoPanelEntity
	{
		static PopupPanelEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("ui/popup")
			});
			AddSubEntitiesToPreload(new List<Type>(){
				typeof(UITextEmptyBtnEntity)
			});
		}

		protected override void DoCreation(Stage _stage)
		{
			base.create(_stage, "ui/popup");

			var okBtnLocation = new ChildPhysicsComponent(new AnchoredLocationComponent(location, AnchorProxy.Anchor.BottomRight), new Vector2(-175, -25));
			okBtn = new UITextEmptyBtnEntity();
			okBtn.Initialize("Start", () => {onOk(curStage);}, okBtnLocation);
			AddSubEntity(okBtn);

			var backBtnLocation = new ChildPhysicsComponent(new AnchoredLocationComponent(location, AnchorProxy.Anchor.BottomRight), new Vector2(-410, -25));
			cancelBtn = new UITextEmptyBtnEntity();
			cancelBtn.Initialize("Back", Hide, backBtnLocation);
			AddSubEntity(cancelBtn);

			var titleDescriptionLocation = new ChildPhysicsComponent(new AnchoredLocationComponent(location, AnchorProxy.Anchor.TopLeft), new Vector2(20, 10));
			titleDescription = new TextComponent(new AnchoredLocationComponent(titleDescriptionLocation, AnchorProxy.Anchor.BottomRight),
													 getPreloadedResource<SpriteFont>("fonts/missionHeading"), "Title",
													 Depth.Type.UI,
													 new RenderConfiguration(RenderConfiguration.TranslationType.UI));
			titleDescription.tint.SetStatic(Colors.TextHighlightColor);
			titleDescription.Alpha = panel.Alpha;
			AddComponent(titleDescription);

			for (int i = 0; i < 2; i++)
			{
				var achievementTextLocation = new ChildPhysicsComponent(new AnchoredLocationComponent(location, AnchorProxy.Anchor.TopLeft), new Vector2(65, 50 + 80 * i));
				achievementsText[i] = new TextComponent(new AnchoredLocationComponent(achievementTextLocation, AnchorProxy.Anchor.BottomRight),
													 getPreloadedResource<SpriteFont>("fonts/missionHeading"), "Achievement " + i,
													 Depth.Type.UI,
													 new RenderConfiguration(RenderConfiguration.TranslationType.UI));
				achievementsText[i].tint.SetStatic(Colors.TextHighlightColor);
				achievementsText[i].Alpha = panel.Alpha;
				AddComponent(achievementsText[i]);
				var achievementIconLocation = new ChildPhysicsComponent(new AnchoredLocationComponent(location, AnchorProxy.Anchor.TopLeft), new Vector2(25, 50 + 80 * i));
				achievementsIcon[i] = new SpriteComponent(new AnchoredLocationComponent(achievementIconLocation, AnchorProxy.Anchor.BottomRight),
													 getPreloadedResource<Texture2D>("ui/forward"),
													 Depth.Type.UI,
													 new RenderConfiguration(RenderConfiguration.TranslationType.UI));
				achievementsIcon[i].tint.SetStatic(Colors.SecondaryTextColor);
				achievementsIcon[i].Alpha = panel.Alpha;
				AddComponent(achievementsIcon[i]);

				var achievementDescriptionLocation = new ChildPhysicsComponent(new AnchoredLocationComponent(location, AnchorProxy.Anchor.TopLeft), new Vector2(85, 95 + 80 * i));
				achievementsDescription[i] = new TextComponent(new AnchoredLocationComponent(achievementDescriptionLocation, AnchorProxy.Anchor.BottomRight),
													 getPreloadedResource<SpriteFont>("fonts/missionText"), "Achievement " + i,
													 Depth.Type.UI,
													 new RenderConfiguration(RenderConfiguration.TranslationType.UI));
				achievementsDescription[i].tint.SetStatic(Colors.SecondaryTextColor);
				achievementsDescription[i].Alpha = panel.Alpha;
				AddComponent(achievementsDescription[i]);
			}
		}

		public new void Show()
		{
			base.Show();
			okBtn.Show();
			cancelBtn.Show();
		}

		public new void Hide()
		{
			base.Hide();
			okBtn.Hide();
			cancelBtn.Hide();
		}

		internal void UpdateInfo(StageDescription newStage)
		{
			curStage = newStage;
			titleDescription.Text = newStage.Name;
			for (int i = 0; i < 2; i++)
			{
				if (newStage.extraGoals.Count > i)
				{
					achievementsText[i].Text = newStage.extraGoals[i].title;
					achievementsDescription[i].Text = newStage.extraGoals[i].description;
					achievementsIcon[i].Alpha = panel.Alpha;
				}
				else
				{
					achievementsText[i].Text = "";
					achievementsDescription[i].Text = "";
					achievementsIcon[i].Alpha = new TweenWrapper<float>(0);
				}
			}
		}

		TextComponent[] achievementsText = new TextComponent[2];
		TextComponent[] achievementsDescription = new TextComponent[2];
		TextComponent titleDescription;
		SpriteComponent[] achievementsIcon = new SpriteComponent[2];

		UITextEmptyBtnEntity okBtn;
		UITextEmptyBtnEntity cancelBtn;

		StageDescription curStage;

		public Action<StageDescription> onOk;
	}

	public class ShipPropertiesInfoPanel : InfoBigPanelEntity
	{
		static ShipPropertiesInfoPanel()
		{
			AddSubEntitiesToPreload(new List<Type>(){ typeof(BarEntity) });
		}

		public ShipPropertiesInfoPanel()
		{
			ship = new PCShipModelEntity();
			ship.Initialize(new Vector2(-250f, 20f));
			AddSubEntity(ship);
		}

		public new void Show()
		{
			base.Show();
			ship.Show();
		}

		public new void Hide()
		{
			base.Hide();
			ship.Hide();
		}

		protected override void DoCreation(Stage _stage)
		{
			base.DoCreation(_stage);

			float infoGrid = 500;
			float barSize = 250;

			var handlingTitleLocation = new ChildPhysicsComponent(new AnchoredLocationComponent(location, AnchorProxy.Anchor.TopLeft), new Vector2(infoGrid, 80));
			var handlingTitle = new TextComponent(new AnchoredLocationComponent(handlingTitleLocation, AnchorProxy.Anchor.BottomRight),
													getPreloadedResource<SpriteFont>("fonts/panelHeadingText"), "Handling",
													Depth.Type.UI,
													new RenderConfiguration(RenderConfiguration.TranslationType.UI));
			handlingTitle.tint.SetStatic(Colors.SecondaryTextColor);
			handlingTitle.Alpha = panel.Alpha;
			AddComponent(handlingTitle);

			var handlingBarLocation = new ChildPhysicsComponent(new AnchoredLocationComponent(location, AnchorProxy.Anchor.TopLeft), new Vector2(infoGrid, 130));
			var handlingBar = new BarEntity();
			handlingBar.initialize(handlingBarLocation, new Color(0, 128, 92), Colors.TextHighlightColor, Colors.SecondaryTextColor, 0.3f, new Vector2(barSize, 19));
			handlingBar.Alpha = panel.Alpha;
			AddSubEntity(handlingBar);

			var speedTitleLocation = new ChildPhysicsComponent(new AnchoredLocationComponent(location, AnchorProxy.Anchor.TopLeft), new Vector2(infoGrid, 170));
			var speedTitle = new TextComponent(new AnchoredLocationComponent(speedTitleLocation, AnchorProxy.Anchor.BottomRight),
													getPreloadedResource<SpriteFont>("fonts/panelHeadingText"), "Speed",
													Depth.Type.UI,
													new RenderConfiguration(RenderConfiguration.TranslationType.UI));
			speedTitle.tint.SetStatic(Colors.SecondaryTextColor);
			speedTitle.Alpha = panel.Alpha;
			AddComponent(speedTitle);

			var speedBarLocation = new ChildPhysicsComponent(new AnchoredLocationComponent(location, AnchorProxy.Anchor.TopLeft), new Vector2(infoGrid, 220));
			var speedBar = new BarEntity();
			speedBar.initialize(speedBarLocation, new Color(255, 255, 151), Colors.TextHighlightColor, Colors.SecondaryTextColor, 0.5f, new Vector2(barSize, 19));
			speedBar.Alpha = panel.Alpha;
			AddSubEntity(speedBar);

			var powerTitleLocation = new ChildPhysicsComponent(new AnchoredLocationComponent(location, AnchorProxy.Anchor.TopLeft), new Vector2(infoGrid, 260));
			var powerTitle = new TextComponent(new AnchoredLocationComponent(powerTitleLocation, AnchorProxy.Anchor.BottomRight),
													getPreloadedResource<SpriteFont>("fonts/panelHeadingText"), "Power",
													Depth.Type.UI,
													new RenderConfiguration(RenderConfiguration.TranslationType.UI));
			powerTitle.tint.SetStatic(Colors.SecondaryTextColor);
			powerTitle.Alpha = panel.Alpha;
			AddComponent(powerTitle);

			var powerBarLocation = new ChildPhysicsComponent(new AnchoredLocationComponent(location, AnchorProxy.Anchor.TopLeft), new Vector2(infoGrid, 310));
			var powerBar = new BarEntity();
			powerBar.initialize(powerBarLocation, new Color(253, 0, 121), Colors.TextHighlightColor, Colors.SecondaryTextColor, 0.2f, new Vector2(barSize, 19));
			powerBar.Alpha = panel.Alpha;
			AddSubEntity(powerBar);
		}

		PCShipModelEntity ship;
	}

	class InfoPanelStar : PreloadedSBaseEntity<InfoPanelStar>
	{
		static InfoPanelStar()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("ui/star_bg"),
				new TextureDefinition("particles/star"),
				new TextureDefinition("ui/star_fg"),
				new SpriteFontDefinition("fonts/missionHeading"),
				new TextureDefinition("ui/checkmark"),
				new TextureDefinition("ui/cross")
			});
		}

		public void Initialize(LocationComponent _panelLocation, Vector2 _position, TimeSpan _glowDelay)
		{
			position = _position;
			panelLocation = _panelLocation;
			glowDelay = _glowDelay;
		}

		protected override void DoCreation(Stage _stage)
		{
			starAlpha.SetDefault(0);
			AddComponent(starAlpha);

			var starLocation = new ChildPhysicsComponent(new AnchoredLocationComponent(panelLocation, AnchorProxy.Anchor.TopLeft), position);
			var starComponent = new SpriteComponent(new AnchoredLocationComponent(starLocation, AnchorProxy.Anchor.BottomRight),
												getPreloadedResource<Texture2D>("ui/star_bg"), Depth.Type.UI2,
									  			new RenderConfiguration(RenderConfiguration.TranslationType.UI));
			AddComponent(starComponent);
			starComponent.Alpha = new TweenWrapper<float>(starAlpha);

			AddComponent(starGlowAlpha);

			var starGlowComponent = new SpriteComponent(new AnchoredLocationComponent(starLocation, AnchorProxy.Anchor.BottomRight),
														getPreloadedResource<Texture2D>("ui/star_fg"), Depth.Type.UI3,
											  			new RenderConfiguration(RenderConfiguration.TranslationType.UI));
			starGlowComponent.Alpha = new TweenWrapper<float>(starGlowAlpha);
			AddComponent(starGlowComponent);

			emitter = new ParticleEmitterComponent(getPreloadedResource<Texture2D>("particles/star"),
				new AnchoredLocationComponent(starLocation, AnchorProxy.Anchor.BottomRight), Depth.Type.UI2, Vector2.Zero,
				new RenderConfiguration(RenderConfiguration.TranslationType.UI))
			{
				angleVariance = 2 * (float)Math.PI,
				cadence = TimeSpan.FromMilliseconds(100000),
				particlesOnEmission = 15,
				lifeTime = TimeSpan.FromMilliseconds(1250),
				lifeTimeVariance = TimeSpan.Zero,
				rotationSpeed = 0.1f,
				rotationSpeedVariance = 0,
				speed = 4,
				beginTint = Color.White,
				endTint = Color.White * 0.0f,
				beginSize = 30,
				endSize = 70
			};
			AddComponent(emitter);
		}

		public void Show()
		{
			starAlpha.Start(0.0f, 1.0f, TimeSpan.FromSeconds(0.5f), ScaleFuncs.CubicEaseIn);
		}

		public void Glow()
		{
			starGlowAlpha.onEnd = () =>
			{
				starGlowAlpha.onEnd = () =>
				{
					onGlow?.Invoke();
					emitter.setEmitting(true);
				};
				starGlowAlpha.Start(0.0f, 1.0f, TimeSpan.FromSeconds(0.5f), ScaleFuncs.CubicEaseIn);
			};
			starGlowAlpha.Start(0.0f, 0.0f, glowDelay, ScaleFuncs.Linear);
		}

		FloatTween starAlpha = new FloatTween();
		FloatTween starGlowAlpha = new FloatTween();
		Vector2 position;
		LocationComponent panelLocation;
		ParticleEmitterComponent emitter;

		TimeSpan glowDelay;

		public Action onGlow;
	}

	public class NextStageInfoPanel : InfoBigPanelEntity
	{
		static NextStageInfoPanel()
		{
			AddSubEntitiesToPreload(new List<Type>()
			{
				typeof(InfoPanelStar)
			});
		}

		public void Show(StageDescription _stage, Dictionary<string, bool> _achievementStatus)
		{
			base.Show();
			star1.Show();
			star2.Show();
			star3.Show();

			int numActiveAchievements = 0;
			foreach(var achievementStatus in _achievementStatus)
			{
				if (achievementStatus.Value)
				{
					numActiveAchievements++;
				}
			}

			if (numActiveAchievements > 1)
			{
				star3.onGlow = onShown;
				star2.onGlow = star3.Glow;
				star1.onGlow = star2.Glow;
			}
			else if (numActiveAchievements > 0)
			{
				star2.onGlow = onShown;
				star1.onGlow = star2.Glow;
			}
			else
			{
				star1.onGlow = onShown;
			}
			star1.Glow();

			UpdateInfo(_stage, _achievementStatus);
		}

		internal void UpdateInfo(StageDescription newStage, Dictionary<string, bool> _achievementStatus)
		{
			for (int i = 0; i < 3; i++)
			{
				if (newStage.extraGoals.Count > i)
				{
					achievementsText[i].Text = newStage.extraGoals[i].title;
					bool passed = false;
					_achievementStatus.TryGetValue(newStage.extraGoals[i].title, out passed);
					if (passed)
					{
						achievementsPassedIcon[i].Alpha = panel.Alpha;
						achievementsFailedIcon[i].Alpha = new TweenWrapper<float>(0);
					}
					else
					{
						achievementsPassedIcon[i].Alpha = new TweenWrapper<float>(0);
						achievementsFailedIcon[i].Alpha = panel.Alpha;
					}
				}
				else
				{
					achievementsText[i].Text = "";
					achievementsPassedIcon[i].Alpha = new TweenWrapper<float>(0);
					achievementsFailedIcon[i].Alpha = new TweenWrapper<float>(0);
				}
			}
		}

		protected override void DoCreation(Stage _stage)
		{
			base.DoCreation(_stage);

			star1 = new InfoPanelStar();
			star1.Initialize(location, new Vector2(270, 100), TimeSpan.FromSeconds(0.15f));
			AddSubEntity(star1);

			star2 = new InfoPanelStar();
			star2.Initialize(location, new Vector2(420, 100), TimeSpan.FromSeconds(0.15f));
			AddSubEntity(star2);

			star3 = new InfoPanelStar();
			star3.Initialize(location, new Vector2(590, 100), TimeSpan.FromSeconds(0.15f));
			AddSubEntity(star3);

			for (int i = 0; i < 3; i++)
			{
				var achievementTextLocation = new ChildPhysicsComponent(new AnchoredLocationComponent(location, AnchorProxy.Anchor.TopLeft), new Vector2(65, 250 + 45 * i));
				achievementsText[i] = new TextComponent(new AnchoredLocationComponent(achievementTextLocation, AnchorProxy.Anchor.BottomRight),
													 getPreloadedResource<SpriteFont>("fonts/missionHeading"), "Achievement " + i,
													 Depth.Type.UI,
													 new RenderConfiguration(RenderConfiguration.TranslationType.UI));
				achievementsText[i].tint.SetStatic(Colors.SecondaryTextColor);
				achievementsText[i].Alpha = panel.Alpha;
				AddComponent(achievementsText[i]);

				var achievementIconLocation = new ChildPhysicsComponent(new AnchoredLocationComponent(location, AnchorProxy.Anchor.TopLeft), new Vector2(20, 240 + 45 * i));
				achievementsPassedIcon[i] = new SpriteComponent(new AnchoredLocationComponent(achievementIconLocation, AnchorProxy.Anchor.BottomRight),
													 getPreloadedResource<Texture2D>("ui/checkmark"),
													 Depth.Type.UI,
													 new RenderConfiguration(RenderConfiguration.TranslationType.UI));
				achievementsPassedIcon[i].tint.SetStatic(Colors.SecondaryTextColor);
				achievementsPassedIcon[i].Alpha = panel.Alpha;
				AddComponent(achievementsPassedIcon[i]);

				achievementsFailedIcon[i] = new SpriteComponent(new AnchoredLocationComponent(achievementIconLocation, AnchorProxy.Anchor.BottomRight),
													 getPreloadedResource<Texture2D>("ui/cross"),
													 Depth.Type.UI,
													 new RenderConfiguration(RenderConfiguration.TranslationType.UI));
				achievementsFailedIcon[i].tint.SetStatic(Colors.SecondaryTextColor);
				achievementsFailedIcon[i].Alpha = panel.Alpha;
				AddComponent(achievementsFailedIcon[i]);
			}
		}

		InfoPanelStar star1;
		InfoPanelStar star2;
		InfoPanelStar star3;

		TextComponent[] achievementsText = new TextComponent[3];
		SpriteComponent[] achievementsPassedIcon = new SpriteComponent[3];
		SpriteComponent[] achievementsFailedIcon = new SpriteComponent[3];

		public Action onShown;
	}
}
