﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace shooter
{
	public class MainMenuScreen : PreloadedSBaseEntity<MainMenuScreen>
	{
		static MainMenuScreen()
		{
			AddSubEntitiesToPreload(new List<Type>()
			{
				typeof(MenuLogoEntity),
				typeof(UITextBtnEntity),
				typeof(UIBtnEntityBase),
				typeof(BGEntity),
				typeof(SpacePizzaEntity)
			});
		}

		public void Initialize(Action _onExit, TitleScreenPlanetEntity _titlePlanet)
		{
			onExitAction = _onExit;
			startBtn = new UITextBtnEntity();
			startBtn.onShown = onShown;
			menuLogo = new MenuLogoEntity();
			menuLogo.OnShown = startBtn.Show;
			spacePizza = new SpacePizzaEntity();
			spacePizza.initialize(_titlePlanet);
			spacePizza.OnShown = menuLogo.Show;
		}

		protected override void DoCreation(Stage _stage)
		{
			AddSubEntity(spacePizza);
			AddSubEntity(menuLogo);
			startBtn.Initialize("Start", onExit, new UILocationComponent(new Vector2(0, 70), UILocationComponent.AnchorPoint.BottomCenter), Keys.Enter, Buttons.B);
			AddSubEntity(startBtn);
		}

		public void Show()
		{
			spacePizza.Show();
		}

		public void Hide()
		{
			onExit();
		}

		private void onExit()
		{
			menuLogo.OnHide = doExit;
			startBtn.Hide();
			menuLogo.Hide();
			spacePizza.Hide();
		}

		void doExit()
		{
			menuLogo.delete();
			startBtn.delete();
			onExitAction();
		}

		MenuLogoEntity menuLogo;
		SpacePizzaEntity spacePizza;
		UITextBtnEntity startBtn;
		public Action onShown;
		Action onExitAction;
	}
}
