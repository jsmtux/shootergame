﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace shooter
{
	public class GamePausedScreen
	{
		public static void Preload(StageLoader _stageLoader)
		{
			_stageLoader.PreloadEntity<InfoPanelEntity>();
			_stageLoader.PreloadEntity<InfoMedPanelEntity>();
			_stageLoader.PreloadEntity<UITextBtnEntity>();
		}

		public GamePausedScreen()
		{
		}

		public void Create(Stage _stage, Action _onContinue, Action _onRestart, Action _onExit)
		{
			titleBanner = new UITextBtnEntity();
			titleBanner.Initialize("Game Paused", null, new UILocationComponent(new Vector2(-120, 180), UILocationComponent.AnchorPoint.TopCenter));
			_stage.addEntity(titleBanner);

			continueBtn = new UITextBtnEntity();
			continueBtn.Initialize("Continue", _onContinue, new UILocationComponent(new Vector2(0, 310), UILocationComponent.AnchorPoint.TopCenter), Keys.Enter, Buttons.A);
			_stage.addEntity(continueBtn);

			restartBtn = new UITextBtnEntity();
			restartBtn.Initialize("Restart", _onRestart, new UILocationComponent(new Vector2(0, 440), UILocationComponent.AnchorPoint.TopCenter), Keys.Enter, Buttons.A);
			_stage.addEntity(restartBtn);

			mainMenuBtn = new UITextBtnEntity();
			mainMenuBtn.Initialize("Exit", _onExit, new UILocationComponent(new Vector2(0, 570), UILocationComponent.AnchorPoint.TopCenter), Keys.Enter, Buttons.B);
			_stage.addEntity(mainMenuBtn);
		}

		public void Show()
		{
			titleBanner.Show();
			restartBtn.Show();
			continueBtn.Show();
			mainMenuBtn.Show();
		}

		public void Hide()
		{
			titleBanner.Hide();
			restartBtn.Hide();
			continueBtn.Hide();
			mainMenuBtn.Hide();
		}

		UITextBtnEntity titleBanner, restartBtn, continueBtn, mainMenuBtn;
	}
}
