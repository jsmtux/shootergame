﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace shooter
{
	public class GameOverScreen
	{
		public static void Preload(StageLoader _stageLoader)
		{
			_stageLoader.PreloadEntity<InfoPanelEntity>();
			_stageLoader.PreloadEntity<InfoMedPanelEntity>();
			_stageLoader.PreloadEntity<UITextBtnEntity>();
		}

		public GameOverScreen()
		{
		}

		public void Create(Stage _stage, Action _onRestart, Action _onExit)
		{
			titleBanner = new UITextBtnEntity();
			titleBanner.Initialize("Game Over", null, new UILocationComponent(new Vector2(-120, 180), UILocationComponent.AnchorPoint.TopCenter));
			_stage.addEntity(titleBanner);

			restartBtn = new UITextBtnEntity();
			restartBtn.Initialize("Restart", _onRestart, new UILocationComponent(new Vector2(0, 310), UILocationComponent.AnchorPoint.TopCenter), Keys.Enter, Buttons.A);
			_stage.addEntity(restartBtn);

			mainMenuBtn = new UITextBtnEntity();
			mainMenuBtn.Initialize("Exit", _onExit, new UILocationComponent(new Vector2(0, 440), UILocationComponent.AnchorPoint.TopCenter), Keys.Enter, Buttons.B);
			_stage.addEntity(mainMenuBtn);
		}

		public void Show()
		{
			titleBanner.Show();
			restartBtn.Show();
			mainMenuBtn.Show();
		}

		public void Hide()
		{
			titleBanner.Hide();
			restartBtn.Hide();
			mainMenuBtn.Hide();
		}

		UITextBtnEntity titleBanner;
		UITextBtnEntity restartBtn;
		UITextBtnEntity mainMenuBtn;
	}
}
