﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace shooter
{
	public class StageFinishedScreen : PreloadedSBaseEntity<StageFinishedScreen>
	{
		static StageFinishedScreen()
		{
			AddSubEntitiesToPreload(new List<Type>() {
				typeof(InfoPanelEntity),
				typeof(InfoBigPanelEntity),
				typeof(NextStageInfoPanel),
				typeof(UITextBtnEntity)
			});
		}

		public StageFinishedScreen()
		{
		}

		public void Initialize(Action _onHidden)
		{
			onHidden = _onHidden;
		}

		protected override void DoCreation(Stage _stage)
		{
			titleBanner = new UITextBtnEntity();
			titleBanner.Initialize("Stage finished", null, new UILocationComponent(new Vector2(0, 80), UILocationComponent.AnchorPoint.TopCenter));
			AddSubEntity(titleBanner);

			restartBtn = new UITextBtnEntity();
			restartBtn.Initialize("Next", onHidden, new UILocationComponent(new Vector2(0, 70), UILocationComponent.AnchorPoint.BottomCenter), Keys.Enter, Buttons.B);
			AddSubEntity(restartBtn);

			infoPanel = new NextStageInfoPanel();
			infoPanel.Initialize(new UILocationComponent(new Vector2(0, 360), UILocationComponent.AnchorPoint.TopCenter));
			AddSubEntity(infoPanel);
		}

		public void Show(StageDescription _stage, Dictionary<string, bool> _achievementStatus)
		{
			titleBanner.Show();
			infoPanel.onShown = restartBtn.Show;
			infoPanel.Show(_stage, _achievementStatus);
		}

		public void Hide()
		{
		}

		UITextBtnEntity titleBanner;
		UITextBtnEntity restartBtn;
		NextStageInfoPanel infoPanel;
		Action onHidden;
	}
}
