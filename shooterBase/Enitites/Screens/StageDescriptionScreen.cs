﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace shooter
{
	class StageDescriptionScreen : PreloadedSBaseEntity<StageDescriptionScreen>
	{
		static StageDescriptionScreen()
		{
			AddSubEntitiesToPreload(new List<Type>()
			{
				typeof(PopupPanelEntity),
				typeof(UITextBtnEntity)
			});
		}

		public void Initialize(Action<StageDescription> _onHidden, Action _onBack)
		{
			onHidden = _onHidden;
			onBack = _onBack;
		}

		protected override void DoCreation(Stage _stage)
		{
			stagePanel = new PopupPanelEntity();
			stagePanel.onHidden = onBack;
			stagePanel.onOk = onHidden;
			stagePanel.Initialize(new UILocationComponent(new Vector2(0, 360), UILocationComponent.AnchorPoint.TopCenter));
			_stage.addEntity(stagePanel);
		}

		public void UpdateInfo(StageDescription _newStage)
		{
			stagePanel.UpdateInfo(_newStage);
			stage = _newStage;
		}

		public void Show()
		{
			stagePanel.Show();
		}

		void Hide()
		{
			stagePanel.Hide();
			onHidden(stage);
		}

		PopupPanelEntity stagePanel;

		StageDescription stage;

		Action<StageDescription> onHidden;
		Action onBack;
	}
}