﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace shooter
{
	public class StageSelectionScreen
	{
		public static void Preload(StageLoader _stageLoader)
		{
			_stageLoader.PreloadEntity<UILevelBtnEntity>();
		}

		public StageSelectionScreen()
		{
		}

		public void Create(Stage _stage, Action<StageDescription> _onHidden)
		{
			titleBanner = new UITextBtnEntity();
			titleBanner.Initialize("Select stage", null, new UILocationComponent(new Vector2(0, 80), UILocationComponent.AnchorPoint.TopCenter));
			_stage.addEntity(titleBanner);

			var savedData = _stage.getGameService<SaveDataManager>().Data;

			bool stageOpened = false;

			foreach (var level in LoadStage.levelList)
			{
				if (savedData.GetStageState(level) == UILevelBtnEntity.State.Open)
				{
					stageOpened = true;
				}
				else if (!stageOpened && savedData.GetStageState(level) == UILevelBtnEntity.State.Locked)
				{
					savedData.AddOpenedStage(level);
					stageOpened = true;
				}
				AddStage(_stage, level, savedData.GetStageState(level), savedData.ShouldNotifyStageUpdate(level));
			}

			onHidden = _onHidden;
		}

		public void Show()
		{
			titleBanner.Show();
			foreach (var stageBtn in stageBtns)
			{
				stageBtn.Show();
			}
		}

		void Hide(StageDescription _stage)
		{
			titleBanner.Hide();
			foreach (var stageBtn in stageBtns)
			{
				stageBtn.Hide();
			}
			onHidden(_stage);
		}

		void AddStage(Stage _stage, StageDescription _stageDescription, UILevelBtnEntity.State _state, bool _notifyUpdate)
		{
			int x = stageNumber % buttonsInRow;
			int y = stageNumber / buttonsInRow;
			int yOffset = -80;
			int xOffset = -290;
			int xDistanceBetweenBtns = 290;
			UILevelBtnEntity stageBtn = new UILevelBtnEntity();
			stageBtn.Initialize(
				() => Hide(_stageDescription),
				new UILocationComponent(new Vector2(xOffset + xDistanceBetweenBtns * x, yOffset + y * 120), UILocationComponent.AnchorPoint.Center),
				_state,
				stageNumber + 1);
			stageBtns.Add(stageBtn);
			_stage.addEntity(stageBtn);
			if (_notifyUpdate)
			{
				stageBtn.Glow();
			}
			stageNumber++;
		}

		UITextBtnEntity titleBanner;
		int stageNumber = 0;
		List<UILevelBtnEntity> stageBtns = new List<UILevelBtnEntity>();
		Action<StageDescription> onHidden;
		int buttonsInRow = 3;
	}
}
