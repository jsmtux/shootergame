﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace shooter
{
	public class ShipSelectionScreen
	{
		public static void Preload(StageLoader _stageLoader)
		{
			_stageLoader.PreloadEntity<PCShipModelEntity>();
			_stageLoader.PreloadEntity<InfoBigPanelEntity>();
			_stageLoader.PreloadEntity<ShipPropertiesInfoPanel>();
		}

		public ShipSelectionScreen()
		{
		}

		public void Create(Stage _stage, Action _onHidden)
		{
			titleBanner = new UITextBtnEntity();
			titleBanner.Initialize("Ship description", null, new UILocationComponent(new Vector2(0, 80), UILocationComponent.AnchorPoint.TopCenter));
			_stage.addEntity(titleBanner);

			continueBtn = new UITextBtnEntity();
			continueBtn.Initialize("Next", Hide, new UILocationComponent(new Vector2(0, 70), UILocationComponent.AnchorPoint.BottomCenter), Keys.Enter, Buttons.B);
			_stage.addEntity(continueBtn);

			shipPropertiesInfo = new ShipPropertiesInfoPanel();
			shipPropertiesInfo.Initialize(new UILocationComponent(new Vector2(0, 360), UILocationComponent.AnchorPoint.TopCenter));
			_stage.addEntity(shipPropertiesInfo);
			shipPropertiesInfo.onHidden = _onHidden;
		}

		public void Show()
		{
			titleBanner.Show();
			continueBtn.Show();
			shipPropertiesInfo.Show();
		}

		void Hide()
		{
			titleBanner.Hide();
			continueBtn.Hide();
			shipPropertiesInfo.Hide();
		}

		UITextBtnEntity titleBanner;
		UITextBtnEntity continueBtn;
		ShipPropertiesInfoPanel shipPropertiesInfo;
	}
}
