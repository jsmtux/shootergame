using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace shooter
{
	public class SpacePizzaEntity : PreloadedSBaseEntity<SpacePizzaEntity>
	{
        class LocalLogicComponent : LogicComponent
        {
            public LocalLogicComponent(UILocationComponent _location)
            {
                location = _location;
            }
            public override void UpdateLogic(GameTime _elapsedTime)
            {
                //location.setModelRotation(location.ModelRotation + new Vector3(0.001f, 0.0f, 0.0f));
                //location.setModelRotation(new Vector3((float)Math.PI, 0.0f, 0.0f));
            }

            UILocationComponent location;
        }
		static SpacePizzaEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("particles/pizza_laser"),
				new TextureDefinition("PizzaEnemy/space_pizza_tex"),
				new ModelDefinition("PizzaEnemy/space_pizza")
			});
		}

		public SpacePizzaEntity() {}

		public virtual void initialize(TitleScreenPlanetEntity _titlePlanet)
		{
			titlePlanet = _titlePlanet;
		}

		protected override void DoCreation(Stage _stage)
		{
			var location = new UILocationComponent(new Vector2(0, -60), UILocationComponent.AnchorPoint.Center);
			AddComponent(location);
			var pizzaModel = new ModelComponent(location, 0f, getPreloadedResource<Model>("PizzaEnemy/space_pizza"),
			                                getPreloadedResource<Texture2D>("PizzaEnemy/space_pizza_tex"), Depth.Type.Action1,
											new RenderConfiguration(RenderConfiguration.TranslationType.UI));
			AddComponent(pizzaModel);
			pizzaModel.Alpha = new TweenWrapper<float>(alphaTween);
			pizzaModel.scale = new TweenWrapper<float>(scaleTween);
			location.position = new TweenWrapper<Vector2>(positionTween);
			location.modelRotation = new TweenWrapper<Vector3>(rotationTween);
            AddComponent(new LocalLogicComponent(location));
			AddComponent(alphaTween);
			alphaTween.SetDefault(1.0f);
			AddComponent(scaleTween);
			AddComponent(positionTween);
			AddComponent(rotationTween);

			/*AnimationSequences pizzaAnimation = new AnimationSequences();
            pizzaAnimation["stop"] = new AnimationSequence(TimeSpan.FromSeconds(80), TimeSpan.FromSeconds(80));
            pizzaAnimation["idle"] = new AnimationSequence(TimeSpan.Zero, TimeSpan.FromSeconds(7.25));
            pizzaAnimation["prepare_shoot"] = new AnimationSequence(TimeSpan.FromSeconds(8.0), TimeSpan.FromSeconds(12.5));
            pizzaAnimation["shooting"] = new AnimationSequence(TimeSpan.FromSeconds(12.5), TimeSpan.FromSeconds(13.5));
			animator = new AnimatorComponent(pizzaModel, pizzaAnimation);
			AddComponent(animator);
			animator.Play("stop");*/

			var laserLocation = new ChildPhysicsComponent(location, new Vector2(20, 220));
			laser = new SpriteComponent(laserLocation, getPreloadedResource<Texture2D>("particles/pizza_laser"), Depth.Type.Bg3,
											new RenderConfiguration(RenderConfiguration.TranslationType.UI));
			AddComponent(laser);
			laser.Alpha = new TweenWrapper<float>(laserAlphaTween);
			laserAlphaTween.SetDefault(0);
			AddComponent(laserAlphaTween);
			var laserLight = new LightEmitterComponent(laserLocation, Color.Red, 0.025f, 0);
			laserLight.intensity = new TweenWrapper<float>(laserLightTween);
		}

		public void Show()
		{
			scaleTween.onEnd = () => {
				ShootPlanet();
			};
			scaleTween.Start(0.0f, 10.0f, TimeSpan.FromSeconds(3.0f), ScaleFuncs.CubicEaseOut);
			positionTween.Start(new Vector2(300, -320), endPos, TimeSpan.FromSeconds(3.0f), ScaleFuncs.ElasticEaseOut);
			rotationTween.Start(new Vector3(0,-(float)Math.PI,(float)Math.PI), new Vector3(0,-(float)Math.PI * 1.4f,(float)Math.PI), TimeSpan.FromSeconds(3.0f), ScaleFuncs.CubicEaseOut);
		}

		void ShootPlanet()
		{
			//animator.Play("shooting",true);
			laserAlphaTween.onEnd = () => {laserAlphaTween.Start(0.5f, 1.0f, TimeSpan.FromSeconds(2.0f), ScaleFuncs.ElasticEaseInOut);};
			laserAlphaTween.Start(0.5f, 1.0f, TimeSpan.FromSeconds(2.0f), ScaleFuncs.ElasticEaseInOut);
			laserLightTween.onEnd = () => {laserAlphaTween.Start(0.5f, 500.0f, TimeSpan.FromSeconds(2.0f), ScaleFuncs.ElasticEaseInOut);};
			laserLightTween.Start(0.5f, 500.0f, TimeSpan.FromSeconds(2.0f), ScaleFuncs.ElasticEaseInOut);
			titlePlanet.KillPlanet(() => {
				laserAlphaTween.onEnd = null;
				laserLightTween.onEnd = null;
				laserAlphaTween.Start(0.0f, 0.0f, TimeSpan.FromSeconds(0.1f), ScaleFuncs.ElasticEaseInOut);
				laserLightTween.Start(0.0f, 0.0f, TimeSpan.FromSeconds(0.1f), ScaleFuncs.ElasticEaseInOut);
				StartFloating();
				
				//animator.Play("idle");
				OnShown?.Invoke();
			});
		}

		void StartFloating()
		{
			positionTween.onEnd = () => {
				positionTween.onEnd = StartFloating;
				positionTween.Start(topPos, endPos, TimeSpan.FromSeconds(2.0f), ScaleFuncs.SineEaseInOut);
			};
			positionTween.Start(endPos, topPos, TimeSpan.FromSeconds(2.0f), ScaleFuncs.SineEaseInOut);
		}

		public void Hide()
		{
			alphaTween.Start(1.0f, 0.0f, TimeSpan.FromSeconds(0.4f), ScaleFuncs.CubicEaseOut);
		}

		public Action OnShown;
		FloatTween alphaTween = new FloatTween();
		FloatTween scaleTween = new FloatTween();
		Vector2Tween positionTween = new Vector2Tween();
		Vector3Tween rotationTween = new Vector3Tween();
		FloatingModelLogicComponent floatingLogic;
		readonly Vector2 endPos = new Vector2(0, -20);
		readonly Vector2 topPos = new Vector2(0, -120);

		SpriteComponent laser;
		FloatTween laserAlphaTween = new FloatTween();
		FloatTween laserLightTween = new FloatTween();

		TitleScreenPlanetEntity titlePlanet;
		//AnimatorComponent animator;
	}
}
