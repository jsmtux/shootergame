using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace shooter
{
	public abstract class ParticleTrailEntity : PreloadedSBaseEntity<ParticleTrailEntity>
	{
		static ParticleTrailEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("particles/star")
			});
		}

        public void initialize(
			LocationComponent _startPosition,
			LocationComponent _endPosition,
			RenderConfiguration.TranslationType _translationType = RenderConfiguration.TranslationType.Camera)
		{
			location = new SimplePhysicsComponent(_startPosition.Position);
			startPosition = _startPosition;
			endPosition = _endPosition;
			translationType = _translationType;
		}

		protected override void DoCreation(Stage _stage)
		{
			AddComponent(location);
			positionTween.SetDefault(startPosition.Position);
			location.PositionTween = new TweenWrapper<Vector2>(positionTween);
			AddComponent(positionTween);
			positionTween.onEnd = delete;
		}

		protected virtual void Start(TimeSpan _time, ScaleFunc scaleFunc)
		{
			positionTween.onEnd = () => {
				onEnd?.Invoke();
				delete();
			};
			positionTween.Start(startPosition.Position, endPosition.Position, _time, scaleFunc);
		}

		internal void Die()
		{
			delete();
		}

        protected SimplePhysicsComponent location;
		Vector2Tween positionTween = new Vector2Tween();
		LocationComponent startPosition;
		LocationComponent endPosition;

		protected RenderConfiguration.TranslationType translationType;
		public Action onEnd;
    }

    class PickUpItemParticleTrailEntity : ParticleTrailEntity
    {

        public void initialize(
			LocationComponent _startPosition,
			LocationComponent _endPosition,
			RenderConfiguration.TranslationType _translationType,
			Color _aColor,
			Color _bColor)
		{
			base.initialize(_startPosition, _endPosition, _translationType);
			aColor = _aColor;
			bColor = _bColor;
		}

        protected override void DoCreation(Stage _stage)
        {
			base.DoCreation(_stage);

            var emitterComponent =
				new ParticleEmitterComponent(getPreloadedResource<Texture2D>("particles/star"),
					location,
					Depth.Type.UI3,
					Vector2.Zero,
					new RenderConfiguration(translationType));
			emitterComponent.angleVariance = 2 * (float)Math.PI;
			emitterComponent.cadence = TimeSpan.FromMilliseconds(4);
			emitterComponent.particlesOnEmission = 1;
			emitterComponent.lifeTime = TimeSpan.FromMilliseconds(40);
			emitterComponent.lifeTimeVariance = TimeSpan.Zero;
			emitterComponent.rotationSpeed = 0.1f;
			emitterComponent.rotationSpeedVariance = 0;
			emitterComponent.speed = 2;
			emitterComponent.beginTint = aColor;
			emitterComponent.endTint = aColor;
			emitterComponent.beginSize = 40;
			emitterComponent.endSize = 80;
			emitterComponent.setEmitting(true);
			AddComponent(emitterComponent);

            var emitterComponent2 =
				new ParticleEmitterComponent(getPreloadedResource<Texture2D>("particles/star"),
					location,
					Depth.Type.UI3,
					Vector2.Zero,
					new RenderConfiguration(translationType));
			emitterComponent2.angleVariance = 2 * (float)Math.PI;
			emitterComponent2.cadence = TimeSpan.FromMilliseconds(4);
			emitterComponent2.particlesOnEmission = 1;
			emitterComponent2.lifeTime = TimeSpan.FromMilliseconds(40);
			emitterComponent2.lifeTimeVariance = TimeSpan.Zero;
			emitterComponent2.rotationSpeed = 0.1f;
			emitterComponent2.rotationSpeedVariance = 0;
			emitterComponent2.speed = 2;
			emitterComponent2.beginTint = bColor;
			emitterComponent2.endTint = bColor;
			emitterComponent2.beginSize = 40;
			emitterComponent2.endSize = 80;
			emitterComponent2.setEmitting(true);
			AddComponent(emitterComponent2);

			Start(TimeSpan.FromSeconds(1.5f), ScaleFuncs.ElasticEaseOut);
        }

		Color aColor, bColor;
    }

    class BananaParticleTrailEntity : ParticleTrailEntity
    {
		static BananaParticleTrailEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("particles/banana")
			});
		}
        protected override void DoCreation(Stage _stage)
        {
			base.DoCreation(_stage);

            var emitterComponent =
				new ParticleEmitterComponent(getPreloadedResource<Texture2D>("particles/banana"),
					location,
					Depth.Type.UI2,
					Vector2.Zero,
					new RenderConfiguration(translationType));
			emitterComponent.angleVariance = 2 * (float)Math.PI;
			emitterComponent.cadence = TimeSpan.FromMilliseconds(20);
			emitterComponent.particlesOnEmission = 1;
			emitterComponent.lifeTime = TimeSpan.FromMilliseconds(400);
			emitterComponent.lifeTimeVariance = TimeSpan.Zero;
			emitterComponent.rotationSpeed = 0.5f;
			emitterComponent.rotationSpeedVariance = 0;
			emitterComponent.speed = 1;
			emitterComponent.beginTint = Color.White;
			emitterComponent.endTint = Color.White * 0.2f;
			emitterComponent.beginSize = 5;
			emitterComponent.endSize = 40;
			emitterComponent.setEmitting(true);
			AddComponent(emitterComponent);

			var light = new LightEmitterComponent(location, Color.Yellow, 0.025f, 6.0f);
			AddComponent(light);

			Start(TimeSpan.FromSeconds(1.5f), ScaleFuncs.CubicEaseIn);
        }
    }

    class BananaAndParticleTrailEntity : BananaParticleTrailEntity
    {
		static BananaAndParticleTrailEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("food/BananaTexture"),
				new ModelDefinition("food/Banana")
			});
		}

		public BananaAndParticleTrailEntity() {}
        protected override void DoCreation(Stage _stage)
        {
			bananaModel = new ModelComponent(
				location,
				30f,
				getPreloadedResource<Model>("food/Banana"),
				getPreloadedResource<Texture2D>("food/BananaTexture"),
				Depth.Type.UI2);
			AddComponent(bananaModel);
			location.setModelRotation(new Vector3((float)Math.PI / 4, 0, (float)Math.PI / 4));

			AddComponent(bananaScaleTween);
			bananaModel.scale = new TweenWrapper<float>(bananaScaleTween);

			base.DoCreation(_stage);
        }

		protected override void Start(TimeSpan _time, ScaleFunc scaleFunc)
		{
			bananaScaleTween.onEnd = () => base.Start(_time, scaleFunc);
			bananaScaleTween.Start(30.0f, 200.0f, TimeSpan.FromSeconds(0.5f), ScaleFuncs.CubicEaseOut);
		}

		ModelComponent bananaModel;
		FloatTween bananaScaleTween = new FloatTween();
    }
}