﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace shooter
{
	public class IntroIconEntity : PreloadedSBaseEntity<IntroIconEntity>
	{
		static IntroIconEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("ui/loading_icon")
			});
		}

		protected override void DoCreation(Stage _stage)
		{
			LocationComponent iconLocation = new UILocationComponent(new Vector2(70, 70), UILocationComponent.AnchorPoint.BottomRight);

			alphaTween.SetDefault(0);
			AddComponent(alphaTween);

			SpriteComponent starSprite = new SpriteComponent(iconLocation, getPreloadedResource<Texture2D>("ui/loading_icon"), Depth.Type.Bg1,
																		new RenderConfiguration(RenderConfiguration.TranslationType.Camera));
			starSprite.Alpha = new TweenWrapper<float>(alphaTween);
			AddComponent(starSprite);
		}

		public void startTween()
		{
			alphaTween.onEnd = () =>
			{
				alphaTween.onEnd = startTween;
				alphaTween.Start(1, 0.1f, TimeSpan.FromSeconds(0.5f), ScaleFuncs.CubicEaseOut);
			};
			alphaTween.Start(0.1f, 1f, TimeSpan.FromSeconds(0.5f), ScaleFuncs.CubicEaseOut);
		}

		Point resolution;
		FloatTween alphaTween = new FloatTween();
	}
}
