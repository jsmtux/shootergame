﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace shooter
{
	public class RockEntity : PreloadedSBaseEntity<RockEntity>
	{
		static RockEntity()
		{
			AddElementsToPreload(new List<ResourceDefinition>()
			{
				new TextureDefinition("rock/square_1_tex"),
				new ModelDefinition("rock/square_1"),
				new TextureDefinition("rock/square_2_tex"),
				new ModelDefinition("rock/square_2"),
				new TextureDefinition("rock/three_squares_1_tex"),
				new ModelDefinition("rock/three_squares_1"),
				new TextureDefinition("rock/three_squares_2_tex"),
				new ModelDefinition("rock/three_squares_2"),
				new TextureDefinition("rock/three_squares_long_tex"),
				new ModelDefinition("rock/three_squares_long"),
				new TextureDefinition("rock/two_squares_tex"),
				new ModelDefinition("rock/two_squares"),
				new TextureDefinition("rock/big_square_tex"),
				new ModelDefinition("rock/big_square")
			});
		}

		public RockEntity() {}

		protected RockEntity(
			Vector2 _position,
			string _baseName,
			float _viewVolume = ModelComponent.DefaultViewVolume,
			float _zRotation = 0)
		{
			position = _position;
			baseName = _baseName;
			viewVolume = _viewVolume;
			zRotation = _zRotation;
		}

		public virtual void initialize()
		{
		}

		protected override void DoCreation(Stage _stage)
		{
			SimplePhysicsComponent location = new SimplePhysicsComponent(position);
			location.setModelRotation(new Vector3(zRotation, 0, 0));

			AddComponent(location);
			var model = new ModelComponent(
				location,
				TiledMapReader.entitySize.X / 2.0f, getPreloadedResource<Model>("rock/" + baseName),
			    getPreloadedResource<Texture2D>("rock/" + baseName + "_tex"),
				Depth.Type.Action1,
				RenderConfiguration.Default,
				ModelComponent.RenderMode.Perspective);
			model.SetViewVolume(viewVolume);
			AddComponent(model);
			AddComponent(new RoundPhysicsBodyComponent(RockBaseCollisionSize, location));
		}

		protected Vector2 position;
		string baseName;
		float viewVolume;
		float zRotation;
		protected static readonly float RockBaseCollisionSize = 0.8f * TiledMapReader.entitySize.X / 2.0f;
	}

	public class RockSquare1Entity : RockEntity
	{
		public RockSquare1Entity(Vector2 _position)
			: base (_position, "square_1")
		{}
	}

	public class RockSquare2Entity : RockEntity
	{
		public RockSquare2Entity(Vector2 _position)
			: base (_position, "square_2")
		{}
	}

	public class RockThreeSquares1Entity : RockEntity
	{
		public RockThreeSquares1Entity(Vector2 _position)
			: base (_position, "three_squares_1", 150)
		{}

		protected override void DoCreation(Stage _stage)
		{
			base.DoCreation(_stage);
			SimplePhysicsComponent location = new SimplePhysicsComponent(position + new Vector2(TiledMapReader.entitySize.X, 0));
			AddComponent(new RoundPhysicsBodyComponent(RockBaseCollisionSize, location));
			SimplePhysicsComponent location2 = new SimplePhysicsComponent(position - new Vector2(0, -TiledMapReader.entitySize.Y));
			AddComponent(new RoundPhysicsBodyComponent(RockBaseCollisionSize, location2));
		}
	}

	public class RockThreeSquares1InvertedEntity : RockEntity
	{
		public RockThreeSquares1InvertedEntity(Vector2 _position)
			: base (_position, "three_squares_1", 150, (float)Math.PI/2.0f)
		{}

		protected override void DoCreation(Stage _stage)
		{
			base.DoCreation(_stage);
			SimplePhysicsComponent location = new SimplePhysicsComponent(position - new Vector2(TiledMapReader.entitySize.X, 0));
			AddComponent(new RoundPhysicsBodyComponent(RockBaseCollisionSize, location));
			SimplePhysicsComponent location2 = new SimplePhysicsComponent(position + new Vector2(0, TiledMapReader.entitySize.Y));
			AddComponent(new RoundPhysicsBodyComponent(RockBaseCollisionSize, location2));
		}
	}

	public class RockThreeSquares2Entity : RockEntity
	{
		public RockThreeSquares2Entity(Vector2 _position)
			: base (_position, "three_squares_1", 150, (float)Math.PI)
		{}

		protected override void DoCreation(Stage _stage)
		{
			base.DoCreation(_stage);
			SimplePhysicsComponent location = new SimplePhysicsComponent(position + new Vector2(-TiledMapReader.entitySize.X, 0));
			AddComponent(new RoundPhysicsBodyComponent(RockBaseCollisionSize, location));
			SimplePhysicsComponent location2 = new SimplePhysicsComponent(position - new Vector2(0, TiledMapReader.entitySize.Y));
			AddComponent(new RoundPhysicsBodyComponent(RockBaseCollisionSize, location2));
		}
	}

	public class RockThreeSquares2InvertedEntity : RockEntity
	{
		public RockThreeSquares2InvertedEntity(Vector2 _position)
			: base (_position, "three_squares_1", 150, 3 * (float)Math.PI/2.0f)
		{}

		protected override void DoCreation(Stage _stage)
		{
			base.DoCreation(_stage);
			SimplePhysicsComponent location = new SimplePhysicsComponent(position - new Vector2(-TiledMapReader.entitySize.X, 0));
			AddComponent(new RoundPhysicsBodyComponent(RockBaseCollisionSize, location));
			SimplePhysicsComponent location2 = new SimplePhysicsComponent(position - new Vector2(0, TiledMapReader.entitySize.Y));
			AddComponent(new RoundPhysicsBodyComponent(RockBaseCollisionSize, location2));
		}
	}

	public class RockTwoSquaresEntity : RockEntity
	{
		public RockTwoSquaresEntity(Vector2 _position)
			: base (_position, "two_squares", 150)
		{}

		protected override void DoCreation(Stage _stage)
		{
			base.DoCreation(_stage);
			SimplePhysicsComponent location = new SimplePhysicsComponent(position + new Vector2(TiledMapReader.entitySize.X, 0));
			AddComponent(new RoundPhysicsBodyComponent(RockBaseCollisionSize, location));
		}
	}

	public class RockTwoSquaresVerticalEntity : RockEntity
	{
		public RockTwoSquaresVerticalEntity(Vector2 _position)
			: base (_position, "two_squares", 150, (float)Math.PI/2.0f)
		{}

		protected override void DoCreation(Stage _stage)
		{
			base.DoCreation(_stage);
			SimplePhysicsComponent location = new SimplePhysicsComponent(position + new Vector2(0, TiledMapReader.entitySize.Y));
			AddComponent(new RoundPhysicsBodyComponent(RockBaseCollisionSize, location));
		}
	}

	public class RockThreeSquaresLongEntity : RockEntity
	{
		public RockThreeSquaresLongEntity(Vector2 _position)
			: base (_position, "three_squares_long", 150)
		{}

		protected override void DoCreation(Stage _stage)
		{
			base.DoCreation(_stage);
			SimplePhysicsComponent location = new SimplePhysicsComponent(position + new Vector2(TiledMapReader.entitySize.X, 0));
			AddComponent(new RoundPhysicsBodyComponent(RockBaseCollisionSize, location));
			SimplePhysicsComponent location2 = new SimplePhysicsComponent(position - new Vector2(TiledMapReader.entitySize.X, 0));
			AddComponent(new RoundPhysicsBodyComponent(RockBaseCollisionSize, location2));
		}
	}

	public class RockThreeSquaresLongVerticalEntity : RockEntity
	{
		public RockThreeSquaresLongVerticalEntity(Vector2 _position)
			: base (_position, "three_squares_long", 150, (float)Math.PI/2.0f)
		{}

		protected override void DoCreation(Stage _stage)
		{
			base.DoCreation(_stage);
			SimplePhysicsComponent location = new SimplePhysicsComponent(position + new Vector2(0, TiledMapReader.entitySize.Y));
			AddComponent(new RoundPhysicsBodyComponent(RockBaseCollisionSize, location));
			SimplePhysicsComponent location2 = new SimplePhysicsComponent(position - new Vector2(0, TiledMapReader.entitySize.Y));
			AddComponent(new RoundPhysicsBodyComponent(RockBaseCollisionSize, location2));
		}
	}

	public class RockBigSquareEntity : RockEntity
	{
		public RockBigSquareEntity(Vector2 _position)
			: base (_position, "big_square", 150)
		{}

		protected override void DoCreation(Stage _stage)
		{
			base.DoCreation(_stage);
			SimplePhysicsComponent location = new SimplePhysicsComponent(position + new Vector2(TiledMapReader.entitySize.X, 0));
			AddComponent(new RoundPhysicsBodyComponent(RockBaseCollisionSize, location));
			SimplePhysicsComponent location2 = new SimplePhysicsComponent(position + new Vector2(0, TiledMapReader.entitySize.Y));
			AddComponent(new RoundPhysicsBodyComponent(RockBaseCollisionSize, location2));
			SimplePhysicsComponent location3 = new SimplePhysicsComponent(position + new Vector2(TiledMapReader.entitySize.X, TiledMapReader.entitySize.Y));
			AddComponent(new RoundPhysicsBodyComponent(RockBaseCollisionSize, location3));
		}
	}
}
