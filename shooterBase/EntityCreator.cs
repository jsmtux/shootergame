﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace shooter
{
	public class EntityCreator
	{
		enum AvailableEntities { Enemy, };
		public EntityCreator(EnemyTarget _player)
		{
			player = _player;
		}

		public EnemyEntity CreateBurgerEnemy(Vector2 _position, List<Point> _enemyPath)
		{
			BurgerEnemyEntity ret = new BurgerEnemyEntity();
			ret.initialize(
					_position,
					player,
					_enemyPath);
			return ret;
		}

		public EnemyEntity CreateNachoEnemy(Vector2 _position, List<Point> _enemyPath)
		{
			NachoEnemyEntity ret = new NachoEnemyEntity();
			ret.initialize(
					_position,
					player,
					_enemyPath);
			return ret;
		}

		public RockEntity CreateRockSquare1(Vector2 _position)
		{
			RockEntity rockEntity = new RockSquare1Entity(_position);
			rockEntity.initialize();
			return rockEntity;
		}

		public RockEntity CreateRockSquare2(Vector2 _position)
		{
			RockEntity rockEntity = new RockSquare2Entity(_position);
			rockEntity.initialize();
			return rockEntity;
		}

		public RockEntity CreateRockThreeSquares1(Vector2 _position)
		{
			RockEntity rockEntity = new RockThreeSquares1Entity(_position);
			rockEntity.initialize();
			return rockEntity;
		}

		public RockEntity CreateRockThreeSquares1Inverted(Vector2 _position)
		{
			RockEntity rockEntity = new RockThreeSquares1InvertedEntity(_position);
			rockEntity.initialize();
			return rockEntity;
		}

		public RockEntity CreateRockThreeSquares2(Vector2 _position)
		{
			RockEntity rockEntity = new RockThreeSquares2Entity(_position);
			rockEntity.initialize();
			return rockEntity;
		}

		public RockEntity CreateRockThreeSquares2Inverted(Vector2 _position)
		{
			RockEntity rockEntity = new RockThreeSquares2InvertedEntity(_position);
			rockEntity.initialize();
			return rockEntity;
		}

		public RockEntity CreateRockTwoSquares(Vector2 _position)
		{
			RockEntity rockEntity = new RockTwoSquaresEntity(_position);
			rockEntity.initialize();
			return rockEntity;
		}

		public RockEntity CreateRockTwoSquaresVertical(Vector2 _position)
		{
			RockEntity rockEntity = new RockTwoSquaresVerticalEntity(_position);
			rockEntity.initialize();
			return rockEntity;
		}

		public RockEntity CreateRockThreeSquaresLong(Vector2 _position)
		{
			RockEntity rockEntity = new RockThreeSquaresLongEntity(_position);
			rockEntity.initialize();
			return rockEntity;
		}

		public RockEntity CreateRockThreeSquaresLongVertical(Vector2 _position)
		{
			RockEntity rockEntity = new RockThreeSquaresLongVerticalEntity(_position);
			rockEntity.initialize();
			return rockEntity;
		}

		public RockEntity CreateRockBigSquare(Vector2 _position)
		{
			RockEntity rockEntity = new RockBigSquareEntity(_position);
			rockEntity.initialize();
			return rockEntity;
		}

        internal SBaseEntity CreateGrassL(Vector2 position)
        {
			var ret = new GrassEntityL(position);
			ret.initialize();
			return ret;
        }

        internal SBaseEntity CreateGrassM(Vector2 position)
        {
			var ret = new GrassEntityM(position);
			ret.initialize();
			return ret;
        }

        internal SBaseEntity CreateGrassR(Vector2 position)
        {
			var ret = new GrassEntityR(position);
			ret.initialize();
			return ret;
        }

		public MushroomsEntity CreateMushrooms(Vector2 _position)
		{
			MushroomsEntity mushroomsEntity = new MushroomsEntity(_position);
			mushroomsEntity.initialize();
			return mushroomsEntity;
		}

		public CristalEntity CreateCristal(Vector2 _position)
		{
			CristalEntity cristalEntity = new CristalEntity(_position);
			cristalEntity.initialize();
			return cristalEntity;
		}

		public CarrotFoodItemEntity CreateCarrot(Vector2 _position)
		{
			CarrotFoodItemEntity carrotItem = new CarrotFoodItemEntity();
			carrotItem.Initialize(_position);
			return carrotItem;
		}

		public StarSeedItemEntity CreateStarSeed(Vector2 _position)
		{
			StarSeedItemEntity starSeedItem = new StarSeedItemEntity();
			starSeedItem.Initialize(_position);
			return starSeedItem;
		}

		public MelonFoodItemEntity CreateMelon(Vector2 _position)
		{
			MelonFoodItemEntity melonItem = new MelonFoodItemEntity();
			melonItem.Initialize(_position);
			return melonItem;
		}

		public PortalEntity CreateEndPortal(Vector2 _position, List<Vector2> bananaPositions)
		{
			PortalEntity portalEntity = new PortalEntity();
			portalEntity.Initialize(_position, true, bananaPositions);
			portalEntity.Open();
			return portalEntity;

		}

		public FenceEntity CreateFence(Vector2 _position)
		{
			FenceEntity fenceEntity = new FenceEntity(_position);
			fenceEntity.initialize();
			return fenceEntity;
		}

		public FenceEntity CreateHorizontalFence(Vector2 _position)
		{
			FenceEntity fenceEntity = new FenceEntity(_position, false);
			fenceEntity.initialize();
			return fenceEntity;
		}

		EnemyTarget player;

		internal SBaseEntity CreateHeart(Vector2 _position)
		{
			HeartItemEntity heart = new HeartItemEntity();
			heart.Initialize(_position);
			return heart;
		}
    }
}
