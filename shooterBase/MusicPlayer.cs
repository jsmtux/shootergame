﻿using System;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Media;

namespace shooter
{
	public class SBaseSong
	{
		public SBaseSong(Song _song)
		{
			song = _song;
		}

		public void Update(TimeSpan _playPosition)
		{
			currentLength = _playPosition;
		}

		public void Play()
		{
			currentLength = TimeSpan.Zero;
			MediaPlayer.Play(song);
		}

		public void Pause()
		{
			MediaPlayer.Pause();
		}

		public void Resume()
		{
			MediaPlayer.Resume();
		}

		public void Stop()
		{
			MediaPlayer.Stop();
			currentLength = TimeSpan.Zero;
		}

		public void OnMediaStateChanged(MediaState _state)
		{
			switch(MediaPlayer.State)
			{
				case MediaState.Playing:
					break;
				case MediaState.Paused:
					break;
				case MediaState.Stopped:
				{
					if (Repeat)
					{
						Play();
					}
					onEnd?.Invoke();
					break;
				}
			}
		}

		public Action onEnd;
		public bool Repeat = false;
		Song song;
		TimeSpan currentLength;
	}

    public class MusicPlayer
	{
		public enum MusicType {MenuSong, StageSong}

		public MusicPlayer(ContentManager _contentManager, SaveDataManager _saveDataManager)
		{
			menuSong = new SBaseSong(_contentManager.Load<Song>("music/menu_music"));
			menuSong.Repeat = true;
			stageSong = new SBaseSong(_contentManager.Load<Song>("music/level_music"));
			stageSong.Repeat = true;
			stageSongIntro = new SBaseSong(_contentManager.Load<Song>("music/intro_level_music"));
			stageSongIntro.onEnd = () => Start(stageSong);

			MediaPlayer.MediaStateChanged += OnStateChanged;
			saveDataManager = _saveDataManager;

			MediaPlayer.Volume = saveDataManager.Data.musicVolume * 80f;

			saveDataManager.SavedDataChanged += (Object sender, SaveDataManager.SavedDataEventArgs e) => {
				MediaPlayer.Volume = e.data.musicVolume * 80f;
			};
		}

		public void Update()
		{
			TimeSpan position = TimeSpan.Zero;
			if (MediaPlayer.State == MediaState.Playing)
			{
				position = MediaPlayer.PlayPosition;
			}
			currentSong?.Update(position);
		}

		public void SetPlaybackPaused()
		{
			Pause();
			playbackPaused = true;
		}

		public void SetPlaybackResumed()
		{
			Resume();
			playbackPaused = false;
		}

		public void Start(SBaseSong _song)
		{
			currentSong?.Stop();
			currentSong = _song;
			currentSong?.Play();
		}

		public void Stop()
		{
			var previousSong = currentSong;
			currentSong = null;
			previousSong?.Stop();
		}

		private void OnStateChanged(object sender, System.EventArgs e)
		{
			if (!playbackPaused)
			{
				currentSong?.OnMediaStateChanged(MediaPlayer.State);
			}
		}

        public void Pause()
        {
			currentSong?.Pause();
        }

        public void Resume()
        {
			currentSong?.Resume();
        }

		public readonly SBaseSong menuSong, stageSongIntro;
		SBaseSong stageSong;
		SBaseSong currentSong;
		SaveDataManager saveDataManager;
		bool playbackPaused = false;
    }
}
