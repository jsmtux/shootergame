﻿// from https://gist.github.com/nickgravelyn/4953988

using System;

using Microsoft.Xna.Framework;

namespace shooter
{
	public delegate float ScaleFunc(float progress);

	public delegate T LerpFunc<T>(T start, T end, float progress);

	public class Tween<T> : CountDownTimerComponent
		where T : struct
	{
		private readonly LerpFunc<T> lerpFunc;

		private ScaleFunc scaleFunc;

		private T start;
		private T end;
		private T value;

		public T StartValue { get { return start; } }
		public T EndValue { get { return end; } }
		public T CurrentValue { get { return value; } }

		public Tween(LerpFunc<T> _lerpFunc)
		{
			if (_lerpFunc == null)
			{
				throw new ArgumentNullException("lerpFunc");
			}
			lerpFunc = _lerpFunc;
		}

		public void SetDefault(T _value)
		{
			if (state == TimerState.Stopped)
			{
				value = _value;
			}
		}

		public void Start(T _start, T _end, TimeSpan _duration, ScaleFunc _scaleFunc)
		{
			if (_scaleFunc == null)
			{
				throw new ArgumentNullException("scaleFunc");
			}

			Start(_duration);

			scaleFunc = _scaleFunc;

			start = _start;
			end = _end;

			UpdateValue();
		}

		public override void Stop(StopBehavior stopBehavior)
		{
			if (state != TimerState.Stopped)
			{
				base.Stop(stopBehavior);
				UpdateValue();
			}
		}

		public override void Update(TimeSpan elapsedTime)
		{
			if (state != TimerState.Running)
			{
				return;
			}

			base.Update(elapsedTime);
			UpdateValue();
		}

		private void UpdateValue()
		{
			value = lerpFunc(start, end, scaleFunc(ElapsedPercentage()));
			onUpdate?.Invoke(value);
		}

		public Action<T> onUpdate;
	}

	public class TweenWrapper<T> where T : struct
	{
		public TweenWrapper(T _value)
		{
			staticValue = _value;
		}

		public TweenWrapper(Tween<T> _tween)
		{
			tween = _tween;
		}

		public virtual T CurrentValue()
		{
			T ret = tween == null ? staticValue : tween.CurrentValue;
			if (valueModifier != null)
			{
				ret = valueModifier(ret);
			}
			return ret;
		}

		public void SetStatic(T _value)
		{
			staticValue = _value;
			tween = null;
		}

		public bool IsTweened()
		{
			return tween != null;
		}

		public static implicit operator T(TweenWrapper<T> _tweenWrapper)
		{
			return _tweenWrapper.CurrentValue();
		}

		Tween<T> tween;
		T staticValue;

		public Func<T, T> valueModifier;
	}

	public class FloatTween : Tween<float>
	{
		private static float LerpFloat(float start, float end, float progress) { return start + (end - start) * progress; }
		private static readonly LerpFunc<float> LerpFunc = LerpFloat;

		public FloatTween() : base(LerpFunc) { }
	}

	public class Vector2Tween : Tween<Vector2>
    {
        private static readonly LerpFunc<Vector2> LerpFunc = Vector2.Lerp;

        public Vector2Tween() : base(LerpFunc) { }
    }

    public class Vector3Tween : Tween<Vector3>
    {
        private static readonly LerpFunc<Vector3> LerpFunc = Vector3.Lerp;

        public Vector3Tween() : base(LerpFunc) { }
    }

    public class Vector4Tween : Tween<Vector4>
    {
        private static readonly LerpFunc<Vector4> LerpFunc = Vector4.Lerp;

        public Vector4Tween() : base(LerpFunc) { }
    }

    public class ColorTween : Tween<Color>
    {
        private static readonly LerpFunc<Color> LerpFunc = Color.Lerp;

        public ColorTween() : base(LerpFunc) { }
    }

    public class QuaternionTween : Tween<Quaternion>
    {
        private static readonly LerpFunc<Quaternion> LerpFunc = Quaternion.Lerp;

        public QuaternionTween() : base(LerpFunc) { }
    }

	public static class ScaleFuncs
	{
		public static readonly ScaleFunc Linear = LinearImpl;

		public static readonly ScaleFunc QuadraticEaseIn = QuadraticEaseInImpl;

		public static readonly ScaleFunc QuadraticEaseOut = QuadraticEaseOutImpl;

		public static readonly ScaleFunc QuadraticEaseInOut = QuadraticEaseInOutImpl;

		public static readonly ScaleFunc CubicEaseIn = CubicEaseInImpl;

		public static readonly ScaleFunc CubicEaseOut = CubicEaseOutImpl;

		public static readonly ScaleFunc CubicEaseInOut = CubicEaseInOutImpl;

		public static readonly ScaleFunc QuarticEaseIn = QuarticEaseInImpl;

		public static readonly ScaleFunc QuarticEaseOut = QuarticEaseOutImpl;

		public static readonly ScaleFunc QuarticEaseInOut = QuarticEaseInOutImpl;

		public static readonly ScaleFunc QuinticEaseIn = QuinticEaseInImpl;

		public static readonly ScaleFunc QuinticEaseOut = QuinticEaseOutImpl;

		public static readonly ScaleFunc QuinticEaseInOut = QuinticEaseInOutImpl;

		public static readonly ScaleFunc SineEaseIn = SineEaseInImpl;

		public static readonly ScaleFunc SineEaseOut = SineEaseOutImpl;

		public static readonly ScaleFunc SineEaseInOut = SineEaseInOutImpl;

		public static readonly ScaleFunc CircularEaseIn = CircularEaseInImpl;

		public static readonly ScaleFunc CircularEaseOut = CircularEaseOutImpl;

		public static readonly ScaleFunc CircularEaseInOut = CircularEaseInOutImpl;

		public static readonly ScaleFunc ExponentialEaseIn = ExponentialEaseInImpl;

		public static readonly ScaleFunc ExponentialEaseOut = ExponentialEaseOutImpl;

		public static readonly ScaleFunc ExponentialEaseInOut = ExponentialEaseInOutImpl;

		public static readonly ScaleFunc ElasticEaseIn = ElasticEaseInImpl;

		public static readonly ScaleFunc ElasticEaseOut = ElasticEaseOutImpl;

		public static readonly ScaleFunc ElasticEaseInOut = ElasticEaseInOutImpl;

		public static readonly ScaleFunc BackEaseIn = BackEaseInImpl;

		public static readonly ScaleFunc BackEaseOut = BackEaseOutImpl;

		public static readonly ScaleFunc BackEaseInOut = BackEaseInOutImpl;

		public static readonly ScaleFunc BounceEaseIn = BounceEaseInImpl;

		public static readonly ScaleFunc BounceEaseOut = BounceEaseOutImpl;

		public static readonly ScaleFunc BounceEaseInOut = BounceEaseInOutImpl;

		private const float Pi = (float)Math.PI;
		private const float HalfPi = Pi / 2f;

		private static float LinearImpl(float progress) { return progress; }
		private static float QuadraticEaseInImpl(float progress) { return EaseInPower(progress, 2); }
		private static float QuadraticEaseOutImpl(float progress) { return EaseOutPower(progress, 2); }
		private static float QuadraticEaseInOutImpl(float progress) { return EaseInOutPower(progress, 2); }
		private static float CubicEaseInImpl(float progress) { return EaseInPower(progress, 3); }
		private static float CubicEaseOutImpl(float progress) { return EaseOutPower(progress, 3); }
		private static float CubicEaseInOutImpl(float progress) { return EaseInOutPower(progress, 3); }
		private static float QuarticEaseInImpl(float progress) { return EaseInPower(progress, 4); }
		private static float QuarticEaseOutImpl(float progress) { return EaseOutPower(progress, 4); }
		private static float QuarticEaseInOutImpl(float progress) { return EaseInOutPower(progress, 4); }
		private static float QuinticEaseInImpl(float progress) { return EaseInPower(progress, 5); }
		private static float QuinticEaseOutImpl(float progress) { return EaseOutPower(progress, 5); }
		private static float QuinticEaseInOutImpl(float progress) { return EaseInOutPower(progress, 5); }

		private static float EaseInPower(float progress, int power)
		{
			return (float)Math.Pow(progress, power);
		}

		private static float EaseOutPower(float progress, int power)
		{
			int sign = power % 2 == 0 ? -1 : 1;
			return (float)(sign * (Math.Pow(progress - 1, power) + sign));
		}

		private static float EaseInOutPower(float progress, int power)
		{
			progress *= 2;
			if (progress < 1)
			{
				return (float)Math.Pow(progress, power) / 2f;
			}
			else
			{
				int sign = power % 2 == 0 ? -1 : 1;
				return (float)(sign / 2.0 * (Math.Pow(progress - 2, power) + sign * 2));
			}
		}

		private static float SineEaseInImpl(float progress)
		{
			return (float)Math.Sin(progress * HalfPi - HalfPi) + 1;
		}

		private static float SineEaseOutImpl(float progress)
		{
			return (float)Math.Sin(progress * HalfPi);
		}

		private static float SineEaseInOutImpl(float progress)
		{
			return (float)(Math.Sin(progress * Pi - HalfPi) + 1) / 2;
		}

		private static float CircularEaseInImpl(float progress)
		{
			return (float)(1 - Math.Sqrt(1 - (progress * progress)));
		}

		private static float CircularEaseOutImpl(float progress)
		{
			return (float)Math.Sqrt((2 - progress) * progress);
		}

		private static float CircularEaseInOutImpl(float progress)
		{
			if (progress < 0.5f)
			{
				return 0.5f * (float)(1 - Math.Sqrt(1 - 4 * (progress * progress)));
			}
			else
			{
				return 0.5f * (float)(Math.Sqrt(-((2 * progress) - 3) * ((2 * progress) - 1)) + 1);
			}
		}

		private static float ExponentialEaseInImpl(float progress)
		{
			return (float)((progress == 0.0f) ? progress : Math.Pow(2, 10 * (progress - 1)));
		}

		private static float ExponentialEaseOutImpl(float progress)
		{
			return (float)((progress == 1.0f) ? progress : 1 - Math.Pow(2, -10 * progress));
		}

		private static float ExponentialEaseInOutImpl(float progress)
		{
			if (progress == 0.0 || progress == 1.0) return progress;

			if (progress < 0.5f)
			{
				return (float)(0.5f * Math.Pow(2, (20 * progress) - 10));
			}
			else
			{
				return (float)(-0.5f * Math.Pow(2, (-20 * progress) + 10) + 1);
			}
		}

		private static float ElasticEaseInImpl(float progress)
		{
			return (float)(Math.Sin(13 * HALFPI * progress) * Math.Pow(2, 10 * (progress - 1)));
		}

		private static float ElasticEaseOutImpl(float progress)
		{
			return (float)(Math.Sin(-13 * HALFPI * (progress + 1)) * Math.Pow(2, -10 * progress) + 1);
		}

		private static float ElasticEaseInOutImpl(float progress)
		{
			if (progress < 0.5f)
			{
				return 0.5f * (float)(Math.Sin(13 * HALFPI * (2 * progress)) * Math.Pow(2, 10 * ((2 * progress) - 1)));
			}
			else
			{
				return 0.5f * (float)(Math.Sin(-13 * HALFPI * ((2 * progress - 1) + 1)) * Math.Pow(2, -10 * (2 * progress - 1)) + 2);
			}
		}

		private static float BackEaseInImpl(float progress)
		{
			return (float)(progress * progress * progress - progress * Math.Sin(progress * PI));
		}

		private static float BackEaseOutImpl(float progress)
		{
			float f = (1 - progress);
			return (float)(1 - (f * f * f - f * Math.Sin(f * PI)));
		}

		private static float BackEaseInOutImpl(float progress)
		{
			if (progress < 0.5f)
			{
				float f = 2 * progress;
				return (float)(0.5f * (f * f * f - f * Math.Sin(f * PI)));
			}
			else
			{
				float f = (1 - (2 * progress - 1));
				return 0.5f * (float)(1 - (f * f * f - f * Math.Sin(f * PI))) + 0.5f;
			}
		}

		private static float BounceEaseInImpl(float progress)
		{
			return (float)(1 - BounceEaseOutImpl(1 - progress));
		}

		private static float BounceEaseOutImpl(float progress)
		{
			if (progress < 4 / 11.0f)
			{
				return (121 * progress * progress) / 16.0f;
			}
			else if (progress < 8 / 11.0f)
			{
				return (363 / 40.0f * progress * progress) - (99 / 10.0f * progress) + 17 / 5.0f;
			}
			else if (progress < 9 / 10.0f)
			{
				return (4356 / 361.0f * progress * progress) - (35442 / 1805.0f * progress) + 16061 / 1805.0f;
			}
			else
			{
				return (54 / 5.0f * progress * progress) - (513 / 25.0f * progress) + 268 / 25.0f;
			}
		}

		private static float BounceEaseInOutImpl(float progress)
		{
			if (progress < 0.5f)
			{
				return 0.5f * BounceEaseInImpl(progress * 2);
			}
			else
			{
				return 0.5f * BounceEaseOutImpl(progress * 2 - 1) + 0.5f;
			}
		}

		private const float HALFPI = (float)(Math.PI / 2.0f);
		private const float PI = (float)(Math.PI);
	}
}
