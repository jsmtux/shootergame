﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace shooter
{
	public class PerformanceLogger
	{
		public PerformanceLogger(ContentManager _contentManager, SpriteBatch _spriteBatch, CameraManager _cameraManager)
		{
			pixelImage = _contentManager.Load<Texture2D>("ui/pixel");
			spriteBatch = _spriteBatch;
			cameraManager = _cameraManager;
		}

		public void Draw(GameTime gameTime)
		{
			spriteBatch.Begin(SpriteSortMode.Immediate, null,
							  null, null, null, null, cameraManager.getUITranslationMatrix());
			int barNumber = 0;
			foreach (var iter in bars)
			{
				var bar = iter.Value;
				int currentOffset = 0;
				foreach (var iterator in bar.segments)
				{
					var segment = iterator.Value;
					var sizePercentage = segment.currentValue / bar.targetValue;
					int scaledSize = (int)(barSize.Y * sizePercentage);
					spriteBatch.Draw(pixelImage,
									 new Rectangle(barPosition.X + barSeparation * barNumber,
												   barPosition.Y + currentOffset,
												   barSize.X,
												   scaledSize),
									 null,
									 segment.color,
									 0,
									 Vector2.Zero,
									 SpriteEffects.None,
									 1.0f);
					currentOffset += scaledSize;
				}
				barNumber++;
			}

			spriteBatch.Draw(pixelImage,
							 new Rectangle(barPosition.X,
										   barPosition.Y + barSize.Y,
										   barSeparation * bars.Count,
										   1),
							 null,
							 Color.Red,
							 0,
							 Vector2.Zero,
							 SpriteEffects.None,
			                 1.0f);

			spriteBatch.Draw(pixelImage,
							 new Rectangle(barPosition.X,
			                               barPosition.Y + (barSize.Y / 2),
										   barSeparation * bars.Count,
										   1),
							 null,
							 Color.Green,
							 0,
							 Vector2.Zero,
							 SpriteEffects.None,
							 1.0f);

			spriteBatch.End();
		}

		public class BarSegment
		{
			public BarSegment(Color _color, TimeSpan _currentTime)
			{
				color = _color;
				currentValue = _currentTime.TotalMilliseconds;
			}

			public BarSegment(Color _color, double _currentUsage)
			{
				color = _color;
				currentValue = _currentUsage;
			}

			public Color color;
			public double currentValue;
		}

		public class Bar
		{
			public void AddToSegment(string _name, Color _color, double _value)
			{
				if (!segments.ContainsKey(_name))
				{
					segments[_name] = new BarSegment(_color, TimeSpan.Zero);
				}
				segments[_name].currentValue += _value;
			}

			public void AddToSegment(string _name, Color _color, TimeSpan _time)
			{
				AddToSegment(_name, _color, (double)_time.TotalMilliseconds);
			}
			internal Dictionary<string, BarSegment> segments = new Dictionary<string, BarSegment>();
			public double targetValue;

			public TimeSpan TargetTime {set {targetValue = value.TotalMilliseconds;}}
		}

		public void NewFrame()
		{
			bars.Clear();
		}

		public Bar GetBar(string _barName)
		{
			if (!bars.ContainsKey(_barName))
			{
				bars.Add(_barName, new Bar());
			}

			return bars[_barName];
		}

		Texture2D pixelImage;
		SpriteBatch spriteBatch;
		CameraManager cameraManager;
		Dictionary<string, Bar> bars = new Dictionary<string, Bar>();
		Point barSize = new Point(25, 100);
		Point barPosition = new Point(50, 10);
		int barSeparation = 30;
	}

}
