﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace shooter
{
	
	public class Colors
	{
		static readonly public Color TextHighlightColor = new Color(6, 223, 242);
		static readonly public Color SecondaryTextColor = new Color(5, 186, 202);
		static readonly public Color complementaryHighlightTextColor = new Color(236, 27, 248);
		static readonly public Color complementaryTextColor = new Color(161, 20, 171);
		static readonly public List<Color> nebulaColors = new List<Color>() { new Color(108, 137, 255), new Color(1, 28, 38) };
		static readonly public Color PCShipColor = new Color(163, 2, 18);
		static readonly public Color enemyColor = new Color(107, 12, 24);
	}
}
