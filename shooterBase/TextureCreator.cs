﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace shooter
{
	public class TextureCreator
	{
		public TextureCreator(GraphicsDevice _device)
		{
			device = _device;
		}

		public Texture2D CreateTexture(int width, int height, Func<Color[], Color[]> paint)
		{
			//initialize a texture
			Texture2D texture = new Texture2D(device, width, height);

			//the array holds the color for each pixel in the texture
			Color[] data = new Color[width * height];
			Vector2 position = Vector2.Zero;
			paint(data);

			//set the color
			texture.SetData(data);

			return texture;
		}

		GraphicsDevice device;
	}
}
