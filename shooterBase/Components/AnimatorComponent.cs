using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace shooter
{
    public class AnimationSequences : Dictionary<String, AnimationSequence> {}
    public class AnimationSequence
    {
        public AnimationSequence(TimeSpan _start, TimeSpan _end)
        {
            startTime = _start;
            endTime = _end;
        }
        public readonly TimeSpan startTime;
        public readonly TimeSpan endTime;
    }

    public class AnimatorComponent : LogicComponent
    {
        public AnimatorComponent(ModelAnimationComponent _modelComponent, AnimationSequences _animationSequences)
        {
            modelComponent = _modelComponent;
            animations = _animationSequences;
        }

        public override void UpdateLogic(GameTime _elapsedTime)
        {
            if(curAnimationSequence != null)
            {
                curAnimCurTime += _elapsedTime.ElapsedGameTime;
                if (curAnimCurTime > curAnimationSequence.endTime)
                {
                    if (loop)
                    {
                        curAnimCurTime = curAnimationSequence.startTime + (curAnimCurTime - curAnimationSequence.endTime);
                    }
                    else
                    {
                        curAnimationSequence = null;
                        onEnd?.Invoke();
                    }
                }
                modelComponent.SetCurrentAnimationTime(curAnimCurTime);
            }
        }

        public void Play(String _animationName, bool _loop = true)
        {
            loop = _loop;
            if (animations.ContainsKey(_animationName))
            {
                var newAnimation = animations[_animationName];
                if (newAnimation != curAnimationSequence)
                {
                    curAnimationSequence = newAnimation;
                    curAnimCurTime = newAnimation.startTime;
                    modelComponent.SetCurrentAnimation("Armature.001");
                }
            }
            else
            {
                Console.WriteLine("Error, " + _animationName + " is not found in current AnimatorComponent");
            }
        }

        public void Stop()
        {
            curAnimationSequence = null;
        }

        AnimationSequence curAnimationSequence;
        AnimationSequences animations = new AnimationSequences();
        ModelAnimationComponent modelComponent;
        TimeSpan curAnimCurTime;
        bool loop;
        public Action onEnd;
    }
}