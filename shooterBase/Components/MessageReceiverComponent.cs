using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;

namespace shooter
{
	public abstract class MessageReceiverComponent : SBaseComponent
	{
		public MessageReceiverComponent()
		{
		}

		public abstract void ReceiveMessage(Message message);
	}
}
