﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace shooter
{
	public class TextureReaderComponent
	{
		public TextureReaderComponent(Texture2D _image)
		{
			rawData = new Color[_image.Width * _image.Height];
			size = new Point(_image.Width, _image.Height);
			_image.GetData<Color>(rawData);
		}

		public Color GetColorAt(Point _pos)
		{
			Color ret = new Color(0,0,0,0);
			if (new Rectangle(Point.Zero, size).Contains(_pos))
			{
				return rawData[_pos.X + _pos.Y * size.X];
			}
			return ret;
		}

		Color[] rawData;
		public readonly Point size;
	}
}
