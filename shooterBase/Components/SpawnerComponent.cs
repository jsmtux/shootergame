﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;

namespace shooter
{
	public class SpawnerComponent<T>
		where T : SBaseEntity, new()
	{
		public SpawnerComponent(Stage _parentStage)
		{
			parentStage = _parentStage;
		}

        public T spawn(Action<T> initializeFunction = null)
		{
			T newEntity = new T();
			initializeFunction?.Invoke(newEntity);
			parentStage.addEntity(newEntity);
			return newEntity;
		}

		private Stage parentStage;
	}
}
