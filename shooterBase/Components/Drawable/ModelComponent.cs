﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
namespace shooter
{
	public class DirectionalLightDefinition
	{
		public bool enabled;
		public Color diffuse;
		public Vector3 direction;
	}

	public class LightingDefinition
	{
		public LightingDefinition()
		{
			light0 = new DirectionalLightDefinition();
			light0.diffuse = new Color(1, 0.9607844f, 0.8078432f);
			light0.direction = new Vector3(-0.5265408f, -0.5735765f, -0.6275069f);
			light0.enabled = true;
		}
		public DirectionalLightDefinition light0;
		public bool ignoreProcessedLights = false;
	}

	public class ModelComponent : DrawableComponent
	{
		public ModelComponent(LocationComponent _locationComponent,
		                      float _scale,
		                      Model _model,
		                      Texture2D _texture,
		                      Depth.Type _depth,
		                      RenderConfiguration _renderConfiguration,
							  RenderMode _renderMode = RenderMode.Ortographic)
			: base(_locationComponent, _depth, _renderConfiguration)
		{
			model = _model;
			texture = _texture;
			scale = new TweenWrapper<float>(_scale);
			renderMode = _renderMode;

			InitializeViewVolume(DefaultViewVolume);
		}

		public ModelComponent(LocationComponent _locationComponent,
		                      float _scale,
							  Model _model,
							  Texture2D _texture,
							  Depth.Type _depth)
			: this(_locationComponent, _scale, _model, _texture, _depth, RenderConfiguration.Default, RenderMode.Ortographic)
		{}

		public virtual void UpdateForDraw(GameTime _time) {}

		public void SetViewVolume(float volume)
		{
			InitializeViewVolume(volume);
		}

		public Model model;
		public String modelPath;
		public Texture2D texture;
		public String texturePath;
		public TweenWrapper<float> scale;
		public Effect customEffect = null;
		public LightingDefinition lightOverride;
		public const int DefaultViewVolume = 50;
		public enum RenderMode {Perspective, Ortographic};
		public readonly RenderMode renderMode;
	}
}
