﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace shooter
{
	public class SpriteComponent : DrawableComponent
	{
		public SpriteComponent(LocationComponent _locationComponent,
		                       Texture2D _image,
							   Depth.Type _layerDepth,
							   RenderConfiguration _renderConfiguration)
			: base(_locationComponent, _layerDepth, _renderConfiguration)
		{
			image = _image;
			imageSize = new Vector2(image.Width, image.Height);
			_locationComponent.InitializeSize(imageSize);
			tint = new TweenWrapper<Color>(Color.White);
		}

		public SpriteComponent(LocationComponent _locationComponent,
		                       Texture2D _image,
							   Depth.Type _layerDepth)
			: this(_locationComponent, _image, _layerDepth, RenderConfiguration.Default)
		{}

		public Texture2D image;
		public Vector2 imageSize;
	}
}
