﻿using System;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.Collections.Generic;

namespace shooter
{
	public class Particle
	{
		public Particle(
			Vector2 _position,
			TimeSpan _lifeTime,
			float _speed,
			float _rotationSpeed,
			float _angle,
			Color _beginTint,
			Color _endTint,
			Vector2 _endMovementOffset)
		{
			position = _position;
			lifeTime = _lifeTime;
			speed = _speed;
			rotationSpeed = _rotationSpeed;
			angle = _angle;
			beginTint = _beginTint;
			endTint = _endTint;
			endMovementOffset = _endMovementOffset;
		}

		public void update(GameTime gameTime)
		{
			elapsedLife += gameTime.ElapsedGameTime;

			position.X += speed * -(float)Math.Sin(angle);
			position.Y += speed * (float)Math.Cos(angle);

			if (endMovementOffset != Vector2.Zero)
				position += (gameTime.ElapsedGameTime.Milliseconds / (float)lifeTime.Milliseconds) * endMovementOffset;

			rotation += rotationSpeed;
		}

		public bool needsToDie()
		{
			return elapsedLife > lifeTime;
		}

		public float lifePercentage()
		{
			return (float)elapsedLife.Ticks / (float)lifeTime.Ticks;
		}

		public TimeSpan lifeTime;
		public float speed;
		public float rotationSpeed;
		public float angle;
		public Vector2 position;
		public float rotation;
		public Color beginTint;
		public Color endTint;

		Vector2 endMovementOffset;

		public TimeSpan elapsedLife = TimeSpan.Zero;
	}

	public class ParticleEmitterComponent : DrawableComponent
	{
		public ParticleEmitterComponent(Texture2D _image,
		                                LocationComponent _locationComponent,
		                                Depth.Type _depth,
		                                Vector2 _endMovementOffset,
		                                RenderConfiguration _renderConfiguration)
			: base(_locationComponent, _depth, _renderConfiguration)
		{
			image = _image;
			endMovementOffset = _endMovementOffset;
		}

		public ParticleEmitterComponent(Texture2D _image,
										LocationComponent _locationComponent,
										Depth.Type _depth,
										Vector2 _endMovementOffset)
			: this(_image, _locationComponent, _depth, _endMovementOffset, RenderConfiguration.Default)
		{}

		public ParticleEmitterComponent(Texture2D _image,
										LocationComponent _locationComponent,
										Depth.Type _depth)
			: this(_image, _locationComponent, _depth, Vector2.Zero, RenderConfiguration.Default)
		{}

		public Particle getNext()
		{
			return new Particle(
				locationComponent.Position,
				lifeTime + new TimeSpan((long)(lifeTimeVariance.Ticks * r.NextDouble())),
				speed + speedVariance * (float)(r.NextDouble() - 0.5),
				rotationSpeed + rotationSpeedVariance * (float)(r.NextDouble() - 0.5),
				locationComponent.Rotation + angleVariance * (float)(r.NextDouble() - 0.5),
				beginTint,
				endTint,
				endMovementOffset
			);
		}

		public void setEmitting(bool _emitting)
		{
			emitting = _emitting;
		}

		public void emitAmount(int _numTimes, Action _onEndAction = null)
		{
			numEmissions = _numTimes;
			onEnd = _onEndAction;
		}

		public void update(GameTime gameTime)
		{
			if (emitting || numEmissions > 0)
			{
				if (!emitTime.IsRunning || emitTime.ElapsedTicks > cadence.Ticks)
				{
					emitTime.Restart();
					if (emitting == false)
					{
						numEmissions--;
					}
					for (var i = 0; i < particlesOnEmission; i++)
					{
						emittedParticles.Add(getNext());
					}
				}
			}
			foreach (var particle in emittedParticles)
			{
				particle.update(gameTime);
			}
			emittedParticles.RemoveAll(item => item.needsToDie());
			if (emittedParticles.Count == 0 && numEmissions == 0 && onEnd != null)
			{
				onEnd();
			}
		}

		public void draw(GameTime gameTime, SpriteBatch spriteBatch)
		{
			Vector2 origin = new Vector2(image.Width / 2, image.Height / 2);
			foreach (var particle in emittedParticles)
			{
				float lifePercentage = particle.lifePercentage();
				float curSize = beginSize * (1.0f - lifePercentage) + endSize * lifePercentage;
				Color drawcolor = Color.Lerp(particle.beginTint, particle.endTint, lifePercentage);
				drawcolor *= Alpha;
				spriteBatch.Draw(image,
									null,
									new Rectangle((int)(particle.position.X),
												(int)(particle.position.Y),
												(int)curSize,
												(int)curSize),
									null,
									origin,
									particle.rotation,
									null,
									drawcolor,
									SpriteEffects.None,
				                 	depth.toFloat());
			}
		}

		List<Particle> emittedParticles = new List<Particle>();

		Stopwatch emitTime = new Stopwatch();

		public Texture2D image;
		// particle description
		public TimeSpan lifeTime;
		public TimeSpan lifeTimeVariance;
		public float speed;
		public float speedVariance;
		public float rotationSpeed;
		public float rotationSpeedVariance;
		public float angleVariance;
		public float beginSize;
		public float endSize;

		private Vector2 endMovementOffset;

		public Color beginTint = Color.White;
		public Color endTint = Color.White;
		//emmiter description
		public TimeSpan cadence;
		public int particlesOnEmission = 1;

		public bool emitting = false;

		int numEmissions;
		Action onEnd = null;


		Random r = new Random();
	}
}
