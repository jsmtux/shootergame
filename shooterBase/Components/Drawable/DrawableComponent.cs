﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace shooter
{
	public class Depth
	{
		public Depth(Type _type)
		{
			type = _type;
		}

		public void Set(Type _type)
		{
			type = _type;
		}

		public Type Get()
		{
			return type;
		}

		public float toFloat()
		{
			return (float)type / numDepthTypes;
		}

		public int toInt()
		{
			return (int)type;
		}

		public enum Type { Bg1, Bg2, Bg3, Action1, Action2, Action3, Top, UI, UI2, UI3};
		public readonly static int numDepthTypes = Enum.GetNames(typeof(Type)).Length;
		Type type;
	}

	public class RenderConfiguration
	{
		public RenderConfiguration(TranslationType _translationType,
		                    BlendType _blendType = BlendType.Alpha,
		                    SBaseEffect _effect = null)
		{
			translationType = _translationType;
			blendType = _blendType;
			effect = _effect;
		}

		public override int GetHashCode()
		{
			int effectHashCode = 0;
			if (effect != null)
			{
				effectHashCode = effect.GetHashCode();
			}
			return translationType.GetHashCode() + blendType.GetHashCode() * 2 + effectHashCode * 4;
		}

		public override bool Equals(Object o)
		{
			if (o == null)
			{
				return false;
			}
			else
			{
				return this.GetHashCode() == (o as RenderConfiguration).GetHashCode();
			}
		}

		public BlendState getBlendState()
		{
			BlendState ret = null;
			switch(blendType)
			{
				case BlendType.Alpha:
					ret = BlendState.AlphaBlend;
					break;
				case BlendType.Additive:
					ret = BlendState.Additive;
					break;
			}
			return ret;
		}

		public static RenderConfiguration Default = new RenderConfiguration(TranslationType.Camera);

		public enum TranslationType { UI, Camera };
		public readonly TranslationType translationType;
		public enum BlendType { Alpha, Additive };
		public readonly BlendType blendType;
		public readonly SBaseEffect effect;
	}

	public class ViewVolume
	{
		internal ViewVolume(LocationComponent _location, float _radius)
		{
			location = _location;
			radius = _radius;
		}

		public bool Intersects(Rectangle _frustum)
		{
			Vector3 vec3 = new Vector3(location.Position.X, location.Position.Y, 0);
			lastCheckResult = Utils.DoRectangleCircleOverlap(location.Position, radius, _frustum);
			return lastCheckResult;
		}

		LocationComponent location;
		float radius;
		bool lastCheckResult;
	}

	public abstract class DrawableComponent : SBaseComponent
	{
		public DrawableComponent(LocationComponent _locationComponent, Depth.Type _depthType, RenderConfiguration _renderConfiguration)
		{
			locationComponent = _locationComponent;
			depth = new Depth(_depthType);
			renderConfiguration = _renderConfiguration;
		}

		protected void InitializeViewVolume(float _radius)
		{
			viewVolume = new ViewVolume(locationComponent, _radius);
		}

		public TweenWrapper<float> Alpha = new TweenWrapper<float>(1.0f);
		public TweenWrapper<Color> tint;

		public LocationComponent locationComponent;
		public Depth depth;
		public RenderConfiguration renderConfiguration;
		public ViewVolume viewVolume;
	}
}
