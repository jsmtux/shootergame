﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace shooter
{
	public class TextComponent : DrawableComponent
	{
		public TextComponent(LocationComponent _locationComponent,
		                     SpriteFont _font,
		                     string _text,
		                     Depth.Type _layerDepth,
		                     RenderConfiguration _renderConfiguration)
			: base(_locationComponent, _layerDepth, _renderConfiguration)
		{
			font = _font;
			Text = _text;
			tint = new TweenWrapper<Color>(Color.White);
		}

		public TextComponent(LocationComponent _locationComponent,
		                     SpriteFont _font,
							 string _text,
							 Depth.Type _layerDepth)
			: this(_locationComponent, _font, _text, _layerDepth, RenderConfiguration.Default)
		{}

		public String Text { 
			get { return text; }
			set {
				text = value;
				locationComponent.Size = font.MeasureString(text);
			} 
		}

		string text;
		public SpriteFont font;
	}
}
