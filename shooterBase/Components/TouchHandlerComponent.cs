﻿using System;
using Microsoft.Xna.Framework;

namespace shooter
{
	public class TouchHandlerComponent : SBaseComponent
	{
		public TouchHandlerComponent(Func<ActiveTouch, bool> _newTouchHandler, Action _stopTouchHandler, LocationComponent _location)
		{
			newTouchHandler = _newTouchHandler;
			stopTouchHandler = _stopTouchHandler;
			location = _location;
		}

		public bool HandleStartTouch(ActiveTouch _touch)
		{
			if (!active)
			{
				return false;
			}
			bool ret = false;
			Rectangle hitBox = new Rectangle((int)(location.Anchors.TopLeft.X),
								  (int)(location.Anchors.TopLeft.Y),
								  (int)location.Size.X,
								  (int)location.Size.Y);
			if (hitBox.Contains(_touch.Position))
			{
				if (newTouchHandler(_touch))
				{
					ret = true;
					touchId = _touch.id;
				}
			}
			return ret;
		}

		public void HandleStopTouch(int _id)
		{
			if (touchId == _id)
			{
				stopTouchHandler();
				touchId = -1;
			}
		}

		public void SetActive(bool _active)
		{
			active = _active;
		}

		Func<ActiveTouch, bool> newTouchHandler;
		Action stopTouchHandler;
		LocationComponent location;
		int touchId = -1;
		bool active = true;
	}
}
