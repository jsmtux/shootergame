﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace shooter
{
	public class SteeringComponent
    {
		public SteeringComponent(SimplePhysicsComponent _physicsComponent, Velocity _maxVelocity, float _mass, Velocity _rotationVelocity)
		{
			physicsComponent = _physicsComponent;
			maxVelocity = _maxVelocity;
			mass = _mass;
			rotationVelocity = _rotationVelocity;
		}

		public void SteerInDirection(Vector2 _direction, GameTime _elapsedTime)
		{
			SteerTowards(physicsComponent.Position + _direction * maxVelocity.NominalValue(), _elapsedTime);
		}

		public bool Pursue(LocationComponent _target, GameTime _elapsedTime)
		{
			float pursueFactor = 6.0f / 30f;
			return SteerTowards(_target.Position + _target.velocity * pursueFactor, _elapsedTime);
		}

		public void RotateAroundCenter(GameTime _elapsedTime)
		{
			float currentRotation = Utils.Wrap(physicsComponent.Rotation, (float)Math.PI, (float)-Math.PI);
			float rotationIncrement = rotationVelocity.GetUnitsIn(_elapsedTime);
			physicsComponent.setRotation(currentRotation + rotationIncrement);
		}

		public bool SteerTowards(Vector2 _target, GameTime _elapsedTime)
		{
			Vector2 targetVelocity = _target - physicsComponent.Position;
			float distance = targetVelocity.Length();

			if (distance < 0.0001)
			{
				return false;
			}

			bool inRange = false;

			float currentRotation = Utils.Wrap(physicsComponent.Rotation, (float)Math.PI, (float)-Math.PI);
			float desiredRotation = Utils.Wrap(Utils.getVectorAngle(targetVelocity), (float)Math.PI, (float)-Math.PI);

			float difference = desiredRotation - currentRotation;

			if (difference > Math.PI)
			{
				difference = (float)Math.PI - difference;
			}
			else if (difference < -Math.PI)
			{
				difference = (float)Math.PI - difference;
			}

			if (difference > rotationVelocity.GetUnitsIn(_elapsedTime))
			{
				difference = rotationVelocity.GetUnitsIn(_elapsedTime);
			}
			else if (difference < -rotationVelocity.GetUnitsIn(_elapsedTime))
			{
				difference = -rotationVelocity.GetUnitsIn(_elapsedTime);
			}
			physicsComponent.setRotation(currentRotation + difference);

			Vector2 currentAcceleration = new Vector2((float)Math.Sin(physicsComponent.Rotation), -(float)Math.Cos(physicsComponent.Rotation));

			if (distance < 100)
			{
				currentAcceleration = Vector2.Zero;
				inRange = true;
			}
			else
			{
				float velocity = physicsComponent.velocity.Length();
				if (Double.IsNaN(velocity))
				{
					velocity = 0;
					physicsComponent.velocity = Vector2.Zero;
				}

				currentAcceleration *= maxVelocity.NominalValue();
			}

			UpdateSpeedForSteering(currentAcceleration - physicsComponent.velocity, _elapsedTime);

			return inRange;
		}

		protected bool MoveTowards(Vector2 _target, GameTime _elapsedTime)
		{
			Vector2 targetPositionDifference = _target - physicsComponent.Position;
			float distance = targetPositionDifference.Length();
			if (distance < 10)
			{
				return true;
			}

			float currentRotation = Utils.Wrap(physicsComponent.Rotation, (float)Math.PI, (float)-Math.PI);
			float desiredRotation = Utils.Wrap(Utils.getVectorAngle(targetPositionDifference), (float)Math.PI, (float)-Math.PI);

			float rotationDifference = desiredRotation - currentRotation;

			if (rotationDifference > Math.PI)
			{
				rotationDifference = (float)Math.PI - rotationDifference;
			}
			else if (rotationDifference < -Math.PI)
			{
				rotationDifference = (float)Math.PI - rotationDifference;
			}

			if (rotationDifference > rotationVelocity.GetUnitsIn(_elapsedTime))
			{
				rotationDifference = rotationVelocity.GetUnitsIn(_elapsedTime);
			}
			else if (rotationDifference < -rotationVelocity.GetUnitsIn(_elapsedTime))
			{
				rotationDifference = -rotationVelocity.GetUnitsIn(_elapsedTime);
			}

			targetPositionDifference.Normalize();
			physicsComponent.velocity = targetPositionDifference * maxVelocity.NominalValue();
			physicsComponent.setRotation(physicsComponent.Rotation + rotationDifference);

			return false;
		}

		void UpdateSpeedForSteering(Vector2 _steering, GameTime _elapsedTime)
		{
			if (_steering.Length() > maxVelocity.NominalValue())
			{
				_steering.Normalize();
				_steering *= maxVelocity.NominalValue();
			}

			float massTimeFactor = _elapsedTime.ElapsedGameTime.Milliseconds / (ShooterGame.ShooterTargetTime.Milliseconds * 2.0f);
			_steering = _steering / (mass * massTimeFactor);

			physicsComponent.velocity = physicsComponent.velocity + _steering;
		}

		public Vector2 getCurrentVelocity()
		{
			return physicsComponent.velocity;
		}

		protected SimplePhysicsComponent physicsComponent;

		public readonly Velocity maxVelocity;
		public readonly float mass;
		public readonly Velocity rotationVelocity;
    }

	public abstract class WanderingSteeringComponent : SteeringComponent
	{
		public WanderingSteeringComponent(SimplePhysicsComponent _physicsComponent, Velocity _maxVelocity, float _mass, Velocity _rotationVelocity)
			: base(_physicsComponent, _maxVelocity, _mass, _rotationVelocity)
		{
		}

		public abstract void Wander(GameTime _elapsedTime);
	}

	public class PathWanderingSteeringComponent : WanderingSteeringComponent
	{
		public PathWanderingSteeringComponent(SimplePhysicsComponent _physicsComponent,
											  Velocity _maxVelocity,
											  float _mass,
											  Velocity _rotationVelocity,
											  List<Point> _pathPoints)
			: base(_physicsComponent, _maxVelocity, _mass, _rotationVelocity)
		{
			pathPoints = _pathPoints;
			currentIndex = 0;
		}

		public override void Wander(GameTime _elapsedTime)
		{
			if (MoveTowards(pathPoints[currentIndex].ToVector2(), _elapsedTime))
			{
				currentIndex = (currentIndex + 1) % pathPoints.Count;
			}
		}

		List<Point> pathPoints;
		int currentIndex;
	}

	public class RandomWanderingSteeringComponent : WanderingSteeringComponent
	{
		public RandomWanderingSteeringComponent(SimplePhysicsComponent _physicsComponent,
										  Velocity _maxVelocity, 
		                                  float _mass, 
		                                  Velocity _rotationVelocity, 
		                                  PhysicsResolver _resolver,
		                                  float _unitRadius)
			: base(_physicsComponent, _maxVelocity, _mass, _rotationVelocity)
		{
			resolver = _resolver;
			unitRadius = _unitRadius;
			targetDuration = TimeSpan.FromSeconds(10.0f);
			targetRadius = 500.0f;
		}

		public override void Wander(GameTime _elapsedTime)
		{
			if (currentTarget == Vector2.Zero || timeSinceChanged > targetDuration)
			{
				Random rnd = new Random();
				int bail = 0;
				timeSinceChanged = TimeSpan.Zero;
				do
				{
					bail++;
					currentTarget = physicsComponent.Position +
					                                Utils.getVectorFromMagnitudeAngle(targetRadius, (float)(rnd.NextDouble() * Math.PI * 2));
				} while (resolver.isColliding(currentTarget, unitRadius) && bail < 10);
			}

			timeSinceChanged += _elapsedTime.ElapsedGameTime;
			SteerTowards(currentTarget, _elapsedTime);
		}

		PhysicsResolver resolver;
		Vector2 currentTarget = Vector2.Zero;
		TimeSpan timeSinceChanged;

		TimeSpan targetDuration;
		float targetRadius;
		float unitRadius;
	}
}
