﻿using System;
using Microsoft.Xna.Framework;

namespace shooter
{
	public class LootItemLogicComponent : LogicComponent
	{
		public LootItemLogicComponent(SimplePhysicsComponent _location, FloatTween _sizeTween, FloatTween _lightIntensityTween)
		{
			location = _location;
			sizeTween = _sizeTween;
			lightIntensityTween = _lightIntensityTween;

			location.setModelRotation(new Vector3((float)Math.PI / 2, 0, (float)Math.PI / 2));
			sizeTween.onEnd = restartTween;
			sizeTween.Start(0, 150, TimeSpan.FromSeconds(1.5f), ScaleFuncs.CubicEaseIn);
		}

		public override void UpdateLogic(GameTime _elapsedTime)
		{
		}

		private void restartTween()
		{
			active = true;
			sizeTween.onEnd = () =>
			{
				if (!alive)
				{
					return;
				}
				sizeTween.onEnd = restartTween;
				lightIntensityTween.Start(10.0f, 3.0f, TimeSpan.FromSeconds(0.5f), ScaleFuncs.CubicEaseOut);
				sizeTween.Start(200f, 150f, TimeSpan.FromSeconds(0.5f), ScaleFuncs.CubicEaseOut);
			};
			if (!alive)
			{
				return;
			}
			lightIntensityTween.Start(3.0f, 10.0f, TimeSpan.FromSeconds(0.5f), ScaleFuncs.CubicEaseIn);
			sizeTween.Start(150f, 200f, TimeSpan.FromSeconds(0.5f), ScaleFuncs.CubicEaseIn);
		}

		public bool Collected()
		{
			if (active)
			{
				alive = false;
				sizeTween.onEnd = () =>
				{
					sizeTween.Start(sizeTween.CurrentValue, 0.0f, TimeSpan.FromSeconds(0.15f), ScaleFuncs.CubicEaseIn);
				lightIntensityTween.Start(lightIntensityTween.CurrentValue, 0.0f, TimeSpan.FromSeconds(0.5f), ScaleFuncs.CubicEaseIn);
				};
				sizeTween.Start(sizeTween.CurrentValue, 300.0f, TimeSpan.FromSeconds(0.25f), ScaleFuncs.CubicEaseOut);
				lightIntensityTween.Start(lightIntensityTween.CurrentValue, 20.0f, TimeSpan.FromSeconds(0.25f), ScaleFuncs.CubicEaseIn);
			}

			return active;
		}

		public bool CanBePickedUp()
		{
			return active;
		}

		SimplePhysicsComponent location;
		FloatTween sizeTween;
		FloatTween lightIntensityTween;
		bool alive = true;
		bool active = false;
	}
}
