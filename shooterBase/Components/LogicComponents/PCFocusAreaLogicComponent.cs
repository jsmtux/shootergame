﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace shooter
{
	public class PCFocusAreaLogicComponent : LogicComponent
	{
		public PCFocusAreaLogicComponent(LocationComponent _playerLocation, IMessagingSystem _messagingSystem, Dictionary<string, Rectangle> _cameraZones)
		{
			playerLocation = _playerLocation;
			messagingSystem = _messagingSystem;
			cameraZones = _cameraZones;
		}

		public override void UpdateLogic(GameTime _elapsedTime)
		{
			List<string> currentCameraZones = new List<string>();

			foreach(var zone in cameraZones)
			{
				if (zone.Value.Contains(playerLocation.Position.ToPoint()))
				{
					currentCameraZones.Add(zone.Key);
				}
			}

			foreach(var zoneName in currentCameraZones)
			{
				if (!notifiedCameraZones.Contains(zoneName))
				{
					messagingSystem.SendMessage(new CameraZoneShipChange(CameraZoneShipChange.ChangeType.Entered, zoneName));
				}
			}

			foreach(var zoneName in notifiedCameraZones)
			{
				if (!currentCameraZones.Contains(zoneName))
				{
					messagingSystem.SendMessage(new CameraZoneShipChange(CameraZoneShipChange.ChangeType.Exited, zoneName));
				}
			}

			notifiedCameraZones = currentCameraZones;
		}


		LocationComponent playerLocation;
		IMessagingSystem messagingSystem;
		Dictionary<string, Rectangle> cameraZones;
		List<string> notifiedCameraZones = new List<string>();
	}
}
