using System;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace shooter
{
	public class DeadShipLogicComponent : LogicComponent
	{
		public DeadShipLogicComponent(DrawableComponent _drawable, SimplePhysicsComponent _location)
		{
			drawable = _drawable;
			location = _location;
		}

		public override void UpdateLogic(GameTime _elapsedTime)
		{
            if (active)
            {
				if (timeSinceDeath == null)
				{
					SimplePhysicsComponent physicsComponent = entity.GetComponent<SimplePhysicsComponent>();
					float currentRotation = physicsComponent.Rotation;
					physicsComponent.setRotation(currentRotation + 0.1f);
					drawable.Alpha.SetStatic(drawable.Alpha.CurrentValue() - 0.02f);
					if (drawable.Alpha <= 0.0f)
					{
						onShipDead();
						timeSinceDeath = TimeSpan.Zero;
					}
				}
				else
				{
					timeSinceDeath += _elapsedTime.ElapsedGameTime;
					if (timeSinceDeath > TimeSpan.FromSeconds(2.0f))
					{
						ShipEntity shipEentity = (ShipEntity)entity;
						shipEentity.delete();
					}
				}
            }
		}

        public void setActive(Action _onShipDead)
        {
            active = true;
			onShipDead = _onShipDead;
			location.velocity = deathDirection;
		}

		internal void setDirection(Vector2 _deathDirection)
		{
			deathDirection = _deathDirection;
			deathDirection.Normalize();
			deathDirection *= 150;
		}

        private bool active = false;
		Action onShipDead;
		DrawableComponent drawable;
		SimplePhysicsComponent location;
		TimeSpan? timeSinceDeath;
		Vector2 deathDirection = new Vector2(100, 100);
	}
}
