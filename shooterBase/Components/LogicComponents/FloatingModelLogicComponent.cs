﻿using System;
using Microsoft.Xna.Framework;

namespace shooter
{
	public class FloatingModelLogicComponent : LogicComponent
	{
		public FloatingModelLogicComponent(SimplePhysicsComponent _modelLocation)
		{
			modelLocation = _modelLocation;
			initialPosition = modelLocation.Position;
		}

		public override void UpdateLogic(GameTime _elapsedTime)
		{
			if (floating)
			{
				modelLocation.Position = initialPosition + new Vector2(0, 10f * (float)Math.Sin(currentYMovementAngle));
				currentYMovementAngle += 0.1f;
			}
		}

		public void Start()
		{
			floating = true;
			initialPosition = modelLocation.Position;
		}

		public void Stop()
		{
			floating = false;
		}

		SimplePhysicsComponent modelLocation;
		float currentYMovementAngle = 0.0f;
		Vector2 initialPosition;
		bool floating = true;
	}
}
