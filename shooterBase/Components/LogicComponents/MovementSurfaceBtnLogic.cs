﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace shooter
{
	public class MovementSurfaceBtnLogic : LogicComponent
    {
		public MovementSurfaceBtnLogic(UILocationComponent _location, PCEntity _playerEntity, CountDownTimerComponent _crashTimeout)
		{
			location = _location;
			playerEntity = _playerEntity;
			crashTimeout = _crashTimeout;
		}

		public bool handleNewActiveTouch(ActiveTouch _activeTouch)
		{
			if (currentActiveTouch == null)
			{
				currentActiveTouch = _activeTouch;
				return true;
			}
			return false;
		}

		public void stopHandlingActiveTouch()
        {
            currentActiveTouch = null;
		}

		public Vector2 getDesktopDirection()
		{
			Vector2 ret = Vector2.Zero;
			keyboardState = Keyboard.GetState();

			if (keyboardState.IsKeyDown(Keys.Left))
			{
				ret.X = -1;
			}
			else if (keyboardState.IsKeyDown(Keys.Right))
			{
				ret.X = 1;
			}

			if (keyboardState.IsKeyDown(Keys.Up))
			{
				ret.Y = -1;
			}
			else if (keyboardState.IsKeyDown(Keys.Down))
			{
				ret.Y = 1;
			}

			if (ret.Length() > 1.0)
			{
				ret.Normalize();
			}
			else if (ret.Length() < 0.1f)
			{
				if (gamepadIndex != null)
				{
					var index = gamepadIndex.GetValueOrDefault();

					GamePadState state = GamePad.GetState(index);
					ret = state.ThumbSticks.Left;
					ret.Y = -ret.Y;

				}
			}

			return ret;
		}

		public override void UpdateLogic(GameTime _elapsedTime)
		{
			if (crashTimeout.State == TimerState.Running)
			{
				velocity = velocity * 0.99f;
			}
			else
			{
				currentAngleSide = -1;
				fingerDistance = 0;
				if (currentActiveTouch == null)
				{
					direction = getDesktopDirection();
					direction *= 0.9f;
				}
				else
				{
					direction = (currentActiveTouch.Position - location.Position) / surfaceRadius;
					fingerDistance = direction.Length();
					float length = direction.LengthSquared();
					if (length > 1)
					{
						length = 1;
					}

					currentAngleSide = Utils.getVectorAngle(direction);
					currentAngleSide = Utils.Wrap(currentAngleSide + (float)Math.PI/8.0f, 2 * (float)Math.PI);
					currentAngleSide = Utils.RadiansToDegrees(currentAngleSide);
					currentAngleSide = ((float)Math.Floor(currentAngleSide / 45));

					direction.Normalize();

					direction = Utils.getVectorFromMagnitudeAngle(length, Utils.DegreesToRadians(currentAngleSide * 45 - 90));
				}

				velocity = velocity * 0.99f;

				Vector2 targetVelocity = direction * playerEntity.MaxVelocity.NominalValue();

				Vector2 diffVelocity = targetVelocity - velocity;
				if (diffVelocity.Length() > playerEntity.AccelerationVelocity.NominalValue())
				{
					diffVelocity.Normalize();
					diffVelocity *= playerEntity.AccelerationVelocity.NominalValue();
				}

				Vector2 noMomentumVelocity = velocity + diffVelocity;

				float momentumPercentage = 0.6f;

				velocity = velocity * momentumPercentage + noMomentumVelocity * (1 - momentumPercentage);
			}
		}

        internal void OnCrash(SBaseEntity _collidedEntity, Vector2 _collisionPos)
        {
			if (_collidedEntity == null)
			{
				return;
			}
			var bodyComponent = _collidedEntity.GetComponent<PhysicsBodyComponent>();
			if (bodyComponent == null)
			{
				return;
			}
			Vector2 colliderPosition = bodyComponent.locationComponent.Position;
			Vector2 currentPosition = playerEntity.getLocation().Position;
			Vector2 normal = Vector2.Normalize(currentPosition - colliderPosition);
			velocity = Vector2.Reflect(velocity * 0.7f, normal);
        }

		UILocationComponent location;
		ActiveTouch currentActiveTouch;
		Vector2 direction = Vector2.Zero;
		float surfaceRadius = 190.0f;
		public Vector2 velocity;

		public float currentAngleSide;
		public float fingerDistance;
		PCEntity playerEntity;
		CountDownTimerComponent crashTimeout;

		KeyboardState keyboardState;
		public PlayerIndex? gamepadIndex = null;
    }

	public class MovementSurfaceEffectLogic : LogicComponent
    {
		public MovementSurfaceEffectLogic(
			MovementSurface _entity,
			MovementSurfaceBtnLogic _movementLogic,
			MovementSurfaceEffect _surfaceEffect,
			PCEntity _playerEntity,
            CountDownTimerComponent _crashTimeout)
		{
			movementLogic = _movementLogic;
			surfaceEffect = _surfaceEffect;
			playerEntity = _playerEntity;
			idleTime = new TimerComponent();
			crashTimeout = _crashTimeout;
			_entity.AddComponent(idleTime);
		}
		public override void UpdateLogic(GameTime _elapsedTime)
		{
			if (crashTimeout.State == TimerState.Running)
			{
				currentEffectMode = EffectMode.Crash;
			}
			else if (movementLogic.fingerDistance == 0)
			{
				if (idleTime.State == TimerState.Stopped)
				{
					idleTime.Start();
				}
				currentEffectMode = EffectMode.Idle;
			}
			else
			{
				idleTime.Stop();
				currentEffectMode = EffectMode.Movement;
			}

			switch(currentEffectMode)
			{
				case EffectMode.Idle:
					UpdateIdleSurfaceEffect(idleTime.CurrentTime);
					break;
				case EffectMode.Movement:
					UpdateMovementSurfaceEffect(movementLogic.fingerDistance, (int)movementLogic.currentAngleSide);
					break;
				case EffectMode.Crash:
					UpdateCrashSurfaceEffect();
					break;
			}
		}

		void UpdateIdleSurfaceEffect(TimeSpan _currentTime)
		{
			surfaceEffect.Clear(MovementSurfaceEffect.ColorType.DARK_BLUE);
			int index = (int)(_currentTime.TotalMilliseconds/ 50.0) % (4 * MovementSurfaceEffect.numRows);
			if (index < 2 * MovementSurfaceEffect.numRows)
			{
				return;
			}
			index = index % MovementSurfaceEffect.numRows;
			for(int i = 0; i < MovementSurfaceEffect.numRows; i++)
			{
				for(int j = 0; j < MovementSurfaceEffect.numCols; j++)
				{
					if (index == (MovementSurfaceEffect.numRows - i - 1))
						surfaceEffect.SetColorValue(j, i, MovementSurfaceEffect.ColorType.LIGHT_BLUE);
				}
			}
		}

		void UpdateMovementSurfaceEffect(float length, int side)
		{
			surfaceEffect.Clear();
			for(int i = 0; i < MovementSurfaceEffect.numRows; i++)
			{
				float curDistance = 1- (i /(float)MovementSurfaceEffect.numRows);
				float increment = -1.0f / (float)MovementSurfaceEffect.numRows;
				for(int j = 0; j < MovementSurfaceEffect.numCols; j++)
				{
					if (Utils.Wrap(j + 1, 8) != side)
					{
						continue;
					}
					if (curDistance <= length)
					{
						if (curDistance <= 0 || i == 0 || curDistance - increment > length)
						{
							surfaceEffect.SetColorValue(j, i, MovementSurfaceEffect.ColorType.LIGHT_RED);
						}
						else if (curDistance < 1 - playerEntity.VelocityPercentage)
						{
							surfaceEffect.SetColorValue(j, i, MovementSurfaceEffect.ColorType.LIGHT_BLUE);
						}
						else
						{
							surfaceEffect.SetColorValue(j, i, MovementSurfaceEffect.ColorType.DARK_BLUE);
						}
					}
				}
			}
		}

		void UpdateCrashSurfaceEffect()
		{
			for(int i = 0; i < MovementSurfaceEffect.numRows; i++)
			{
				float curDistance = 1- (i /(float)MovementSurfaceEffect.numRows);
				for(int j = 0; j < MovementSurfaceEffect.numCols; j++)
				{
					if (curDistance > crashTimeout.ElapsedPercentage())
					{
						surfaceEffect.SetColorValue(j, i, MovementSurfaceEffect.ColorType.LIGHT_RED);
					}
					else
					{
						surfaceEffect.SetColorValue(j, i, MovementSurfaceEffect.ColorType.DARK_BLUE);
					}
				}
			}
		}

		MovementSurfaceEffect surfaceEffect;
		TimerComponent idleTime;
		CountDownTimerComponent crashTimeout;
		MovementSurfaceBtnLogic movementLogic;
		PCEntity playerEntity;

		enum EffectMode {Idle, Movement, Crash};
		EffectMode currentEffectMode = EffectMode.Idle;
	}
}
