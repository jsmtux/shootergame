﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace shooter
{
	public class DesktopBtnEntityLogicComponent : LogicComponent
	{
		public DesktopBtnEntityLogicComponent(Keys _key)
		{
			key = _key;
		}

		public override void UpdateLogic(GameTime _elapsedTime)
		{
			KeyboardState keyboardState = Keyboard.GetState();
			state = keyboardState.IsKeyDown(key);
			if (state != prevState)
			{
				onChange?.Invoke(state);
				onePressState = state;
			}
			prevState = state;
		}

		public bool GetState()
		{
			return state;
		}

		public bool GetAndResetState()
		{
			bool ret = onePressState;
			onePressState = false;
			return ret;
		}

		public void OnStateChange(Action<bool> _action)
		{
			onChange = _action;
		}

		bool state;
		bool prevState;
		bool onePressState;
		Keys key;
		public Action<bool> onChange;
	}
}
