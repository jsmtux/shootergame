﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace shooter
{
	public class JoystickBtnLogicComponent : LogicComponent
	{
		public JoystickBtnLogicComponent(PlayerIndex _index, Buttons _button)
		{
			index = _index;
			button = _button;
		}

		public override void UpdateLogic(GameTime _elapsedTime)
		{
			GamePadState padState = GamePad.GetState(index);

			state = padState.IsButtonDown(button);

			if (state != prevState)
			{
				onChange?.Invoke(state);
				onePressState = state;
			}
			prevState = state;
		}

		public bool GetState()
		{
			return state;
		}

		public bool GetAndResetState()
		{
			bool ret = onePressState;
			onePressState = false;
			return ret;
		}

		public void OnStateChange(Action<bool> _action)
		{
			onChange = _action;
		}

		bool state;
		bool prevState;
		bool onePressState;
		PlayerIndex index;
		Buttons button;
		public Action<bool> onChange;
	}
}
