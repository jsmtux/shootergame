﻿using System;
using Microsoft.Xna.Framework;

namespace shooter
{
	public class ProjectileLogicComponent : LogicComponent
	{
		public ProjectileLogicComponent(LocationComponent _location, float _projectileSpeed, float _range)
		{
			location = _location;
			projectileSpeed = _projectileSpeed;
			originalRotation = location.Rotation;
			range = _range;
		}

		public override void UpdateLogic(GameTime _elapsedTime)
		{
			entity.GetComponent<SimplePhysicsComponent>().velocity.X =(float)(projectileSpeed * Math.Sin(originalRotation));
			entity.GetComponent<SimplePhysicsComponent>().velocity.Y =(float)(projectileSpeed * -Math.Cos(originalRotation));
			movement += (projectileSpeed / 30); //TODO: fix this! 30 should be matched with the PhysicsUpdater
			if (range > 0 && movement > range)
			{
				((ProjectileEntity)entity).Destroy(ProjectileEntity.DestructionReason.OutOfRange);
			}
		}

		LocationComponent location;
		float originalRotation;
		float projectileSpeed;
		float movement = 0;
		float range;
	}

	public class LouseProjectileLogicComponent : LogicComponent
	{
		public LouseProjectileLogicComponent(SimplePhysicsComponent _location)
		{
			location = _location;
		}

		public override void UpdateLogic(GameTime _elapsedTime)
		{
			float stepRotation = 0.1f;
			location.setRotation(location.Rotation + stepRotation);
		}

		SimplePhysicsComponent location;
	}
}
