﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace shooter
{
	public class PortalLogicComponent : LogicComponent
	{
		public PortalLogicComponent()
		{
		}

		public void Initialize(
			List<ChildPhysicsComponent> _portalComponentsLocation,
			List<ModelComponent> _portalComponentsModel,
			ChildPhysicsComponent _bgLocation,
			FloatTween _scale,
			FloatTween _alpha,
			int _missingBananas)
		{
			portalComponentsLocation = _portalComponentsLocation;
			portalComponentsModel = _portalComponentsModel;
			bgLocation = _bgLocation;
			scale = _scale;
			alpha = _alpha;
			missingBananas = _missingBananas;
		}

		public override void UpdateLogic(GameTime _elapsedTime)
		{
			if (missingBananas == 0)
			{
				UpdateActiveLogic(_elapsedTime);
			}
			else
			{
				UpdateNotActiveLogic(_elapsedTime);
			}
		}

		public void UpdateActiveLogic(GameTime _elapsedTime)
		{
			curRotation += _elapsedTime.ElapsedGameTime.Milliseconds * 0.001f;

			bgLocation.Size = scale.CurrentValue * new Vector2(150, 150);

			var currentAlpha = (scale.CurrentValue == 0.0f) ? 0.0f : 1.0f;
			alpha.SetDefault(currentAlpha);

			int numComponents = portalComponentsLocation.Count;
			for (int i = 0; i < numComponents; i++)
			{
				portalComponentsModel[i].Alpha.SetStatic(currentAlpha);
				var componentLocation = portalComponentsLocation[i];
				var angle = i * Math.PI * 2 / numComponents + curRotation;
				bool isEven = i % 2 == 0;
				float curRad = rad + (isEven ? 1 : -1) * radDif * (float)Math.Cos(curRotation * 4);
				curRad *= scale.CurrentValue;
				var offset = new Vector2(curRad * (float)Math.Cos(angle), curRad * (float)Math.Sin(angle));
				componentLocation.Offset = offset;
				componentLocation.ModelRotationOffset = new Vector3(Utils.getVectorAngle(offset) + (float)Math.PI / 2, 0, -(float)Math.PI / 2);
			}
		}

		public void UpdateNotActiveLogic(GameTime _elapsedTime)
		{
			curRotation += _elapsedTime.ElapsedGameTime.Milliseconds * 0.001f;
			alpha.SetDefault(0);

			bgLocation.Size = Vector2.Zero;

			int numComponents = portalComponentsLocation.Count;

			List<bool> shownBananas = ShownBananas();

			for (int i = 0; i < numComponents; i++)
			{
				if (shownBananas[i])
				{
					portalComponentsModel[i].Alpha.SetStatic(1.0f);
					var componentLocation = portalComponentsLocation[i];
					var angle = i * Math.PI * 2 / numComponents + curRotation;
					var offset = new Vector2(rad * (float)Math.Cos(angle), rad * (float)Math.Sin(angle));
					componentLocation.Offset = offset;
					componentLocation.ModelRotationOffset = new Vector3(Utils.getVectorAngle(offset) + (float)Math.PI / 2, 0, -(float)Math.PI / 2);
				}
				else
				{
					portalComponentsModel[i].Alpha.SetStatic(0.0f);
				}
			}

			if (shouldEmmitBananas)
			{
				if (lastEmmitedBananaTime + bananaEmissionInterval < _elapsedTime.TotalGameTime)
				{
					((PortalEntity)entity).EmitParticlesToBananas();
					lastEmmitedBananaTime = _elapsedTime.TotalGameTime;
				}
			}
			// Will be set to true in the Entity collision check
			shouldEmmitBananas = false;
		}

		List<bool> ShownBananas()
		{
			List<bool> ret = new List<bool>();
			int k = 1;
			int numComponents = portalComponentsLocation.Count;
			while (missingBananas > k)
			{
				k *= 2;
			}

			int numberActivated = 0;
			for (int i = 0; i < portalComponentsLocation.Count; i++)
			{
				if (i % (int)Math.Ceiling(numComponents / (float)k) == 0 && numberActivated < missingBananas)
				{
					numberActivated++;
					ret.Add(false);
				}
				else
				{
					ret.Add(true);
				}
			}

			return ret;
		}

		readonly float rad = 100f;
		readonly float radDif = 15f;
		float curRotation = 0;
		List<ChildPhysicsComponent> portalComponentsLocation;
		List<ModelComponent> portalComponentsModel;
		ChildPhysicsComponent bgLocation;

		public int missingBananas;
		public bool shouldEmmitBananas = false;
		TimeSpan lastEmmitedBananaTime = TimeSpan.Zero;
		readonly TimeSpan bananaEmissionInterval = TimeSpan.FromMilliseconds(1000);

		FloatTween scale;
		FloatTween alpha;
	}
}
