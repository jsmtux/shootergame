﻿using System;
using Microsoft.Xna.Framework;

namespace shooter
{
	public class FoodLogicComponent : LogicComponent
	{
		public FoodLogicComponent(SimplePhysicsComponent _location, FloatTween _alphaTween)
		{
			location = _location;
			alphaTween = _alphaTween;

			Random rnd = new Random();
			stepRotation = new Vector3((float)rnd.NextDouble() * 0.02f, 0, 0);

			restartTween();
		}

		public override void UpdateLogic(GameTime _elapsedTime)
		{
			Vector3 originalRotation = location.ModelRotation;
			originalRotation += stepRotation;

			location.setModelRotation(originalRotation);
		}

		private void restartTween()
		{
			alphaTween.onEnd = () =>
			{
				if (!alive)
				{
					return;
				}
				alphaTween.onEnd = restartTween;
				alphaTween.Start(200f, 150f, TimeSpan.FromSeconds(1.5f), ScaleFuncs.Linear);
			};
			if (!alive)
			{
				return;
			}
			alphaTween.Start(150f, 200f, TimeSpan.FromSeconds(1.5f), ScaleFuncs.CubicEaseIn);
		}

		public void die()
		{
			alive = false;
			alphaTween.onEnd = () =>
			{
				alphaTween.Start(alphaTween.CurrentValue, 0.0f, TimeSpan.FromSeconds(0.15f), ScaleFuncs.CubicEaseIn);
			};
			alphaTween.Start(alphaTween.CurrentValue, 300.0f, TimeSpan.FromSeconds(0.25f), ScaleFuncs.CubicEaseOut);
		}

		SimplePhysicsComponent location;
		Vector3 stepRotation;
		FloatTween alphaTween;
		bool alive = true;
	}
}
