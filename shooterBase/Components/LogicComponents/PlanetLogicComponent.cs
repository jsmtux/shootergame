﻿using System;
using Microsoft.Xna.Framework;

namespace shooter
{
	public class PlanetLogicComponent : LogicComponent
	{
		public PlanetLogicComponent(SimplePhysicsComponent _planetLocation, float _rotationSpeed)
		{
			planetLocation = _planetLocation;
			rotationIncrement = new Vector3(0, 0, _rotationSpeed);
		}

		public override void UpdateLogic(GameTime _elapsedTime)
		{
			Vector3 curRotation = planetLocation.ModelRotation;
			curRotation += rotationIncrement;
			planetLocation.setModelRotation(curRotation);
		}

		SimplePhysicsComponent planetLocation;
		Vector3 rotationIncrement;
	}
}
