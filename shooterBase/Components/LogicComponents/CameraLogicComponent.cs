﻿using System;
using Microsoft.Xna.Framework;

namespace shooter
{
    public class SetAreaCameraLogicComponent : LogicComponent
    {
		public SetAreaCameraLogicComponent(SimplePhysicsComponent _cameraLocationComponent,
		                            	   CameraComponent _camera,
										   Platform _platform)
		{
            cameraPosition = _cameraLocationComponent;
            camera = _camera;
			platform = _platform;
			UpdateOnPause = true;			
		}

        public override void UpdateLogic(GameTime _elapsedTime)
        {
			if (zoomTween.State == TimerState.Running)
			{
				zoomTween.Update(_elapsedTime.ElapsedGameTime);
				positionTween.Update(_elapsedTime.ElapsedGameTime);
				camera.zoom = zoomTween.CurrentValue;
				cameraPosition.Position = positionTween.CurrentValue;
			}
        }

		private float GetZoneDesiredZoom(Rectangle _zone)
		{
			return platform.Resolution.Y / (float)_zone.Height;
		}

		public void GoTowardsZone(Rectangle _rectangle, TimeSpan _time, ScaleFunc _scaleFunc, Action _onAreaSet)
		{
			isActive = true;

			zoomTween.Start(camera.zoom, GetZoneDesiredZoom(_rectangle), _time, _scaleFunc);
			positionTween.Start(cameraPosition.Position, _rectangle.Center.ToVector2(), _time, _scaleFunc);
			positionTween.onEnd = _onAreaSet;

			onAreaSet = _onAreaSet;
		}

		public void GoToZone(Rectangle _zone)
		{
			cameraPosition.Position = (_zone.Center.ToVector2());
			camera.zoom = GetZoneDesiredZoom(_zone);
		}

		public void Reset()
		{
			zoomTween.Stop(StopBehavior.ForceComplete);
			positionTween.Stop(StopBehavior.ForceComplete);
		}

		Platform platform;
        CameraComponent camera;
        SimplePhysicsComponent cameraPosition;

		public bool isActive = false;
		public bool Active {set {isActive = value;}}

		FloatTween zoomTween = new FloatTween();
		Vector2Tween positionTween = new Vector2Tween();

		Action onAreaSet;
    }

    public class FollowShipCameraLogicComponent : LogicComponent
    {
		public FollowShipCameraLogicComponent(SimplePhysicsComponent _cameraLocationComponent,
		                            SimplePhysicsComponent _steeringPhysicsComponent,
		                            CameraComponent _camera,
									Platform _platform)
        {
            cameraPosition = _cameraLocationComponent;
			steeringPhysicsComponent = _steeringPhysicsComponent;
            camera = _camera;
			shipLocation = new SimplePhysicsComponent(cameraPosition.Position);
			platform = _platform;
			UpdateOnPause = false;
        }

		public override void UpdateLogic(GameTime _elapsedTime)
		{
			if (isActive && shipLocation != null)
			{
				Vector2 velocityPercentage = shipLocation.velocity / 500; // Hack, max PCEntity velocity

				Vector2 resolution = platform.Resolution.ToVector2();

				Vector2 target = shipLocation.Position + velocityPercentage * resolution * 0.3f;

				Vector2 movement = GetShipMovementTowardsAndCallBack(_elapsedTime, target);

				cameraPosition.Position = movement + cameraPosition.Position + getShakeOffset(_elapsedTime);

				if(camera.zoom - Zoom > zoomSpeed)
				{
					camera.zoom -= zoomSpeed;
				}
				else if (Zoom - camera.zoom > zoomSpeed)
				{
					camera.zoom += zoomSpeed;
				}
			}
		}

		private Vector2 GetShipMovementTowardsAndCallBack(GameTime _elapsedTime, Vector2 _target)
		{
			Vector2 vectorToTarget = _target - cameraPosition.Position;

			if (vectorToTarget.Length() > cameraAccel.NominalValue())
			{
				vectorToTarget.Normalize();
				
				vectorToTarget *= cameraAccel.NominalValue();
			}

			cameraSpeed *= 0.9f;
			cameraSpeed = vectorToTarget * 0.5f + cameraSpeed * 0.5f;
/*
			if (cameraSpeed.LengthSquared() > maxCameraSpeed.NominalValue() * maxCameraSpeed.NominalValue())
			{
				cameraSpeed.Normalize();
				cameraSpeed *= maxCameraSpeed.NominalValue();
			}*/

			return cameraSpeed;
		}

		public Vector2 getShakeOffset(GameTime _elapsedTime)
        {
            Vector2 ret = Vector2.Zero;

			if (timeToShake.TotalMilliseconds > 0)
			{
				timeToShake -= _elapsedTime.ElapsedGameTime;
				currentWaveTime += _elapsedTime.ElapsedGameTime;

				float currentWaveCompletion = ((float)currentWaveTime.TotalMilliseconds - shakeCadenceMs) / shakeCadenceMs;
				if (currentWaveCompletion > 1.0)
				{
					prevShakePos = nextShakePos;
					Random rnd = new Random();
					float shakeCompletion = (totalShakeDuration - (float)timeToShake.TotalMilliseconds) / totalShakeDuration;
					float maxDisplacement = shakeAmplitude * (1.0f - shakeCompletion * shakeCompletion);
					nextShakePos = new Vector2((float)(rnd.NextDouble() - 0.5f )* maxDisplacement, (float)(rnd.NextDouble() - 0.5f ) * maxDisplacement);
					currentWaveCompletion = 0;
				}
				ret = currentWaveCompletion * nextShakePos + (1.0f - currentWaveCompletion) * prevShakePos;
			}

            return ret;
        }

		public void SetShipLocation(LocationComponent _position)
		{
			shipLocation = _position;
		}
		
		public void Shake(TimeSpan _duration)
        {
			timeToShake = _duration;
			totalShakeDuration = (int)_duration.TotalMilliseconds;
			currentWaveTime = TimeSpan.Zero;
			nextShakePos = Vector2.Zero;
			currentWaveTime = TimeSpan.FromMilliseconds(shakeCadenceMs);
        }

		TimeSpan timeToShake = TimeSpan.Zero;
		int totalShakeDuration;

        Vector2 prevShakePos;
		Vector2 nextShakePos;
		TimeSpan currentWaveTime;

        readonly static int shakeCadenceMs = 50;
		readonly static int shakeAmplitude = 40;

		LocationComponent shipLocation;

		SimplePhysicsComponent steeringPhysicsComponent;

		Vector2 cameraSpeed = Vector2.Zero;
		readonly static Velocity maxCameraSpeed = new Velocity(400);
		readonly static Velocity cameraAccel = new Velocity(40);
		readonly static float zoomSpeed = 0.05f;
		Platform platform;

        CameraComponent camera;
        SimplePhysicsComponent cameraPosition;

		public bool isActive = false;
		public bool Active {set {isActive = value;}}

		public readonly static float DefaultZoom = 1f;
		public float Zoom = DefaultZoom;
	}
}
