using System;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace shooter
{
	public class ShooterLogicComponent : LogicComponent
	{
		public ShooterLogicComponent(SimplePhysicsComponent _location,
		                             Stage _stage,
		                          	 List<Vector2> _cannonPositions,
		                          	 DrawableComponent _fireDrawable,
		                          	 SimplePhysicsComponent _fireDrawableLocation,
									 SoundEffectComponent _shootSoundComponent)
		{
			location = _location;
			cannonPositions = _cannonPositions;
			fireDrawable = _fireDrawable;
			fireDrawable.Alpha = new TweenWrapper<float>(0);
			fireDrawableLocation = _fireDrawableLocation;
			shootSoundComponent = _shootSoundComponent;
		}

		protected void DoShootEffects(Vector2 _projectilePosition, float _recoilStrength)
		{
			fireDrawableLocation.Position = _projectilePosition;
			fireDrawableLocation.setRotation(location.Rotation);
			fireDrawable.Alpha.SetStatic(1.0f);

			location.velocity += new Vector2((float)-Math.Sin(location.Rotation) * _recoilStrength, (float)Math.Cos(location.Rotation) * _recoilStrength);
		}

		public override void UpdateLogic(GameTime _elapsedTime)
		{
			if (fireDrawable.Alpha > 0)
			{
				fireDrawable.Alpha.SetStatic(fireDrawable.Alpha.CurrentValue() - 0.2f);
			}
		}

		DrawableComponent fireDrawable;
		protected SoundEffectComponent shootSoundComponent;
		SimplePhysicsComponent fireDrawableLocation;

		protected List<Vector2> cannonPositions;
		protected SimplePhysicsComponent location;
	}

	public class PlayerShooterLogicComponent : ShooterLogicComponent
	{
		public PlayerShooterLogicComponent(
			SimplePhysicsComponent _location,
			Stage _stage,
			List<Vector2> _cannonPositions,
			DrawableComponent _fireDrawable,
			SimplePhysicsComponent _fireDrawableLocation,
			SoundEffectComponent _shootSoundComponent)
			: base(_location, _stage, _cannonPositions, _fireDrawable, _fireDrawableLocation, _shootSoundComponent)
		{
			projectileSpawnerComponent = new SpawnerComponent<PlayerProjectileEntity>(_stage);
			seedSpawnerComponent = new SpawnerComponent<PlayerSeedEntity>(_stage);
		}

		public void Shoot(AmmoType _type)
		{
			Vector2 projectilePosition1 = location.Position + Utils.rotateVector(cannonPositions[0], location.Rotation);
			Vector2 projectilePosition2 = location.Position + Utils.rotateVector(cannonPositions[1], location.Rotation);

			float recoilStrength = 0;
			ProjectileEntity newProjectile;

			switch (_type)
			{
				case AmmoType.Carrot:
					newProjectile = createCarrotProjectile(projectilePosition1);
					newProjectile = createCarrotProjectile(projectilePosition2);
					recoilStrength = 20;
					break;
			}

			DoShootEffects(projectilePosition1, recoilStrength);
			DoShootEffects(projectilePosition2, recoilStrength);
			shootSoundComponent.Play();
		}

		public ProjectileEntity createCarrotProjectile(Vector2 _pos)
		{
			ProjectileEntity ret = (ProjectileEntity)projectileSpawnerComponent.spawn();
			ret.initialize(_pos, location.Rotation, entity);
			return ret;
		}

		public ProjectileEntity createSeedProjectile(Vector2 _pos)
		{
			PlayerSeedEntity ret = (PlayerSeedEntity)seedSpawnerComponent.spawn();
			ret.initialize(_pos, location.Rotation, entity);
			return ret;
		}

		SpawnerComponent<PlayerProjectileEntity> projectileSpawnerComponent;
		SpawnerComponent<PlayerSeedEntity> seedSpawnerComponent;
	}

	public class EnemyShooterLogicComponent : ShooterLogicComponent
	{
		public EnemyShooterLogicComponent(
			SimplePhysicsComponent _location,
			Stage _stage,
			List<Vector2> _cannonPositions,
			DrawableComponent _fireDrawable,
			SimplePhysicsComponent _fireDrawableLocation,
			SoundEffectComponent _shootSoundComponent)
			: base(_location, _stage, _cannonPositions, _fireDrawable, _fireDrawableLocation, _shootSoundComponent)
		{
			projectileSpawnerComponent = new SpawnerComponent<LouseProjectileEntity>(_stage);
		}

		public void Shoot()
		{
			Vector2 projectilePosition = location.Position + Utils.rotateVector(cannonPositions[0], location.Rotation);

			ProjectileEntity newProjectile;
			newProjectile = createProjectile(projectilePosition);
			DoShootEffects(projectilePosition, 0);
			shootSoundComponent.Play();
		}

		public ProjectileEntity createProjectile(Vector2 _pos)
		{
			ProjectileEntity ret = (ProjectileEntity)projectileSpawnerComponent.spawn();
			ret.initialize(_pos, location.Rotation, entity);
			return ret;
		}

		SpawnerComponent<LouseProjectileEntity> projectileSpawnerComponent;
	}
}
