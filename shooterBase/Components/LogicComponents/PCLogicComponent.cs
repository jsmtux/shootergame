﻿using System;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace shooter
{
	public class PCLogicComponent : LogicComponent
	{
		public PCLogicComponent(SimplePhysicsComponent _location,
								PlayerShooterLogicComponent _shooterLogicComponent,
								ParticleEmitterComponent _smokeEmitter,
								FloatTween _shieldIntensity,
								ControllerEntity _controller,
		                        IMessagingSystem _messagingSystem,
								SoundEffectComponent _collisionSoundComponent,
								FloatTween _cloackIntensity,
                                SpawnerComponent<CollisionEffectEntity> _collisionEffectSpawner)
		{
			location = _location;
			shooterLogicComponent = _shooterLogicComponent;
			smokeEmitter = _smokeEmitter;
			shieldIntensity = _shieldIntensity;
			controller = _controller;
			fireTimer = new Stopwatch();
			messagingSystem = _messagingSystem;
			collisionSoundComponent = _collisionSoundComponent;
			cloackIntensity = _cloackIntensity;
			collisionEffectSpawner = _collisionEffectSpawner;
		}

		public override void UpdateLogic(GameTime _elapsedTime)
		{
			location.velocity = controller.getVelocity();
			float currentRotation =  Utils.Wrap(location.Rotation, (float)Math.PI, (float)-Math.PI);
			float desiredRotation = Utils.Wrap(Utils.getVectorAngle(location.velocity), (float)Math.PI, (float)-Math.PI);
			location.setRotation(desiredRotation);

			float targetRoll = (desiredRotation - currentRotation)  * 6;
			if (targetRoll > shipRoll)
			{
				shipRoll += shipRollSpeed;
				if (shipRoll > targetRoll)
				{
					shipRoll = targetRoll;
				}
			}
			else
			{
				shipRoll -= shipRollSpeed;
				if (shipRoll < targetRoll)
				{
					shipRoll = targetRoll;
				}
			}

			Vector3 curRotation = location.ModelRotation;
			curRotation.Z = shipRoll;
			location.setModelRotation(curRotation);

			if (controller.slot != null && controller.slot.GetState())
			{
				if (!fireTimer.IsRunning || fireTimer.ElapsedMilliseconds > fireCadenceMs)
				{
					fireTimer.Restart();
					if (controller.slot.CurrentAmmoType == AmmoType.Melon)
					{
						SetCloacked(true);
					}
					else
					{
						shooterLogicComponent.Shoot(controller.slot.CurrentAmmoType);
						messagingSystem.SendMessage(new PlayerShotMessage(controller.slot.CurrentAmmoType));
					}
					controller.slot.ModifyAmount(-1, controller.slot.CurrentAmmoType);
				}
			}

			float velocityPercentage = ((PCEntity)entity).VelocityPercentage;
			smokeEmitter.cadence = TimeSpan.FromMilliseconds(minCadenceSmokeEmitter + velocityPercentage * velocityPercentage * maxCadenceSmokeEmitter);
		}

		public void SetCloacked(bool _cloacked)
		{
			if (_cloacked)
			{
				cloackIntensity.Start(0.0f, 1.0f, TimeSpan.FromSeconds(1), ScaleFuncs.CubicEaseOut);
			}
			else
			{
				cloackIntensity.Start(1.0f, 0.0f, TimeSpan.FromSeconds(1), ScaleFuncs.CubicEaseOut);
			}
		}

		public void die()
		{
			smokeEmitter.setEmitting(false);
		}

		SimplePhysicsComponent location;
		ParticleEmitterComponent smokeEmitter;
		ControllerEntity controller;
		Stopwatch fireTimer;
		FloatTween cloackIntensity;

		FloatTween shieldIntensity;

		SoundEffectComponent collisionSoundComponent;
		SpawnerComponent<CollisionEffectEntity> collisionEffectSpawner;

		readonly int fireCadenceMs = 150;

		float shipRoll = 0.0f;
		readonly float shipRollSpeed = 0.25f;		

		readonly int minCadenceSmokeEmitter = 4;
		readonly int maxCadenceSmokeEmitter = 400;

		IMessagingSystem messagingSystem;
		PlayerShooterLogicComponent shooterLogicComponent;
	}
}
