﻿using System;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace shooter
{
	public class NachoEnemyLogicComponent : EnemyLogicComponent
    {
		public NachoEnemyLogicComponent(
            SimplePhysicsComponent _location,
            EnemyTarget _target,
            SteeringComponent _steering,
            ColorTween _drawableTint,
            SoundEffectComponent _dyingSoundComponent)
            : base(_location, _target, _steering)
		{
			location = _location;
			drawableTint = _drawableTint;
			dyingSoundComponent = _dyingSoundComponent;

			AssignNewBehavior(new WaitingEnemyBehavior(this));
			AddRegularAction(UpdateBehaviorVariables);
		}

		internal ColorTween drawableTint;
		SoundEffectComponent dyingSoundComponent;

		internal float sightDistance = 1000;

		int health = 100;

		TimeSpan roundTime = TimeSpan.Zero;
		TimeSpan fireTime = TimeSpan.Zero;
	}
}
