﻿using System;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace shooter
{
	public class SodaEnemyLogicComponent : BehavioralLogicComponent
	{
		public SodaEnemyLogicComponent(
		                           LocationComponent _target, 
		                           SimplePhysicsComponent _location,
		                           SteeringComponent _steering,
								   WanderingSteeringComponent _wanderingSteering,
		                           ColorTween _drawableTint,
								   SoundEffectComponent _dyingSoundComponent)
		{
			target = _target;
			steering = _steering;
			wanderingSteering = _wanderingSteering;
			location = _location;
			drawableTint = _drawableTint;
			dyingSoundComponent = _dyingSoundComponent;

			//AssignNewBehavior(new AgressiveEnemyBehavior(this));
			//AssignNewBehavior(new WanderingEnemyBehavior(this));
			AddRegularAction(UpdateFiringLogic);
			AddRegularAction(UpdateBehaviorVariables);
		}

		void UpdateBehaviorVariables(GameTime _elapsedTime)
		{
			distanceToTarget = (location.Position - target.Position).Length();
		}

		void UpdateFiringLogic(GameTime _elapsedTime)
		{
			if (isShooting || roundTime.TotalMilliseconds > 0)
			{
				roundTime += _elapsedTime.ElapsedGameTime;
				fireTime -= _elapsedTime.ElapsedGameTime;

				int roundsMSTime = shotsInRound * fireCadenceMs;
				if (roundTime.TotalMilliseconds < roundsMSTime)
				{
					if (fireTime.TotalMilliseconds <= 0)
					{
						shooterLogicComponent.Shoot();
						fireTime = TimeSpan.FromMilliseconds(fireCadenceMs);
					}
				}
				else if (roundTime.TotalMilliseconds > roundsMSTime + fireRoundCadenceMs)
				{
					roundTime = TimeSpan.Zero;
					fireTime = TimeSpan.Zero;
				}
			}
		}

		public Vector2? collided(SBaseEntity _e, IMessagingSystem _messagingSystem, BurgerEnemyEntity _entity)
		{
			Vector2? colliderPosition = null;
			ProjectileEntity projectile = _e as ProjectileEntity;
			if (projectile != null)
			{
				var shooter = projectile.getShooter();
				if (shooter != _entity)
				{
					health -= 50;
					drawableTint.Start(Color.Red, Color.Black, 500f, ScaleFuncs.CubicEaseIn);

					if (health <= 0)
					{
						if (shooter is PCEntity)
						{
							_messagingSystem.SendMessage(new EnemyKilledMessage(_entity));
							dyingSoundComponent.Play();
							_entity.dropHeart();
						}
						colliderPosition = projectile.Location.Position;
					}
				}
			}
			else if (_e is PCEntity pc)
			{
				_messagingSystem.SendMessage(new EnemyKilledMessage(_entity));
				dyingSoundComponent.Play();
				colliderPosition = pc.getLocation().Position;

			}

			if (colliderPosition != null)
			{
				return _entity.getLocation().Position - colliderPosition.GetValueOrDefault();
			}

			return null;
		}

		internal bool isTargetting()
		{
			Vector2 desiredTrajectory = target.Position - location.Position;
			float angle = (float)Math.Atan2(desiredTrajectory.X, -desiredTrajectory.Y);
			float difference = angle - location.Rotation;
			return Math.Abs(difference) < targettingAngle;
		}

		internal bool isShooting = false;
		internal SimplePhysicsComponent location;
		internal LocationComponent target;
		internal SteeringComponent steering;
		internal WanderingSteeringComponent wanderingSteering;
		internal ColorTween drawableTint;
		SoundEffectComponent dyingSoundComponent;

		int fireRoundCadenceMs = 2000;
		int shotsInRound = 3;
		int fireCadenceMs = 400;

		float targettingAngle = 0.2f;
		internal float sightDistance = 1000;

		int health = 100;

		internal float distanceToTarget;

		TimeSpan roundTime = TimeSpan.Zero;
		TimeSpan fireTime = TimeSpan.Zero;

		ShooterLogicComponent shooterLogicComponent;
	}
}
