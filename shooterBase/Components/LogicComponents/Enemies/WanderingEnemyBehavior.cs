﻿using System;
using Microsoft.Xna.Framework;

namespace shooter
{
	class WanderingEnemyBehavior : Behavior
	{
		public WanderingEnemyBehavior(BurgerEnemyLogicComponent _unit)
		{
			unit = _unit;
		}

		public override void DoStep(GameTime _elapsedTime)
		{
			unit.isShooting = false;
			unit.wanderingSteering.Wander(_elapsedTime);
			
			unit.burgerAnimation.Play("idle");
		}

		public override int GetPriority(GameTime _elapsedTime)
		{
			return 10;
		}

		BurgerEnemyLogicComponent unit;
	}
}
