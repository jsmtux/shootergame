using System;
using Microsoft.Xna.Framework;

namespace shooter
{
	public class KnockedOutEnemyBehavior : Behavior
	{
		public KnockedOutEnemyBehavior(BurgerEnemyLogicComponent _unit)
		{
			unit = _unit;
		}

		public override void DoStep(GameTime _elapsedTime)
		{
			unit.steering.RotateAroundCenter(_elapsedTime);
			
			unit.burgerAnimation.Play("idle");
		}

		public override int GetPriority(GameTime _elapsedTime)
		{
            if(_elapsedTime.TotalGameTime < knockedOutUntil)
            {
                return 100;
            }
            else
            {
                return 0;
            }
		}

		public void SetKnockedOutUntil(TimeSpan _knockedOutUntil)
		{
			knockedOutUntil = _knockedOutUntil;
		}

		BurgerEnemyLogicComponent unit;
		TimeSpan knockedOutUntil;
	}
}
