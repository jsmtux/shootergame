﻿using System;
using Microsoft.Xna.Framework;

namespace shooter
{
	class AgressiveEnemyBehavior : Behavior
	{
		public AgressiveEnemyBehavior(BurgerEnemyLogicComponent _unit)
		{
			unit = _unit;
		}

		public override void DoStep(GameTime _elapsedTime)
		{
			unit.steering.SteerInDirection(startTargetDirection, _elapsedTime);

			unit.isShooting = unit.isTargetting();
			
			unit.burgerAnimation.Play("eat");
		}

		public override int GetPriority(GameTime _elapsedTime)
		{
			int ret = 0;
			if (!unit.target.IsCloacked() && 
			    (unit.distanceToTarget < 350 ||
				 (unit.LastUsedBehavior == this && _elapsedTime.TotalGameTime - startTime < pursueTime)))
			{
				ret = 90;
				if (unit.LastUsedBehavior != this)
				{
					startTime = _elapsedTime.TotalGameTime;
					startTargetDirection = unit.target.GetLocation().Position - unit.location.Position;
				}
			}
			return ret;
		}

		BurgerEnemyLogicComponent unit;
		TimeSpan pursueTime = TimeSpan.FromSeconds(2);

		TimeSpan startTime;
		Vector2 startTargetDirection;
	}
}
