﻿using System;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace shooter
{
	public class BurgerEnemyLogicComponent : EnemyLogicComponent
    {
		public BurgerEnemyLogicComponent(
            SimplePhysicsComponent _location,
            EnemyTarget _target,
            SteeringComponent _steering,
            WanderingSteeringComponent _wanderingSteering,
            ColorTween _drawableTint,
            SoundEffectComponent _dyingSoundComponent,
			AnimatorComponent _burgerAnimation)
            : base(_location, _target, _steering)
		{
			location = _location;
			wanderingSteering = _wanderingSteering;
			drawableTint = _drawableTint;
			dyingSoundComponent = _dyingSoundComponent;
			burgerAnimation = _burgerAnimation;

			AssignNewBehavior(new AgressiveEnemyBehavior(this));
			AssignNewBehavior(new WanderingEnemyBehavior(this));
			knockedOutBehavior = new KnockedOutEnemyBehavior(this);
			AssignNewBehavior(knockedOutBehavior);
			AddRegularAction(UpdateBehaviorVariables);
		}

		public Vector2? collided(SBaseEntity _e, IMessagingSystem _messagingSystem, BurgerEnemyEntity _entity)
		{
			Vector2? colliderPosition = null;
			ProjectileEntity projectile = _e as ProjectileEntity;
			float knockedOutTime = 0.0f;
			if (projectile != null)
			{
				if (projectile.ProducesPlayerDestruction())
				{
					health -= 50;
					drawableTint.Start(Color.Red, Color.Black, TimeSpan.FromSeconds(0.5f), ScaleFuncs.CubicEaseIn);
				}
				colliderPosition = projectile.Location.Position;
				knockedOutTime = 1.0f;
				_entity.ReactToCollision(_e);
			}
			else if (_e is PCEntity pc)
			{
				//health -= 5;
				colliderPosition = pc.getLocation().Position;
			}
			else if(_e is RockEntity || _e is FenceEntity)
			{
				knockedOutTime = 0.2f;
			}
			knockedOutBehavior.SetKnockedOutUntil(lastUpdateTime.TotalGameTime + TimeSpan.FromSeconds(knockedOutTime));
			
			if (health <= 0)
			{
				_messagingSystem.SendMessage(new EnemyKilledMessage(_entity));
				dyingSoundComponent.Play();
				_entity.dropHeart();
				_entity.die();
			}

			if (colliderPosition != null)
			{
				return _entity.getLocation().Position - colliderPosition.GetValueOrDefault();
			}

			return null;
		}

		internal override bool isTargetting()
		{
			Vector2 desiredTrajectory = target.GetLocation().Position - location.Position;
			float angle = (float)Math.Atan2(desiredTrajectory.X, -desiredTrajectory.Y);
			float difference = angle - location.Rotation;
			bool inAngle = Math.Abs(difference) < targettingAngle;

			Vector2 distance = location.Position - target.GetLocation().Position;
			bool inRange = distance.Length() <= sightDistance;

			return inAngle && inRange;
		}

		internal ColorTween drawableTint;
		SoundEffectComponent dyingSoundComponent;

		float targettingAngle = 0.2f;
		internal float sightDistance = 1000;

		int health = 200;

		TimeSpan roundTime = TimeSpan.Zero;
		TimeSpan fireTime = TimeSpan.Zero;
		KnockedOutEnemyBehavior knockedOutBehavior;

		internal WanderingSteeringComponent wanderingSteering;
		internal AnimatorComponent burgerAnimation;
	}
}
