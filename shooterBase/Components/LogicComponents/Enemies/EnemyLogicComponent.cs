﻿using System;
using Microsoft.Xna.Framework;

namespace shooter
{
public class EnemyLogicComponent : BehavioralLogicComponent
	{
		public EnemyLogicComponent(
			SimplePhysicsComponent _location,
			EnemyTarget _target,
			SteeringComponent _steering)
		{
			target = _target;
			steering = _steering;
		}

		internal void UpdateBehaviorVariables(GameTime _elapsedTime)
		{
			var difference = (location.Position - target.GetLocation().Position);
			distanceToTarget = difference.Length();
			angleToTarget = Utils.getVectorAngle(difference);
			lastUpdateTime = _elapsedTime;
		}

		internal virtual bool isTargetting()
		{
			return false;
		}

		internal bool isShooting = false;
		internal EnemyTarget target;
		internal SteeringComponent steering;
		internal SimplePhysicsComponent location;

		protected GameTime lastUpdateTime;

		internal float distanceToTarget;
		internal float angleToTarget;
	}

}
