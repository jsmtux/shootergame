﻿using System;
using Microsoft.Xna.Framework;

namespace shooter
{
	public class WaitingEnemyBehavior : Behavior
	{
		public WaitingEnemyBehavior(EnemyLogicComponent _unit)
		{
			unit = _unit;
		}

		public override void DoStep(GameTime _elapsedTime)
		{
			Console.WriteLine("Enemy behavior activated");
			unit.steering.SteerInDirection(new Vector2(0, -100), _elapsedTime);
		}

		public override int GetPriority(GameTime _elapsedTime)
		{
			int ret = 0;
			if (unit.LastUsedBehavior == this)
			{
				ret = 100;
			}
			else if (!unit.target.IsCloacked() && Math.Abs(unit.angleToTarget) > 2.9)
			{
				ret = 100;
			}
			return ret;
		}

		EnemyLogicComponent unit;
	}
}
