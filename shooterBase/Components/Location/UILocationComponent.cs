﻿using System;
using Microsoft.Xna.Framework;
using System.Diagnostics;

namespace shooter
{
	public class UILocationComponent : LocationComponent
	{

		public UILocationComponent(Vector2 _position, AnchorPoint _anchorPoint = AnchorPoint.TopLeft)
		{
			position = new TweenWrapper<Vector2>(_position);
			modelRotation = new TweenWrapper<Vector3>(Vector3.Zero);
			anchorPoint = _anchorPoint;
		}

		public override Vector2 Position
		{
			get
			{
				Vector2 ret = Vector2.Zero;

				Vector2 currentPosition = position.CurrentValue();
				switch (anchorPoint)
				{
					case AnchorPoint.TopLeft:
						ret = currentPosition;
						break;
					case AnchorPoint.TopCenter:
						ret = new Vector2(Resolution.X, 0) / 2 + currentPosition;
						break;
					case AnchorPoint.TopRight:
						ret = new Vector2(Resolution.X, 0) + new Vector2(-currentPosition.X, currentPosition.Y);
						break;
					case AnchorPoint.BottomLeft:
						ret = new Vector2(0, Resolution.Y) + new Vector2(currentPosition.X, -currentPosition.Y);
						break;
					case AnchorPoint.BottomCenter:
						ret = new Vector2(Resolution.X / 2, Resolution.Y) + new Vector2(currentPosition.X, -currentPosition.Y);
						break;
					case AnchorPoint.BottomRight:
						ret = new Vector2(Resolution.X, Resolution.Y) - currentPosition;
						break;
					case AnchorPoint.Center:
						ret = new Vector2(Resolution.X, Resolution.Y) / 2 + currentPosition;
						break;
				}
				return ret;
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		public override Vector2 Size { get { return size; } set { size = value; } }

		public override Vector3 ModelRotation { get { return modelRotation; } }

		public void SetDrawable(DrawableComponent _drawable)
		{
			drawable = _drawable;
		}

		public static void SetPlatform(Platform _platform)
		{
			platform = _platform;
		}

		public TweenWrapper<Vector2> position;

		AnchorPoint anchorPoint;
		static public Point Resolution
		{
			get { return platform.Resolution; }
		}

		static Platform platform;

		public enum AnchorPoint { TopLeft, TopRight, TopCenter, BottomLeft, BottomRight, BottomCenter, Center};

		public TweenWrapper<Vector3> modelRotation;

		DrawableComponent drawable;
		Vector2 size;
	}
}
