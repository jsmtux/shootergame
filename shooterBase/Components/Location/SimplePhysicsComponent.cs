﻿using System;
using Microsoft.Xna.Framework;

namespace shooter
{
	public class SimplePhysicsComponent : LocationComponent
	{
		public SimplePhysicsComponent(Vector2 _position, float _rotation = 0, Vector2? _size = null)
		{
			position = new TweenWrapper<Vector2>(_position);
			size = _size.GetValueOrDefault();
			rotation.X = _rotation;
		}

		public override Vector2 Position { get { return position.CurrentValue(); } set { if(!position.IsTweened()) position.SetStatic(value); } }
		public TweenWrapper<Vector2> PositionTween { get { return position; } set { position = value; } }
		public override Vector2 Size { get { return size; } set { size = value; }}
		public override Vector3 ModelRotation { get { return rotation; } }

		public void setRotation(float _rotation)
		{
			rotation.X = _rotation;
		}

		public void setModelRotation(Vector3 _rotation)
		{
			rotation = _rotation;
		}

		private TweenWrapper<Vector2> position;
		private Vector2 size;
		private Vector3 rotation;
	}
}
