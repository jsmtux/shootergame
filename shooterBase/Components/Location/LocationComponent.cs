﻿using System;
using System.Diagnostics.Contracts;
using Microsoft.Xna.Framework;

namespace shooter
{
	public abstract class LocationComponent : SBaseComponent
	{
		protected LocationComponent()
		{
			Anchors = new AnchorProxy(this);
		}

		public abstract Vector2 Position { get; set; }
		public abstract Vector2 Size { get; set; }
		public abstract Vector3 ModelRotation { get; }

		public virtual void InitializeSize(Vector2 _size)
		{
			if (Size == Vector2.Zero)
			{
				Size = _size;
			}
		}

		public AnchorProxy Anchors;

		public float Rotation { get { return ModelRotation.X;} }
		public Vector2 velocity = Vector2.Zero;
	}

	public class AnchoredLocationComponent : LocationComponent
	{
		public AnchoredLocationComponent(LocationComponent _location, AnchorProxy.Anchor _anchor)
		{
			location = _location;
			anchor = _anchor;
		}

		public override Vector2 Position { get { return location.Anchors.Get(anchor); } 
			set {location.Position = value;} }
		public override Vector2 Size { get { return location.Size; } set {
				location.Size = value;
			} }

		public override Vector3 ModelRotation
		{
			get { return location.ModelRotation; }
		}

		LocationComponent location;
		AnchorProxy.Anchor anchor;
	}

	public class AnchorProxy
	{
		public AnchorProxy(LocationComponent _parentEntity)
		{
			parentEntity = _parentEntity;
		}

		public enum Anchor { TopLeft, TopRight, BottomLeft, BottomRight };

		internal Vector2 Get(Anchor _anchor)
		{
			switch (_anchor)
			{
				case Anchor.TopLeft:
					return TopLeft;
				case Anchor.TopRight:
					return TopRight;
				case Anchor.BottomLeft:
					return BottomLeft;
				case Anchor.BottomRight:
					return BottomRight;
			}
			throw new Exception();
		}

		internal void Set(Anchor _anchor, Vector2 _value)
		{
			switch (_anchor)
			{
				case Anchor.TopLeft:
					TopLeft = _value;
					break;
				case Anchor.TopRight:
					TopRight = _value;
					break;
				case Anchor.BottomLeft:
					BottomLeft = _value;
					break;
				case Anchor.BottomRight:
					BottomRight = _value;
					break;
			}
		}

		public Vector2 TopLeft {
			get { return parentEntity.Position - halfSize; }
			set { parentEntity.Position = value + halfSize; }
		}
		public Vector2 TopRight {
			get { return parentEntity.Position + new Vector2(halfSize.X, -halfSize.Y); }
			set { parentEntity.Position = value - new Vector2(halfSize.X, -halfSize.Y);}
		}
		public Vector2 BottomLeft {
			get { return parentEntity.Position + new Vector2(-halfSize.X, halfSize.Y); }
			set { parentEntity.Position = value - new Vector2(-halfSize.X, halfSize.Y); }
		}
		public Vector2 BottomRight {
			get { return parentEntity.Position + halfSize; }
			set { parentEntity.Position = value - halfSize; }
		}

		Vector2 halfSize { get { return parentEntity.Size / 2.0f; } }

		LocationComponent parentEntity;
	}
}
