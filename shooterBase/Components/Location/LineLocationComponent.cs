﻿using System;
using Microsoft.Xna.Framework;

namespace shooter
{
	public class LineLocationComponent : LocationComponent
	{
		public LineLocationComponent(LocationComponent _startLine, LocationComponent _endLine, float _width)
		{
			width = _width;
			startLine = _startLine;
			endLine = _endLine;
		}

		public override Vector2 Position
		{
			get
			{
				return (startLine.Position + endLine.Position) / 2;
			}
			set
			{
				Vector2 startDiff = startLine.Position - Position;
				Vector2 endDiff = endLine.Position - Position;
				startLine.Position = value + startDiff;
				endLine.Position = value + endDiff;
			}
		}
		public override Vector2 Size
		{
			get
			{
				float length = (endLine.Position - startLine.Position).Length();
				return new Vector2(length, width);
			}
			set {
				throw new NotSupportedException();
			}
		}
		public override Vector3 ModelRotation
		{
			get
			{
				float angle = Utils.getVectorAngle(endLine.Position - startLine.Position) + (float)Math.PI / 2;
				return new Vector3(angle, 0, 0);
			}
		}
		public override void InitializeSize(Vector2 _size) { }


		float width;
		LocationComponent startLine;
		LocationComponent endLine;
	}
}
