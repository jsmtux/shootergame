﻿using System;
using Microsoft.Xna.Framework;

namespace shooter
{
	public class ChildPhysicsComponent : LocationComponent
	{
		//TODO make factory or separate constructors
		//TODO add rotation
		public ChildPhysicsComponent(LocationComponent _parent, Vector2 _offset, Vector2? _size = null, float _ratio = 1.0f)
		{
			parent = _parent;
			offset = new TweenWrapper<Vector2>(_offset);
			size = new TweenWrapper<Vector2>(_size.GetValueOrDefault());
			ratio = _ratio;
		}

		public override Vector2 Position
		{
			get { return parent.Position * ratio + Utils.rotateVector(offset, parent.Rotation); }
			set { offset.SetStatic(value - parent.Position * ratio); }
		}
		public override Vector2 Size { get { return size; } set { size.SetStatic(value); } }
		public TweenWrapper<Vector2> SizeTween { get { return size; } set { size = value; } }
		public override Vector3 ModelRotation { get { return parent.ModelRotation + ModelRotationOffset; } }

		public Vector2 Offset { get { return offset; } set { offset.SetStatic(value); } }
		public TweenWrapper<Vector2> OffsetTween { get { return offset; } set { offset = value; } }

		public LocationComponent Parent { get { return parent; } }

		LocationComponent parent;
		TweenWrapper<Vector2> offset;
		public Vector3 ModelRotationOffset = Vector3.Zero;
		public float RotationOffset { get { return ModelRotationOffset.X; }  set { ModelRotationOffset.X = value; } }
		TweenWrapper<Vector2> size;
		float ratio;
	}

	public class NoRotationChildPhysicsComponent : ChildPhysicsComponent
	{
		public NoRotationChildPhysicsComponent(LocationComponent _parent, Vector2 _offset, Vector2? _size = null, float _ratio = 1.0f)
			: base(_parent, _offset, _size, _ratio)
		{
		}

		public override Vector3 ModelRotation { get { return ModelRotationOffset; } }
	}
}
