﻿using System;
using Microsoft.Xna.Framework;

namespace shooter
{
	public class CameraComponent : SBaseComponent
	{
		public CameraComponent(LocationComponent _location, Platform _platform)
		{
			platform = _platform;
			location = _location;
		}

		public Vector2 ViewportCenter
		{
			get
			{
				return platform.Resolution.ToVector2() / 2;
			}
		}

		public Matrix TranslationMatrix
		{
			get
			{
				return Matrix.CreateTranslation(-(int)location.Position.X,
				   -(int)location.Position.Y, 0) *
				   Matrix.CreateRotationZ(location.Rotation) *
				   Matrix.CreateScale(new Vector3(zoom, zoom, 1)) *
				   Matrix.CreateTranslation(new Vector3(ViewportCenter, 0));
			}
		}

		public Rectangle FrustumRectangle
		{
			get
			{
				Point topLeft = ScreenToWorld(Vector2.Zero).ToPoint();
				Point bottomRight = ScreenToWorld(platform.Resolution.ToVector2()).ToPoint();
				return new Rectangle(topLeft, bottomRight - topLeft);
			}
		}

		public Matrix InvTranslationMatrix
		{
			get
			{
				return Matrix.CreateTranslation((int)location.Position.X,
				   0, (int)location.Position.Y) *
				   Matrix.CreateRotationZ(location.Rotation) *
				   Matrix.CreateTranslation(new Vector3(-ViewportCenter.X, 0, -ViewportCenter.Y)) *
				   Matrix.CreateScale(new Vector3(zoom, 1, zoom));
			}
		}

		public Vector2 WorldToScreen(Vector2 worldPosition)
		{
			return Vector2.Transform(worldPosition, TranslationMatrix);
		}

		public Vector2 ScreenToWorld(Vector2 screenPosition)
		{
			return Vector2.Transform(screenPosition,
				Matrix.Invert(TranslationMatrix));
		}

		Platform platform;
		public LocationComponent location;
		public float zoom = 1.0f;
	}
}
