﻿using System;
using ECS;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace shooter
{
	public class SBaseComponent : IComponent
	{
		#region IComponent implementation
		public Entity entity { set; get; }
		#endregion

		public SBaseEntity getEntity()
		{
			return (SBaseEntity)entity;
		}
	}
}
