﻿using System;
namespace shooter
{
	public enum TimerState
	{
		Running,
		Paused,
		Stopped
	}

	public enum StopBehavior
	{
		AsIs,
		ForceComplete
	}

	public abstract class BaseTimerComponent : SBaseComponent
	{
		internal BaseTimerComponent(bool _updateWhenPaused)
		{
			state = TimerState.Stopped;
			UpdateWhenPaused = _updateWhenPaused;
		}

		public abstract void Update(TimeSpan elapsedTime);

		protected TimerState state;
		public TimerState State { get { return state; } }

		protected TimeSpan currentTime;
		public TimeSpan CurrentTime { get { return currentTime; } }

		public readonly bool UpdateWhenPaused;
	}

	public class TimerComponent : BaseTimerComponent
	{
		public TimerComponent(bool _updateWhenPaused = true) : base(_updateWhenPaused) { }

		public void Start()
		{
			currentTime = TimeSpan.Zero;
			state = TimerState.Running;
		}

		public virtual void Stop()
		{
			state = TimerState.Stopped;
			currentTime = TimeSpan.Zero;
		}

		public override void Update(TimeSpan elapsedTime)
		{
			if (state != TimerState.Running)
			{
				return;
			}

			currentTime += elapsedTime;
		}
	}

	public class CountDownTimerComponent : BaseTimerComponent
	{
		public CountDownTimerComponent(bool _updateWhenPaused = true) : base(_updateWhenPaused) { }

		public void Start(TimeSpan _duration)
		{
			if (_duration == TimeSpan.Zero)
			{
				throw new ArgumentException("duration must be greater than 0");
			}

			currentTime = TimeSpan.Zero;
			duration = _duration;
			state = TimerState.Running;
		}

		public virtual void Stop(StopBehavior stopBehavior)
		{
			state = TimerState.Stopped;

			if (stopBehavior == StopBehavior.ForceComplete)
			{
				currentTime = duration;
			}
		}

		public override void Update(TimeSpan elapsedTime)
		{
			if (state != TimerState.Running)
			{
				return;
			}

			currentTime += elapsedTime;
			if (currentTime >= duration)
			{
				currentTime = duration;
				state = TimerState.Stopped;
				onEnd?.Invoke();
			}
		}

		public float ElapsedPercentage()
		{
			return (float)currentTime.TotalMilliseconds / (float)duration.TotalMilliseconds;
		}

		public TimeSpan Duration { get { return duration; } }

		private TimeSpan duration;

		public Action onEnd;
	}
}
