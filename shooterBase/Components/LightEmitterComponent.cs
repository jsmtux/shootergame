﻿using System;
using Microsoft.Xna.Framework;

namespace shooter
{
	public class LightEmitterComponent : SBaseComponent
	{
		public LightEmitterComponent(LocationComponent _locationComponent, Color _color, float _attenuation, float _intensity = 1.0f)
		{
			locationComponent = _locationComponent;
			color = _color;
			attenuation = _attenuation;
			intensity = new TweenWrapper<float>(_intensity);
		}

		public LocationComponent locationComponent;
		public Color color;
		public float attenuation;
		public TweenWrapper<float> intensity;
	}
}
