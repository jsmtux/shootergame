﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Xna.Framework.Audio;

namespace shooter
{
	public class SoundEffectComponentInstance
	{
		internal SoundEffectComponentInstance(SoundEffectInstance _instance)
		{
			instance = _instance;
		}

		public void Play() => instance.Play();
		public void Stop() => instance.Stop();

		public bool Looped { get { return instance.IsLooped; } set { instance.IsLooped = value; }}

		SoundEffectInstance instance;
	}

	public class SoundEffectComponent : SBaseComponent
	{
		public SoundEffectComponent(SoundEffect _effect)
			: this(new List<SoundEffect>{_effect})
		{
		}

		public SoundEffectComponent(List<SoundEffect> _effects)
		{
			Debug.Assert(_effects != null && _effects.Count > 0);
			foreach(var effect in _effects)
			{
				Debug.Assert(effect != null);
			}
			effects = _effects;
		}

		public SoundEffectComponentInstance Play()
		{
			var effect= effects[rnd.Next(0,effects.Count)];
			var instance = new SoundEffectComponentInstance(effect.CreateInstance());
			instance.Play();
			return instance;
		}

		List<SoundEffect> effects;
		static Random rnd = new Random();
	}
}
