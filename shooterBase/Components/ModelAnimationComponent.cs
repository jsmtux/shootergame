﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using tainicom.Aether.Animation;

namespace shooter
{
	public class ModelAnimationComponent : ModelComponent
	{
		public ModelAnimationComponent(LocationComponent _locationComponent,
		                      float _scale,
							  Model _model,
							  Texture2D _texture,
							  Depth.Type _depth,
		                      RenderConfiguration _renderConfiguration,
							  ModelComponent.RenderMode _renderMode)
			: base(_locationComponent, _scale, _model, _texture, _depth, _renderConfiguration, _renderMode)
		{
			animations = model.GetAnimations();
		}

		public ModelAnimationComponent(LocationComponent _locationComponent,
		                      float _scale,
							  Model _model,
							  Texture2D _texture,
							  Depth.Type _depth)
			: this(_locationComponent, _scale, _model, _texture, _depth, RenderConfiguration.Default, ModelComponent.RenderMode.Ortographic)
		{}

		public override void UpdateForDraw(GameTime _time)
		{
			if (animations.CurrentClip != null)
			{
				animations.Update(currentTime, false, Matrix.Identity);
				foreach (ModelMesh mesh in model.Meshes)
				{
					foreach (var part in mesh.MeshParts)
					{
						part.UpdateVertices(animations.AnimationTransforms);
					}
				}
			}
		}

		public void SetCurrentAnimationTime(TimeSpan _currentTime)
		{
			currentTime = _currentTime;
		}

		public void SetCurrentAnimation(string _animationName)
		{
			if (_animationName != null)
			{
				var newClip = animations.Clips[_animationName];
				if (newClip != animations.CurrentClip)
				{
					animations.SetClip(newClip);
				}
			}
		}

		Animations animations;
		bool running = false;
		TimeSpan currentTime = TimeSpan.Zero;
	}
}
