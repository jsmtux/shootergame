﻿using System;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace shooter
{
	public abstract class LogicComponent : SBaseComponent
	{
		public LogicComponent()
		{
		}

		public abstract void UpdateLogic(GameTime _elapsedTime);
		public bool UpdateOnPause = false;
	}

	public abstract class Behavior
	{
		public abstract void DoStep(GameTime _elapsedTime);
		public abstract int GetPriority(GameTime _elapsedTime);
	}

	public class BehavioralLogicComponent : LogicComponent
	{
		public override void UpdateLogic(GameTime _elapsedTime)
		{
			foreach(var action in regularActions)
			{
				action(_elapsedTime);
			}
			lastUsedBehavior = GetPriorityBehavior(_elapsedTime);
			lastUsedBehavior?.DoStep(_elapsedTime);
		}

		Behavior GetPriorityBehavior(GameTime _elapsedTime)
		{
			Behavior ret = null;
			int greatestScore = 0;
			foreach (var behavior in assignedBehaviors)
			{
				int priority = behavior.GetPriority(_elapsedTime);
				if (priority > greatestScore)
				{
					ret = behavior;
					greatestScore = priority;
				}
			}
			return ret;
		}

		protected void AssignNewBehavior(Behavior _behavior)
		{
			assignedBehaviors.Add(_behavior);
		}

		protected void AddRegularAction(UpdateAction _regular)
		{
			regularActions.Add(_regular);
		}

		public Behavior LastUsedBehavior { get { return lastUsedBehavior; } }

		Behavior lastUsedBehavior;
		List<Behavior> assignedBehaviors = new List<Behavior>();
		public delegate void UpdateAction(GameTime _elapsedTime);
		List<UpdateAction> regularActions = new List<UpdateAction>();
	}
}
