﻿using System;
using Microsoft.Xna.Framework;

namespace shooter
{
	public class PhysicsBodyComponent : SBaseComponent
	{
		public delegate bool CollisionDelegate(SBaseEntity _e, Vector2 _collisionPos);

		public PhysicsBodyComponent(LocationComponent _locationComponent,
									CollisionDelegate _collisionDelegate,
									bool _fixPositionOnCollision = false)
		{
			fixPositionOnCollision = _fixPositionOnCollision;
			locationComponent = _locationComponent;
			collisionDelegate = _collisionDelegate;
		}
		public LocationComponent locationComponent;
		public CollisionDelegate collisionDelegate;
		public readonly bool fixPositionOnCollision;
		public bool alwaysCheck = false;
	}

	public class RoundPhysicsBodyComponent : PhysicsBodyComponent
	{
		public RoundPhysicsBodyComponent(float _radius,
									LocationComponent _locationComponent,
									CollisionDelegate _collisionDelegate = null,
									bool _fixPositionOnCollision = false)
			: base(_locationComponent, _collisionDelegate, _fixPositionOnCollision)
		{
			radius = _radius;
		}

		public float Radius { get { return radius; } set { radius = value; alwaysCheck = true; }}
		float radius;
	}
}
