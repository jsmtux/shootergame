using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace shooter
{
	public class SBaseEffect
	{
		public SBaseEffect(Effect _baseEffect)
		{
			baseEffect = _baseEffect;
		}

		public virtual void SetContentDimensions(Vector2 dimensions)
		{}

		public Effect BaseEffect {get {return baseEffect;} }

		protected Effect baseEffect;
	}

	public class PortalEffect : SBaseEffect
	{
		public PortalEffect(Effect _baseEffect)
			: base(_baseEffect)
		{
		}

		public override void SetContentDimensions(Vector2 _dimensions)
		{
		}

		public void Update(float _time)
		{
			baseEffect.Parameters["Time"].SetValue((float)(_time % (float)(Math.PI / 0.001)));
		}
	}

	public class CloudsEffect : SBaseEffect
	{
		public CloudsEffect(Effect _baseEffect, Texture2D noiseTexture)
			: base(_baseEffect)
		{
			baseEffect.Parameters["NoiseTexture"].SetValue(noiseTexture);
		}

		public override void SetContentDimensions(Vector2 _dimensions)
		{
		}

		public void SetCameraPosition(Vector2 _pos)
		{
			cameraPosition = _pos / 50000.0f;
		}

		public void Update(float _time)
		{
			baseEffect.Parameters["iTime"].SetValue((float)Math.Floor(_time/10)/10000.0f);
			baseEffect.Parameters["camPos"].SetValue(cameraPosition);
		}

		Vector2 cameraPosition = Vector2.Zero;
	}

	public class MovementSurfaceEffect : SBaseEffect
	{
		public MovementSurfaceEffect(Effect _baseEffect, Texture2D _segmentationTexture)
			: base(_baseEffect)
		{
			segmentationTexture = _segmentationTexture;
			baseEffect.Parameters["SegmentationTexture"].SetValue(segmentationTexture);
			Clear();
		}

		int GetCodeForColor(ColorType _color)
		{
			int ret = 0;
			switch(_color)
			{
				case ColorType.OFF:
					ret = 0;
					break;
				case ColorType.LIGHT_BLUE:
					ret = 1;
					break;
				case ColorType.DARK_BLUE:
					ret = 2;
					break;
				case ColorType.LIGHT_RED:
					ret = 3;
					break;
			}
			return ret;
		}

		public void Clear(ColorType _type = ColorType.OFF)
		{
			for(int i = 0; i < numCols; i++)
			{
				for(int j = 0; j < numRows; j++)
				{
					colorValues[i,j] = _type;
				}
			}
		}

		public void SetColorValue(int col, int row, ColorType _value)
		{
			colorValues[col,row] = _value;
		}

		public void Update(int _seconds)
		{
			float[] values = new float[numCols*numRows];
			for(int i = 0; i < numCols; i++)
			{
				for(int j = 0; j < numRows; j++)
				{
					values[i*numRows+j] = GetCodeForColor(colorValues[i,j]);
				}
			}
			baseEffect.Parameters["colors"].SetValue(values);
		}

		Texture2D segmentationTexture;

		public const int numCols = 8;
		public const int numRows = 7;
		ColorType[,] colorValues = new ColorType[numCols,numRows];
		public enum ColorType {OFF, LIGHT_BLUE, DARK_BLUE, LIGHT_RED};
	}

	public class EffectsManager
	{
		public EffectsManager(ContentManager _shaderContent, ContentManager _mainContent, Platform _currentPlatform)
		{
			PortalEffect = new PortalEffect(_shaderContent.Load<Effect>("portalEffect"));
			MovementSurfaceEffect =
				new MovementSurfaceEffect(
					_shaderContent.Load<Effect>("movement_surface"),
					_mainContent.Load<Texture2D>("ui/movement_surface_segmentation"));
			CloudsEffect = new CloudsEffect(
				_shaderContent.Load<Effect>("clouds"),
				_mainContent.Load<Texture2D>("particles/noise"));
			currentPlatform = _currentPlatform;
		}

		public void UpdateLogic(GameTime _elapsedTime)
		{
			float time = ((float)_elapsedTime.TotalGameTime.TotalMilliseconds);

			PortalEffect.Update(time);
			CloudsEffect.Update(time);
			MovementSurfaceEffect.Update((int)_elapsedTime.TotalGameTime.TotalSeconds);
		}

		public PortalEffect PortalEffect;
		public MovementSurfaceEffect MovementSurfaceEffect;
		public CloudsEffect CloudsEffect;
		Platform currentPlatform;
	}
}
