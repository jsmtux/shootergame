﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace shooter
{
    public abstract class Platform
    {
        public enum Type{ Android, Desktop };

        public abstract bool isFullScreen();

        public Rectangle getDrawPosition()
        {
            return drawPositionRectangle;
        }

        protected void RecalculateDrawPosition(Point _realResolution)
        {
            Vector2 drawPosition = Vector2.Zero;
			float deviceAspectRatio = _realResolution.X / (float)_realResolution.Y;
            
            float baseAspectRatio = baseResolution.X / (float)baseResolution.Y;
            float scale;

            if (deviceAspectRatio > baseAspectRatio)
            {
				scale = _realResolution.Y / (float)baseResolution.Y;
                resolution = new Point((int)(baseResolution.Y * deviceAspectRatio), baseResolution.Y);
            }
            else
            {
				scale = _realResolution.X / (float)baseResolution.X;
                resolution = new Point(baseResolution.X, (int)(baseResolution.X / deviceAspectRatio));
            }
            drawPositionRectangle = new Rectangle(0, 0, (int)(resolution.X * scale), (int)(resolution.Y * scale));
            onResize?.Invoke();
        }

        public abstract void Update(ShooterGame _game);

		public abstract Type GetPlatformType();

		public Point Resolution { get {return resolution;} }
        Point resolution= new Point(1280, 720);
        static protected readonly Point baseResolution = new Point(1280, 720);

        static float minAspectRatio = 1.775f;
        static float maxAspectRatio = 2.165f;
        static public readonly Point maxResolution = new Point((int)(baseResolution.Y * maxAspectRatio), (int)(baseResolution.X / minAspectRatio));

        Rectangle drawPositionRectangle;

        public Action onResize;
    }

    public class AndroidPlatform : Platform
    {
        public AndroidPlatform(Func<Point> _realResolutionCb, Action _exitCb)
        {
			realResolutionCb = _realResolutionCb;
            exitCb = _exitCb;
            storedResolution = realResolutionCb();
            RecalculateDrawPosition(storedResolution);
        }

        public override void Update(ShooterGame _game)
        {
            /*if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
            {
                _game.Exit();
                exitCb();
            }*/
            var newResolution = realResolutionCb();
            if (storedResolution != newResolution)
            {
                storedResolution = newResolution;
                RecalculateDrawPosition(storedResolution);
            }
        }

		public override Type GetPlatformType()
		{
			return Type.Android;
		}

		public override bool isFullScreen()
        {
            return true;
        }

        Func<Point> realResolutionCb;
        Action exitCb;
        Point storedResolution;
    }

    public class DesktopPlatform : Platform
    {
        public DesktopPlatform()
        {
            RecalculateDrawPosition(baseResolution);
        }     

		public override Type GetPlatformType()
		{
			return Type.Desktop;
		}

		public override void Update(ShooterGame _game)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                _game.Exit();
        }

        public override bool isFullScreen()
        {
            return false;
        }

        public void OnResize(Object sender, EventArgs e)
        {
            if (sender is GameWindow gameWindow)
            {
                var realResolution = new Point(gameWindow.ClientBounds.Width, gameWindow.ClientBounds.Height);
                RecalculateDrawPosition(realResolution);
            }
        }
    }
}
