﻿using System;
using ECS;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace shooter
{
	public class SpriteRenderer : ComponentDrawerSystem<SpriteComponent>
	{
		public SpriteRenderer(SpriteBatch _spriteBatch, ContentManager _contentManager, CameraManager _cameraManager)
			: base(_cameraManager)
		{
			SystemObserver.Subscribe(this);
			spriteBatch = _spriteBatch;
			cameraManager = _cameraManager;
		}

		Vector2 origin = new Vector2();
		Rectangle drawPosition = new Rectangle();

		public override void Draw(GameTime gameTime, Depth depth)
		{
			foreach(KeyValuePair<RenderConfiguration, List<SpriteComponent>> componentSublist in orderedComponents[depth.toInt()])
			{
				RenderConfiguration curRenderConf = componentSublist.Key;
				Matrix curMatrix;
				if (curRenderConf.translationType == RenderConfiguration.TranslationType.UI)
				{
					curMatrix = cameraManager.getUITranslationMatrix();
				}
				else
				{
					curMatrix = cameraManager.getTranslationMatrix();
				}

				List<SpriteComponent> curList = componentSublist.Value;
				Effect curEffect = null;
				if (curRenderConf.effect != null)
				{
					curEffect = curRenderConf.effect.BaseEffect;
				}
				spriteBatch.Begin(SpriteSortMode.Immediate, curRenderConf.getBlendState(),
				                  null, null, null, curEffect, curMatrix);

				foreach (var curSprite in curList)
				{
					if (!InView(curRenderConf, curSprite))
						continue;
					if (Math.Abs(curSprite.Alpha) < float.Epsilon)
						continue;
					LocationComponent locationComponent = curSprite.locationComponent;

					Vector2 size = locationComponent.Size;
					curRenderConf.effect?.SetContentDimensions(size);

					origin.X = curSprite.imageSize.X / 2;
					origin.Y = curSprite.imageSize.Y / 2;
					drawPosition.Width = (int)locationComponent.Size.X;
					drawPosition.Height = (int)locationComponent.Size.Y;
					drawPosition.X = (int)locationComponent.Position.X;
					drawPosition.Y = (int)locationComponent.Position.Y;
					spriteBatch.Draw(curSprite.image,
					                 drawPosition,
					                 null,
					                 (Color)curSprite.tint * (float)curSprite.Alpha,
									 locationComponent.Rotation,
									 origin,
									 SpriteEffects.None,
					                 curSprite.depth.toFloat());
				}

				spriteBatch.End();
			}
		}

		SpriteBatch spriteBatch;
		Effect effect;
	}
}
