﻿using ECS;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace shooter
{
	public class ParticleSystem : ComponentDrawerSystem<ParticleEmitterComponent>
	{
		public ParticleSystem(SpriteBatch _spriteBatch, CameraManager _cameraManager)
			: base(_cameraManager)
		{
			SystemObserver.Subscribe(this);
			spriteBatch = _spriteBatch;
			cameraManager = _cameraManager;
		}

		public override void UpdateLogic(GameTime gameTime)
		{
			base.UpdateLogic(gameTime);
			foreach (var emitterInstance in componentList)
			{
				emitterInstance.update(gameTime);
			}
		}

		public override void Draw(GameTime gameTime, Depth depth)
		{
			foreach (KeyValuePair<RenderConfiguration, List<ParticleEmitterComponent>> componentSublist in orderedComponents[depth.toInt()])
			{
				RenderConfiguration curRenderConf = componentSublist.Key;
				Matrix curMatrix;
				if (curRenderConf.translationType == RenderConfiguration.TranslationType.UI)
				{
					curMatrix = cameraManager.getUITranslationMatrix();
				}
				else
				{
					curMatrix = cameraManager.getTranslationMatrix();
				}

				Effect curEffect = null;
				if (curRenderConf.effect != null)
				{
					curEffect = curRenderConf.effect.BaseEffect;
				}
				spriteBatch.Begin(SpriteSortMode.Deferred, curRenderConf.getBlendState(),
								  null, null, null, curEffect, curMatrix);

				List<ParticleEmitterComponent> curList = componentSublist.Value;
				foreach (var emitterInstance in curList)
				{
					if (!InView(curRenderConf, emitterInstance))
						continue;
					emitterInstance.draw(gameTime, spriteBatch);
				}

				spriteBatch.End();
			}
		}

		SpriteBatch spriteBatch;
	}
}

