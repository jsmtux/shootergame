using System;
using ECS;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace shooter
{
	public interface IMessagingSystem
	{
		void SendMessage(Message _message);
	}

	public class MessagingSystem : SBaseSystem<MessageReceiverComponent>, IMessagingSystem
	{
		public override void UpdateLogic(GameTime gameTime)
		{
            base.UpdateLogic(gameTime);
            if (messagesToSend.Count > 0)
            {
			    List<Message> readyMessages = messagesToSend;
                messagesToSend = new List<Message>();
                foreach(var message in readyMessages)
                {
                    foreach (var component in componentList)
                    {
                        component.ReceiveMessage(message);
                    }
                }
            }
		}

        public void SendMessage(Message _message)
        {
            messagesToSend.Add(_message);
        }

        List<Message> messagesToSend = new List<Message>();
	}
}
