﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace shooter
{
	public class Maze
	{
		public enum CellStatus { Empty, Wall, Undetermined, Unexposed };

		public Maze(Point _size, Point _startPoint)
		{
			cells = new CellStatus[_size.X, _size.Y];

			for (int x = 0; x < cells.GetLength(0); x += 1)
			{
				for (int y = 0; y < cells.GetLength(1); y += 1)
				{
					cells[x, y] = CellStatus.Unexposed;
				}
			}

			size = _size;
			branchrate = 0;
			startPoint = _startPoint;
		}

		public CellStatus[,] getMaze()
		{
			Random rnd = new Random();

			Carve(startPoint);

			while (frontier.Count > 0)
			{
				/*** Fixme
				float pos = (int)Math.Pow(rnd.Next(), Math.Pow(Math.E, frontier.Count));
				Point choice = frontier[(int)(pos * frontier.Count)];
				*/
				Point choice = frontier[rnd.Next(0, frontier.Count)];
				if (CanBeSpace(choice))
				{
					Carve(choice);
				}
				else
				{
					Harden(choice);
				}
				frontier.Remove(choice);
			}

			return cells;
		}

		void Carve(Point _point)
		{
			cells[_point.X, _point.Y] = CellStatus.Empty;

			var extra = new List<Point>();

			if (_point.X > 0)
			{
				if (cells[_point.X - 1, _point.Y] == CellStatus.Unexposed)
				{
					cells[_point.X - 1, _point.Y] = CellStatus.Undetermined;
					extra.Add(_point + new Point(-1, 0));
				}
			}
			if (_point.X < size.X - 1)
			{
				if (cells[_point.X + 1, _point.Y] == CellStatus.Unexposed)
				{
					cells[_point.X + 1, _point.Y] = CellStatus.Undetermined;
					extra.Add(_point + new Point(1, 0));
				}
			}
			if (_point.Y > 0)
			{
				if (cells[_point.X, _point.Y - 1] == CellStatus.Unexposed)
				{
					cells[_point.X, _point.Y - 1] = CellStatus.Undetermined;
					extra.Add(_point + new Point(0, -1));
				}
			}
			if (_point.Y < size.Y - 1)
			{
				if (cells[_point.X, _point.Y + 1] == CellStatus.Unexposed)
				{
					cells[_point.X, _point.Y + 1] = CellStatus.Undetermined;
					extra.Add(_point + new Point(0, 1));
				}
			}

			Utils.ShuffleList(extra);
			frontier.AddRange(extra);
		}

		void Harden(Point _point)
		{
			cells[_point.X, _point.Y] = CellStatus.Wall;
		}

		bool CanBeSpace(Point _point)
		{
			int edgeState = 0;

			if (_point.X > 0 && cells[_point.X - 1, _point.Y] == CellStatus.Empty)
			{
				edgeState += 1;
			}
			if (_point.X < size.X - 1 && cells[_point.X + 1, _point.Y] == CellStatus.Empty)
			{
				edgeState += 2;
			}
			if (_point.Y > 0 && cells[_point.X, _point.Y - 1] == CellStatus.Empty)
			{
				edgeState += 4;
			}
			if (_point.Y < size.Y - 1 && cells[_point.X, _point.Y + 1] == CellStatus.Empty)
			{
				edgeState += 8;
			}

			switch (edgeState)
			{
				case 1:
					if (_point.X < size.X - 1)
					{
						if (_point.Y > 0 && cells[_point.X + 1, _point.Y - 1] == CellStatus.Empty)
							return false;
						if (_point.Y < size.Y - 1 && cells[_point.X + 1, _point.Y + 1] == CellStatus.Empty)
							return false;
					}
					return true;
				case 2:
					if (_point.X > 0)
					{
						if (_point.Y > 0 && cells[_point.X - 1, _point.Y - 1] == CellStatus.Empty)
							return false;
						if (_point.Y < size.Y - 1 && cells[_point.X - 1, _point.Y + 1] == CellStatus.Empty)
							return false;
					}
					return true;
				case 4:
					if (_point.Y < size.Y - 1)
					{
						if (_point.X > 0 && cells[_point.X - 1, _point.Y + 1] == CellStatus.Empty)
							return false;
						if (_point.X < size.X - 1 && cells[_point.X + 1, _point.Y + 1] == CellStatus.Empty)
							return false;
					}
					return true;
				case 8:
					if (_point.Y > 0)
					{
						if (_point.X > 0 && cells[_point.X - 1, _point.Y - 1] == CellStatus.Empty)
							return false;
						if (_point.X < size.X - 1 && cells[_point.X + 1, _point.Y - 1] == CellStatus.Empty)
							return false;
					}
					return true;
				default:
					return false;
			}
		}

		Point size;
		int branchrate;
		CellStatus[,] cells;
		List<Point> frontier = new List<Point>();
		Point startPoint;
	}

}
