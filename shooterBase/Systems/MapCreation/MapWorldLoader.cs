﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace shooter
{
	public class MapWorldLoader
	{
		public MapWorldLoader(EntityCreator _entityCreator, string _mapName)
		{
			tiledReader = new TiledMapReader(_mapName, _entityCreator);

			mapTiles = tiledReader.GetTiles();
		}

		public void AddToStage(Stage _stage)
		{
			foreach (var entity in mapTiles)
			{
				_stage.addEntity(entity);
			}
		}

		public void RemoveFromStage()
		{
			foreach (var entity in mapTiles)
			{
				entity.delete();
			}
		}

		public Dictionary<string, Rectangle> CameraZones { get { return tiledReader.CameraZones; } }
		public Dictionary<Point, EnemyEntity> Enemies { get { return tiledReader.Enemies; } }

		TiledMapReader tiledReader;
		List<SBaseEntity> mapTiles;
	}
}
