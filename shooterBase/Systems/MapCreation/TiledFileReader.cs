﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using Microsoft.Xna.Framework;

namespace shooter
{
	using tiledLayer = List<List<int>>;
	using enemyPath = List<Point>;

	class TiledFileReader
	{
		public TiledFileReader(string _path)
		{
            XmlDocument doc = new XmlDocument();
            using (var stream = TitleContainer.OpenStream(_path))
            {
                using (var reader = new StreamReader(stream))
                {
                    string content = reader.ReadToEnd();
                    doc.LoadXml(content);
                }
            }
            XmlNode mapNode = doc.DocumentElement.SelectSingleNode("/map");
			Point mapTileSize;
			mapTileSize.X = Int32.Parse(mapNode.Attributes["tilewidth"].Value);
			mapTileSize.Y = Int32.Parse(mapNode.Attributes["tileheight"].Value);

			foreach (XmlNode node in mapNode.ChildNodes)
			{
				if (node.Name == "layer")
				{
					tiledLayer currentLayer = new tiledLayer();

					string content = node.ChildNodes[0].InnerXml;
					int width = Int32.Parse(node.Attributes["width"].Value);
					int height = Int32.Parse(node.Attributes["height"].Value);

					List<int> line = new List<int>();

					foreach (string value in content.Split(','))
					{
						if (line.Count >= width)
						{
							currentLayer.Add(line);
							line = new List<int>();
						}
						line.Add(Int32.Parse(value));
					}
					currentLayer.Add(line);

					layers[node.Attributes["name"].Value] = currentLayer;
				}
				if (node.Name == "objectgroup" && node.Attributes["name"].InnerText == "enemy_path")
				{
					foreach(XmlNode path in node.ChildNodes)
					{
						Point basePosition;
						basePosition.X = (int)Convert.ToDouble(path.Attributes["x"].InnerText);
						basePosition.Y = (int)Convert.ToDouble(path.Attributes["y"].InnerText);

						string pointsInfo = path.ChildNodes[0].Attributes["points"].InnerText;

						enemyPath enemyPath = new List<Point>();
						foreach (string pointInfo in pointsInfo.Split(' '))
						{
							Point position;
							position.X = (int)Convert.ToDouble(pointInfo.Split(',')[0]);
							position.Y = (int)Convert.ToDouble(pointInfo.Split(',')[1]);
							position += basePosition;
							position /= mapTileSize;
							enemyPath.Add(position);
						}

						rawEnemyPaths.Add(enemyPath);
					}
				}
				else if (node.Name == "objectgroup" && node.Attributes["name"].InnerText == "camera_zones")
				{
					foreach (XmlNode path in node.ChildNodes)
					{
						Rectangle zoneRect = Rectangle.Empty;
						zoneRect.X = (int)Convert.ToDouble(path.Attributes["x"].InnerText);
						zoneRect.Y = (int)Convert.ToDouble(path.Attributes["y"].InnerText);
						zoneRect.Location /= mapTileSize;
						zoneRect.Width = (int)Convert.ToDouble(path.Attributes["width"].InnerText);
						zoneRect.Height = (int)Convert.ToDouble(path.Attributes["height"].InnerText);
						zoneRect.Size /= mapTileSize;

						string name = path.Attributes["name"].InnerText;

						rawCameraZones[name] = zoneRect;
					}
				}
			}
		}

		internal enum TileType {
			RockSquare1 = 0,
			RockSquare2 = 34,
			RockThreeSquares1 = 10,
			RockThreeSquares1Inverted = 27,
			RockThreeSquares2 = 31,
			RockThreeSquares2Inverted = 35,
			RockThreeSquaresLong = 16,
			RockThreeSquaresLongVertical = 28,
			RockTwoSquares = 12,
			RockTwoSquaresVertical = 14,
			RockBigSquare = 22,
			Mushrooms = 40,
			Cristal = 41,
			BurgerEnemy = 1,
			Carrot = 2,
			StarSeed = 5,
			Ship = 3,
			Heart = 4,
			EndPortal = 6,
			Fence = 19,
			HorizontalFence = 42,
			GrassL = 44,
			GrassM = 45,
			GrassR = 47,
			Melon = 8 };
		internal enum RootType { Horizontal = 101, Vertical = 120, TopLeftCorner = 111, BottomRightCorner = 132, TopRightCorner = 105, BottomLeftCorner = 113, CherryTree = 123, CarrotTree = 124, PalmTree = 125 }

		protected Dictionary<string, tiledLayer> layers = new Dictionary<string, tiledLayer>();
		protected List<enemyPath> rawEnemyPaths = new List<enemyPath>();
		protected Dictionary<string, Rectangle> rawCameraZones = new Dictionary<string, Rectangle>();
	}

	class TiledMapReader : TiledFileReader
	{
		public TiledMapReader(string _path, EntityCreator _entityCreator)
			: base(_path)
		{
			entityCreator = _entityCreator;
			tiles = CreateMap();
		}

		List<SBaseEntity> CreateMap()
		{
			var ret = new List<SBaseEntity>();

			offset = Vector2.Zero;
			foreach (KeyValuePair<string, tiledLayer> layer in layers)
			{
				for (var i = 0; i < layer.Value.Count; i++)
				{
					for (var j = 0; j < layer.Value[i].Count; j++)
					{
						Vector2 tilePosition = new Vector2(j * entitySize.X, i * entitySize.Y);
						if (layer.Value[i][j] - 1 == (int)TileType.Ship)
						{
							offset = tilePosition;
						}
						else if (layer.Value[i][j] - 1 == (int)RootType.PalmTree)
						{
							bananaPositions.Add(tilePosition);
						}
						else if (layer.Value[i][j] - 1 == (int)TileType.EndPortal)
						{
							portalPosition = tilePosition;
						}
					}
				}
			}

			for(int i = 0; i < bananaPositions.Count; i++)
			{
				bananaPositions[i] -= offset;
			}
			portalPosition -= offset;

			foreach (KeyValuePair<string, tiledLayer> layer in layers)
			{
				for(var i = 0; i < layer.Value.Count; i++)
				{
					for (var j = 0; j < layer.Value[i].Count; j++)
					{
						if (layer.Value[i][j] - 1 != (int)TileType.Ship && layer.Value[i][j] > 0)
						{
							var position = new Vector2(j * entitySize.X, i * entitySize.Y) - offset;
							if (layer.Key == "roots")
							{
								var root = CreateRootFor((TiledFileReader.RootType)layer.Value[i][j] - 1, position, new Point(j, i));
								if (root != null)
									ret.Add(root);
							}
							else
							{
								var tile = CreateTileFor((TiledFileReader.TileType)layer.Value[i][j] - 1, position, new Point(j, i));
								if (tile != null)
									ret.Add(tile);
							}
						}
					}
				}
			}

			foreach(var zone in rawCameraZones)
			{
				cameraZonesWithOffset[zone.Key] = new Rectangle(
					zone.Value.Location * entitySize - offset.ToPoint() + cameraZoneOffset,
					zone.Value.Size * entitySize);
			}

			return ret;
		}

		enemyPath getEnemyPathForPosition(Point _enemyPosition)
		{
			foreach (var path in rawEnemyPaths)
			{
				foreach (var pathPoint in path)
				{
					if (pathPoint == _enemyPosition)
					{
						enemyPath ret = new enemyPath();
						foreach (Point point in path)
						{
							ret.Add(point * entitySize - offset.ToPoint());
						}
						return ret;
					}
				}
			}
			return null;
		}

		public SBaseEntity CreateTileFor(TiledFileReader.TileType _type, Vector2 _position, Point _positionInMap)
		{
			SBaseEntity tile = null;

			switch (_type)
			{
				case TileType.RockSquare1:
					tile = entityCreator.CreateRockSquare1(_position);
					break;
				case TileType.RockSquare2:
					tile = entityCreator.CreateRockSquare2(_position);
					break;
				case TileType.RockThreeSquares1:
					tile = entityCreator.CreateRockThreeSquares1(_position);
					break;
				case TileType.RockThreeSquares1Inverted:
					tile = entityCreator.CreateRockThreeSquares1Inverted(_position);
					break;
				case TileType.RockThreeSquares2:
					tile = entityCreator.CreateRockThreeSquares2(_position);
					break;
				case TileType.RockThreeSquares2Inverted:
					tile = entityCreator.CreateRockThreeSquares2Inverted(_position);
					break;
				case TileType.RockThreeSquaresLong:
					tile = entityCreator.CreateRockThreeSquaresLong(_position);
					break;
				case TileType.RockThreeSquaresLongVertical:
					tile = entityCreator.CreateRockThreeSquaresLongVertical(_position);
					break;
				case TileType.RockTwoSquares:
					tile = entityCreator.CreateRockTwoSquares(_position);
					break;
				case TileType.RockTwoSquaresVertical:
					tile = entityCreator.CreateRockTwoSquaresVertical(_position);
					break;
				case TileType.RockBigSquare:
					tile = entityCreator.CreateRockBigSquare(_position);
					break;
				case TileType.Mushrooms:
					tile = entityCreator.CreateMushrooms(_position);
					break;
				case TileType.Cristal:
					tile = entityCreator.CreateCristal(_position);
					break;
				case TileType.BurgerEnemy:
					{
						enemyPath currentEnemyPath = getEnemyPathForPosition(_positionInMap);
						EnemyEntity enemy = entityCreator.CreateBurgerEnemy(_position, currentEnemyPath);
						tile = enemy;
						Enemies.Add(_positionInMap, enemy);
						break;
					}
				case TileType.Heart:
					tile = entityCreator.CreateHeart(_position);
					break;
				case TileType.Carrot:
					tile = entityCreator.CreateCarrot(_position);
					break;
				case TileType.StarSeed:
					tile = entityCreator.CreateStarSeed(_position);
					break;
				case TileType.EndPortal:
					tile = entityCreator.CreateEndPortal(_position, bananaPositions);
					break;
				case TileType.Fence:
					tile = entityCreator.CreateFence(_position);
					break;
				case TileType.HorizontalFence:
					tile = entityCreator.CreateHorizontalFence(_position);
					break;
				case TileType.Melon:
					tile = entityCreator.CreateMelon(_position);
					break;
				case TileType.GrassL:
					tile = entityCreator.CreateGrassL(_position);
					break;
				case TileType.GrassM:
					tile = entityCreator.CreateGrassM(_position);
					break;
				case TileType.GrassR:
					tile = entityCreator.CreateGrassR(_position);
					break;
				//default:
					//throw new NotSupportedException("Tile of type " + _type + " is not supported");
			}

			return tile;
		}

		public SBaseEntity CreateRootFor(TiledFileReader.RootType _type, Vector2 _position, Point _positionInMap)
		{
			SBaseEntity root = null;

			switch (_type)
			{
				case RootType.Horizontal:
					root = new StraightRootEntity(_position, 0);
					break;
				case RootType.Vertical:
					root = new StraightRootEntity(_position, (float)Math.PI / 2);
					break;
				case RootType.TopLeftCorner:
					root = new CornerRootEntity(_position, 0);
					break;
				case RootType.TopRightCorner:
					root = new CornerRootEntity(_position, (float)Math.PI / 2);
					break;
				case RootType.BottomRightCorner:
					root = new CornerRootEntity(_position, (float)Math.PI);
					break;
				case RootType.BottomLeftCorner:
					root = new CornerRootEntity(_position, (float)Math.PI * 3 / 2);
					break;
				case RootType.CarrotTree:
					root = new CarrotTreeEntity(_position);
					break;
				case RootType.PalmTree:
					root = new BananaTreeEntity(_position, portalPosition);
					break;
				default:
					break;
			}

			return root;
		}

		public List<SBaseEntity> GetTiles()
		{
			return tiles;
		}

		public EntityCreator entityCreator;

		public static readonly Point entitySize = new Point(100, 100);
		static readonly Point cameraZoneOffset = new Point(0, 0);

		Vector2 offset;
		List<Vector2> bananaPositions = new List<Vector2>();
		Vector2 portalPosition;

		List<SBaseEntity> tiles = new List<SBaseEntity>();
		Dictionary<string, Rectangle> cameraZonesWithOffset = new Dictionary<string, Rectangle>();
		public Dictionary<string, Rectangle> CameraZones { get { return cameraZonesWithOffset; } }
		public Dictionary<Point, EnemyEntity> Enemies = new Dictionary<Point, EnemyEntity>();
	}
}
