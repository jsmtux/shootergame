﻿using System;
using Microsoft.Xna.Framework;

namespace shooter
{
	public class CameraManager : SBaseSystem<CameraComponent>
	{
		public Matrix getTranslationMatrix()
		{
			return cachedTranslationMatrix;
		}

		public Rectangle getFustrumRectangle()
		{
			return cachedFrustumRectangle;
		}

		public Matrix getInvTranslationMatrix()
		{
			return cachedInvTranslationMatrix;
		}

		public Matrix getUITranslationMatrix()
		{
			return Matrix.Identity;
		}

		public override void UpdateLogic(GameTime gameTime)
		{
			base.UpdateLogic(gameTime);
			if (componentList.Count > 0)
			{
				cachedTranslationMatrix = componentList[0].TranslationMatrix;
				cachedFrustumRectangle = componentList[0].FrustumRectangle;
				cachedInvTranslationMatrix = componentList[0].InvTranslationMatrix;
			}
			else
			{
				cachedTranslationMatrix = Matrix.Identity;
				cachedFrustumRectangle = Rectangle.Empty;
				cachedInvTranslationMatrix = Matrix.Identity;
			}
		}

		Matrix cachedTranslationMatrix = Matrix.Identity;
		Rectangle cachedFrustumRectangle = Rectangle.Empty;
		Matrix cachedInvTranslationMatrix = Matrix.Identity;
	}
}
