﻿using System;
using ECS;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace shooter
{
	public class ModelRenderer : ComponentDrawerSystem<ModelComponent>
	{
		public ModelRenderer(Platform _platform, CameraManager _cameraManager, GraphicsDevice _graphicsDevice, LightingSystem _lightingSystem)
			: base(_cameraManager)
		{
			platform = _platform;
			cameraManager = _cameraManager;
			graphicsDevice = _graphicsDevice;
			lightingSystem = _lightingSystem;

			view = Matrix.CreateLookAt(new Vector3(0, 1000, 0), new Vector3(0, 0, 0), Vector3.UnitZ);
		}

		class ProcessedLight
		{
			public ProcessedLight(float _intensity, Color _color, Vector3 _direction)
			{
				intensity = _intensity;
				color = _color;
				direction = _direction;
			}
			public float intensity;
			public Color color;
			public Vector3 direction;
		}

		public override void Draw(GameTime gameTime, Depth depth)
		{

			var lights = lightingSystem.getLights();
			foreach (KeyValuePair<RenderConfiguration, List<ModelComponent>> componentSublist in orderedComponents[depth.toInt()])
			{
				List<ModelComponent> curList = componentSublist.Value;
				Matrix curMatrix;
				if (componentSublist.Key.translationType == RenderConfiguration.TranslationType.Camera)
				{
					curMatrix = cameraManager.getInvTranslationMatrix();
				}
				else
				{
					curMatrix = cameraManager.getUITranslationMatrix();
				}
				foreach (ModelComponent modelComponent in curList)
				{
					if (!InView(modelComponent.renderConfiguration, modelComponent))
						continue;

					Matrix projection;
					if (modelComponent.renderMode == ModelComponent.RenderMode.Perspective)
					{
						projection = Matrix.CreateTranslation(-0.5f, -0.5f, 0) *
								Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, (float)platform.Resolution.X/(float)platform.Resolution.Y, 1.0f, 1000f);
					}
					else
					{
						projection = Matrix.CreateTranslation(-0.5f, -0.5f, 0) *
								Matrix.CreateOrthographic(platform.Resolution.X, platform.Resolution.Y, 0.0f, 1000.0f);
					}
					modelComponent.UpdateForDraw(gameTime);

					LocationComponent locationComponent = modelComponent.locationComponent;

					Matrix worldMatrix = Matrix.CreateScale(modelComponent.scale)
								* Matrix.CreateFromYawPitchRoll(-locationComponent.ModelRotation.X, -locationComponent.ModelRotation.Y, -locationComponent.ModelRotation.Z)
								* Matrix.CreateTranslation(-locationComponent.Position.X + platform.Resolution.X / 2,
														   150,
														   -locationComponent.Position.Y + platform.Resolution.Y / 2)
								* curMatrix;

					foreach (ModelMesh mesh in modelComponent.model.Meshes)
					{
						if (modelComponent.customEffect == null)
						{
							foreach (Effect effect in mesh.Effects)
							{
								if (effect is BasicEffect basicEffect)
								{
									handleLighting(basicEffect, modelComponent, lights);
									basicEffect.World = worldMatrix;
									basicEffect.View = view;
									basicEffect.Projection = projection;
									basicEffect.TextureEnabled = true;
									basicEffect.Texture = modelComponent.texture;

									graphicsDevice.BlendState = modelComponent.renderConfiguration.getBlendState();
									basicEffect.Alpha = modelComponent.Alpha;
								}
								else if (effect is AlphaTestEffect alphaTestEffect)
								{
									alphaTestEffect.World = worldMatrix;
									alphaTestEffect.View = view;
									alphaTestEffect.Projection = projection;
									//alphaTestEffect.TextureEnabled = true;
									alphaTestEffect.Texture = modelComponent.texture;

									graphicsDevice.BlendState = modelComponent.renderConfiguration.getBlendState();
									alphaTestEffect.Alpha = modelComponent.Alpha;
								}
							}
						}
						else
						{
							foreach (ModelMeshPart part in mesh.MeshParts)
							{
								part.Effect = modelComponent.customEffect;
								modelComponent.customEffect.Parameters["World"].SetValue(worldMatrix);
								modelComponent.customEffect.Parameters["View"].SetValue(view);
								modelComponent.customEffect.Parameters["Projection"].SetValue(projection);
							}
						}

						mesh.Draw();
					}
				}
			}
		}

		private void handleLighting(BasicEffect _effect, ModelComponent _modelComponent, List<LightEmitterComponent> _lights)
		{
			_effect.EnableDefaultLighting();
			if (_modelComponent.tint != null && _modelComponent.tint != Color.Transparent)
			{
				_effect.AmbientLightColor = _modelComponent.tint.CurrentValue().ToVector3();
			}
			else
			{
				_effect.AmbientLightColor = baseAmbientColor;
			}

			_effect.DirectionalLight1.Enabled = false;
			_effect.DirectionalLight2.Enabled = false;

			_effect.DirectionalLight0.Enabled = true;

			if (_modelComponent.lightOverride != null && _modelComponent.lightOverride.light0 != null)
			{
				_effect.DirectionalLight0.DiffuseColor = _modelComponent.lightOverride.light0.diffuse.ToVector3();
				_effect.DirectionalLight0.Direction = _modelComponent.lightOverride.light0.direction;
				_effect.DirectionalLight0.Enabled = _modelComponent.lightOverride.light0.enabled;
			}
            
			if (_modelComponent.lightOverride != null && _modelComponent.lightOverride.ignoreProcessedLights)
			{
				return;
			}

			processedLights.Clear();
			foreach(var processedLight in processedLights)
			{
				processedLight.intensity = 0;
			}

			for(int i = 0; i < _lights.Count; i++)
			{
				LightEmitterComponent light = _lights[i];
				if (light.intensity < float.Epsilon)
				{
					continue;
				}
				var posDifference = light.locationComponent.Position - _modelComponent.locationComponent.Position;
				var curDistance = posDifference.LengthSquared();
				float percentage = 0f;
				if (curDistance > 0)
				{
					percentage = 1.0f / (light.attenuation * curDistance);
				}
				else
				{
					percentage = 1.0f;
				}
				if (percentage < 0.0001f)
				{
					continue;
				}

				var processedLight = new ProcessedLight(percentage * light.intensity, light.color, new Vector3(posDifference.X, 0, posDifference.Y));
				processedLights.Add(processedLight);
			}

			processedLights.Sort(delegate (ProcessedLight x, ProcessedLight y)
			{
				if (x.intensity > y.intensity) return -1;
				return 1;
			});

			if (processedLights.Count > 0)
			{
				_effect.DirectionalLight1.Enabled = true;
				_effect.DirectionalLight1.DiffuseColor = processedLights[0].color.ToVector3() * processedLights[0].intensity;
				_effect.DirectionalLight1.Direction = processedLights[0].direction;
			}

			if (processedLights.Count > 1)
			{
				_effect.DirectionalLight2.Enabled = true;
				_effect.DirectionalLight2.DiffuseColor = processedLights[1].color.ToVector3() * processedLights[1].intensity;
				_effect.DirectionalLight2.Direction = processedLights[1].direction;
			}
		}

		private Matrix view;

		private Platform platform;

		GraphicsDevice graphicsDevice;
		LightingSystem lightingSystem;

		List<ProcessedLight> processedLights = new List<ProcessedLight>();

		static readonly Vector3 baseAmbientColor = new Vector3(0.4f,0.4f,0.4f);
	}
}
