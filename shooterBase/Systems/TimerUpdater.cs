﻿using System;
using Microsoft.Xna.Framework;

namespace shooter
{
	public class TimerUpdater : SBaseSystem<BaseTimerComponent>
	{
		public void UpdateLogic(GameTime gameTime, bool isPaused)
		{
			base.UpdateLogic(gameTime);
			foreach (var component in componentList)
			{
				if (component.UpdateWhenPaused || !isPaused)
				{
					component.Update(gameTime.ElapsedGameTime);
				}
			}
		}
	}
}
