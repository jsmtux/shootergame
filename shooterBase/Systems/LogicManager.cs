﻿using System;
using ECS;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace shooter
{
	public class LogicManager : SBaseSystem<LogicComponent>
	{
		public void UpdateLogic(GameTime gameTime, bool gamePaused)
		{
			base.UpdateLogic(gameTime);
			foreach (var component in componentList)
			{
				if (!gamePaused || component.UpdateOnPause)
				{
					component.UpdateLogic(gameTime);
				}
			}
		}
	}
}
