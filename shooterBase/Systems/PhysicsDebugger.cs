﻿using System;
using ECS;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace shooter
{
	public class PhysicsDebugger : SBaseSystem<PhysicsBodyComponent>
	{
		public PhysicsDebugger(ContentManager _contentManager, SpriteBatch _spriteBatch, CameraManager _cameraManager)
		{
			contentManager = _contentManager;
			sphereImage = contentManager.Load<Texture2D>("debug/sphere");
			pixelImage = contentManager.Load<Texture2D>("ui/pixel");

			spriteBatch = _spriteBatch;

			cameraManager = _cameraManager;
		}

		public void Draw(GameTime gameTime)
		{
			spriteBatch.Begin(SpriteSortMode.Immediate, null,
							  null, null, null, null, cameraManager.getTranslationMatrix());

			foreach (var curComponent in componentList)
			{
				LocationComponent locationComponent = curComponent.locationComponent;
				if (curComponent is RoundPhysicsBodyComponent)
				{
					RoundPhysicsBodyComponent curBody = curComponent as RoundPhysicsBodyComponent;
					float size = curBody.Radius * 2;
					Vector2 origin = new Vector2(size / 2, size / 2);
					spriteBatch.Draw(sphereImage,
									 null,
									 new Rectangle((int)(locationComponent.Position.X),
												   (int)(locationComponent.Position.Y),
												   (int)size,
												   (int)size),
									 null,
									 new Vector2(50, 50),
									 0,
									 null,
									 null,
									 SpriteEffects.None);
				}
			}

			spriteBatch.End();
		}

		ContentManager contentManager;

		Texture2D sphereImage;
		Texture2D pixelImage;

		SpriteBatch spriteBatch;

		CameraManager cameraManager;
	}
}
