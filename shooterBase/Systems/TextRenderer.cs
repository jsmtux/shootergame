﻿using System;
using ECS;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace shooter
{
	public class TextRenderer : ComponentDrawerSystem<TextComponent>
	{
		public TextRenderer(SpriteBatch _spriteBatch, CameraManager _cameraManager)
			: base(_cameraManager)
		{
			SystemObserver.Subscribe(this);
			spriteBatch = _spriteBatch;
			cameraManager = _cameraManager;
		}

		public override void Draw(GameTime gameTime, Depth depth)
		{
			Vector2 origin = new Vector2();
			Vector2 position = new Vector2();
			foreach (KeyValuePair<RenderConfiguration, List<TextComponent>> componentSublist in orderedComponents[depth.toInt()])
			{
				RenderConfiguration curRenderConf = componentSublist.Key;
				Matrix curMatrix;
				if (curRenderConf.translationType == RenderConfiguration.TranslationType.UI)
				{
					curMatrix = cameraManager.getUITranslationMatrix();
				}
				else
				{
					curMatrix = cameraManager.getTranslationMatrix();
				}

				List<TextComponent> curList = componentSublist.Value;
				
				Effect curEffect = null;
				if (curRenderConf.effect != null)
				{
					curEffect = curRenderConf.effect.BaseEffect;
				}
				spriteBatch.Begin(SpriteSortMode.Deferred, curRenderConf.getBlendState(),
								  null, null, null, curEffect, curMatrix);

				foreach (var curText in curList)
				{
					if (!InView(curRenderConf, curText))
						continue;
					if (curText.Alpha > 0)
					{
						float scale = 1;
						LocationComponent locationComponent = curText.locationComponent;
						origin.X = locationComponent.Size.X / 2;
						origin.Y = locationComponent.Size.Y / 2;
						position.X = locationComponent.Position.X;
						position.Y = locationComponent.Position.Y;
						spriteBatch.DrawString(curText.font,
											   curText.Text,
						                       position,
						                       (Color)(curText.tint) * curText.Alpha,
											   0,
											   origin,
											   scale,
											   SpriteEffects.None,
											   curText.depth.toFloat());
					}
				}
				spriteBatch.End();
			}
		}

		SpriteBatch spriteBatch;
	}
}
