﻿using System;
using ECS;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace shooter
{
	public class PhysicsResolver : SBaseSystem<PhysicsBodyComponent>
	{
		public override void UpdateLogic(GameTime gameTime)
		{
			base.UpdateLogic(gameTime);
			foreach (var body1 in componentList)
			{
				if (!(body1 is RoundPhysicsBodyComponent)
				    || (body1.locationComponent.velocity == Vector2.Zero && !body1.alwaysCheck) // if not moving, won't crash
				    || body1.collisionDelegate == null)
				{
					continue;
				}
				var roundBody1 = body1 as RoundPhysicsBodyComponent;
				foreach (var body2 in componentList)
				{
					if (body1 == body2)
					{
						continue;
					}

					if (body2 is RoundPhysicsBodyComponent)
					{
						AddManifoldIfCollided(body1 as RoundPhysicsBodyComponent, body2 as RoundPhysicsBodyComponent);
						AddManifoldIfCollided(body2 as RoundPhysicsBodyComponent, body1 as RoundPhysicsBodyComponent);
					}
				}
			}

			foreach (var it in manifolds)
			{
				List<Vector2> suggestedPositions = new List<Vector2>();
				foreach (var collider in it.Value)
				{
					if (it.Key.collisionDelegate(collider.collidingEntity, collider.collisionPos) && it.Key.fixPositionOnCollision)
					{
						suggestedPositions.Add(collider.position);
					}
				}
				// TODO use a real prediction instead of just the first position found
#if false
				if (suggestedPositions.Count > 0)
				{
					it.Key.locationComponent.Position = suggestedPositions[0];
				}
#else
				if (suggestedPositions.Count > 0)
				{
					Vector2 meanPosition = Vector2.Zero;
					foreach(var suggestedPosition in suggestedPositions)
					{
						meanPosition += suggestedPosition;
					}
					meanPosition = meanPosition / suggestedPositions.Count;
					it.Key.locationComponent.Position = meanPosition;
				}
#endif
			}
			manifolds.Clear();
		}


		void AddManifoldIfCollided(RoundPhysicsBodyComponent collider, RoundPhysicsBodyComponent collided)
		{
			Vector2? clearPosition;
			if (collider.collisionDelegate != null && (clearPosition = GetClearPositionRoundRound(collider, collided)) != null)
			{
				var collision = GetCollisionPositionRoundRound(collider, collided);
				var manifold = new Manifold(collider, collided.getEntity(), clearPosition.GetValueOrDefault(), collision);
				AddManifoldToBody(collider, manifold);
			}
		}

		void AddManifoldToBody(PhysicsBodyComponent _body, Manifold _manifold)
		{
			if (!manifolds.ContainsKey(_body))
			{
				manifolds[_body] = new List<Manifold>();
			}
			foreach (var manifold in manifolds[_body])
			{
				if (manifold.collidingEntity == _manifold.collidingEntity)
				{
					return;
				}
			}
			manifolds[_body].Add(_manifold);
		}

		public Vector2? GetClearPositionRoundRound(RoundPhysicsBodyComponent _body1, RoundPhysicsBodyComponent _body2)
		{
			Vector2? newPosition = null;
			float minDistance = _body1.Radius + _body2.Radius;
			float distance = Vector2.Distance(_body1.locationComponent.Position, _body2.locationComponent.Position);
			if (distance < minDistance)
			{
				Vector2 positionDifference = _body1.locationComponent.Position - _body2.locationComponent.Position;
				positionDifference.Normalize();
				newPosition = _body2.locationComponent.Position + positionDifference * minDistance * 1.2f;
			}

			return newPosition;
		}

		public Vector2 GetCollisionPositionRoundRound(RoundPhysicsBodyComponent _body1, RoundPhysicsBodyComponent _body2)
		{
			Vector2 positionDifference = _body2.locationComponent.Position - _body1.locationComponent.Position;
			positionDifference.Normalize();
			return _body1.locationComponent.Position + positionDifference * _body1.Radius;
		}

		public bool isColliding(Vector2 _position, float _radius)
		{
			foreach (var body in componentList)
			{
				if (body is RoundPhysicsBodyComponent)
				{
					float minDistance = (body as RoundPhysicsBodyComponent).Radius + _radius;
					float distance = Vector2.Distance(body.locationComponent.Position, _position);
					if (distance < minDistance)
					{
						return true;
					}
				}
			}

			return false;
		}

		internal class Manifold
		{
			internal Manifold(PhysicsBodyComponent _body1, SBaseEntity _collidingEntity, Vector2 _position, Vector2 _collisionPos)
			{
				body1 = _body1;
				collidingEntity = _collidingEntity;
				position = _position;
				collisionPos = _collisionPos;
			}
			public readonly PhysicsBodyComponent body1;
			public readonly SBaseEntity collidingEntity;
			public readonly Vector2 position;
			public readonly Vector2 collisionPos;
		}

		private Dictionary<PhysicsBodyComponent, List<Manifold>> manifolds = new Dictionary<PhysicsBodyComponent, List<Manifold>>();
	}
}
