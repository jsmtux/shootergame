﻿using System;
using ECS;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace shooter
{
	public abstract class SBaseSystem<T> : IReactiveSystem where T : SBaseComponent
	{
		#region IReactiveSystem implementation
		public Filter filterMatch
		{
			get { return new Filter().AllOf(typeof(T)); }
		}

		public void Execute(Entity _entity)
		{
			if (!newEntityList.Contains(_entity))
			{
				newEntityList.Add(_entity);
			}
		}
		#endregion

		public SBaseSystem()
		{
			SystemObserver.Subscribe(this);
			componentList = new List<T>();
			newEntityList = new List<Entity>();
		}

		public virtual void UpdateLogic(GameTime gameTime)
		{
			foreach (Entity entity in newEntityList)
			{
				foreach (var component in entity.GetComponents<T>())
				{
					componentList.Add(component);
					AddComponent(component);
				}
			}

			newEntityList.Clear();

			for (int i = componentList.Count - 1; i >= 0; i--)
			{
				if (componentList[i].getEntity() == null)
				{
					RemoveComponent(componentList[i]);
					componentList.RemoveAt(i);
				}
				else if(componentList[i].getEntity().ShouldBeDeleted)
				{
					componentList[i].entity.RemoveComponent<T>();
					RemoveComponent(componentList[i]);
					componentList.RemoveAt(i);
				}
			}
		}

		public void AddManualComponent(Entity _e)
		{
			newEntityList.Add(_e);
		}

		virtual protected void AddComponent(T component)
		{
		}

		virtual protected void RemoveComponent(T component)
		{
		}

		private List<Entity> newEntityList;
		protected List<T> componentList;
	}
}
