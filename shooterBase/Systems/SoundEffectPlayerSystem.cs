using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace shooter
{
	public class SoundEffectPlayerSystem : SBaseSystem<SoundEffectComponent>
	{
        public SoundEffectPlayerSystem(SaveDataManager _saveDataManager)
        {
            SoundEffect.MasterVolume = _saveDataManager.Data.audioFxVolume;
            _saveDataManager.SavedDataChanged += (Object sender, SaveDataManager.SavedDataEventArgs e) => {
                SoundEffect.MasterVolume = e.data.audioFxVolume;
            };
        }
	}
}
