﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;

namespace shooter
{
	public class ActiveTouch
	{
		internal ActiveTouch(Vector2 _position)
		{
			id = tId++;
			position = _position;
		}

        static private int tId = 0;
		public readonly int id;
		public Vector2 Position { get { return position; } }
		internal Vector2 position;
	}

	public abstract class TouchManager : SBaseSystem<TouchHandlerComponent>
	{
		public TouchManager(Platform _platform)
		{
			platform = _platform;
			RecalculateScreenOffset();
		}

		public void RecalculateScreenOffset()
		{
			Rectangle outRect = platform.getDrawPosition();
			screenRatio = outRect.Width / (float)platform.Resolution.X;
            screenOffset = new Vector2(outRect.Location.X, outRect.Location.Y);
		}

		protected void notifyHandlersNewTouch(ActiveTouch _touch)
		{
			foreach(var handler in componentList)
			{
				if (handler.HandleStartTouch(_touch))
				{
					break;
				}
			}
		}

        protected Vector2 projectPosition(Vector2 _touchPosition)
        {
            return ((_touchPosition - screenOffset) / screenRatio);
        }

		protected void notifyHandlersStopTouch(ActiveTouch _touch)
		{
			foreach (var handler in componentList)
			{
				handler.HandleStopTouch(_touch.id);
			}
		}

        float screenRatio;
        Vector2 screenOffset;
		Platform platform;
	}

	public class MobileTouchManager : TouchManager
	{
		public MobileTouchManager(Platform _platform)
			: base(_platform)
		{
        }

        public override void UpdateLogic(GameTime _elapsedTime)
		{
			base.UpdateLogic(_elapsedTime);
            movement = Vector2.Zero;
            TouchCollection touchCollection = TouchPanel.GetState();
            foreach (TouchLocation touch in touchCollection)
            {
                Vector2 touchPosition = projectPosition(touch.Position);
                if (activeTouches.ContainsKey(touch.Id))
                {
                    activeTouches[touch.Id].position = touchPosition;
                }
                else
                {
                    activeTouches[touch.Id] = new ActiveTouch(touchPosition);
                    notifyHandlersNewTouch(activeTouches[touch.Id]);
                }
            }

            foreach(TouchLocation touch in touchCollection)
            {
                if (!activeTouches.ContainsKey(touch.Id) || touch.State == TouchLocationState.Released)
                {
                    notifyHandlersStopTouch(activeTouches[touch.Id]);
                    activeTouches[touch.Id] = null;
                }
            }
        }

        Dictionary<int, ActiveTouch> activeTouches = new Dictionary<int, ActiveTouch>();
        Vector2 movement;
    }

	public class DesktopTouchManager : TouchManager
	{
		public DesktopTouchManager(Platform _platform)
			: base(_platform)
		{
		}

		public override void UpdateLogic(GameTime _elapsedTime)
		{
			base.UpdateLogic(_elapsedTime);
			MouseState mouseState = Mouse.GetState();
			//Mouse.SetPosition((int)UILocationComponent.Resolution.X / 2, (int)UILocationComponent.Resolution.Y / 2);
			//prevPosition = new Vector2(UILocationComponent.Resolution.X / 2, UILocationComponent.Resolution.Y / 2);
			
			if (mouseState.LeftButton == ButtonState.Pressed)
			{
				Vector2 currentPosition = projectPosition(new Vector2(mouseState.Position.X, mouseState.Position.Y));
				if (pressed)
				{
					currentActiveTouch.position = currentPosition;
				}
				else
				{
					currentActiveTouch = new ActiveTouch(currentPosition);
					notifyHandlersNewTouch(currentActiveTouch);
					currentActiveTouch.position = Vector2.Zero;
					pressed = true;
				}
			}
			else
			{
				if (pressed)
				{
					pressed = false;
					notifyHandlersStopTouch(currentActiveTouch);
					currentActiveTouch = null;
				}
			}
		}

		bool pressed = false;
		ActiveTouch currentActiveTouch;
	}
}
