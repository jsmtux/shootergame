﻿using System;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace shooter
{
	public abstract class ComponentDrawerSystem<T> : SBaseSystem<T> where T : DrawableComponent
	{
		protected ComponentDrawerSystem(CameraManager _cameraManager)
		{
			cameraManager = _cameraManager;
			for (int i = 0; i < Depth.numDepthTypes; i++)
			{
				orderedComponents[i] = new Dictionary<RenderConfiguration, List<T>>();
			}
		}

		public abstract void Draw(GameTime gameTime, Depth depth);

		protected bool InView(RenderConfiguration _configuration, T _component)
		{
			Rectangle cameraFrustum = cameraManager.getFustrumRectangle();
			return _configuration.translationType == RenderConfiguration.TranslationType.UI ||
						   _component.viewVolume == null || _component.viewVolume.Intersects(cameraFrustum);
		}

		public bool NeedsDrawing(int _depthToDraw)
		{
			return orderedComponents[_depthToDraw].Count > 0;
		}

		override protected void AddComponent(T component)
		{
			if (!orderedComponents[component.depth.toInt()].ContainsKey(component.renderConfiguration))
			{
				orderedComponents[component.depth.toInt()][component.renderConfiguration] = new List<T>();
			}
			orderedComponents[component.depth.toInt()][component.renderConfiguration].Add(component);
		}

		override protected void RemoveComponent(T component)
		{
			orderedComponents[component.depth.toInt()][component.renderConfiguration].Remove(component);
		}

		protected Dictionary<RenderConfiguration, List<T>>[] orderedComponents = new Dictionary<RenderConfiguration, List<T>>[Depth.numDepthTypes];
		protected CameraManager cameraManager;
	}
}
