﻿using System;
using Microsoft.Xna.Framework;

namespace shooter
{
	public class PhysicsUpdater : SBaseSystem<LocationComponent>
	{
		public override void UpdateLogic(GameTime gameTime)
		{
			base.UpdateLogic(gameTime);
			foreach (var component in componentList)
			{
				if (component is SimplePhysicsComponent)
				{
					SimplePhysicsComponent body = (SimplePhysicsComponent)component;
					Vector2 initPos = body.Position;

					initPos.X += body.velocity.X * gameTime.ElapsedGameTime.Ticks / TimeSpan.TicksPerSecond;
					initPos.Y += body.velocity.Y * gameTime.ElapsedGameTime.Ticks / TimeSpan.TicksPerSecond;

					body.Position = initPos;
				}
			}
		}
	}
}
