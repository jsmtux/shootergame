﻿using System;
using Microsoft.Xna.Framework;

namespace shooter
{
	public class PlayerCrashedMessage : Message
	{
		public PlayerCrashedMessage(SBaseEntity _e, Vector2 _collisionPos)
		{
			collidedEntity = _e;
			collisionPos = _collisionPos;
		}

		public SBaseEntity collidedEntity;
		public Vector2 collisionPos;
	}
}
