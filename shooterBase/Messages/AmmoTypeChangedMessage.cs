using System;
namespace shooter
{
	public class AmmoTypeChangedMessage : Message
	{
		public AmmoTypeChangedMessage(AmmoType _type)
		{
			type = _type;
		}

		public readonly AmmoType type;
	}
}
