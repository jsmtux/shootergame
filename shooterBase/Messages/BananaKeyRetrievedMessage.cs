﻿using System;
using Microsoft.Xna.Framework;

namespace shooter
{
	public class BananaKeyRetrievedMessage : Message
	{
		public BananaKeyRetrievedMessage(Vector2 _position)
		{
			position = _position;
		}

		public readonly Vector2 position;
	}
}
