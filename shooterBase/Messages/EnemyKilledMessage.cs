﻿namespace shooter
{
	public class EnemyKilledMessage : Message
	{
		public EnemyKilledMessage(EnemyEntity _entity)
		{
			entity = _entity;
		}

		public EnemyEntity getEnemy()
		{
			return entity;
		}

		EnemyEntity entity;
	}
}
