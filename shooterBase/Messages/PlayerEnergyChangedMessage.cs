namespace shooter
{
	public class PlayerEnergyChangedMessage : Message
	{
		public PlayerEnergyChangedMessage(float _totalPercentage)
		{
			totalPercentage = _totalPercentage;
		}

		readonly public float totalPercentage;
	}
}
