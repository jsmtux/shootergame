﻿namespace shooter
{
	public class PortalTouchedMessage : Message
	{
		public PortalTouchedMessage(PortalEntity _entity)
		{
			entity = _entity;
		}

		public readonly PortalEntity entity;
	}
}
