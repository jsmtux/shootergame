using System;
namespace shooter
{
	public class PlayerShotMessage : Message
	{
		public PlayerShotMessage(AmmoType _type)
		{
            ammoType = _type;
		}

        public AmmoType ammoType;
	}
}
