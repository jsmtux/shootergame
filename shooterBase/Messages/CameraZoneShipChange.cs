﻿using System;
namespace shooter
{
	public class CameraZoneShipChange : Message
	{
		public CameraZoneShipChange(ChangeType _type, string _zoneName)
		{
			type = _type;
			zoneName = _zoneName;
		}

		public enum ChangeType { Entered, Exited}
		public ChangeType type;
		public string zoneName;
	}
}
