namespace shooter
{
	public class AmmoDroppedMessage : Message
	{
		public AmmoDroppedMessage(AmmoType _type, int _amount)
		{
            type  = _type;
            amount = _amount;
		}

        public readonly AmmoType type;
        public readonly int amount;
	}
}
