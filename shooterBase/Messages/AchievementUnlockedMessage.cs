﻿namespace shooter
{
	public class AchievementUnlockedMessage : Message
	{
		public AchievementUnlockedMessage(AchievementDescription _achievementDescription)
		{
			achievementDescription = _achievementDescription;
		}

		readonly public AchievementDescription achievementDescription;
	}
}
