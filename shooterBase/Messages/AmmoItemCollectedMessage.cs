using System;
namespace shooter
{
	public class AmmoItemCollectedMessage : Message
	{
		public AmmoItemCollectedMessage(AmmoType _type)
		{
			type = _type;
		}

		public readonly AmmoType type;
	}
}
