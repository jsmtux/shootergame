﻿namespace shooter
{
	public class PlayerLifeChangedMessage : Message
	{
		public PlayerLifeChangedMessage(float _totalPercentage)
		{
			totalPercentage = _totalPercentage;
		}

		readonly public float totalPercentage;
	}
}
