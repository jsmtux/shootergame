﻿namespace shooter
{
	public class EnemySpawnedMessage : Message
	{
		public EnemySpawnedMessage(EnemyEntity _entity)
		{
			entity = _entity;
		}

		public EnemyEntity getEnemy()
		{
			return entity;
		}

		EnemyEntity entity;
	}
}
