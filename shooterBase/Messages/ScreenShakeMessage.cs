﻿using System;

namespace shooter
{
	public class ScreenShakeMessage : Message
	{
		public ScreenShakeMessage(TimeSpan _time)
		{
			time = _time;
		}

		readonly public TimeSpan time;
	}
}
