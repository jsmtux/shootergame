﻿using System;
using Microsoft.Xna.Framework;

namespace shooter
{
	public class Velocity
	{
		public Velocity(float _unitsPerSecond)
		{
			unitsPerSecond = _unitsPerSecond;
		}

		public float GetUnitsIn(GameTime _gameTime)
		{
			return unitsPerSecond * _gameTime.ElapsedGameTime.Ticks / TimeSpan.TicksPerSecond;
		}

		public float NominalValue()
		{
			return unitsPerSecond;
		}

		float unitsPerSecond;
	}
}
