﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
#if MONOMAC
using MonoMac.AppKit;
using MonoMac.Foundation;
#elif __IOS__ || __TVOS__
using Foundation;
using UIKit;
#endif
#endregion
using Microsoft.Xna.Framework;

namespace shooter
{
#if __IOS__ || __TVOS__
    [Register("AppDelegate")]
    class Program : UIApplicationDelegate
#else
	static class Program
#endif
	{
		private static ShooterGame game;

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
#if !MONOMAC && !__IOS__ && !__TVOS__
		[STAThread]
#endif
		static void Main(string[] args)
		{
#if MONOMAC
            NSApplication.Init ();

            using (var p = new NSAutoreleasePool ()) {
                NSApplication.SharedApplication.Delegate = new AppDelegate();
                NSApplication.Main(args);
            }
#elif __IOS__ || __TVOS__
            UIApplication.Main(args, null, "AppDelegate");
#else

            var desktopPlatform = new DesktopPlatform();
			game = new ShooterGame(desktopPlatform);


            GameWindow gameWindow = game.Window;
			gameWindow.AllowUserResizing = true;
			gameWindow.ClientSizeChanged += desktopPlatform.OnResize;

            bool properStart = true;
            int argNo = 0;
            while (argNo < args.Count())
            {
                switch(args[argNo])
                {
                    case "StartGameStage":
                        game.LaunchParameters.Add("StartGameStage", args[++argNo]);
                        break;
                    default:
                        Console.WriteLine("Error starting game. Command line parameter " + args[argNo] + " not supported.");
                        properStart = false;
                        break;
                }
                argNo++;
            }

            if (properStart)
            {
                game.Run();
            }
            else
            {
                Console.WriteLine("Command line options for shooter game");
                Console.WriteLine("* StartGameStage [Maps/01_basic_flight.tmx]: start define game stage directly");
            }

#if !__IOS__ && !__TVOS__
			game.Dispose();
#endif

#endif
		}

#if __IOS__ || __TVOS__
        public override void FinishedLaunching(UIApplication app)
        {
            RunGame();
        }
#endif
	}

#if MONOMAC
    class AppDelegate : NSApplicationDelegate
    {
        public override void FinishedLaunching (MonoMac.Foundation.NSObject notification)
        {
            AppDomain.CurrentDomain.AssemblyResolve += (object sender, ResolveEventArgs a) =>  {
                if (a.Name.StartsWith("MonoMac")) {
                    return typeof(MonoMac.AppKit.AppKitFramework).Assembly;
                }
                return null;
            };
            Program.RunGame();
        }

        public override bool ApplicationShouldTerminateAfterLastWindowClosed (NSApplication sender)
        {
            return true;
        }
    }  
#endif
}
