﻿using System;

namespace shooter
{
	public class AchievementDescription
	{
		internal AchievementDescription(string _title, string _description, Type _achievementType)
		{
			title = _title;
			description = _description;
			achievementEntityType = _achievementType;
		}

		string getTitle()
		{
			return title;
		}

		string getDescription()
		{
			return description;
		}

		public AchievementEntity CreateNewAchievementLogic()
		{
			return (AchievementEntity)Activator.CreateInstance(achievementEntityType, this);
		}

		public readonly string title;
		public readonly string description;
		bool active = true;
		public readonly Type achievementEntityType;

	}

	public abstract class AchievementEntity : SBaseEntity
	{
		protected AchievementEntity(AchievementDescription _parent)
		{
			Parent = _parent;
		}

		protected override void DoCreation(Stage _stage)
		{ }


		public virtual void Restart()
		{
		}

		public abstract bool checkUnlockedAchievement(Message _receivedMessage);

		public virtual bool checkUnlockedAchievementAtStageEnd()
		{
			return false;
		}
		public abstract bool IsAchievable();

		public readonly AchievementDescription Parent;
	}
}
