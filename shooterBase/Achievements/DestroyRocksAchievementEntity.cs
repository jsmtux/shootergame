﻿using System;
namespace shooter
{
	public class DestroyRocksAchievementEntity : AchievementEntity
	{
		public DestroyRocksAchievementEntity(AchievementDescription parent, int _numRocks)
			: base(parent)
		{
			neededRocks = _numRocks;
		}

		public override bool checkUnlockedAchievement(Message _receivedMessage)
		{
			if (_receivedMessage is BrokenFenceMessage)
			{
				destroyedRocks++;
			}
			return destroyedRocks >= neededRocks;
		}

		public override bool IsAchievable()
		{
			return true;
		}

		public int GetDestroyedRocks()
		{
			return destroyedRocks;
		}

		public int GetTotalRocks()
		{
			return neededRocks;
		}

		int destroyedRocks = 0;
		public readonly int neededRocks;
	}

	public class Destroy9RocksAchievementEntity : DestroyRocksAchievementEntity
	{
		public Destroy9RocksAchievementEntity(AchievementDescription parent) : base(parent, numRocks)
		{
		}

		public static int numRocks = 9;
	}
}
