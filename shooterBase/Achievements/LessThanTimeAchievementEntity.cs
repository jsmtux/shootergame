﻿using System;
namespace shooter
{
	public class LessThanTimeAchievementEntity : AchievementEntity
	{
		public LessThanTimeAchievementEntity(AchievementDescription parent, TimeSpan _maxTime)
			: base(parent)
		{
			maxTime = _maxTime;
		}

		public override void Restart()
		{
			timer.Start();
		}

		public override bool checkUnlockedAchievement(Message _receivedMessage)
		{
			return false;
		}

		public override bool checkUnlockedAchievementAtStageEnd()
		{
			Console.WriteLine("Elapsed time is " + timer.CurrentTime + " and maximum is " + maxTime);
			return timer.CurrentTime <= maxTime;
		}

		protected override void DoCreation(Stage _stage)
		{
			timer = new TimerComponent(false);
			AddComponent(timer);
		}

		public override bool IsAchievable()
		{
			return timer.CurrentTime < maxTime;
		}

		public int GetElapsedSeconds()
		{
			return (int)timer.CurrentTime.TotalSeconds;
		}

		TimerComponent timer;
		TimeSpan maxTime;
	}

	public class LessThan1MinuteAchievement : LessThanTimeAchievementEntity
	{
		public LessThan1MinuteAchievement(AchievementDescription parent)
			: base(parent, TimeSpan.FromMinutes(1.0f))
		{}
	}

	public class LessThan90sAchievement : LessThanTimeAchievementEntity
	{
		public LessThan90sAchievement(AchievementDescription parent)
			: base(parent, TimeSpan.FromSeconds(90.0f))
		{}
	}

	public class LessThan2MAchievement : LessThanTimeAchievementEntity
	{
		public LessThan2MAchievement(AchievementDescription parent)
			: base(parent, TimeSpan.FromMinutes(2))
		{}
	}

	public class LessThan3MAchievement : LessThanTimeAchievementEntity
	{
		public LessThan3MAchievement(AchievementDescription parent)
			: base(parent, TimeSpan.FromMinutes(3))
		{}
	}
}
