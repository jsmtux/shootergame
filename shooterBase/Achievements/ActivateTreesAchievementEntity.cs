using System;
namespace shooter
{
	public class ActivateTreesAchievementEntity : AchievementEntity
	{
		public ActivateTreesAchievementEntity(AchievementDescription parent, int _totalTrees)
			: base(parent)
		{
			totalTrees = _totalTrees;
		}

		public override bool checkUnlockedAchievement(Message _receivedMessage)
		{
			if (_receivedMessage is TreeActivatedMessage)
			{
				numTrees++;
			}
			return numTrees >= totalTrees;
		}

		public override bool checkUnlockedAchievementAtStageEnd()
		{
			return numTrees >= totalTrees;
		}

		public override bool IsAchievable()
		{
			return true;
		}

		public int GetActivatedTrees()
		{
			return numTrees;
		}

        public int GetTargetTrees()
        {
            return totalTrees;
        }

		TimerComponent timer;
		int totalTrees;
        int numTrees = 0;
	}

	public class Activate4TreesAchievementEntity : ActivateTreesAchievementEntity
	{
		public Activate4TreesAchievementEntity(AchievementDescription parent)
			: base(parent, 4)
		{}
	}

	public class Activate7TreesAchievementEntity : ActivateTreesAchievementEntity
	{
		public Activate7TreesAchievementEntity(AchievementDescription parent)
			: base(parent, 7)
		{}
	}

	public class Activate10TreesAchievementEntity : ActivateTreesAchievementEntity
	{
		public Activate10TreesAchievementEntity(AchievementDescription parent)
			: base(parent, 10)
		{}
	}
}
