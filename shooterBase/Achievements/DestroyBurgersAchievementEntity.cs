using System;
namespace shooter
{
	public class DestroyBurgersAchievementEntity : AchievementEntity
	{
		public DestroyBurgersAchievementEntity(AchievementDescription parent, int _numBurgers)
			: base(parent)
		{
			neededBurgers = _numBurgers;
		}

		public override bool checkUnlockedAchievement(Message _receivedMessage)
		{
			if (_receivedMessage is EnemyKilledMessage enemyKilledMessage &&
                enemyKilledMessage.getEnemy() is BurgerEnemyEntity)
			{
				destroyedBurgers++;
			}
			return destroyedBurgers >= neededBurgers;
		}

		public override bool IsAchievable()
		{
			return true;
		}

		public int GetDestroyedBurgers()
		{
			return destroyedBurgers;
		}

		public int GetTotalBurgers()
		{
			return neededBurgers;
		}

		int destroyedBurgers = 0;
		public readonly int neededBurgers;
	}

	public class Destroy5BurgersAchievementEntity : DestroyBurgersAchievementEntity
	{
		public Destroy5BurgersAchievementEntity(AchievementDescription parent) : base(parent, 5)
		{
		}
	}

	public class Destroy7BurgersAchievementEntity : DestroyBurgersAchievementEntity
	{
		public Destroy7BurgersAchievementEntity(AchievementDescription parent) : base(parent, 7)
		{
		}
	}
}
