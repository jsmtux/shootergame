﻿using System;
namespace shooter
{
	public class LessThanCrashAchievementEntity : AchievementEntity
	{
		public LessThanCrashAchievementEntity(int _maxCrashes, AchievementDescription parent)
			: base(parent)
		{
			maxCrashes = _maxCrashes;
		}

		public override bool checkUnlockedAchievement(Message _receivedMessage)
		{
			if (_receivedMessage is PlayerCrashedMessage)
			{
				if (lastCrashTime.CurrentTime > TimeSpan.FromMilliseconds(500))
				{
					lastCrashTime.Start();
					numCrashes++;
				}
			}
			return false;
		}
		
		protected override void DoCreation(Stage _stage)
		{
			lastCrashTime = new TimerComponent();
			AddComponent(lastCrashTime);
			lastCrashTime.Start();
		}

		public override bool IsAchievable()
		{
			return numCrashes < maxCrashes;
		}

		public override bool checkUnlockedAchievementAtStageEnd()
		{
			return numCrashes < maxCrashes;
		}

		public int GetNumCrashes()
		{
			return numCrashes;
		}

		int numCrashes = 0;
		int maxCrashes;
		TimerComponent lastCrashTime;
	}

	public class LessThan3CrashAchievementEntity: LessThanCrashAchievementEntity
	{
		public LessThan3CrashAchievementEntity(AchievementDescription parent)
			: base(3, parent)
		{
		}
	}

	public class LessThan5CrashAchievementEntity: LessThanCrashAchievementEntity
	{
		public LessThan5CrashAchievementEntity(AchievementDescription parent)
			: base(5, parent)
		{
		}
	}
}
