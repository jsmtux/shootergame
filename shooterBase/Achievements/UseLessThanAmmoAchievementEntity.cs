using System;
namespace shooter
{
	public class UseLessThanAmmoAchievementEntity : AchievementEntity
	{
		public UseLessThanAmmoAchievementEntity(AchievementDescription parent, int _totalSeeds, AmmoType _type)
			: base(parent)
		{
            totalAmmo = _totalSeeds;
			type = _type;
		}

		public override bool checkUnlockedAchievement(Message _receivedMessage)
		{
			if (_receivedMessage is PlayerShotMessage shootMessage && shootMessage.ammoType == type)
			{
				numUsedAmmo++;
			}
			return false;
		}

		public override bool checkUnlockedAchievementAtStageEnd()
		{
			return numUsedAmmo <= totalAmmo;
		}

		public override bool IsAchievable()
		{
			return numUsedAmmo <= totalAmmo;
		}

		public int GetUsedAmmo()
		{
			return numUsedAmmo;
		}

        public int GetAvailableAmmo()
        {
            return totalAmmo;
        }

		int totalAmmo;
        int numUsedAmmo = 0;
		AmmoType type;
	}

	public class UseLessThanCarrotsAchievementEntity : UseLessThanAmmoAchievementEntity
	{
		public UseLessThanCarrotsAchievementEntity(AchievementDescription parent, int _totalSeeds)
			: base(parent, _totalSeeds, AmmoType.Carrot)
		{
		}
	}

	public class Shoot20ThanCarrotsAchievementEntity : UseLessThanCarrotsAchievementEntity
	{
		public Shoot20ThanCarrotsAchievementEntity(AchievementDescription parent)
			: base(parent, 20)
		{}
	}
}
