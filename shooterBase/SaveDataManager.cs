﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.Xna.Framework;

namespace shooter
{
	[System.Serializable]
	public class StageDescriptionSavedData
	{
		public StageDescriptionSavedData(StageDescription _stageDescription, Dictionary<string, bool> _achievementState)
		{
			path = _stageDescription.Path;
			goalStates = _achievementState;
		}
		public StageDescriptionSavedData(string _stagePath)
		{
			path = _stagePath;
		}

		public SaveData Clone()
		{
			return (SaveData)this.MemberwiseClone();
		}

		public UILevelBtnEntity.State GetStageState()
		{
			var ret = UILevelBtnEntity.State.Locked;

			int achievedGoals = 0;
			foreach(var state in goalStates)
			{
				if(state.Value)
				{
					achievedGoals++;
				}
			}
			if (achievedGoals > 1)
			{
				ret = UILevelBtnEntity.State.Star3;
			}
			else if (achievedGoals > 0)
			{
				ret = UILevelBtnEntity.State.Star2;
			}
			else if (passed)
			{
				ret = UILevelBtnEntity.State.Star;
			}
			else if (accesible)
			{
				ret = UILevelBtnEntity.State.Open;
			}

			return ret;
		}

		public string path;
		public Dictionary<string, bool> goalStates = new Dictionary<string, bool>();
		public bool accesible = false;
		public bool passed = false;
		public bool notifyAchieved = false;
	}

	[System.Serializable]
	public class SaveData
	{
		public SaveData Clone()
		{
			return (SaveData)this.MemberwiseClone();
		}

		public UILevelBtnEntity.State GetStageState(StageDescription _stage)
		{
			var ret = UILevelBtnEntity.State.Locked;

			if (savedStages.ContainsKey(_stage.Path))
			{
				return savedStages[_stage.Path].GetStageState();
			}

			return ret;
		}

		public bool ShouldNotifyStageUpdate(StageDescription _stage)
		{
			bool ret = false;

			if (savedStages.ContainsKey(_stage.Path))
			{
				ret = savedStages[_stage.Path].notifyAchieved;
				savedStages[_stage.Path].notifyAchieved = false;
			}

			return ret;
		}

		public Dictionary<string, bool> UpdateWithFinishedStatus(
			StageDescription _stage,
			Dictionary<string, bool> _achievementStatus)
		{
			Dictionary<string, bool> newStatus = new Dictionary<string, bool>();
			if (!savedStages.ContainsKey(_stage.Path))
			{
				CreateStageSavedData(_stage.Path);
			}
			Dictionary<string, bool> currentStatus = savedStages[_stage.Path].goalStates;
			bool achievedNew = false;
			foreach(var achievement in _achievementStatus)
			{
				bool previouslyAchieved = false;
				currentStatus.TryGetValue(achievement.Key, out previouslyAchieved);
				if (achievement.Value && !previouslyAchieved)
				{
					achievedNew = true;
				}
				newStatus[achievement.Key] = achievement.Value || previouslyAchieved;
			}
			savedStages[_stage.Path].notifyAchieved |= achievedNew;
			savedStages[_stage.Path].passed = true;
			savedStages[_stage.Path].goalStates = newStatus;
			return newStatus;
		}

		public void AddOpenedStage(StageDescription stage)
		{
			CreateStageSavedData(stage.Path);
		}

		void CreateStageSavedData(string _stagePath)
		{
			StageDescriptionSavedData stageDescriptionSavedData = new StageDescriptionSavedData(_stagePath);
			stageDescriptionSavedData.accesible = true;
			savedStages[_stagePath] = stageDescriptionSavedData;
		}

		public Dictionary<string, StageDescriptionSavedData> savedStages = new Dictionary<string, StageDescriptionSavedData>();
		public float musicVolume = 1.0f;
		public float audioFxVolume = 1.0f;
	}

	public class SaveDataManager
	{
		public SaveDataManager()
		{
			savedData = new SaveData();
			inUseData = new SaveData();

            var path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            dataPath = Path.Combine(path, fileName);
        }

		public void Save()
		{
			XmlWriter xmlWriter = XmlWriter.Create(dataPath);
			xmlWriter.WriteStartDocument();

			xmlWriter.WriteStartElement("pizza");
			foreach (var stage in inUseData.savedStages)
			{
				xmlWriter.WriteStartElement("stage");
				xmlWriter.WriteAttributeString("path", stage.Key);
				xmlWriter.WriteAttributeString("accesible", stage.Value.accesible ? "yes" : "no");
				xmlWriter.WriteAttributeString("passed", stage.Value.passed ? "yes" : "no");
				xmlWriter.WriteAttributeString("notifyUpdate", stage.Value.notifyAchieved ? "yes" : "no");

				xmlWriter.WriteStartElement("goalStates");
				foreach (var goal in stage.Value.goalStates)
				{
					xmlWriter.WriteStartElement("goal");
					xmlWriter.WriteAttributeString("name", goal.Key);
					xmlWriter.WriteAttributeString("passed", goal.Value ? "yes" : "no");
					xmlWriter.WriteEndElement();
				}

				xmlWriter.WriteEndElement(); // goalStages

				xmlWriter.WriteEndElement(); // stage
			}
			xmlWriter.WriteStartElement("audio");
			xmlWriter.WriteAttributeString("musicVolume", inUseData.musicVolume.ToString());
			xmlWriter.WriteAttributeString("audioFxVolume", inUseData.audioFxVolume.ToString());
			xmlWriter.WriteEndElement(); //audio

			xmlWriter.WriteEndElement(); // pizza
			xmlWriter.WriteEndDocument();
			xmlWriter.Close();
		}

		public void Load()
		{
			try
			{
				LoadfromXml();
			}
			catch (Exception)
			{
			}
		}

		void LoadfromXml()
		{
			Dictionary<string, StageDescriptionSavedData> savedStages = new Dictionary<string, StageDescriptionSavedData>();
			StageDescriptionSavedData currentStageDescription = null;
			using (XmlReader reader = XmlReader.Create(dataPath))
			{
				while (reader.Read())
				{
					if (reader.NodeType == XmlNodeType.Element)
					{
						if (reader.Name == "stage")
						{
							string path = reader.GetAttribute("path");
							bool accesible = reader.GetAttribute("accesible") == "yes";
							bool passed = reader.GetAttribute("passed") == "yes";
							currentStageDescription = new StageDescriptionSavedData(path);
							currentStageDescription.accesible = accesible;
							currentStageDescription.passed = passed;
							currentStageDescription.notifyAchieved = reader.GetAttribute("notifyUpdate") == "yes";
							savedStages[path] = currentStageDescription;
						}
						else if (reader.Name == "goal")
						{
							string goalName = reader.GetAttribute("name");
							if (goalName != null)
							{
								currentStageDescription.goalStates[goalName] = reader.GetAttribute("passed") == "yes";
							}
						}
						else if (reader.Name == "audio")
						{
							float musicVolume = float.Parse(reader.GetAttribute("musicVolume"));
							inUseData.musicVolume = musicVolume;
							float audioFxVolume = float.Parse(reader.GetAttribute("audioFxVolume"));
							inUseData.audioFxVolume = audioFxVolume;
						}
					}
					else if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "goalStates")
					{
						currentStageDescription = null;
					}
				}
				reader.MoveToContent();
			}
			inUseData.savedStages = savedStages;
		}

		public SaveData Data {
			get { return inUseData; }
			set {
				if (inUseData != value)
				{
					SavedDataChanged(this, new SavedDataEventArgs(value));
					inUseData = value;
				}
			}
		}

        readonly string fileName = "main.sav";
        string dataPath;
		SaveData savedData;
		SaveData inUseData;

		public class SavedDataEventArgs : EventArgs
		{
			public SavedDataEventArgs(SaveData _data)
			{
				data = _data;
			}
			public SaveData data;
		}

		public EventHandler<SavedDataEventArgs> SavedDataChanged;
	}
}
