using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace shooter
{
	public class TestStage : Stage
	{
		public override void Preload(StageLoader _stageLoader)
		{
			_stageLoader.PreloadEntity<CloudsEntity>();
		}

		public TestStage(ShooterGame _game, Platform _platform)
			: base(_game)
		{
			platform = _platform;
		}

		public override void Create()
		{
			var cloudsEntity = new CloudsEntity();
            cloudsEntity.Initialize(platform.Resolution.ToVector2() /2.0f , platform.Resolution.ToVector2());
			addEntity(cloudsEntity);
		}

		Platform platform;
	}
}
