﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace shooter
{
	public abstract class Stage
	{
		public Stage(ShooterGame _game)
		{
			game = _game;
			entities = new List<SBaseEntity>();
			newEntities = new List<SBaseEntity>();
		}

		public abstract void Preload(StageLoader _stageLoader);
		public abstract void Create();

		public virtual void Start () {}

		public void startUnload()
		{
			foreach (var entity in entities)
			{
				entity.delete();
			}
		}

		public bool isActive()
		{
			return entities.Count > 0;
		}

		public void addEntity(SBaseEntity _entity)
		{
			newEntities.Add(_entity);
		}

		public void update(GameTime gameTime)
		{
			List<SBaseEntity> entitiesToAdd = newEntities;
			newEntities = new List<SBaseEntity>();
			foreach (SBaseEntity entity in entitiesToAdd)
			{
				entity.Create(this);
			}
			entities.AddRange(entitiesToAdd);
			entities.RemoveAll(item => item.ShouldBeDeleted == true && item.components.Count == 0);
			if (/*debug*/false)
			{
				foreach (var entity in entities)
				{
					if (entity.ShouldBeDeleted)
					{
						Console.WriteLine("Entity " + entity + " needs to be deleted");
						foreach (var component in entity.components)
						{
							Console.WriteLine("Component " + component + "is still waiting");
						}
					}
				}
			}
			if (!hasRunOnce)
			{
				hasRunOnce = true;
				Start();
			}
		}

		public virtual void ShowPauseScreen() {}

		public T getGameService<T>() where T : class
		{
			Object ret = game.Services.GetService(typeof(T));
			if (ret != null)
			{
				return (T)ret;
			}

			return null;
		}

		protected ShooterGame game;

		List<SBaseEntity> entities = new List<SBaseEntity>();
		List<SBaseEntity> newEntities = new List<SBaseEntity>();

		bool hasRunOnce = false;
	}
}
