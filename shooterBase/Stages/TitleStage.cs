﻿using System;
using Microsoft.Xna.Framework;

namespace shooter
{
	public class TitleStage : Stage
	{
		public override void Preload(StageLoader _stageLoader)
		{
			_stageLoader.PreloadEntity<MainMenuScreen>();
			_stageLoader.PreloadEntity<TitleScreenPlanetEntity>();
			_stageLoader.PreloadEntity<PopupPanelEntity>();
			ShipSelectionScreen.Preload(_stageLoader);
			StageSelectionScreen.Preload(_stageLoader);
		}

		public TitleStage(ShooterGame _game, LoadStage _loadStage, Platform _platform, bool _startStageSelection = false)
			: base(_game)
		{
			loadStage = _loadStage;
			startStageSelection = _startStageSelection;
			platform = _platform;
		}

		public override void Create()
		{
			camera = new CameraEntity();
			camera.initialize(platform, Vector2.Zero);
			addEntity(camera);

			BGEntity backgroundEntity = new BGEntity();
			backgroundEntity.initialize(camera, Platform.maxResolution, getGameService<TextureCreator>(), Colors.nebulaColors);
			addEntity(backgroundEntity);

			var planet = new TitleScreenPlanetEntity();
			addEntity(planet);

			stageDescriptionScreen = new StageDescriptionScreen();
			stageSelectionScreen = new StageSelectionScreen();

			stageDescriptionScreen.Initialize(startGame, stageSelectionScreen.Show);
			stageDescriptionScreen.Create(this);

			stageSelectionScreen.Create(this, (StageDescription _stage) => { stageDescriptionScreen.UpdateInfo(_stage); stageDescriptionScreen.Show(); });

			shipSelectionScreen = new ShipSelectionScreen();
			shipSelectionScreen.Create(this, stageSelectionScreen.Show);

			mainMenuScreen = new MainMenuScreen();
			mainMenuScreen.Initialize(shipSelectionScreen.Show, planet);
			addEntity(mainMenuScreen);

			if (startStageSelection)
			{
				stageSelectionScreen.Show();
			}
			else
			{
				mainMenuScreen.Show();
			}

			MusicPlayer player = getGameService<MusicPlayer>();
			player.Start(player.menuSong);
		}

		public void startGame(StageDescription _stage)
		{
			getGameService<MusicPlayer>().Stop();
			loadStage.RequestGameStage(_stage.Path);
		}

		MainMenuScreen mainMenuScreen;
		ShipSelectionScreen shipSelectionScreen;
		StageSelectionScreen stageSelectionScreen;
		StageDescriptionScreen stageDescriptionScreen;

		CameraEntity camera;
		LoadStage loadStage;

		Platform platform;
		bool startStageSelection;
	}
}
