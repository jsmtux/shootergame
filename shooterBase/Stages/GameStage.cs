﻿using System;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Input;

namespace shooter
{
	public class GameStage : Stage
	{

		public override void Preload(StageLoader _stageLoader)
		{
			_stageLoader.PreloadEntity<RockEntity>();
			_stageLoader.PreloadEntity<GrassEntity>();
			_stageLoader.PreloadEntity<FenceEntity>();
			_stageLoader.PreloadEntity<StraightRootEntity>();
			_stageLoader.PreloadEntity<CornerRootEntity>();
			_stageLoader.PreloadEntity<MushroomsEntity>();
			_stageLoader.PreloadEntity<CristalEntity>();
			_stageLoader.PreloadEntity<BurgerEnemyEntity>();
			_stageLoader.PreloadEntity<NachoEnemyEntity>();
			_stageLoader.PreloadEntity<FireBtnEntity>();
			_stageLoader.PreloadEntity<MovementSurface>();
			_stageLoader.PreloadEntity<PCEntity>();
			_stageLoader.PreloadEntity<PortalEntity>();
			_stageLoader.PreloadEntity<PlayerProjectileEntity>();
			_stageLoader.PreloadEntity<PlayerSeedEntity>();
			_stageLoader.PreloadEntity<LouseProjectileEntity>();
			_stageLoader.PreloadEntity<StatsEntity>();
			_stageLoader.PreloadEntity<BarEntity>();
			_stageLoader.PreloadEntity<AmmoItemEntity>();
			_stageLoader.PreloadEntity<CarrotFoodItemEntity>();
			_stageLoader.PreloadEntity<StarSeedItemEntity>();
			_stageLoader.PreloadEntity<MelonFoodItemEntity>();
			_stageLoader.PreloadEntity<HeartItemEntity>();
			_stageLoader.PreloadEntity<StageFinishedScreen>();
			_stageLoader.PreloadEntity<PauseBtnEntity>();
			_stageLoader.PreloadEntity<CarrotTreeEntity>();
			_stageLoader.PreloadEntity<BananaTreeEntity>();
			_stageLoader.PreloadEntity<AchievementBanner>();
			_stageLoader.PreloadEntity<PickUpItemParticleTrailEntity>();
			_stageLoader.PreloadEntity<BananaParticleTrailEntity>();
			_stageLoader.PreloadEntity<BananaAndParticleTrailEntity>();
			_stageLoader.PreloadEntity<StageDescriptionMessageEntity>();
			GameOverScreen.Preload(_stageLoader);
			GamePausedScreen.Preload(_stageLoader);
		}

		public GameStage(ShooterGame _game, LoadStage _loadStage, StageDescription _stage, Platform _platform)
			: base(_game)
		{
			stageAchievementsManager = new StageAchievementsManagerEntity(getGameService<IMessagingSystem>(), _stage.extraGoals);
			addEntity(stageAchievementsManager);
			loadStage = _loadStage;
			stage = _stage;
			platform = _platform;
		}

		public override void Create()
		{
			currentLevel = stage.GetNewLevelEntity();
			currentLevel.Initialize(this);
			addEntity(currentLevel);

			playerTarget = new EnemyTarget();
			entityCreator = new EntityCreator(playerTarget);

			currentMapLoader = new MapWorldLoader(entityCreator, stage.Path);
			currentMapLoader.AddToStage(this);

			camera = new CameraEntity();
			camera.initialize(platform, Vector2.Zero, currentMapLoader.CameraZones);
			addEntity(camera);

			playerLocationComponent = new SimplePhysicsComponent(Vector2.Zero, (float)(Math.PI / 2));

			BGEntity backgroundEntity = new BGEntity();
			backgroundEntity.initialize(camera, Platform.maxResolution, getGameService<TextureCreator>(), Colors.nebulaColors);
			addEntity(backgroundEntity);

			gameOverScreen = new GameOverScreen();
			gameOverScreen.Create(this, () => ExitGameStage(ExitType.Restart), () => ExitGameStage(ExitType.Title));

			gamePausedScreen = new GamePausedScreen();
			gamePausedScreen.Create(this,
				() =>
				{
					HidePauseScreen();
				},
				() =>
				{
					ExitGameStage(ExitType.Restart);
					game.SetPaused(false);
				},
				() =>
				{
					ExitGameStage(ExitType.Title);
					game.SetPaused(false);
				});

			stageFinishedScreen = new StageFinishedScreen();
			stageFinishedScreen.Initialize(() => ExitGameStage(ExitType.StageMenu));
			addEntity(stageFinishedScreen);


			pauseBtn = new PauseBtnEntity();
			pauseBtn.Initialize(() => { 
				if (game.IsPaused())
				{
					HidePauseScreen();
				}
				else
				{
					ShowPauseScreen();
				}
			}, new UILocationComponent(new Vector2(80, 70), UILocationComponent.AnchorPoint.TopRight));
			addEntity(pauseBtn);

			saveDataManager = getGameService<SaveDataManager>();

			AddChallengeNotifications(stageAchievementsManager.ActiveAchievements);

			descriptionMessageEntity = new StageDescriptionMessageEntity();
			addEntity(descriptionMessageEntity);

			addEntity(new GameStageMessageReceiverEntity());

			playerEntity = new PCEntity();

			InitializeController(playerEntity);
		}

		public override void Start()
		{
			currentLevel.Start();
		}

		public override void ShowPauseScreen()
		{
			HideController();
			gamePausedScreen.Show();
			game.SetPaused(true);
			getGameService<MusicPlayer>().Pause();
		}

		void HidePauseScreen()
		{
			ShowController();
			gamePausedScreen.Hide();
			game.SetPaused(false);
			getGameService<MusicPlayer>().Resume();
		}

		public void SetGamePaused(bool _paused)
		{
			game.SetPaused(_paused);
		}

		enum ExitType {Restart, StageMenu, Title};
		void ExitGameStage(ExitType _type)
		{
			getGameService<MusicPlayer>().Stop();
			switch(_type)
			{
				case ExitType.Restart:
					loadStage.ResetGameStage();
					break;
				case ExitType.Title:
					loadStage.GetTitleStage();
					break;
				case ExitType.StageMenu:
					loadStage.OnGameStageFinished();
					break;
			}
		}

		void AddChallengeNotifications(List<AchievementEntity> challenges)
		{
			int y = 150;
			int spacing = 60;

			foreach(var challenge in challenges)
			{
				var entity = AchievementBanner.CreateSubEntity(challenge);
				entity.Initialize(new Vector2(55, y));
				addEntity(entity);
				challengeIndicatorEntities.Add(entity);
				y += spacing;
			}

			return;
		}

		void InitializeController(PCEntity _playerEntity)
		{
			var fireBtn = new PlatformFireBtnEntity();
			fireBtn.Initialize();

			MovementSurface movementStick = new MovementSurface();
			movementStick.Initialize(_playerEntity);

			if (platform.GetPlatformType() == Platform.Type.Desktop)
			{
				bool joystickConnected = GamePad.GetCapabilities(PlayerIndex.One).IsConnected;

				if (joystickConnected)
				{
					fireBtn.InitializeGamePad(PlayerIndex.One, Buttons.RightTrigger);
					movementStick.InitializeJoystick(PlayerIndex.One);
				}
			}

			addEntity(fireBtn);
			addEntity(movementStick);

			controller = new ControllerEntity();
			controller.initialize(fireBtn, movementStick);
			addEntity(controller);
		}

		public void ShowController(bool _showFireBtn = true)
		{
			controller.Show(_showFireBtn);
			pauseBtn.Show();
		}

		public void HideController()
		{
			controller.Hide();
			pauseBtn.Hide();
		}

		public void ShowShip()
		{
			playerEntity.initialize(playerLocationComponent, camera, controller, currentMapLoader.CameraZones);
			addEntity(playerEntity);
			playerTarget.SetEntity(playerEntity);

			statsEntity = new StatsEntity();
			statsEntity.initialize();
			addEntity(statsEntity);

			statsEntity.Show();

			foreach(var challengeEntity in challengeIndicatorEntities)
			{
				challengeEntity.Show();
			}
		}

		public Rectangle GetPortalCameraZone()
		{
			Point initialFocusAreaSize = new Point(250 * (platform.Resolution.X / platform.Resolution.Y), 250);
			return new Rectangle(Point.Zero - initialFocusAreaSize / new Point(2, 2), initialFocusAreaSize);
		}

		private void HideUI()
		{
			statsEntity.Hide();
			controller.Hide();
			pauseBtn.Hide();

			foreach (var challengeEntity in challengeIndicatorEntities)
			{
				challengeEntity.Hide();
			}
		}

		public void ShowGameOver()
		{
			HideUI();
			gameOverScreen.Show();
			stageAchievementsManager.delete();
		}

		public void ShowNextStageScreen(PortalEntity _exitPortal)
		{
			HideUI();

			var achievementStatus = stageAchievementsManager.CheckUnlockedOnStageEnd();

			stageAchievementsManager.delete();

			achievementStatus = saveDataManager.Data.UpdateWithFinishedStatus(stage, achievementStatus);
			var nextStage = loadStage.GetNextGameStage();
			if (nextStage != null && saveDataManager.Data.GetStageState(nextStage) == UILevelBtnEntity.State.Locked)
			{
				saveDataManager.Data.AddOpenedStage(nextStage);
			}
			saveDataManager.Save();

			_exitPortal.onClose = () =>
			{
				stageFinishedScreen.Show(stage, achievementStatus);
			};

			playerEntity.StartJump(() =>
			{
				_exitPortal.Close();
			});
		}

		public void OnEnemyKilled()
		{
			points += 100;
		}

		public void ShowDescriptionMessage(string _text, Action _onFinished = null)
		{
			descriptionMessageEntity.ShowText(_text, _onFinished);
		}

		private void DropPickUpToStage(AmmoType _type, int _amount)
		{
			Debug.Assert(_type != AmmoType.None);
			var entitiesToAdd = new List<PickUpItemEntity>();
			for (int i = 0; i < _amount; i++)
			{
				PickUpItemEntity entity = null;
				switch(_type)
				{
					case AmmoType.Carrot:
						entity = entityCreator.CreateCarrot(playerEntity.GetDropPosition());
						break;
					default:
						continue;
				}
				entitiesToAdd.Add(entity);
			}
			if (entitiesToAdd.Count > 0)
			{
				var trail = new PickUpItemParticleTrailEntity();
				trail.onEnd = () => {
					foreach(var entity in entitiesToAdd)
					{
						addEntity(entity);
					}
				};
				trail.initialize(
					camera.TransformLocationToUI(controller.slot.GetLocation().position),
					new SimplePhysicsComponent(entitiesToAdd[0].GetPosition()),
					RenderConfiguration.TranslationType.Camera,
					entitiesToAdd[0].aColor,
					entitiesToAdd[0].bColor);
				addEntity(trail);
			}
		}

        class GameStageMessageReceiverEntity : SBaseEntity
        {
            protected override void DoCreation(Stage _stage)
            {
                AddComponent(new GameStageMessageReceiver(_stage as GameStage));
            }
			public class GameStageMessageReceiver : MessageReceiverComponent
        	{
				public GameStageMessageReceiver(GameStage _stage)
				{
					stage = _stage;
				}
				public override void ReceiveMessage(Message message)
				{
					if (message is AmmoDroppedMessage ammoDroppedMessage)
					{
						stage.DropPickUpToStage(ammoDroppedMessage.type, ammoDroppedMessage.amount);
					}
				}

				GameStage stage;
			}
        }

        CameraEntity camera;
		public CameraEntity Camera {get {return camera;}}
		ControllerEntity controller;
		StatsEntity statsEntity;
		PauseBtnEntity pauseBtn;
		StageDescriptionMessageEntity descriptionMessageEntity;

		public PCEntity playerEntity;
		EnemyTarget playerTarget;

		SimplePhysicsComponent playerLocationComponent;

		EntityCreator entityCreator;

		GameOverScreen gameOverScreen;
		GamePausedScreen gamePausedScreen;
		StageFinishedScreen stageFinishedScreen;
		public MapWorldLoader currentMapLoader;
		
		GameLevelEntity currentLevel;

		List<AchievementBanner> challengeIndicatorEntities = new List<AchievementBanner>();

		public StageAchievementsManagerEntity stageAchievementsManager;
		SaveDataManager saveDataManager;

		LoadStage loadStage;
		readonly StageDescription stage;

		Platform platform;

		int points = 0;
		bool skipIntro = false;
    }
}
