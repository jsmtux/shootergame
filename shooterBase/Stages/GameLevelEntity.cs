﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace shooter
{
	public class GameLevelEntity : SBaseEntity
	{
		public void Initialize(GameStage _gameStage)
		{
			gameStage = _gameStage;
		}

		protected override void DoCreation(Stage _stage)
		{
			receiver = new GameLifeCycleReceiver(gameStage);
			AddComponent(receiver);
		}

		public void Start()
		{
			MusicPlayer player = gameStage.getGameService<MusicPlayer>();
			player.Start(player.stageSongIntro);
			StartThisLevel();
		}

		public virtual void StartThisLevel()
		{
			gameStage.stageAchievementsManager.Reset();
			gameStage.Camera.ResetToFocusArea(gameStage.GetPortalCameraZone());
			ShowPortal(() => ShowShipAndController());
		}

		protected void ShowPortal(Action _onOpened = null)
		{
			portalEntity = new PortalEntity();
			portalEntity.Initialize(Vector2.Zero);
			gameStage.addEntity(portalEntity);

			portalEntity.onOpen = _onOpened;
			portalEntity.Open();
		}
		
		public void ShowShip()
		{
			portalEntity.onOpen = null;

			gameStage.ShowShip();

			gameStage.Camera.SetFollowLocation(gameStage.playerEntity.getLocation());

			gameStage.Camera.SetFollowShip();

			portalEntity.Close();
		}

		public void ShowShipAndController(bool _showFireBtn = true)
		{
			ShowShip();
			gameStage.ShowController(_showFireBtn);
		}

		public class GameLifeCycleReceiver : MessageReceiverComponent
		{
			public GameLifeCycleReceiver(GameStage _lifeCycle)
			{
				lifeCycle = _lifeCycle;
			}
			public override void ReceiveMessage(Message message)
			{
				if (message is PlayerKilledMessage)
				{
					lifeCycle.ShowGameOver();
				}
				if (message is EnemyKilledMessage)
				{
					lifeCycle.OnEnemyKilled();
				}
				if (message is PortalTouchedMessage portalTouchedMessage)
				{
					lifeCycle.ShowNextStageScreen(portalTouchedMessage.entity);
					lifeCycle.Camera.SetFollowLocation(null);
				}
			}

			GameStage lifeCycle;
		}

		protected PortalEntity portalEntity;
		protected GameStage gameStage;
		GameLifeCycleReceiver receiver;
	}

	class FirstLevelEntity : GameLevelEntity
	{
		protected override void DoCreation(Stage _stage)
		{
			base.DoCreation(_stage);
			AddComponent(startTimer);
		}
		public override void StartThisLevel()
		{
			gameStage.Camera.ResetToFocusArea("cam_portal");
			
			Action onPortalOpen = () => {
				gameStage.ShowDescriptionMessage("Fly your ship to the end\nBANANAPORTAL!", () => ShowShipAndController(false));
				gameStage.stageAchievementsManager.Reset();
			};

			Action onOverviewFinished = () => {
				gameStage.Camera.SetCurrentFocusArea(gameStage.GetPortalCameraZone(), TimeSpan.FromSeconds(2.0f), ScaleFuncs.CubicEaseOut, () =>{
						onPortalOpen();
					});
			};

			Action onTextFinished = () => {
				gameStage.Camera.SetCurrentFocusArea("cam_overview", TimeSpan.FromSeconds(2.0f), ScaleFuncs.CubicEaseOut, () =>{
					ShowPortal(() => {});
					gameStage.ShowDescriptionMessage("BANANAPORTALs are used to\ntravel through SPACE.", onOverviewFinished);
				});
			};

			gameStage.ShowDescriptionMessage("This is a BANANAPORTAL.\nPowered by NATURE.", onTextFinished);
		}

		CountDownTimerComponent startTimer = new CountDownTimerComponent();
	}

	class BetaLevel0Entity : GameLevelEntity
	{
		public class BetaLevel0Receiver : MessageReceiverComponent
		{
			public BetaLevel0Receiver(GameStage _lifeCycle)
			{
				lifeCycle = _lifeCycle;
			}
			public override void ReceiveMessage(Message message)
			{
				if (message is CameraZoneShipChange cameraZoneShipChange)
				{
					if (cameraZoneShipChange.type == CameraZoneShipChange.ChangeType.Entered)
					{
						lifeCycle.Camera.SetFollowShip(0.6f);
					}
					else
					{
						lifeCycle.Camera.SetFollowShip();
					}
				}
			}

			GameStage lifeCycle;
		}
		protected override void DoCreation(Stage _stage)
		{
			base.DoCreation(_stage);
			AddComponent(new BetaLevel0Receiver(_stage as GameStage));
		}

		public override void StartThisLevel()
		{
			gameStage.stageAchievementsManager.Reset();
			gameStage.Camera.ResetToFocusArea(gameStage.GetPortalCameraZone());
			ShowPortal(() => ShowShipAndController(false));
		}

		CountDownTimerComponent startTimer = new CountDownTimerComponent();
	}

	class BetaLevel1Entity : GameLevelEntity
	{
		public class BetaLevel1Receiver : MessageReceiverComponent
		{
			public BetaLevel1Receiver(GameStage _lifeCycle)
			{
				lifeCycle = _lifeCycle;
			}
			public override void ReceiveMessage(Message message)
			{
				if (message is CameraZoneShipChange cameraZoneShipChange)
				{
					if (cameraZoneShipChange.type == CameraZoneShipChange.ChangeType.Entered)
					{
						lifeCycle.Camera.SetFollowShip(0.6f);
					}
					else
					{
						lifeCycle.Camera.SetFollowShip();
					}
				}
			}

			GameStage lifeCycle;
		}
		protected override void DoCreation(Stage _stage)
		{
			base.DoCreation(_stage);
			AddComponent(new BetaLevel1Receiver(_stage as GameStage));
		}

		CountDownTimerComponent startTimer = new CountDownTimerComponent();
	}

	class SecondLevelEntity : GameLevelEntity
	{
		protected override void DoCreation(Stage _stage)
		{
			base.DoCreation(_stage);
			AddComponent(startTimer);
		}
		public override void StartThisLevel()
		{
			gameStage.Camera.ResetToFocusArea("cam_portal");

			Action onExplanationFinished = () => {
				gameStage.Camera.SetCurrentFocusArea(gameStage.GetPortalCameraZone(), TimeSpan.FromSeconds(2.0f), ScaleFuncs.Linear, () =>{
					ShowPortal(() => ShowShipAndController());
					gameStage.stageAchievementsManager.Reset();
				});
			};

			Action onOverview = () => {
				gameStage.ShowDescriptionMessage("See those STARSEEDS?\n Try to grab some!", onExplanationFinished);
			};

			Action onTextFinished = () => {
				gameStage.Camera.SetCurrentFocusArea("cam_overview", TimeSpan.FromSeconds(2.0f), ScaleFuncs.Linear, onOverview);
			};

			gameStage.ShowDescriptionMessage("BANANAPORTAL is inactive.\nSome BANANAS are missing.", onTextFinished);
		}

		CountDownTimerComponent startTimer = new CountDownTimerComponent();
	}

	class ThirdLevelEntity : GameLevelEntity
	{

		public class ThirdLevelReceiver : MessageReceiverComponent
		{
			public ThirdLevelReceiver(GameStage _lifeCycle, ThirdLevelEntity _level)
			{
				lifeCycle = _lifeCycle;
				level = _level;
			}
			public override void ReceiveMessage(Message message)
			{
				if (message is CameraZoneShipChange cameraZoneShipChange)
				{
					if (!level.obstacleNotified && cameraZoneShipChange.zoneName == "cam_blocked")
					{
						level.obstacleNotified = true;
						lifeCycle.HideController();
						lifeCycle.SetGamePaused(true);
					
						lifeCycle.ShowDescriptionMessage("Try reviving the tree to find\na way to overcome this obstacle.", () => {
							lifeCycle.SetGamePaused(false);
							lifeCycle.ShowController();
							lifeCycle.Camera.SetFollowLocation(lifeCycle.playerEntity.getLocation());
						});
						lifeCycle.Camera.SetCurrentFocusArea("cam_blocked", TimeSpan.FromSeconds(0.5f), ScaleFuncs.Linear);
					}
				}
			}

			GameStage lifeCycle;
			ThirdLevelEntity level;
		}
		protected override void DoCreation(Stage _stage)
		{
			base.DoCreation(_stage);
			AddComponent(startTimer);
			AddComponent(new ThirdLevelReceiver((GameStage)_stage, this));
			AddComponent(treeTimer);
		}
		public override void StartThisLevel()
		{
			gameStage.Camera.ResetToFocusArea("cam_tree");
			treeTimer.onEnd = () => gameStage.Camera.SetCurrentFocusArea("cam_tree_2", TimeSpan.FromSeconds(4.0f), ScaleFuncs.Linear);
			treeTimer.Start(TimeSpan.FromSeconds(1.0f));

			Action onPortalClosed = () =>
			{
				ShowShip();
				gameStage.stageAchievementsManager.Reset();
				gameStage.ShowDescriptionMessage("Your ship has STARSEEDS now.\nShoot them to revive TREEs", () => gameStage.ShowController());
			};

			Action onExplanationFinished = () => {
				gameStage.Camera.SetCurrentFocusArea(gameStage.GetPortalCameraZone(), TimeSpan.FromSeconds(2.0f), ScaleFuncs.Linear, () =>{
					ShowPortal(onPortalClosed);
				});
			};

			gameStage.ShowDescriptionMessage("TREEs in this area are dead\nFAST FOOD is absorbing their ENERGY.", onExplanationFinished);
		}

		CountDownTimerComponent startTimer = new CountDownTimerComponent();
		internal bool obstacleNotified = false;
		CountDownTimerComponent treeTimer = new CountDownTimerComponent();
	}

	class FifthLevelEntity : GameLevelEntity
	{
		protected override void DoCreation(Stage _stage)
		{
			base.DoCreation(_stage);
			AddComponent(startTimer);
		}
		public override void StartThisLevel()
		{
			gameStage.ShowDescriptionMessage("BURGERS are FAST FOOD minions.\nBe careful with them", ()=>
			{
				gameStage.stageAchievementsManager.Reset();
				gameStage.Camera.ResetToFocusArea(gameStage.GetPortalCameraZone());
				gameStage.Camera.SetFollowLocation(null);
				ShowPortal(() => ShowShipAndController());
			});
			gameStage.Camera.SetFollowLocation(gameStage.currentMapLoader.Enemies[new Point(20, 9)].getLocation());
		}

		CountDownTimerComponent startTimer = new CountDownTimerComponent();
	}
}
