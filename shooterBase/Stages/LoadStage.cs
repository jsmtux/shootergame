﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace shooter
{
	public class StageDescription
	{
		public StageDescription(string _name, string _path, List<AchievementDescription> _extraGoals)
			: this(_name, _path, _extraGoals, null)
		{}

		public StageDescription(
			string _name,
			string _path,
			List<AchievementDescription> _extraGoals,
			Type _stageType)
		{
			Name = _name;
			Path = _path;
			extraGoals = _extraGoals;
			stagePrototype = _stageType;
		}

		public GameLevelEntity GetNewLevelEntity()
		{
			GameLevelEntity ret;

			if(stagePrototype == null)
			{
				ret = new GameLevelEntity();
			}
			else
			{
				ret = (GameLevelEntity)Activator.CreateInstance(stagePrototype);
			}

			return ret;
		}

		public readonly string Name;
		public string Path;
		public List<AchievementDescription> extraGoals;
		Type stagePrototype;
	}

	public class LoadStage : Stage
	{
		public override void Preload(StageLoader _stageLoader)
		{
			_stageLoader.PreloadEntity<GuanacoEntity>();
			_stageLoader.PreloadEntity<IntroIconEntity>();
			_stageLoader.PreloadEntity<AchievementBanner>();
			_stageLoader.PreloadEntity<LessThanTimeAchievementBanner>();
		}

		public LoadStage(ShooterGame _game,
                   StageManager _stageManager,
                   Platform _platform,
                   bool _firstLoad = false,
                   string _stageToLoad = "")
			: base(_game)
		{
			stageManager = _stageManager;
			platform = _platform;
			firstLoad = _firstLoad;
			stageToLoad = _stageToLoad;
		}

		public override void Create()
		{
			var introIcon = new IntroIconEntity();
			addEntity(introIcon);

			if (firstLoad)
			{
				stageManager.PreloadStage(new TitleStage(game, this, platform), (Stage s) =>
				{
					if (stageToLoad == null)
					{
						stageManager.RequestNewStage(s);
					}
					else
					{
						RequestGameStage(stageToLoad);
					}
				});
				
				guanacoEntity = new GuanacoEntity();
				guanacoEntity.initialize(() =>
				{
					introIcon.startTween();
				});
				addEntity(guanacoEntity);
			}
			else
			{
				introIcon.startTween();
			}
		}

		public void RequestGameStage(string _stagePath)
		{
			int index = levelList.FindIndex(stage => stage.Path == _stagePath);
			if (index >= 0)
			{
				currentLevel = index;
				ResetGameStage();
			}
			else
			{
				Console.WriteLine(_stagePath + " is not a stage");
			}
		}

		public void OnGameStageFinished()
		{
			currentLevel++;
			if (currentLevel >= levelList.Count)
			{
				GetTitleStage();
			}
			else
			{
				GetStageSelectionScreen();
			}
		}

		private void GetStageSelectionScreen()
		{
			stageManager.PreloadStage(new TitleStage(game, this, platform, true), (Stage s) =>
			{
				stageManager.RequestNewStage(s);
			});
		}

		public StageDescription GetNextGameStage()
		{
			StageDescription ret = null;
			if (currentLevel + 1 < levelList.Count)
			{
				ret = levelList[currentLevel + 1];
			}

			return ret;
		}

		public void ResetGameStage()
		{
			stageManager.RequestNewStage(new LoadStage(game, stageManager, platform));
			stageManager.PreloadStage(new GameStage(game, this, levelList[currentLevel], platform), (Stage s) =>
			{
				stageManager.RequestNewStage(s);
			});
		}

		public void GetTitleStage()
		{
			currentLevel = 0;
			stageManager.PreloadStage(new TitleStage(game, this, platform), (Stage s) =>
			{
				stageManager.RequestNewStage(s);
			});
		}

		GuanacoEntity guanacoEntity;

		static AchievementDescription NoCrash5Achievement = new AchievementDescription("Take care of the ship", "Crash less than 5 times", typeof(LessThan5CrashAchievementEntity));
		static AchievementDescription NoCrash3Achievement = new AchievementDescription("Easy, pilot", "Crash less than 3 times", typeof(LessThan5CrashAchievementEntity));
		static AchievementDescription LessThan1MinuteAchievement = new AchievementDescription("Snappy!", "Complete stage in less than 1 minute", typeof(LessThan1MinuteAchievement));
		static AchievementDescription LessThan90sAchievement = new AchievementDescription("Snappy!", "Complete stage in less than 90 seconds", typeof(LessThan90sAchievement));
		static AchievementDescription LessThan2MAchievement = new AchievementDescription("Snappy!", "Complete stage in less than 2 minutes", typeof(LessThan2MAchievement));
		static AchievementDescription LessThan3MAchievement = new AchievementDescription("Snappy!", "Complete stage in less than 3 minutes", typeof(LessThan3MAchievement));
		static AchievementDescription DestroyNineRocksAchievement = new AchievementDescription("Destructor", "Destroy at least 9 rocks", typeof(Destroy9RocksAchievementEntity));
		static AchievementDescription Activate4TreesAchievement = new AchievementDescription("Restorer", "Bring all trees to live", typeof(Activate4TreesAchievementEntity));
		static AchievementDescription Activate7TreesAchievement = new AchievementDescription("Restorer", "Bring all trees to live", typeof(Activate7TreesAchievementEntity));
		static AchievementDescription Activate10TreesAchievement = new AchievementDescription("Restorer", "Bring all trees to live", typeof(Activate10TreesAchievementEntity));
		static AchievementDescription Kill5BurgersAchievement = new AchievementDescription("Avenger's first steps", "Destroy all burgers in this stage", typeof(Destroy5BurgersAchievementEntity));
		static AchievementDescription Kill7BurgersAchievement = new AchievementDescription("Avenger's first steps", "Destroy all burgers in this stage", typeof(Destroy7BurgersAchievementEntity));
		static AchievementDescription LeaveSomeCarrotsAchievement = new AchievementDescription("Accurate", "Don't shoot more than 20 carrots", typeof(Shoot20ThanCarrotsAchievementEntity));


		public static List<StageDescription> levelList = new List<StageDescription>() {
			new StageDescription(
				"1. BananaPortals",
				"Maps/01_basic_flight.tmx",
				new List<AchievementDescription>(){ NoCrash3Achievement, LessThan1MinuteAchievement },
				typeof(FirstLevelEntity)),
			new StageDescription(
				"2. Flight practice",
				"Maps/beta_map_00.tmx",
				new List<AchievementDescription>(){ NoCrash5Achievement, LessThan90sAchievement },
				typeof(BetaLevel0Entity)),
			new StageDescription(
				"3. Bananas",
				"Maps/02_basic_flight.tmx",
				new List<AchievementDescription>(){ LessThan2MAchievement },
				typeof(SecondLevelEntity)),
			new StageDescription(
				"4. More bananas",
				"Maps/beta_map_01a.tmx",
				new List<AchievementDescription>(){ NoCrash5Achievement, LessThan2MAchievement },
				typeof(BetaLevel1Entity)),
			new StageDescription(
				"5. Fences on the way",
				"Maps/beta_map_01.tmx",
				new List<AchievementDescription>(){ LeaveSomeCarrotsAchievement, LessThan1MinuteAchievement}),
			new StageDescription(
				"6. Long exit",
				"Maps/beta_map_02.tmx",
				new List<AchievementDescription>(){ NoCrash5Achievement, LessThan2MAchievement}),
			new StageDescription(
				"7. Find Seeds",
				"Maps/beta_map_03.tmx",
				new List<AchievementDescription>(){ NoCrash5Achievement, LessThan2MAchievement}),
			new StageDescription(
				"8. Complications",
				"Maps/beta_map_04.tmx",
				new List<AchievementDescription>(){ NoCrash5Achievement, LessThan2MAchievement}),
			new StageDescription(
				"9. Last",
				"Maps/beta_map_05a.tmx",
				new List<AchievementDescription>(){ NoCrash5Achievement, LessThan3MAchievement},
				typeof(BetaLevel1Entity)),
			new StageDescription(
				"10. Enemies",
				"Maps/enemies_map.tmx",
				new List<AchievementDescription>(){})
		};

		StageManager stageManager;
		bool firstLoad;
		string stageToLoad;
		Platform platform;
		int currentLevel = 0;
	}
}
