﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

#if DEBUG
using System.Diagnostics;
#endif

namespace shooter
{
	public class ShooterGame : Game
	{
		public ShooterGame(Platform _currentPlatform)
		{
			currentPlatform = _currentPlatform;
			currentPlatform.onResize = OnResize;

			graphics = new GraphicsDeviceManager(this);
			graphics.IsFullScreen = _currentPlatform.isFullScreen();
			Content.RootDirectory = "Content";

			shaderContent = new ContentManager(Services);

			shaderContent.RootDirectory = "CompiledShaders";

			IsFixedTimeStep = true;
			//graphics.SynchronizeWithVerticalRetrace = false;

			graphics.PreferredBackBufferWidth = (int)currentPlatform.Resolution.X;
			graphics.PreferredBackBufferHeight = (int)currentPlatform.Resolution.Y;
			graphics.ApplyChanges();
			ShooterTargetTime = TimeSpan.FromMilliseconds(1000.0f / 60.0f);
			TargetElapsedTime = ShooterTargetTime;
		}

		/// <summary>
		/// Allows the game to perform any initialization it needs to before starting to run.
		/// This is where it can query for any required services and load any non-graphic
		/// related content.  Calling base.Initialize will enumerate through any components
		/// and initialize them as well.
		/// </summary>
		protected override void Initialize()
		{
			this.IsMouseVisible = true;

			UILocationComponent.SetPlatform(currentPlatform);

			messagingSystem = new MessagingSystem();
			Services.AddService(typeof(IMessagingSystem), messagingSystem);

			if (currentPlatform is DesktopPlatform)
			{
				touchManager = new DesktopTouchManager(currentPlatform);
			}
			else
			{
				touchManager = new MobileTouchManager(currentPlatform);
			}
			physicsResolver = new PhysicsResolver();
			Services.AddService(typeof(PhysicsResolver), physicsResolver);

			textureCreator = new TextureCreator(GraphicsDevice);
			Services.AddService(typeof(TextureCreator), textureCreator);

			saveDataManager = new SaveDataManager();
			Services.AddService(typeof(SaveDataManager), saveDataManager);

			base.Initialize();
		}

		/// <summary>
		/// LoadContent will be called once per game and is the place to load
		/// all of your content.
		/// </summary>
		protected override void LoadContent()
		{
			saveDataManager.Load();

			spriteBatch = new SpriteBatch(graphics.GraphicsDevice);
			renderTarget = RecreateRenderTarget(currentPlatform.Resolution);
			stageManager = new StageManager(this, Content, textureCreator);
			cameraManager = new CameraManager();
			spriteRenderer = new SpriteRenderer(spriteBatch, shaderContent, cameraManager);
			textRenderer = new TextRenderer(spriteBatch, cameraManager);
			logicManager = new LogicManager();
			lightingSystem = new LightingSystem();
			modelRenderer = new ModelRenderer(currentPlatform, cameraManager, graphics.GraphicsDevice, lightingSystem);
			physicsDebugger = new PhysicsDebugger(Content, spriteBatch, cameraManager);
			physicsUpdater = new PhysicsUpdater();
			particleSystem = new ParticleSystem(spriteBatch, cameraManager);
			timerUpdater = new TimerUpdater();
			performanceLogger = new PerformanceLogger(Content, spriteBatch, cameraManager);
			musicPlayer = new MusicPlayer(Content, saveDataManager);
			soundEffectPlayerSystem = new SoundEffectPlayerSystem(saveDataManager);

			Services.AddService(typeof(MusicPlayer), musicPlayer);

			effectsManager = new EffectsManager(shaderContent, Content, currentPlatform);
			Services.AddService(typeof(EffectsManager), effectsManager);

			string startGameStage;
			LaunchParameters.TryGetValue("StartGameStage", out startGameStage);
			stageManager.RequestNewStage(new LoadStage(this, stageManager, currentPlatform, true, startGameStage));
			//stageManager.RequestNewStage(new TestStage(this, currentPlatform));
		}

		void OnResize()
		{
			touchManager?.RecalculateScreenOffset();
			renderTarget = RecreateRenderTarget(currentPlatform.Resolution);
		}

		public void OnPause()
		{
			musicPlayer.SetPlaybackPaused();
			stageManager.SetPausedStage();
		}

		public void OnResume()
		{
			musicPlayer?.SetPlaybackResumed();
			musicPlayer?.Resume();
		}

		public RenderTarget2D RecreateRenderTarget(Point _resolution)
		{
			return new RenderTarget2D(graphics.GraphicsDevice,
									_resolution.X,
									_resolution.Y,
									false,
									GraphicsDevice.PresentationParameters.BackBufferFormat,
									DepthFormat.Depth24,
									0,
									RenderTargetUsage.PreserveContents);
		}

		/// <summary>
		/// Allows the game to run logic such as updating the world,
		/// checking for collisions, gathering input, and playing audio.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Update(GameTime gameTime)
		{
            currentPlatform.Update(this);
#if DEBUG
			Stopwatch updateTime = new Stopwatch();
			updateTime.Start();
			TimeSpan prevUpdateTime;
			var updateBar = performanceLogger.GetBar("Update");
			updateBar.TargetTime = TimeSpan.FromSeconds(1f / 30f);
#endif
			stageManager.Update(gameTime);
#if DEBUG
			updateBar.AddToSegment("stage", Color.Red, updateTime.Elapsed);
			prevUpdateTime = updateTime.Elapsed;
#endif
			logicManager.UpdateLogic(gameTime, paused);
#if DEBUG
			updateBar.AddToSegment("logic", Color.Blue, updateTime.Elapsed - prevUpdateTime);
			prevUpdateTime = updateTime.Elapsed;
#endif
			if (!paused)
				physicsUpdater.UpdateLogic(gameTime);
#if DEBUG
			updateBar.AddToSegment("physics", Color.Green, updateTime.Elapsed - prevUpdateTime);
			prevUpdateTime = updateTime.Elapsed;
#endif
			if (!paused)
				physicsResolver.UpdateLogic(gameTime);
#if DEBUG
			updateBar.AddToSegment("resolver", Color.Yellow, updateTime.Elapsed - prevUpdateTime);
			prevUpdateTime = updateTime.Elapsed;
#endif
			messagingSystem.UpdateLogic(gameTime);
#if DEBUG
			updateBar.AddToSegment("messaging", Color.Brown, updateTime.Elapsed - prevUpdateTime);
			prevUpdateTime = updateTime.Elapsed;
#endif
			if (!paused)
				particleSystem.UpdateLogic(gameTime);
#if DEBUG
			updateBar.AddToSegment("particles", Color.Pink, updateTime.Elapsed - prevUpdateTime);
			prevUpdateTime = updateTime.Elapsed;
#endif
			timerUpdater.UpdateLogic(gameTime, paused);
#if DEBUG
			updateBar.AddToSegment("tweens", Color.Orange, updateTime.Elapsed - prevUpdateTime);
			prevUpdateTime = updateTime.Elapsed;
#endif
			modelRenderer.UpdateLogic(gameTime);
			spriteRenderer.UpdateLogic(gameTime);
			physicsDebugger.UpdateLogic(gameTime);
			textRenderer.UpdateLogic(gameTime);
#if DEBUG
			updateBar.AddToSegment("fustrumCulling", Color.Cyan, updateTime.Elapsed - prevUpdateTime);
			prevUpdateTime = updateTime.Elapsed;
#endif
			cameraManager.UpdateLogic(gameTime);
			touchManager.UpdateLogic(gameTime);
#if DEBUG
			updateTime.Stop();

			var mainBar = performanceLogger.GetBar("Main");
			mainBar.TargetTime = TimeSpan.FromSeconds(1f / 30f);
			mainBar.AddToSegment("logic", Color.Green, updateTime.Elapsed);
#endif
			lightingSystem.UpdateLogic(gameTime);
			soundEffectPlayerSystem.UpdateLogic(gameTime);

			musicPlayer.Update();

			base.Update(gameTime);
		}

		/// <summary>
		/// This is called when the game should draw itself.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Draw(GameTime gameTime)
		{
#if DEBUG
			Stopwatch drawTime = new Stopwatch();
			drawTime.Start();
			var drawBar = performanceLogger.GetBar("Draw");
			drawBar.TargetTime = TimeSpan.FromSeconds(1f / 30f);
			TimeSpan prevDrawTime = TimeSpan.Zero;
#endif
			GraphicsDevice.SetRenderTarget(renderTarget);
			GraphicsDevice.Clear(Color.Transparent);

			Depth depth = new Depth(0);

			GraphicsDevice.Clear(Color.Transparent);
			for (int i = 0; i < Depth.numDepthTypes; i++)
			{
				depth.Set((Depth.Type)i);

				effectsManager.UpdateLogic(gameTime);
				spriteRenderer.Draw(gameTime, depth);
#if DEBUG
				drawBar.AddToSegment("sprite", Color.Blue, drawTime.Elapsed - prevDrawTime);
				prevDrawTime = drawTime.Elapsed;
#endif
				textRenderer.Draw(gameTime, depth);
#if DEBUG
				drawBar.AddToSegment("text", Color.Yellow, drawTime.Elapsed - prevDrawTime);
				prevDrawTime = drawTime.Elapsed;
#endif
				particleSystem.Draw(gameTime, depth);
#if DEBUG
				drawBar.AddToSegment("particles", Color.Pink, drawTime.Elapsed - prevDrawTime);
				prevDrawTime = drawTime.Elapsed;
#endif
				if (modelRenderer.NeedsDrawing(i))
				{
					GraphicsDevice.DepthStencilState = modelStencilState;
					modelRenderer.Draw(gameTime, depth);
					GraphicsDevice.DepthStencilState = spriteStencilState;
					GraphicsDevice.Clear(ClearOptions.DepthBuffer, Color.White, 1, 0);
				}
#if DEBUG
				drawBar.AddToSegment("model", Color.Red, drawTime.Elapsed - prevDrawTime);
				prevDrawTime = drawTime.Elapsed;
#endif
			}

			performanceLogger.Draw(gameTime);
			performanceLogger.NewFrame();

			GraphicsDevice.SetRenderTarget(null);
			GraphicsDevice.Clear(Color.White);

            spriteBatch.Begin(SpriteSortMode.Deferred, null, null, null, null, null, null);
            spriteBatch.Draw(renderTarget, null, currentPlatform.getDrawPosition());
            spriteBatch.End();

#if DEBUG
			drawBar.AddToSegment("SHADING", Color.Black, drawTime.Elapsed - prevDrawTime);
			prevDrawTime = drawTime.Elapsed;
			//physicsDebugger.Draw(gameTime);
			drawTime.Stop();
			var mainBar = performanceLogger.GetBar("Main");
			mainBar.TargetTime = TimeSpan.FromSeconds(1f / 30f);
			mainBar.AddToSegment("render", Color.Red, drawTime.Elapsed);

			var memoryBar = performanceLogger.GetBar("Memory");
			memoryBar.targetValue = 30000;
			memoryBar.AddToSegment("total", Color.DeepPink, (int)GC.GetTotalMemory(false) / 1024);
#endif

            base.Draw(gameTime);
		}

		public void SetPaused(bool _paused)
		{
			paused = _paused;
		}

		public bool IsPaused()
		{
			return paused;
		}

		GraphicsDeviceManager graphics;
        RenderTarget2D renderTarget;
        SpriteBatch spriteBatch;

        SpriteRenderer spriteRenderer;
		ModelRenderer modelRenderer;
		TextRenderer textRenderer;
		LogicManager logicManager;
		PhysicsResolver physicsResolver;
		PhysicsDebugger physicsDebugger;
		PhysicsUpdater physicsUpdater;
		CameraManager cameraManager;
		MessagingSystem messagingSystem;
		ParticleSystem particleSystem;
		TouchManager touchManager;
		StageManager stageManager;
		TimerUpdater timerUpdater;
		PerformanceLogger performanceLogger;
		LightingSystem lightingSystem;
		SaveDataManager saveDataManager;
		MusicPlayer musicPlayer;
		SoundEffectPlayerSystem soundEffectPlayerSystem;
		EffectsManager effectsManager;

		bool paused = false;

        Platform currentPlatform;
		TextureCreator textureCreator;
		DepthStencilState spriteStencilState = new DepthStencilState() { DepthBufferEnable = false };
		DepthStencilState modelStencilState = new DepthStencilState() { DepthBufferEnable = true };

		static public TimeSpan ShooterTargetTime;

		ContentManager shaderContent;
	}
}
