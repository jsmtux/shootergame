#include "SimplexNoise3D.hlsl"

#if OPENGL
	#define SV_POSITION POSITION
	#define VS_SHADERMODEL vs_3_0
	#define PS_SHADERMODEL ps_3_0
#else
	#define VS_SHADERMODEL vs_4_0_level_9_1
	#define PS_SHADERMODEL ps_4_0_level_9_1
#endif

Texture2D SpriteTexture;
sampler s0;
float Time;

sampler2D SpriteTextureSampler = sampler_state
{
	Texture = <SpriteTexture>;
};

struct VertexShaderOutput
{
	float4 Position : SV_POSITION;
	float4 Color : COLOR0;
	float2 TextureCoordinates : TEXCOORD0;
};

float ramp(float y, float start, float end)
{
	float inside = step(start,y) - step(end,y);
	float fact = (y-start)/(end-start)*inside;
	return (1.-fact) * inside;
	
}

float4 MainPS(VertexShaderOutput input) : COLOR
{
	float2 increment = float2(0.0008f, 0.0014f);

	float noiseZoom = 300;
	float timeDifference = Time / 100;
	float3 xVector = {input.TextureCoordinates.x * noiseZoom, input.TextureCoordinates.y * noiseZoom, timeDifference};
	float xNoise = snoise(xVector) * increment * 0.5;
	float yNoise = snoise(xVector + 17.0) * increment * 0.5;

	float timeVar = Time / 5000;
	float noi = snoise(input.TextureCoordinates * float2(0.5,1.) + float2(1.,3.));
	float bandIntensity = ramp(fmod(fmod(input.TextureCoordinates.y*4. + timeVar/2.+sin(timeVar + sin(timeVar*0.63)),1.) * 10, 1), 0.5f, 0.6f)*noi;

	float2 angle = float2(-1.0, 0.5);
	float2 secondPos = input.TextureCoordinates + increment * angle * 1 + float2(xNoise, yNoise);
	float2 thirdPos = input.TextureCoordinates + increment * angle * 2 + float2(xNoise, yNoise);
	float2 fourthPos = input.TextureCoordinates + increment * angle * 3 + float2(xNoise, yNoise);
	float2 fifthPos = input.TextureCoordinates + increment * angle * 6 + float2(xNoise, yNoise) * 2;
	float2 sixthPos = input.TextureCoordinates + increment * angle * 7 + float2(xNoise, yNoise) * 2;
	float2 seventhPos = input.TextureCoordinates + increment * angle * 1/ + float2(xNoise, yNoise) * 3;
	float2 eightPos = input.TextureCoordinates + increment * angle * 1/ + float2(xNoise, yNoise) * 3;

	float4 color =
		max( tex2D(s0, input.TextureCoordinates),
			max( tex2D(s0, secondPos) * 0.6f * clamp(bandIntensity + 0.6, 0.0, 1.0),
				max( tex2D(s0, thirdPos) * 0.6f * clamp(bandIntensity + 0.6, 0.0, 1.0),
					max( tex2D(s0, fourthPos) * 0.6f * clamp(bandIntensity + 0.6, 0.0, 1.0),
						max( tex2D(s0, fifthPos) * 0.4f * clamp(bandIntensity + 0.3, 0.0, 1.0),
							max( tex2D(s0, sixthPos) * 0.4f * clamp(bandIntensity + 0.3, 0.0, 1.0),
								max( tex2D(s0, seventhPos) * 0.2f * clamp(bandIntensity + 0.2, 0.0, 1.0),
									tex2D(s0, eightPos) * 0.2f * clamp(bandIntensity + 0.2, 0.0, 1.0))
							)
						)
					)
				)
			)
		);
	return color;
}

technique SpriteDrawing
{
	pass P0
	{
		PixelShader = compile PS_SHADERMODEL MainPS();
	}
};
