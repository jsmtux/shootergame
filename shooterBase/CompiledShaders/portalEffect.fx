#if OPENGL
	#define SV_POSITION POSITION
	#define VS_SHADERMODEL vs_3_0
	#define PS_SHADERMODEL ps_3_0
#else
	#define VS_SHADERMODEL vs_4_0_level_9_1
	#define PS_SHADERMODEL ps_4_0_level_9_1
#endif

Texture2D SpriteTexture;
sampler s0;
float Time;
float2 Resolution;

sampler2D SpriteTextureSampler = sampler_state
{
	Texture = <SpriteTexture>;
};

static const float PI = 3.14159265f;
static const float halfPI = PI / 2.0f;

int getColorInd(float angle)
{
    float intensity = (angle + PI) / (2.f * PI);
    float ind = ceil(intensity * 8.f);
    ind = fmod(ind, 6.f);
    return int(ind);
}

float3 getColorFromInd(int ind)
{
	static const float3 colorValues[6] = {
		float3(0.06,0.43,0.92),
		float3(0.455,1.0,0.286),
		float3(1.0,0.992,0.231),
		float3(0.925,0.925,0.118),
		float3(0.475,0.0,0.553),
		float3(0.925,0.925,0.118)};
	return colorValues[ind];
}

struct VertexShaderOutput
{
	float4 Position : SV_POSITION;
	float4 Color : COLOR0;
	float2 TextureCoordinates : TEXCOORD0;
};

float4 MainPS(VertexShaderOutput input) : COLOR
{
    float2 p = input.TextureCoordinates - 0.5f;

	float arc_tan = atan2(p.x, p.y);
    float curvature = sin(Time * 0.002);
    float f = cos(length(p) * curvature * 4.0f + arc_tan*5.0 + Time * 0.002);

    int colorInd = getColorInd(f);

	float4 color;
	color.rgb = getColorFromInd(colorInd) * tex2D(s0, input.TextureCoordinates).a;
	color.a = tex2D(s0, input.TextureCoordinates).a;
	return color;
}

technique SpriteDrawing
{
	pass P0
	{
		PixelShader = compile PS_SHADERMODEL MainPS();
	}
};
