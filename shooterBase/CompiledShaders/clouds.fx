#if OPENGL
	#define SV_POSITION POSITION
	#define VS_SHADERMODEL vs_3_0
	#define PS_SHADERMODEL ps_3_0
#else
	#define VS_SHADERMODEL vs_4_0_level_9_1
	#define PS_SHADERMODEL ps_4_0_level_9_1
#endif

Texture2D SpriteTexture;
Texture2D NoiseTexture;

float iTime;
float2 camPos;

sampler2D SpriteTextureSampler = sampler_state
{
    Texture = <SpriteTexture>;
};

sampler2D NoiseTextureSampler = sampler_state
{
    Texture = <NoiseTexture>;
    AddressU = Wrap;
    AddressV = Wrap;
};

static const float timeScale = 0.01;
static const float curlStrain = 3.0;
static const int noiseOctaves = 5;
static const float softness = 0.2;
static const float brightness = 1.0;

struct VertexShaderOutput
{
	float4 Position : SV_POSITION;
	float4 Color : COLOR0;
	float2 TextureCoordinates : TEXCOORD0;
};

float noise(float2 uv)
{
    float3 tex = tex2D(NoiseTextureSampler, uv);
    return tex.r;
}

float2 rotate(float2 uv)
{
    uv = uv + noise(uv*0.2)*0.005;
    float rot = curlStrain;
    float sinRot=sin(rot);
    float cosRot=cos(rot);
    float2x2 rotMat = float2x2(cosRot,-sinRot,sinRot,cosRot);
    return mul(uv, rotMat);
}

float fbm (float2 uv)
{
    float rot = 1.57;
    float f = 0.0;
    float total = 0.0;
    float mul = 0.5;
    
    for(int i = 0;i < noiseOctaves;i++)
    {
        f += noise(uv+iTime*0.15*(1.0-mul))*mul;
        total += mul;
        uv *= 3.0;
        uv=rotate(uv);
        mul *= 0.5;
    }
    return f/total;
}

float4 cloud(float2 _coord, float4 _color, float2 _offsets, float cover)
{
    float2 uv = (float2(_coord.x, _coord.y) / 20) + camPos;
    float color1 = fbm(uv + _offsets.x +iTime*0.04);
    float color2 = fbm(uv + _offsets.y +iTime*0.02);

    float clouds2 = smoothstep(1.0-cover,min((1.0-cover)+softness,1.0),color2);
    float clouds1 = smoothstep(1.0-cover,min((1.0-cover)+softness*2.0,1.0),color1);
    
    float bright = brightness*(1.8-cover);

    float cloudsFormComb = saturate(clouds1+clouds2);

    float cloudCol = saturate(saturate(1.0-pow(color1,1.0)*0.2)*bright);
    float4 clouds1Color = _color * cloudCol;

    float4 transparent = float4(0, 0, 0, 0);

    float4 clouds2Color = lerp(clouds1Color, transparent,0.25);
    float4 cloudColComb = lerp(clouds1Color,clouds2Color,saturate(clouds2-clouds1));


	return lerp(transparent,cloudColComb,cloudsFormComb);
}

float4 MainPS(VertexShaderOutput input) : COLOR
{
    return cloud(input.TextureCoordinates, float4(0.965,0.843,0.467,1), float2(-0.5,-10.5), 0.3)
        + cloud(input.TextureCoordinates, float4(0.898,0.384,0.808,1), float2(-20.3,-33.5), 0.2)
        + tex2D(SpriteTextureSampler, input.TextureCoordinates);
}

technique SpriteDrawing
{
	pass P0
	{
		PixelShader = compile PS_SHADERMODEL MainPS();
	}
};
