#if OPENGL
	#define SV_POSITION POSITION
	#define VS_SHADERMODEL vs_3_0
	#define PS_SHADERMODEL ps_3_0
#else
	#define VS_SHADERMODEL vs_4_0_level_9_1
	#define PS_SHADERMODEL ps_4_0_level_9_1
#endif

#define LIGHT_BLUE float4(0.024, 0.875, 0.949, 1.0)
#define DARK_BLUE float4(0.008, 0.255, 0.275, 0.4)
#define LIGHT_RED float4(1.0, 0.298, 0.404, 0.9)


Texture2D SpriteTexture;
Texture2D SegmentationTexture;

uniform float colors[8*7];

sampler2D SpriteTextureSampler = sampler_state
{
	Texture = <SpriteTexture>;
};

sampler2D SegmentationTextureSampler = sampler_state
{
	Texture = <SegmentationTexture>;
};

struct VertexShaderOutput
{
	float4 Position : SV_POSITION;
	float4 Color : COLOR0;
	float2 TextureCoordinates : TEXCOORD0;
};

float cubicPulse( float c, float w, float x ){
    x = abs(x - c);
    if( x>w ) return 0.0;
    return 1.0;
}

float segmentCol(int target, float4 color){
	return cubicPulse(target * 0.1f, 0.04f, color.r);
}

float segmentRow(int target, float4 color)
{
	return cubicPulse(target * 0.1f, 0.04, color.g);
}

float4 MainPS(VertexShaderOutput input) : COLOR
{
    float4 segmentationColor = tex2D(SegmentationTextureSampler,input.TextureCoordinates);
	int column = floor(segmentationColor.r * 10) - 1;
	int row = floor(segmentationColor.g * 10) - 1;
	int rowValue = colors[column*7+row];

	return tex2D(SpriteTextureSampler,input.TextureCoordinates) *
		((rowValue == 1) * LIGHT_BLUE + (rowValue == 2) * DARK_BLUE + (rowValue == 3) * LIGHT_RED);
}

technique SpriteDrawing
{
	pass P0
	{
		PixelShader = compile PS_SHADERMODEL MainPS();
	}
};