using System;
using System.Diagnostics;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using System.Threading;
using System.Globalization;

namespace shooter
{
	public class StageLoader
	{
		public StageLoader(ContentManager _contentManager, TextureCreator _textureCreator)
		{
			contentManager = _contentManager;
			textureCreator = _textureCreator;
		}

		public void PreloadEntity<E>() where E : IPreloadedSBaseEntity, new()
		{
			var resourcesToPreload = (new E()).getPreloadedResources();
			foreach (var path in new List<string>(resourcesToPreload.Keys))
			{
#if DEBUG
				Console.WriteLine("Loading resource: " + path);
#endif
				var value = resourcesToPreload[path];
				Type resourceType = value.Item1;
				Object loadedResource = null;
				if (resourceType == typeof(Texture2D))
				{
					if (path.StartsWith("#", StringComparison.Ordinal))
					{
						int r = Convert.ToInt32(path.Substring(1, 2), 16);
						int g = Convert.ToInt32(path.Substring(3, 2), 16);
						int b = Convert.ToInt32(path.Substring(5, 2), 16);
						Color requestedColor = new Color(r, g, b);
						loadedResource = textureCreator.CreateTexture(1, 1, (Color[] _data) => { _data[0] = requestedColor; return _data; });
					}
					else
					{
						loadedResource = contentManager.Load<Texture2D>(path);
					}
				}
				else if (resourceType == typeof(Model))
				{
					loadedResource = contentManager.Load<Model>(path);
				}
				else if (resourceType == typeof(SpriteFont))
				{
					loadedResource = contentManager.Load<SpriteFont>(path);
				}
				else if (resourceType == typeof(SoundEffect))
				{
					loadedResource = contentManager.Load<SoundEffect>(path);
				}
				else if (resourceType == typeof(Effect))
				{
					loadedResource = contentManager.Load<Effect>(path);
				}
				else
				{
					throw new Exception("Incorrect type to preload");
				}
#if DEBUG
				Console.WriteLine("Loaded: " + path);
#endif
				resourcesToPreload[path] = new Tuple<Type, object>(resourcesToPreload[path].Item1, loadedResource);
			}

			List<Type> subEntitiesToPreload = (new E()).getPreloadedSubEntities();

			var preloadEntityRef = typeof(StageLoader).GetMethod("PreloadEntity");
			foreach (var type in subEntitiesToPreload)
			{
				preloadEntityRef.MakeGenericMethod(type).Invoke(this, null);
			}
		}

		public readonly ContentManager contentManager;
		public readonly TextureCreator textureCreator;
	}

	public class StageManager
	{
		public StageManager(ShooterGame _game, ContentManager _contentManager, TextureCreator _textureCreator)
		{
			game = _game;
			contentManager = _contentManager;
			textureCreator = _textureCreator;
		}

		public void PreloadStage<T>(T _stage, Action<T> _onLoad) where T : Stage
		{
			var stageLoader = GetStageLoaderFor<T>();
			Thread thread1 = new Thread(() => {
				_stage.Preload(stageLoader);
				_onLoad(_stage);
			});
			thread1.Start();
		}

		public void RequestNewStage<T>(T _stage) where T : Stage
		{
			_stage.Preload(GetStageLoaderFor<T>());
			nextStage = _stage;
		}

		void StartStage(Stage _stage)
		{
			Debug.Assert(currentStage == null);
			currentStage = _stage;
			stageRunning = true;
		}

		void StopStage()
		{
			Debug.Assert(currentStage != null);
			currentStage.startUnload();
			stageRunning = false;
		}

		public void Update(GameTime gameTime)
		{
			if (currentStage != null)
			{
				currentStage.update(gameTime);
				if (!stageRunning)
				{
					currentStage.startUnload();
				}
				if (!currentStage.isActive())
				{
					currentStage = null;
				}
			}

			if (nextStage != null)
			{
				if (currentStage != null)
				{
					StopStage();
					if (!currentStage.isActive())
					{
						currentStage = null;
					}
				}
				else
				{
					nextStage.Create();
					StartStage(nextStage);
					nextStage = null;
				}
			}
		}

        internal void SetPausedStage()
        {
			currentStage?.ShowPauseScreen();
        }

        StageLoader GetStageLoaderFor<T>()
		{
			if (!stageLoaders.ContainsKey(typeof(T)))
			{
				var stageContentManager = new ContentManager(contentManager.ServiceProvider, contentManager.RootDirectory);
				stageLoaders[typeof(T)] = new StageLoader(stageContentManager, textureCreator);
			}

			return stageLoaders[typeof(T)];
		}

		Stage currentStage;
		Stage nextStage;
		bool stageRunning = false;

		ShooterGame game;

		Dictionary<Type, StageLoader> stageLoaders = new Dictionary<Type, StageLoader>();
		ContentManager contentManager;
		TextureCreator textureCreator;
    }
}
