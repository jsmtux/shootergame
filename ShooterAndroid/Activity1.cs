using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Android.Util;
using Microsoft.Xna.Framework;

namespace ShooterAndroid
{
    [Activity(Label = "Space Pizza"
        , MainLauncher = true
        , Icon = "@mipmap/ic_launcher"
        , RoundIcon = "@mipmap/ic_launcher_round"
        , Theme = "@android:style/Theme.NoTitleBar.Fullscreen"
        , AlwaysRetainTaskState = true
        , LaunchMode = Android.Content.PM.LaunchMode.SingleInstance
        , ScreenOrientation = ScreenOrientation.Landscape
        , ConfigurationChanges = ConfigChanges.Orientation | ConfigChanges.KeyboardHidden | ConfigChanges.ScreenSize)]
    public class Activity1 : Microsoft.Xna.Framework.AndroidGameActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            platform = new shooter.AndroidPlatform(GetRes, KillProcess);
            game = new shooter.ShooterGame(platform);
            View vw = (View)game.Services.GetService(typeof(View));

            Window.SetStatusBarColor(Android.Graphics.Color.Transparent);
            if (Android.OS.Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.P)
            {
                Window.Attributes.LayoutInDisplayCutoutMode = LayoutInDisplayCutoutMode.ShortEdges;
            }
            if (Android.OS.Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.Cupcake)
            {
                Window.Attributes.ScreenBrightness = 10.0f;
            }
            Window.AddFlags(WindowManagerFlags.Fullscreen);
            Window.AddFlags (WindowManagerFlags.LayoutNoLimits);
            Window.AddFlags(WindowManagerFlags.KeepScreenOn);

            HideSystemUi();

            SetContentView(vw);
            game.Run();
        }

        protected override void OnResume()
        {
            base.OnResume();
            HideSystemUi();
            game?.OnResume();
        }

        protected override void OnPause()
        {
            base.OnPause();
            game.OnPause();
        }

        private void KillProcess()
        {
            Process.KillProcess(Process.MyPid());
        }

        private Point GetRes()
        {
            WindowManager.DefaultDisplay.GetRealMetrics(metrics);
            return new Point(metrics.WidthPixels /*+ getNavigationBarHeight()*/, metrics.HeightPixels);
        }

        private int getNavigationBarHeight() {
            if (navigationBarHeight == -1)
            {
                int resourceId = this.Resources.GetIdentifier("navigation_bar_height", "dimen", "android");
                if (resourceId > 0)
                {
                    navigationBarHeight = this.Resources.GetDimensionPixelSize(resourceId);
                }
            }
            return navigationBarHeight;
        }

        private void HideSystemUi()
        {
            if (Build.VERSION.SdkInt >= BuildVersionCodes.Kitkat)
            {
                var decorView = Window.DecorView;
                var uiVisibility = (int)decorView.SystemUiVisibility;
                var options = uiVisibility;

                options |= (int)SystemUiFlags.LowProfile;
                options |= (int)SystemUiFlags.Fullscreen;
                options |= (int)SystemUiFlags.HideNavigation;
                options |= (int)SystemUiFlags.ImmersiveSticky;
                options |= (int)SystemUiFlags.LayoutStable;
                options |= (int)SystemUiFlags.LayoutHideNavigation;

                decorView.SystemUiVisibility = (StatusBarVisibility)options;
            }
            
        }

        shooter.AndroidPlatform platform;
        shooter.ShooterGame game;
        DisplayMetrics metrics = new DisplayMetrics();

        int navigationBarHeight = -1;
    }
}

